//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2015.08.21 a las 04:06:07 PM ART 
//


package ar.com.gestionit.cashmanagement.service.wsclient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ar.com.gestionit.services package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetComprobanteResponse_QNAME = new QName("http://services.gestionit.com.ar/", "getComprobanteResponse");
    private final static QName _GetComprobante_QNAME = new QName("http://services.gestionit.com.ar/", "getComprobante");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ar.com.gestionit.services
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetComprobante }
     * 
     */
    public GetComprobante createGetComprobante() {
        return new GetComprobante();
    }

    /**
     * Create an instance of {@link GetComprobanteResponse }
     * 
     */
    public GetComprobanteResponse createGetComprobanteResponse() {
        return new GetComprobanteResponse();
    }

    /**
     * Create an instance of {@link Comprobante }
     * 
     */
    public Comprobante createComprobante() {
        return new Comprobante();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetComprobanteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.gestionit.com.ar/", name = "getComprobanteResponse")
    public JAXBElement<GetComprobanteResponse> createGetComprobanteResponse(GetComprobanteResponse value) {
        return new JAXBElement<GetComprobanteResponse>(_GetComprobanteResponse_QNAME, GetComprobanteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetComprobante }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://services.gestionit.com.ar/", name = "getComprobante")
    public JAXBElement<GetComprobante> createGetComprobante(GetComprobante value) {
        return new JAXBElement<GetComprobante>(_GetComprobante_QNAME, GetComprobante.class, null, value);
    }

}
