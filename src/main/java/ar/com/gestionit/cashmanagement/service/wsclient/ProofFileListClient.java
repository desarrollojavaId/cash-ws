package ar.com.gestionit.cashmanagement.service.wsclient;

import javax.xml.bind.JAXBElement;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

/**
 * WS Client to get the proof files
 * @author jdigruttola
 */
public class ProofFileListClient extends WebServiceGatewaySupport {
	
	public static final String SOAP_ACTION = "http://srappl001v005.terceros.banco.com.ar:8080/WSCPADJ/ServicioComprobante";

	public GetComprobanteResponse getProof(String paymentNumber, String paymentSubnumber) {
		GetComprobante request = new GetComprobante();
		request.setNumeroDeOrdenDePago(paymentNumber);
		request.setSubnumeroDeOrdenDePago(paymentSubnumber);
		request.setNumeroMail(0);

		@SuppressWarnings("unchecked")
		JAXBElement<GetComprobanteResponse> element = (JAXBElement<GetComprobanteResponse>)getWebServiceTemplate().marshalSendAndReceive(
				request,
				new SoapActionCallback(SOAP_ACTION));
		return element.getValue();
	}
}