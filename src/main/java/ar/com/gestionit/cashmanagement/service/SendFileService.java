package ar.com.gestionit.cashmanagement.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.DictionaryBO;
import ar.com.gestionit.cashmanagement.persistence.bo.SendFileBO;
import ar.com.gestionit.cashmanagement.persistence.bo.TransactionBO;
import ar.com.gestionit.cashmanagement.persistence.dto.DictionaryListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentWayListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.SendFileDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.TransactionDTO;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManager;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManagerDTO;
import ar.com.gestionit.cashmanagement.service.util.POFileReport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;
import ar.com.gestionit.cashmanagement.util.XMLUtil;
import ar.com.gestionit.cashmanagement.util.ZIPUtil;

/**
 * This service manages the sending files to AS400
 * General function:
 * 	- Frontend persists the file in MS SQL Server. So, we take the file from there.
 * 	- We validate basic points about the file (adherents, file type,...)
 * 	- Persist in AS400 the file with status APVI. So, an AS400 process validates the file
 * and modifies its status (APA1 or AERR)
 * END
 * 
 * Special cases:
 * 	- For PO, we generate a report about the amounts in the file content and send it in the response
 * 	- For RF, we need the agreement and subagreement to work
 * 
 * @author jdigruttola
 *
 */
public class SendFileService extends AbstractService {


	private static final String FILETYPE_RF = "RF";
	private static final String FILETYPE_RT = "PT";
	private static final String FILETYPE_PD = "PD";

	private static final String FILETYPE_PO = "PO";
	private static final String FILETYPE_PC = "PC";
	// private static final String FILETYPE_PB = "PB";

	/*
	 * All the file types start with this prefix in the header line
	 * Exception: PD
	 */
	private static final String FILE_HEADER_PREFIX = "FH";
	private static final String FILE_FOOTER_PREFIX = "FT";

	private static final String INVOKEDTRANSACTION_RECAUD = "Envio_archivo_recaud";
	private static final String INVOKEDTRANSACTION_PAGO = "Envio_archivo_pago";

	private static final int INSERT_ROW_MAX = 32000;
	private static final int FILESTATUS_ATTEMPTS = 3;
	private static final int FILESTATUS_TIME = 1500;

	private static final String CHECKSUM_REQUIRED_YES = "S";
	private static final String CHECKSUM_REQUIRED_NO = "N";

	/**
	 * BOs use in this service
	 */
	private SendFileBO bo;
	private TransactionBO transactionBo;
	private FileManager manager;
	private DictionaryBO boDic;
	/**
	 * Service DTO
	 */
	private SendFileDTO dto;

	/**
	 * File Manager DTO
	 */
	private FileManagerDTO fmDto;

	/**
	 * These attributes are used to register the transaction in SSTRANS by
	 * buildTransactionDto() method. NOTE: if the file is RF, so we get the
	 * agreement and sub-agreement, if not we keep the values by default (0)
	 */
	private String agreementNumber = "0";
	private String subagreementNumber = "0";

	/**
	 * These attributes are used to get the file status after the AS400 process
	 * (file validator) has been executed to validate the file. The service
	 * waits and tries to get the file status if the AS400 process finishes. So,
	 * these attributes indicate the number of the attempts and the time between
	 * them.
	 */
	private int fileStatusAttempts;
	private int fileStatusTime;

	/**
	 * This attribute contains the number of rows maximum that we can use to
	 * persist per each INSERT. This is used to persist the file content in the
	 * datasource
	 */
	private int rowNumberMax;

	/**
	 * This attribute contains the lines of the file content
	 */
	private String[] lines;

	/**
	 * These attributes are about PO report
	 * amountTotal: is a amount total of all the payments of the file
	 * accountTotal: is the number of accounts of the file
	 * paymentWayList: report
	 */
	private String amountTotal = "0.00";
	private long accountTotal;
	private PaymentWayListDTO paymentWayList;
	private String totalLines;
	private String currencyAdherent;

	@Override
	protected void initialize() throws ServiceException {
		bo = new SendFileBO();
		transactionBo = new TransactionBO();
		fmDto = new FileManagerDTO();
		dto = new SendFileDTO();
		manager = new FileManager();
		boDic = new DictionaryBO();

		try {
			fileStatusAttempts = CashManagementWsApplication
					.getPropertyAsInteger(ServiceConstant.PROP_SENDFILE_FILESTATUS_ATTEMPTS);
		} catch (Exception e) {
			DefinedLogger.SERVICE.error(
					"No se pudo obtener la cantidad de intentos para esperar que el proceso de validacion termina. En consecuencia, se toma uno por defecto");
			fileStatusAttempts = FILESTATUS_ATTEMPTS;
		}
		try {
			fileStatusTime = CashManagementWsApplication
					.getPropertyAsInteger(ServiceConstant.PROP_SENDFILE_FILESTATUS_TIME);
		} catch (Exception e) {
			DefinedLogger.SERVICE.error(
					"No se pudo obtener el tiempo entre intentos para esperar que el proceso de validacion termina. En consecuencia, se toma uno por defecto");
			fileStatusTime = FILESTATUS_TIME;
		}
		try {
			rowNumberMax = CashManagementWsApplication.getPropertyAsInteger(ServiceConstant.PROP_SENDFILE_INSERT_MAX);
		} catch (Exception e) {
			DefinedLogger.SERVICE.error(
					"No se pudo obtener la cantidad de registros maximos para persistir el contenido del archivo por lotes. En consecuencia, se toma uno por defecto");
			rowNumberMax = INSERT_ROW_MAX;
		}
	}

	@Override
	protected void loadInputs() throws ServiceException {
		// Loading...
		dto.setAdherent(getDataFromVector(1));
		dto.setAgreement(getDataFromVector(2));
		dto.setSubagreement(getDataFromVector(3));
		fmDto.setTokenId(getDataFromVector(4));
		dto.setFileType(getDataFromVector(9));
		dto.setNewSchema(getDataFromVector(10));

		// Validating...
		if (fmDto.getTokenId() == null || fmDto.getTokenId().trim().equals("")) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_TOKENNOTVALID);
		}

		if (dto.getFileType() == null || dto.getFileType().trim().equals("")) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFILETYPE);
		}
		
		if (dto.getFileType().length() > 2) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDFILETYPELENGTH);
		}

		if (StringUtil.isEmpty(dto.getNewSchema()) || (dto.getNewSchema().trim().equals(ServiceConstant.TRUE)
				&& dto.getNewSchema().trim().equals(ServiceConstant.FALSE))) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFOUNDNEWSCHEMA);
		}

		// Fixing...
		dto.setFileType(dto.getFileType().toUpperCase());
	}

	@Override
	public void execute() throws ServiceException {
		/*
		 * Get the sent file from SQL Server by the frontend
		 */
		loadFileContent();

		/*
		 * If the file is RF, we must load agreement and sub-agreement
		 */
		loadAgreement();

		/*
		 * The application must trace the file and its status. So, this
		 * registers it in the datasource (SSTRANS) NOTE: Even though the file
		 * is not right, we must register the tried and its real status
		 */
		registerTransaction();

		/*
		 * This method validates the file content and compares it with the data
		 * given by the request NOTE: if the method finds errors, this updates
		 * the file status in the datasource (SSTRANS) and interrupts the
		 * execution to notify to the frontend
		 */
		validateFileContent();

		/*
		 * Validate if the given file checksum is equal to the algorithm's
		 * checksum
		 */
		validateChecksum();

		/*
		 * Generate the report and persist it in the data source
		 */
		loadReport();

		/*
		 * A AS400 process (we execute it via a stored procedure) gives us the
		 * library and the table where we must persist the file.
		 */
		getLibraryAndTable();

		/*
		 * Persist the file in the data source. For it, we persist the file
		 * content in the library and table given by the process.
		 */
		persistFile();

		/*
		 * After we persisted the file in the data source, a AS400 process
		 * validates if the file has errors and persists the validation result
		 * in the data source. So,
		 */
		getFileStatus();
	}

	/**
	 * This method generates or updates a register in SSTRANS table
	 * 
	 * @throws ServiceException
	 */
	private void registerTransaction() throws ServiceException {
		TransactionDTO tdto = super.buildTransactionDto();
		tdto.setStqtrs(serviceKey);

		if (dto.getFileType().equalsIgnoreCase(FILETYPE_RF) || dto.getFileType().equalsIgnoreCase(FILETYPE_RT)
				|| dto.getFileType().equalsIgnoreCase(FILETYPE_PD)) {
			tdto.setStktrs(INVOKEDTRANSACTION_RECAUD);
		} else {
			tdto.setStktrs(INVOKEDTRANSACTION_PAGO);
		}

		tdto.setStkcnl(CHANNEL);
		tdto.setStkest(TransactionDTO.FILE_STATUS_ATRS);
		tdto.setStkcvn(agreementNumber);
		tdto.setStktbo(subagreementNumber);
		tdto.setStfau3(TransactionDTO.DATE_EMPTY);
		tdto.setSthau3(TransactionDTO.HOUR_EMPTY);
		tdto.setStkstr(dto.getFileType());
		tdto.setStnarc(fmDto.getFileName());
		tdto.setStqtot(totalLines);
		// Bandera para determinar si es nuevo esquema de firmantes
		if (dto.getNewSchema().trim().equals(ServiceConstant.TRUE)) {
			tdto.setStkapr("S");
			tdto.setStunec("0"); // Cantidad de usuarios necesarios
		} else {
			tdto.setStkapr("N");
			tdto.setStunec("2"); // Cantidad de usuarios necesarios
		}

		transactionBo.registerTransaction(tdto);
	}

	/**
	 * This method registers the file status in SSTRANS table
	 * 
	 * @param status
	 * @throws ServiceException
	 */
	private void registerFileStatus(String status) throws ServiceException {
		TransactionDTO tdto = new TransactionDTO();
		tdto.setStqtrs(serviceKey);
		tdto.setStkest(status);
		transactionBo.updateStkest(tdto);
	}

	/**
	 * This method registers the table name in SSTRANS table where the file will
	 * be hosted
	 * 
	 * @param tableName
	 * @throws ServiceException
	 */
	private void registerTableName(String tableName) throws ServiceException {
		DefinedLogger.SERVICE.debug("Registrando nombre de tabla en SSTRANS...");
		TransactionDTO tdto = new TransactionDTO();
		tdto.setStqtrs(serviceKey);
		tdto.setStnars(tableName);
		transactionBo.updateStnars(tdto);
	}

	/**
	 * This method validates the file content and compares it with the data
	 * given by the request
	 * 
	 * @throws ServiceException
	 * @throws IOException
	 */
	private void validateFileContent() throws ServiceException {
		//Validate if the file type is PD. In this case, the validations are different
		if(dto.getFileType().equalsIgnoreCase(FILETYPE_PD)) {
			validateFileContentPD();
			return;
		}

		// Validate if the file type given by request is equal to the file type
		// specified inside the file
		if (!dto.getFileType().equalsIgnoreCase(fmDto.getContenidoMap().substring(2, 4))) {
			DefinedLogger.SERVICE
			.error("El tipo de archivo indicado no coincide con el tipo de archivo contenido en el archivo");

			// Update the file status in the data source
			DefinedLogger.SERVICE.debug("Actualizando el estado del archivo en SSTRANS...");
			registerFileStatus(TransactionDTO.FILE_STATUS_ANPT);

			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDFILETYPE);
		}

		// Validate if the adherent given by request is equal to the adherent
		// specified inside the file
		String adherent = dto.getAdherent();
		while (adherent.length() < 7) {
			adherent = "0" + adherent;
		}
		if (!adherent.trim().equals(fmDto.getContenidoMap().substring(4, 11))) {
			DefinedLogger.SERVICE.error("El adherente indicado no coincide con el adherente contenido en el archivo");

			// Update the file status in the data source
			DefinedLogger.SERVICE.debug("Actualizando el estado del archivo en SSTRANS...");
			registerFileStatus(TransactionDTO.FILE_STATUS_ANPA);

			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDADHERENT);
		}

		//Validate if the file type is RF. In this case, validate the agreement/sub-agreement between the request and file content
		if(dto.getFileType().equalsIgnoreCase(FILETYPE_RF)) {
			//Validation
			if(agreementNumber == null
					|| subagreementNumber == null
					|| dto.getAgreement() == null
					|| dto.getSubagreement() == null
					|| !NumberUtil.isNumeric(dto.getAgreement())
					|| !NumberUtil.isNumeric(dto.getSubagreement())
					|| Integer.parseInt(agreementNumber) != Integer.parseInt(dto.getAgreement())
					|| Integer.parseInt(subagreementNumber) != Integer.parseInt(dto.getSubagreement())) {
				registerFileStatus(TransactionDTO.FILE_STATUS_ANPA);

				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDAGREEMENT);
			}
		}
	}
	
	/**
	 * This method validates the file content and compares it with the data
	 * given by the request only for PD type
	 * 
	 * @throws ServiceException
	 * @throws IOException
	 */
	private void validateFileContentPD() throws ServiceException {
		//Get CUIT from the file content
		if(lines[0].substring(0, 1).equalsIgnoreCase("H")) {
			//So, this is a PD Standard
			dto.setCuit(lines[0].substring(1, 12));
		} else {
			//So, this is a PD Lloyds
			dto.setCuit(lines[0].substring(0, 11));
		}
		
		//Validate if the obtained CUIT is number. In this way, we validate if the file is good or trash
		if(!NumberUtil.isNumeric(dto.getCuit())) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFILEVALID);
		}
		
		//Validate if the adherent (from request) and CUIT (from file content) are associated in the datasource 
		String verify = bo.verifyAdherenteForPD(dto);
		if(verify == null || !verify.trim().equals("1")) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDADHERENT);
		}
	}

	/**
	 * This method loads the file content in main memory
	 * @throws ServiceException
	 */
	private void loadFileContent() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo contenido de archivo");

		// Get file from File Manager datasource (MS SQL Server engine)
		fmDto = manager.getFile(fmDto);

		// Validate if the file could be obtained
		if (fmDto == null || StringUtil.isEmpty(fmDto.getContenidoMap())) {
			DefinedLogger.SERVICE.error("No se pudo obtener el archivo.");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FAILEDTOGETFILE);
		}
		if (StringUtil.isEmpty(fmDto.getFileName())) {
			DefinedLogger.SERVICE.error("No se pudo obtener el nombre del archivo.");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFOUNDFILENAME);
		}

		// Validate if the file is zipped. In this case, we must uncompress the file
		if(ZIPUtil.isZipFile(fmDto.getFileName())) {
			// So, we must uncompress the file and get the file content sent
			try {
				fmDto.setContenidoMap(ZIPUtil.uncompress(fmDto.getFileContent()));
			} catch(IllegalArgumentException e) {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDCOMPRESSEDFILENAME, e);
			} catch (ServiceException e) {
				throw e;
			} catch (Exception e) {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FAILEDUNZIPFILE, e);
			}
		}

		// Load the file content as lines
		lines = StringUtil.splitByEOL(fmDto.getContenidoMap());
		totalLines = Integer.toString(lines.length);
		
		//Validate if the file is valid or is trash
		validateFileValid();

	}

	/**
	 * This method validates if the file is valid or is trash.
	 * In the last case, this method throws an exception
	 * In other case, this method does nothing
	 * @throws ServiceException
	 */
	private void validateFileValid() throws ServiceException {
		//Validate if the file type is not PD
		if(!FILETYPE_PD.equals(dto.getFileType()) &&
				( lines == null
				|| lines.length <= 0
				|| lines[0].length() <2
				|| !lines[0].substring(0, 2).equalsIgnoreCase(FILE_HEADER_PREFIX))) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFILEVALID);
		}
	}

	/**
	 * This method validates if the file is RF. In this case, it loads the
	 * numbers of agreement and sub-agreement. If not, it does nothing
	 */
	private void loadAgreement() throws ServiceException {
		/*
		 * If the file is RF, get agreement (convenio) and sub-agreement
		 * (subconvenio)
		 */
		if (dto.getFileType() != null && dto.getFileType().trim().equalsIgnoreCase(FILETYPE_RF)) {
			DefinedLogger.SERVICE.debug("Obteniendo convenio y sub-convenio");
			agreementNumber = fmDto.getContenidoMap().substring(46, 53);
			subagreementNumber = fmDto.getContenidoMap().substring(61, 68);

			//Validate if the agreement given by the file content is valid
			if(!NumberUtil.isNumeric(agreementNumber) || !NumberUtil.isNumeric(subagreementNumber)) {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDAGREEMENTINFILE);
			}
		}
	}

	/**
	 * This method reads the file and generates the reports about its accounts
	 * 
	 * NOTE: this method (at moment) only generates the report for PO files
	 * 
	 * @param vecArch
	 * @param stqtrs
	 * @param usu
	 * @param adh
	 * @param tipoArchivo
	 * @return
	 * @throws SQLException
	 * @throws Exception
	 */
	private void loadReport() throws ServiceException {
		// Validate if the file type is PO
		if (!dto.getFileType().trim().equalsIgnoreCase(FILETYPE_PO))
			return;

		POFileReport reporter = new POFileReport();
		reporter.loadReportForSendFile(bo, lines, serviceKey, dto);
		amountTotal = reporter.getAmountTotal();
		accountTotal = reporter.getAccountSize();
		paymentWayList = reporter.getPaymentWayList();
		currencyAdherent = reporter.getCurrencyAdh();
		
	}


	/**
	 * This method gets important file data
	 * 
	 * @throws ServiceException
	 */
	private void getFileData() throws ServiceException {
		// Define and initialize variables
		int location;
		int totalAmount = 0;
		String result = "0";

		// Get the amount total location and the register number location
		dto = bo.findFileData(dto);

		if (NumberUtil.isNumeric(dto.getLocationRegisterNumber())
				&& !dto.getFileType().equalsIgnoreCase(FILETYPE_PD)) {
			location = Integer.parseInt(dto.getLocationRegisterNumber());
			DefinedLogger.SERVICE.debug("location:" + location);
			DefinedLogger.SERVICE.debug("linea_completa:" + lines[lines.length - 1]);
			DefinedLogger.SERVICE.debug("cantidad_lineas:" + lines.length);			
			if(location >= 10) {
				result = lines[lines.length - 1].substring(location - 10, location).trim().replaceAll(" ", "0");
				DefinedLogger.SERVICE.debug("resultado:" + result);
			} else {
				result = "0";
			}
			dto.setRegisterNumber(result);
		} else {
			dto.setRegisterNumber("0");
		}

		//Validate if the file type is PO or PC. In other case, the file does not have amounts
		if(!dto.getFileType().trim().equalsIgnoreCase(FILETYPE_PC)
				&& !dto.getFileType().trim().equalsIgnoreCase(FILETYPE_PO)
				&& !dto.getFileType().trim().equalsIgnoreCase(FILETYPE_RF)) {
			dto.setAmountTotal("0");
		} else {
			// Para los archivos de tipo "PC" guardo en el importe total, la
			// cantidad de pÃ¡ginas
			if (dto.getFileType().trim().equalsIgnoreCase(FILETYPE_PC)) {
				int i = 0;
				while (i != (lines.length - 1)) {
					/*
					 * Si la linea es un total de pÃ¡gina, quiere decir que era una
					 * pÃ¡gina por lo que incremento el contador
					 */
					if (lines[i] != null && lines[i].length() >= 2 && lines[i].substring(0, 2).equalsIgnoreCase("T1")) {
						totalAmount++;
					}
					i++;
				}

				dto.setAmountTotal(String.valueOf(totalAmount));

				amountTotal = String.valueOf(totalAmount) + ".00";
			} else if (NumberUtil.isNumeric(dto.getLocationAmountTotal())) {
				location = Integer.parseInt(dto.getLocationAmountTotal());
				result = lines[lines.length - 1].substring(location - 25, location).trim();

				result = new StringBuffer(result.substring(0, result.length() - 2).replaceAll(" ", "0")).append(".")
						.append(result.substring(result.length() - 2)).toString();

				dto.setAmountTotal(result);
				amountTotal = NumberUtil.amountFormat(result);
				DefinedLogger.SERVICE.debug("MontoTotal:" + amountTotal);
			}

		}
	}

	/**
	 * This method tries to get the validation status.
	 * 
	 * @throws ServiceException
	 */
	private void getFileStatus() throws ServiceException {
		DefinedLogger.SERVICE.info("Obteniendo estado de validacion...");
		int attempts = 0;
		while (attempts < fileStatusAttempts) {
			// Sleep to wait the AS400 process finishes
			try {
				Thread.sleep(fileStatusTime);
			} catch (Exception e) {
				DefinedLogger.SERVICE.warn(new StringBuffer(DefinedLogger.TABULATOR)
						.append("El thread de SendFileService no pudo dormir en la iteracion ")
						.append(fileStatusAttempts).append(" por ").append(fileStatusTime)
						.append(" milisegundos para esperar que el proceso validador de archivos finalice").toString());
			}
			attempts++;

			// Try to get the file status
			dto = bo.getFileStatus(dto);

			// Validate if the file status was found in this iteration
			if (!StringUtil.isEmpty(dto.getFileStatus())) {
				DefinedLogger.SERVICE.debug(new StringBuffer(DefinedLogger.TABULATOR)
						.append("Se obtuvo estado de validacion en la iteracion ").append(attempts).append(": ")
						.append(dto.getFileStatus()).toString());
				break;
			}
		}
		
		// Get the amount total and the register number to update in SSTRANS
		getFileData();

		// Validate if the file status was found
		if (StringUtil.isEmpty(dto.getFileStatus())) {
			// In this case, the file status was not found... so I report it in
			// SSTRANS table
			transactionBo.updateForSendFileService(serviceKey, dto.getAmountTotal(), dto.getRegisterNumber());
			dto.setFileStatus(TransactionBO.STKEST_APVI);
		} else if (dto.getFileStatus().trim().equalsIgnoreCase("S")) {
			transactionBo.updateForSendFileService(serviceKey, dto.getAmountTotal(), dto.getRegisterNumber(),
					TransactionBO.STKEST_AERR);
			dto.setFileStatus(TransactionBO.STKEST_AERR);
			reportFileErrors();
		} else if (dto.getFileStatus().trim().equalsIgnoreCase("N")) {
			transactionBo.updateForSendFileService(serviceKey, dto.getAmountTotal(), dto.getRegisterNumber(),
					TransactionBO.STKEST_APA1);
			dto.setFileStatus(TransactionBO.STKEST_APA1);
		}
	}

	/**
	 * This method gets the library and the table (this process create the table
	 * where the file will be hosted). For it, we execute a stored procedure
	 * (this SP call to an AS400 process)
	 * 
	 * @throws ServiceException
	 */
	private void getLibraryAndTable() throws ServiceException {
		// Get library and table where we will persist the file content
		// -----------------------------------------
		DefinedLogger.SERVICE
		.debug("Obteniendo biblioteca y tabla para persistir archivo (corriendo proceso SS0001)...");
		dto = bo.getLibraryAndTable(dto);

		// Validate the execution result
		if (StringUtil.isEmpty(dto.getLibrary())) {
			DefinedLogger.SERVICE.error("No se pudo hallar la biblioteca donde persistir el contenido de archivo");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFOUNDLIBRARY);
		}

		if (StringUtil.isEmpty(dto.getTable())) {
			DefinedLogger.SERVICE.error("No se pudo hallar la tabla donde persistir el contenido de archivo");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFOUNDTABLE);
		}

		// Register table name
		// -------------------------------------------------------------------------------------

		// Register table name where the file will be hosted in SSTRANS
		registerTableName(dto.getTable());

		// Register in the SAT data source
		DefinedLogger.SERVICE.debug("Registrando en SAT...");
		dto.setInitialDate(TransactionDTO.DATE_EMPTY);
		dto.setInitialHour(TransactionDTO.HOUR_EMPTY);
		bo.registerInSAT(dto);
	}

	/**
	 * This method persists the file in the data source. We insert the file
	 * content in the table.
	 * 
	 * @throws ServiceException
	 */
	private void persistFile() throws ServiceException {
		// Persist file content
		// -------------------------------------------------------------------------------------
		DefinedLogger.SERVICE.debug("Persistiendo contenido de archivo...");

		// Validate if the amount of lines is greater than the amount of row
		// that an INSERT can generate
		List<String> list = Arrays.asList(lines);
		if (lines.length <= rowNumberMax) {
			DefinedLogger.SERVICE
			.debug(DefinedLogger.TABULATOR + "El archivo sera persistido en su totalidad con un solo INSERT");
			dto.setFileLines(list);
			bo.insertFileContent(dto);
		} else {
			/*
			 * Insert the lines by batches ITERATIONS: this variable indicates
			 * the amount of batches I*INSERT_ROW_MAX: this indicates the
			 * beginning of a batch (I+1)*INSERT_ROW_MAX: this indicates the
			 * ending of a batch
			 */
			int iterations = lines.length / rowNumberMax;
			DefinedLogger.SERVICE
			.debug(DefinedLogger.TABULATOR + "El archivo sera persistido en " + iterations + " partes");
			if (lines.length % rowNumberMax > 0) {
				iterations++;
			}
			for (int i = 0; i < iterations; i++) {
				dto.setFileLines(list.subList(i * rowNumberMax, (i + 1) * rowNumberMax));
				bo.insertFileContent(dto);
			}
		}
		DefinedLogger.SERVICE.debug(DefinedLogger.TABULATOR + "Se persistio el archivo de forma exitosa.");
	}

	/**
	 * This method loads file errors to report in the response
	 * 
	 * @throws ServiceException
	 */
	private void reportFileErrors() throws ServiceException {
		// Try to get the file errors
		DefinedLogger.SERVICE.info("Reportando errores del archivo " + dto.getTable());
		dto = bo.findFileErrors(dto);
	}

	/**
	 * This method generates the checksum according to the file content to
	 * compare with the checksum given in the file
	 * 
	 * @param key
	 * @return
	 * @throws ServiceException
	 */
	private String generateChecksum(String key) throws ServiceException {
		DefinedLogger.SERVICE.info("Generando checksum para comparar con el envio en el archivo...");

		if (StringUtil.isEmpty(key)) {
			DefinedLogger.SERVICE
			.error("SendFileService.generateChecksum: La key para generar el checksum esta en NULL");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT);
		}

		// Define and initialize variables
		long checksumDeLaLinea = 0;
		long checksumTotal = 0;
		long numeroDeCaracter = 0;
		String linea = "";
		int numeroDeLinea = 0;
		char caracter;

		// Get dictionary to generate checksum
		HashMap<Character, Short> dictionary = getDictionary();

		// Generate checksum
		for (int i = 0; i < lines.length; i++) {
			numeroDeLinea++;
			checksumDeLaLinea = 0;
			linea = lines[i];

			for (int j = 0; j < linea.length(); j++) {
				caracter = linea.charAt(j);

				numeroDeCaracter = dictionary.containsKey(caracter) ? dictionary.get(caracter) : 0;
				numeroDeCaracter = (long) numeroDeCaracter * (j + 1);

				checksumDeLaLinea += numeroDeCaracter;
			}

			checksumDeLaLinea *= (long) numeroDeLinea * Long.parseLong(key);
			checksumTotal = redondearChecksum(checksumDeLaLinea + checksumTotal, "8");
		}

		return String.valueOf(checksumTotal);
	}

	private long redondearChecksum(long checksum, String strChecksumSize) {
		short checksumSize = Short.parseShort(strChecksumSize);
		if (checksum > 0) {
			String check = Long.toString(checksum);
			// si el valor tiene mas que checksumSize caracteres se toman los
			// ultimos checksumSize
			if (check.length() > checksumSize) {
				check = check.substring(check.length() - checksumSize);
			}

			checksum = Long.parseLong(check);
			check = Long.toString(checksum);

			// si tiene menos que checksumSize se completa a la izquierda con
			// 1[2...]
			if (check.length() < checksumSize) {
				int diff = checksumSize - check.length();
				String prefijo = "";
				for (byte i = 1; i <= diff; i++) {
					prefijo += Byte.toString(i);
				}
				check = prefijo + check;
			}
			checksum = Long.parseLong(check);
		}
		return checksum;
	}

	/**
	 * This method returns the dictionary to generate checksum
	 * 
	 * @return
	 * @throws ServiceException
	 */
	private HashMap<Character, Short> getDictionary() throws ServiceException {
		HashMap<Character, Short> dictionary = new HashMap<Character, Short>();
		List<DictionaryListItemDTO> dictionaryList = boDic.findDic();
		String dic = "";

		if (dictionaryList != null && dictionaryList.size() > 0) {
			Iterator<DictionaryListItemDTO> i = dictionaryList.iterator();
			DictionaryListItemDTO dtoDicItem;
			while (i.hasNext()) {
				dtoDicItem = i.next();
				dic = dic + dtoDicItem.getSschar() + "|-|" + dtoDicItem.getSsvalu() + ",-,";
			}
		}

		String[] valores = dic.split(",-,");
		String[] valor;
		for (int i = 0; i < valores.length; i++) {
			valor = valores[i].split("\\|-\\|");
			dictionary.put(valor[0].charAt(0), Short.parseShort(valor[1]));
		}

		return dictionary;
	}

	/**
	 * This method validates if the adherent uses checksum for sending files. In
	 * this case, this validates if the given checksum is right
	 * 
	 * @throws ServiceException
	 */
	private void validateChecksum() throws ServiceException {

		//If the file type is PD, not validate checksum
		if(FILETYPE_PD.equalsIgnoreCase(dto.getFileType())) {
			return;
		}

		DefinedLogger.SERVICE.info("Validando si se usa checksum...");

		// Get and validate checksum data
		SendFileDTO aux = bo.findChecksumData(dto);
		if (aux == null) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTCHECKSUMDATA);
		}

		// Validate if the data is valid
		if (aux.getChecksumRequired() == null
				|| (!aux.getChecksumRequired().trim().equalsIgnoreCase(CHECKSUM_REQUIRED_YES)
						&& !aux.getChecksumRequired().trim().equalsIgnoreCase(CHECKSUM_REQUIRED_NO)) && !"".equals(aux.getChecksumRequired().trim()))  {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTCHECKSUMDATA);
		}

		// Validate if the checksum is required
		if (aux.getChecksumRequired().trim().equalsIgnoreCase(CHECKSUM_REQUIRED_NO) || "".equals(aux.getChecksumRequired().trim())) {
			DefinedLogger.SERVICE.info(DefinedLogger.TABULATOR + "No se requiere checksum para el envio de archivos");
			return;
		}
		
		
		// If the checksum is required, so we validate if the checksum key
		// exists
		if (StringUtil.isEmpty(aux.getChecksumKey())) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTCHECKSUMDATA);
		}

		// Get checksum from the file content
		String checksum = null;
		String line;
		for (int i = 0; i < lines.length; i++) {
			line = lines[i];
			if (line.length() > 1 && line.substring(0, 2).equals(FILE_FOOTER_PREFIX) && line.length() >= 45) {
				// Get checksum
				checksum = line.substring(37, 45);

				// Replace checksum per blanks
				lines[i] = line.substring(0, 37) + "        " + line.substring(45);
			}
		}

		// Validate if the checksum could be obtained
		if (checksum == null) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTCHECKSUMINFILE);
		}

		// Get the generated checksum to compare
		String generatedChecksum = generateChecksum(aux.getChecksumKey());
		if (StringUtil.isEmpty(generatedChecksum)) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTGENERATEDCHECKSUM);
		}

		// Validate if the checksums are equals
		if (!checksum.trim().equals(generatedChecksum)) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDCHECKSUM);
		}
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(5, dto.getTable()); // The table name is the file ID
		// (file name)
		setDataInVector(6, dto.getFileStatus());
		if (dto.getErrorList() != null && dto.getErrorList().getList() != null
				&& dto.getErrorList().getList().size() > 0) {
			setDataInVector(8, XMLUtil.dtoToXML(dto.getErrorList()));
		}
		setDataInVector(12, fmDto.getFileName());

		if (dto.getFileType().trim().equalsIgnoreCase(FILETYPE_PO)) {
			setDataInVector(7, XMLUtil.dtoToXML(paymentWayList));
			setDataInVector(14, String.valueOf(accountTotal));
		}
		setDataInVector(13, amountTotal);
		setDataInVector(15, String.valueOf(totalLines));
		setDataInVector(16, Integer.toString(serviceKey));
		setDataInVector(17, currencyAdherent);
		DefinedLogger.SERVICE.debug("Moneda del adherente es: " + currencyAdherent);
		// Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}

}