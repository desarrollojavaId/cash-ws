package ar.com.gestionit.cashmanagement.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Priority;

import ar.com.gestionit.cashmanagement.ServiceId;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.message.ExecuteRequest;
import ar.com.gestionit.cashmanagement.message.ExecuteResponse;
import ar.com.gestionit.cashmanagement.message.entity.ArrayOfstring20;
import ar.com.gestionit.cashmanagement.persistence.bo.TransactionBO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.TransactionDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.service.util.ServiceReturn;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;
import ar.com.gestionit.cashmanagement.util.XMLUtil;
import ch.qos.logback.classic.Logger;

/**
 * This abstract class resolves some general aspects of the service
 * @author jdigruttola
 */
public abstract class AbstractService implements IService {
	
	/**
	 * This is the channel used by the application.
	 * Use this constant to persist the channel field in SSTRANS
	 */
	protected static final short CHANNEL = (short)21;

	/**
	 * Service request
	 */
	protected ExecuteRequest request;

	/**
	 * Service response
	 */
	protected ExecuteResponse response;
	
	/**
	 * Transaction Business Object
	 */
	protected TransactionBO transactionBo = new TransactionBO();
	
	/**
	 * DEPURATION DATE for the queries (this value is obtained from MTCONVAL).
	 * If the user does not specify a "date from" for the query, we use the
	 * depuration date by default.
	 * 
	 * NOTE: this value must be loaded before the service execution because of
	 * the service will use it as possible input for the queries
	 */
	protected Date depurationDate;
	
	/**
	 * Our own SERVICE KEY to persist in SSTRANS (this value is obtained from MTCONVAL)
	 * 
	 * NOTE: although the Process ID exists, this is given by the frontend. So, we
	 * have our own Service ID for control.
	 */
	protected int serviceKey;
	

	/**
	 * This attribute is used to indicate what function was invoked
	 * 
	 * WHEN THE SERVICE HAS ONLY ONE FUNCTION ------------------------------
	 * If the service implementation has only one option for this
	 * attribute, the option must be to indicate in the service.id.properties
	 * file and we must do nothing in the service implementation (because
	 * this class, AbstractService,  will do it in the reportTransaction method)
	 * 
	 * WHEN THE SERVICE HAS SEVERAL FUNCTIONS ------------------------------
	 * If the service implementation can have several functions to implement,
	 * this will be responsible to indicate what function was invoked
	 * by the setInvokedFunction method (optional method)
	 */
	private String invokedFunction;
	
	/**
	 * This attribute is used by the most services to give the content
	 * to Exporter service.
	 * Exporter service, unlike the rest of the services, is only a mask. This executes
	 * other service (for example CardListService) and, after, it gets the content to export
	 * via this attribute (thus, this attribute has its getter).
	 * 
	 *  NOTE: this attribute won't be used by all the service, only by the services that have
	 *  content to export (report service which have grids).
	 */
	protected List<List<? extends IDTO>> contentToExport = new ArrayList<List<? extends IDTO>>();
	
	/**
	 * This attribute indicates if the file exporter is active while the service is executing, i. e.,
	 * if the service was called directly or by the exporter.
	 * In this way, we can make some particular actions only for the file exporter.
	 * For example, if the service was executed by the file exporter, we can access to data source to take
	 * the dynamic field title (if it is necessary), if not we can avoid this action and the data source access.
	 */
	protected boolean isFileExporterActive = false;

	/**
	 * This internal method executes the service routine with the context
	 * loaded (this already has loaded the request and the basic data in the response)
	 * @return
	 * @throws ServiceException
	 */
	abstract protected void execute() throws ServiceException;

	/**
	 * This internal method is used to load information
	 * in the DTOs and to do validations in the inputs 
	 * @return
	 * @throws ServiceException
	 */
	abstract protected void loadInputs() throws ServiceException;

	/**
	 * This internal method is used to load the results
	 * already processed in the response 
	 * @return
	 * @throws ServiceException
	 */
	abstract protected void loadOutputs() throws ServiceException;

	/**
	 * This internal method is used to initialize the service context
	 * @return
	 * @throws ServiceException
	 */
	abstract protected void initialize() throws ServiceException;

	/**
	 * This internal method is used to establish validations before
	 * run Execute method. By default, this method does nothing. In each
	 * service, this can be override to validate the info and throw
	 * a ServiceExecption if the data are not valid
	 */
	protected void validateBefore() throws ServiceException {
		//Validate if the request is NULL
		if(request == null) {
			try {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTREQUEST);
			} catch(ServiceException e) {
				throw e;
			}
		}

		//Validate if the adherent was given
		String adherent = getDataFromVector(1);
		
		adherent = ServiceUtil.validateNumberAndLengthObligatory(adherent, 7, ServiceConstant.SERVICERETURN_KEY_ADHERENTLENGTHERROR, ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
	}
	
	

	/**
	 * This internal method is used to establish validations after
	 * run Execute method. By default, this method does nothing. In each
	 * service, this can be override to validate the info and throw
	 * a ServiceExecption if the data are not valid
	 */
	protected void validateAfter() throws ServiceException {
		//Generate result code by default if this does not exist
		String resultCode;
		try {
			resultCode = getDataFromVector(0);
		} catch(ServiceException e) {
			resultCode = null;
		}
		if(resultCode == null
				|| resultCode.trim().equals("")
				|| resultCode.trim().equals("?"))
			createResponseByDefault();
	}

	/**
	 * This method initializes the response with the basic data
	 * @throws ServiceException
	 */
	protected void initializeResponse() throws ServiceException {
		//Load the response with basic data of request
		response = new ExecuteResponse();
		response.setDrmod(request.getDcmod());
		response.setDrpgc(request.getDrpgc());
		response.setDrrel(request.getDrrel());
		response.setDrsuc(request.getDrsuc());
		response.setDrtrn(request.getDrtrn());
		response.setEcantlin(request.getEcantlin());
		response.setEdatos(request.getEdatos());
		response.setEtdats(request.getEtdats());
		response.setEvalcs(request.getEvalcs());
		response.setEvalls(request.getEvalls());
	}

	/**
	 * This method processes the service routine according to the specified request
	 * and returns the response to send to the front end by the web service
	 * @param request
	 * @return
	 * @throws ServiceException
	 */
	public ExecuteResponse execute(ExecuteRequest request) throws ServiceException {
		try {
			//Assign the request
			this.request = request;
			
			//Initialize service
			initialize();

			//Validations before execute the query
			validateBefore();
			
			//Load basic data for the service context
			loadBasicData();

			//Logging request as entry
			DefinedLogger.SERVICE.info("Nombre del servicio: " + this.getClass().getSimpleName()
					+ ", Process ID: " + request.getProcessId()
					+ ", Message: Nueva peticion");
			DefinedLogger.SERVICE.debug(request.toString());

			//Load the response with basic data of request
			initializeResponse();

			loadInputs();

			//Variable to trace execution time
			long initTime = new Date().getTime();

			execute();

			//Trace execution time
			DefinedLogger.PERFORMANCE.info(this.getClass().getSimpleName()
					+ ": ProcessID[" + request.getProcessId()
					+ "] Execute - Time execution: "
					+ String.valueOf(new Date().getTime() - initTime)
					+ " milliseconds");


			loadOutputs();
			validateAfter();

			reportTransaction();
		} catch(ServiceException e) {
			reportResponse(e);
		} catch(Throwable e) {
			DefinedLogger.SERVICE.error(e.getMessage(), e);
			reportResponse(new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT));
		}
		DefinedLogger.SERVICE.info(response.toString());
		return response;
	}
	
	
	/*
	 * Transaction methods -------------------------------------------------------------------------------------
	 */
	
	/**
	 * This method load basic data for the service
	 * 
	 * - SERVICE KEY to persist in SSTRANS (this value is obtained from MTCONVAL)
	 * 
	 * - DEPURATION DATE for the queries (this value is obtained from MTCONVAL).
	 * If the user does not specify a "date from" for the query, we use the
	 * depuration date by default.
	 * @throws ServiceException 
	 */
	protected void loadBasicData() throws ServiceException {
		try {
			TransactionDTO dto = transactionBo.getTransactionData();
			if(dto != null) {
				serviceKey = dto.getMtvutr();
				try {
					depurationDate = ServiceConstant.SDF_DATE_FOR_INPUT.parse(dto.getMtvfdp());
				} catch (ParseException e) {
					DefinedLogger.SERVICE.error("La fecha de depuracion no pudo ser parseada");
					DefinedLogger.SERVICE.error(e.getMessage(), e);
				} 
			}
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No se pudieron obtener los datos de la DB");
			DefinedLogger.SERVICE.error(e.getMessage(), e);
			serviceKey = 0;
			depurationDate = null;
		}
	}

	/**
	 * This method generates a transaction DTO with the data
	 * by default already loaded
	 * @return
	 * @throws ServiceException
	 */
	protected TransactionDTO buildTransactionDto() throws ServiceException {
		//Generate new transaction
		TransactionDTO dto = new TransactionDTO();

		//Service name --------------------------------------
		String serviceName = this.getClass().getSimpleName();
		if(serviceName != null && serviceName.length() > 60)
			dto.setServiceName(serviceName.substring(0, 59));
		else
			dto.setServiceName(serviceName);

		/*
		 * Invoked function --------------------------------------
		 * 
		 * Validate if the service implementation loads this attribute
		 * (this case should happen only if the service implementation
		 * has more than one option).
		 * If not, this attribute should be NULL always
		 */
		if(invokedFunction == null)
			invokedFunction = ServiceId.getProperty(serviceName);
		//If there is not a mapped invoked function, so this will be empty...
		if(invokedFunction == null)
			invokedFunction = "";
		//Validate the variable long
		if(invokedFunction.length() > 25) {
			invokedFunction = invokedFunction.substring(0, 24);
		}
		dto.setStktrs(invokedFunction);
		
		//Other fields --------------------------------------
		dto.setStkcnl(CHANNEL);
		try {
			dto.setStqadh(Long.parseLong(getDataFromVector(1)));
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No pude convertir a long el adherente para persistir transaccion", e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		}
		dto.setStidpr(request.getProcessId());
		
		return dto;
	}

	/**
	 * This method logs the transactions in the data source
	 * @throws ServiceException
	 */
	protected void reportTransaction() throws ServiceException {
		try {
			TransactionDTO dto = buildTransactionDto();
			new TransactionBO().registerTransaction(dto);
		} catch (Exception e) {
			DefinedLogger.SERVICE.error("No se pudo registrar la ejecucion de este servicio en la fuente de datos");
			DefinedLogger.SERVICE.error(e.getMessage(), e);
		}
	}
	
	/*
	 * Data methods -------------------------------------------------------------------------------------
	 */

	/**
	 * This method validates where the data is according to the ETDats vector
	 * and returns it
	 * @param position
	 * @return
	 */
	protected String getDataFromVector(int position) throws ServiceException {
		try {
			String result;

			//Validate if the request has the requested data
			if(request.getEtdats() == null
					|| request.getEtdats().getItem() == null
					|| request.getEtdats().getItem().size() < position)
				return null;

			if(request.getEtdats().getItem().get(position).trim().toUpperCase().equals("C")) {
				result = request.getEvalcs().getItem().get(position).trim();
			} else {
				if (request.getEtdats().getItem().get(position).trim().toUpperCase().equals("L")) {
					result = request.getEvalls().getItem().get(position).trim();
				} else {
					//The position exists, but this has nothing
					result = null;
				}
			}
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTDATAINVECTOR);
		}
	}

	/**
	 * This method persists the data according to the position given by argument
	 * @param position
	 * @return
	 */
	protected void setDataInVector(int position, String data) {
		List<String> list;
		if(request.getEtdats().getItem().get(position).trim().toUpperCase().equals("C")) {
			//Validations...
			if(response.getEvalcs() == null)
				response.setEvalcs(new ArrayOfstring20());
			if(response.getEvalcs().getItem() == null)
				response.getEvalcs().setItem(new ArrayList<String>());
			list = response.getEvalcs().getItem();
		} else {
			//Validations...
			if(response.getEvalls() == null)
				response.setEvalls(new ArrayOfstring20());
			if(response.getEvalls().getItem() == null)
				response.getEvalls().setItem(new ArrayList<String>());
			list = response.getEvalls().getItem();
		}

		//Auto-filling...
		if(list.size() <= position) {
			int difference = position - list.size();
			for(int i = 0; i <= difference; i++) {
				list.add("");
			}
		}

		//Data validations...
		if(data == null) data = "";

		//Add data
		list.add(position, data);
		list.remove(position + 1);
	}
	
	/*
	 * Reporting methods -------------------------------------------------------------------------------------
	 */

	/**
	 * This method is used to log the number of the executed queries
	 * by the services control of performance
	 * @param queryCounter: number of the executed queries
	 */
	protected void reportQueryNumber(int queryCounter) {
		DefinedLogger.PERFORMANCE.info(this.getClass().getSimpleName()
				+ ": ProcessID[" + request.getProcessId()
				+ "] Execute - Number of executed queries: "
				+ queryCounter);
	}
	
	/**
	 * This method processes the error happened and generates
	 * a response to notify to the client
	 * @param request
	 * @param e
	 * @return
	 */
	protected void reportResponse(ServiceException e) {
		//Validate is the service exception is valid
		if(e == null) {
			return;
		}

		//Get Process ID
		String processId = "";
		if(request != null && request.getDrqcl() != null) {
			processId = request.getDrqcl().trim();
		}

		//Report full error
		DefinedLogger.SERVICE.error("Nombre del servicio: " + this.getClass().getSimpleName()
				+ ", Process ID: " + processId
				+ ", Message: " + e.getMessage(), e);

		try {
			reportResponse(ServiceReturnFactory.build(e.getKey()),
					Logger.ERROR_INT);
		} catch (ServiceException e1) {
			DefinedLogger.SERVICE.error(e1.getMessage(), e1);
			reportResponse(new ServiceReturn(ServiceReturn.ERROR_CODE, ServiceReturn.ERROR_DESC),
					Logger.ERROR_INT);
		}
	}

	/**
	 * This method loads the result code in the response
	 * @param key
	 * @param level
	 */
	private void reportResponse(String key, int level) {
		//Validate if the key is valid
		if(key == null || key.trim().equals(""))
			return;

		//Get the specified service return
		ServiceReturn r;
		try {
			r = ServiceReturnFactory.build(key);
		} catch (ServiceException e) {
			r = new ServiceReturn(ServiceReturn.ERROR_CODE, ServiceReturn.ERROR_DESC);
			level = Logger.ERROR_INT;
		}
		reportResponse(r, level);
	}

	/**
	 * This method loads the result code in the response
	 * @param key
	 * @param level
	 */
	@SuppressWarnings("deprecation")
	protected void reportResponse(ServiceReturn r, int level) {
		//Get Process ID
		String processId = "";
		if(request != null) {
			processId = request.getProcessId();
		}

		/*
		 * If the response does not exist, we create it. It is possible
		 * if a problem happened in some service implementation
		 */
		if(response == null) {
			response = new ExecuteResponse();
		}

		//Reporting...
		DefinedLogger.SERVICE.log(Priority.toPriority(level), "Nombre del servicio: " + this.getClass().getSimpleName()
				+ ", Process ID: " + processId
				+ ", Codigo: " + r.getCode()
				+ ", Descripcion: " + r.getDescription());
		setDataInVector(0, r.getCode());
		try {
			response.setXml(XMLUtil.dtoToXML(r));
		} catch (ServiceException e) {
			response.setXml("");
		}
	}

	/**
	 * This method reports an error (shortcut)
	 * @param key
	 */
	protected void reportError(String key) {
		reportResponse(key, Logger.ERROR_INT);
	}

	/**
	 * This method reports a possible error (shortcut)
	 * @param key
	 */
	protected void reportWarning(String key) {
		reportResponse(key, Logger.WARN_INT);
	}

	/**
	 * This method reports a service return successfully (shortcut)
	 * @param key
	 */
	protected void reportSuccess(String key) {
		reportResponse(key, Logger.INFO_INT);
	}

	/**
	 * This method reports that the service was processed successfully
	 */
	protected void createResponseByDefault() {
		reportSuccess(ServiceConstant.SERVICERETURN_KEY_OK);
	}
	
	/*
	 * Setters and getters methods -------------------------------------------------------------------------------------
	 */

	/**
	 * This method must be used if there are more than one function
	 * per service
	 * @param invokedFunction the invokedFunction to set
	 */
	protected void setInvokedFunction(String invokedFunction) {
		this.invokedFunction = invokedFunction;
	}

	/**
	 * Perhaps, you would want to know the status of this attribute
	 * from the service implementation
	 * @return
	 */
	public String getInvokedFunction() {
		return invokedFunction;
	}

	/**
	 * This method returns the content to export and this is used only by Exporter service
	 * @return the contentToExport
	 */
	public List<List<? extends IDTO>> getContentToExport() {
		return contentToExport;
	}
	
	/**
	 * This method indicates if the service was called directly or by the file exporter
	 * @param value: TRUE is the service was called by the service; FALSE is the service
	 * was called directly
	 */
	public void setExporterIndicator(boolean value) {
		this.isFileExporterActive = value;
	}
}