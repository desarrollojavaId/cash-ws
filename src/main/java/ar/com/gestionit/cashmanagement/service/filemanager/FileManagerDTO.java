package ar.com.gestionit.cashmanagement.service.filemanager;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_FILE_ROOT)
public class FileManagerDTO implements Serializable {
	
	private static final long serialVersionUID = 4969943919872788248L;
	
	//Inputs
	@XmlTransient
	private String contenidoMap; 
	@XmlTransient
	private String tokenId;
	@XmlTransient
	private String fileName;
	@XmlTransient
	private List<String> files;
	@XmlTransient
	private List<String> tokens;
	@XmlTransient
	private byte[] fileContent;
	
	//Outputs
	@XmlElement(name=ServiceConstant.DTO_FILE, defaultValue="")
	private List<FileManagerOutputDTO> list;
	

	public String getContenidoMap() {
		return contenidoMap;
	}

	public void setContenidoMap(String contenidoMap) {
		this.contenidoMap = contenidoMap;
	}

	/**
	 * @return the tokenId
	 */

	public String getTokenId() {
		return tokenId;
	}

	/**
	 * @param fileId the tokenId to set
	 */
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	
	/**
	 * @return the contenidoMap
	 */
//	public byte[] getContenidoMap() {
//		return contenidoMap;
//	}
	/**
	 * @param fileContent the fileContent to set
	 */
//	public void setContenidoMap(byte[] contenidoMap) {
//		this.contenidoMap = contenidoMap;
//	}
	
	/**
	 * @return the files
	 */
	public List<String> getFiles() {
		return files;
	}

	/**
	 * @param files to set
	 */
	public void setFiles(List<String> files) {
		this.files = files;
	}
	
	
	public List<String> getTokens() {
		return tokens;
	}

	public void setTokens(List<String> tokens) {
		this.tokens = tokens;
	}

	/**
	 * @return the list
	 */
	public List<FileManagerOutputDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<FileManagerOutputDTO> list) {
		this.list = list;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @return the fileContent
	 */
	public byte[] getFileContent() {
		return fileContent;
	}

	/**
	 * @param fileContent the fileContent to set
	 */
	public void setFileContent(byte[] fileContent) {
		this.fileContent = fileContent;
	}
}