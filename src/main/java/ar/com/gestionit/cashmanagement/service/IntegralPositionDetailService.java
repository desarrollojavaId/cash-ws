package ar.com.gestionit.cashmanagement.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.IntegralPositionDetailBO;
import ar.com.gestionit.cashmanagement.persistence.bo.ReferencesRecoveryBO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailFormasCobroDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailInputDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailPaymentListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailPaymentListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailReferencesDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailResumeListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionDetailResumeListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ReferencesRecoveryDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public class IntegralPositionDetailService extends AbstractService{

	private static final String LABEL_CHEQUESPD = "Cheques Pago Diferido";
	private static final String LABEL_IMPORTERECAUDADO = "Importe Recaudado";
	private static final String LABEL_IMPORTEPUBLICADO = "Importe Publicado";
	private static final String LABEL_EFECTIVO = "Efectivo";
	private static final String LABEL_CHEQUE48 = "Cheques al Día";
	private static final String LABEL_VALORESTOTALES = "Valores a Confirmar";
	private static final String LABEL_NOTACREDTOTAL = "Notas de Crédito";
	private static final String LABEL_CERTRETTOTAL = "Certificados de Retencion";
	private static final String LABEL_VALCOBTOTAL = "Valores al Cobro";
	private static final String LABEL_TRANSFERENCIA = "Transferencia";

	/**
	 * This attribute contains the total amount according to currency
	 */
	private HashMap<String, IntegralPositionDetailResumeListItemDTO> currencyMap = new HashMap<String, IntegralPositionDetailResumeListItemDTO>();
	private HashMap<String, IntegralPositionDetailPaymentListItemDTO> medioCurrencyMap = new HashMap<String, IntegralPositionDetailPaymentListItemDTO>();
	private HashMap<String, Double> medioTotal = new HashMap<String, Double>();

	private IntegralPositionDetailListDTO dtoItem;
	private IntegralPositionDetailListDTO dtoItemExport;
	private IntegralPositionDetailBO bo;
	private IntegralPositionDetailInputDTO filtros;
	private IntegralPositionDetailReferencesDTO referencias;
	private IntegralPositionDetailFormasCobroDTO formasCobro;
	private IntegralPositionDetailDTO input;
	private IntegralPositionDetailPaymentListDTO paymentMethods;
	private IntegralPositionDetailPaymentListDTO paymentMethodsExporter;
	private IntegralPositionDetailListDTO dtoPayment;
	private IntegralPositionDetailResumeListDTO resumen;
	private IntegralPositionDetailResumeListDTO resumenExport;

	private double porcentaje = 0;

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");

		bo = new IntegralPositionDetailBO();

		filtros = new IntegralPositionDetailInputDTO();
		referencias = new IntegralPositionDetailReferencesDTO();
		input = new IntegralPositionDetailDTO();
		formasCobro = new IntegralPositionDetailFormasCobroDTO();

		dtoItem = new IntegralPositionDetailListDTO();

		paymentMethods = new IntegralPositionDetailPaymentListDTO();
		paymentMethods.setList(new ArrayList<IntegralPositionDetailPaymentListItemDTO>());

		dtoPayment = new IntegralPositionDetailListDTO();

		resumen = new IntegralPositionDetailResumeListDTO();
		resumen.setList(new ArrayList<IntegralPositionDetailResumeListItemDTO>());

		dtoItemExport = new IntegralPositionDetailListDTO();
		dtoItemExport.setList(new ArrayList<IntegralPositionDetailListItemDTO>());

		resumenExport = new IntegralPositionDetailResumeListDTO();
		resumenExport.setList(new ArrayList<IntegralPositionDetailResumeListItemDTO>());

		paymentMethodsExporter = new IntegralPositionDetailPaymentListDTO();
		paymentMethodsExporter.setList(new ArrayList<IntegralPositionDetailPaymentListItemDTO>());

	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		DefinedLogger.SERVICE.debug("Cargando filtros...");
		filtros = (IntegralPositionDetailInputDTO)bo.xmlToDto(getDataFromVector(4), IntegralPositionDetailInputDTO.class);

		//Validate if the filters could be obtained
		if(filtros == null) {
			DefinedLogger.SERVICE.error("Los filtros para el serivicio IntegralPositionService no pudieron ser obtenidos");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_EMPTYFILTERS);
		}

		String convenio = getDataFromVector(2);
		String subconvenio = getDataFromVector(3);
		String nroConsulta = filtros.getNroConsulta();
		String fechaCobroDesde = filtros.getFechaCobroDesde();
		String fechaCobroHasta = filtros.getFechaCobroHasta();
		String tipoRec = filtros.getTipoRec();

		filtros.setConvenio(ServiceUtil.validateNumberAndLengthObligatory(convenio, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYAGREEMENT));
		filtros.setSubconvenio(ServiceUtil.validateNumberAndLengthObligatory(subconvenio, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDSUBAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBAGREEMENT));

		filtros.setNroConsulta(ServiceUtil.validateNumberAndLengthObligatory(nroConsulta, 15, ServiceConstant.SERVICERETURN_KEY_INVALIDQUERYNUMBER, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYQUERYNUMBER));

		filtros.setFechaCobroDesde(ServiceUtil.validateDate(fechaCobroDesde, ServiceConstant.SERVICERETURN_KEY_NOTDATEFROM));
		filtros.setFechaCobroHasta(ServiceUtil.validateDate(fechaCobroHasta, ServiceConstant.SERVICERETURN_KEY_NOTDATETO));
		ServiceUtil.validateDateInterval(fechaCobroDesde, fechaCobroHasta, ServiceConstant.SDF_AMERICAN_DATE);


		filtros.setTipoRec(ServiceUtil.validateNumberAndLengthObligatory(tipoRec, 3, ServiceConstant.SERVICERETURN_KEY_INVALIDRECTYPE, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYRECTYPE));

		DefinedLogger.SERVICE.debug("Cargando referencias...");
		referencias = (IntegralPositionDetailReferencesDTO)bo.xmlToDto(getDataFromVector(5), IntegralPositionDetailReferencesDTO.class);
		if (referencias == null) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_DEBREFERENCELENGTHERROR);
		}

		String ref1deudor = referencias.getRef1deudor();
		String ref1comprobante = referencias.getRef1comprobante();

		ServiceUtil.validateLength(ref1deudor, 25, ServiceConstant.SERVICERETURN_KEY_NOTVALIDREFERENCE);
		ServiceUtil.validateLength(ref1comprobante, 25, ServiceConstant.SERVICERETURN_KEY_NOTVALIDREFERENCE);

		ref1deudor = '%' + ref1deudor + "%";
		ref1comprobante = '%' + ref1comprobante + "%";

		referencias.setRef1deudor(ref1deudor);
		referencias.setRef1comprobante(ref1comprobante);

		DefinedLogger.SERVICE.debug("Cargando formas de cobro...");
		formasCobro = (IntegralPositionDetailFormasCobroDTO)bo.xmlToDto(getDataFromVector(12), IntegralPositionDetailFormasCobroDTO.class);

		bo.enablePager(getDataFromVector(6));
		
		input.setInputList(filtros);
		input.setReferencesList(referencias);
     	input.setFormasCobroList(formasCobro);

	}

	@SuppressWarnings("unchecked")
	@Override
	protected void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando querys...");
		dtoItem.setList((List<IntegralPositionDetailListItemDTO>)bo.findList(input));

		if (dtoItem.getList() != null && dtoItem.getList().size() > 0){

			//We have to load the reference list only if the service was called by the file exporter
			List<ReferencesRecoveryDTO> referenceList = null;
			if(super.isFileExporterActive) {
				ReferencesRecoveryDTO referenceRecoveryDto = new ReferencesRecoveryDTO();
				referenceRecoveryDto.setMrecnv(filtros.getConvenio());
				referenceRecoveryDto.setMretbo(filtros.getSubconvenio());
				ReferencesRecoveryBO referencesRecoveryBo = new ReferencesRecoveryBO();
				referenceList = (List<ReferencesRecoveryDTO>) referencesRecoveryBo.findList(referenceRecoveryDto);
			}

			for(IntegralPositionDetailListItemDTO item : dtoItem.getList()) {
				//We have to load the dynamic titles only if the service was called by the file exporter
				if(super.isFileExporterActive) {
					for(ReferencesRecoveryDTO ref : referenceList) {
						if(ref.getMreccg() != null && ref.getMreccg().trim().equals("1")) {
							item.setScrrd1Title(ref.getMredcg());
						} else if(ref.getMreccg() != null && ref.getMreccg().trim().equals("2")) {
							item.setScrrd2Title(ref.getMredcg());
						} else if(ref.getMreccg() != null && ref.getMreccg().trim().equals("11")) {
							item.setScrre1Title(ref.getMredcg());
						} else if(ref.getMreccg() != null && ref.getMreccg().trim().equals("12")) {
							item.setScrre2Title(ref.getMredcg());
						} else if(ref.getMreccg() != null && ref.getMreccg().trim().equals("13")) {
							item.setScrre3Title(ref.getMredcg());
						} else if(ref.getMreccg() != null && ref.getMreccg().trim().equals("14")) {
							item.setScrre4Title(ref.getMredcg());
						} else if(ref.getMreccg() != null && ref.getMreccg().trim().equals("15")) {
							item.setScrre5Title(ref.getMredcg());
						}
					}
				}

				//Formating... (this is used for the service and the exporter)
				item.setScritr(NumberUtil.amountFormat(item.getScritr()));
				item.setScritrExporter(NumberUtil.amountFormatExporter(item.getScritr()));
				item.setScrivcExporter(NumberUtil.amountFormatExporter(item.getScrivc()));
				item.setScrivc(NumberUtil.amountFormat(item.getScrivc()));
				item.setScriceExporter(NumberUtil.amountFormatExporter(item.getScrice()));
				item.setScrice(NumberUtil.amountFormat(item.getScrice()));
				item.setScriccExporter(NumberUtil.amountFormatExporter(item.getScricc()));
				item.setScricc(NumberUtil.amountFormat(item.getScricc()));
				item.setScripdExporter(NumberUtil.amountFormatExporter(item.getScripd()));
				item.setScripd(NumberUtil.amountFormat(item.getScripd()));
				item.setScrincExporter(NumberUtil.amountFormatExporter(item.getScrinc()));
				item.setScrinc(NumberUtil.amountFormat(item.getScrinc()));
				item.setScricrExporter(NumberUtil.amountFormatExporter(item.getScricr()));
				item.setScricr(NumberUtil.amountFormat(item.getScricr()));
				item.setScricvExporter(NumberUtil.amountFormatExporter(item.getScricv()));
				item.setScricv(NumberUtil.amountFormat(item.getScricv()));
				item.setScr1im(NumberUtil.amountFormat(item.getScr1im()));
				item.setScr1imExporter(NumberUtil.amountFormatExporter(item.getScr1im()));
				item.setScritf(NumberUtil.amountFormatExporter(item.getScritf()));
				item.setScriac(NumberUtil.amountFormatExporter(item.getScriac()));
				item.setScrivr(NumberUtil.amountFormatExporter(item.getScrivr()));
				item.setScriad(NumberUtil.amountFormatExporter(item.getScriad()));

				//FIXME: formato temporal
				try {
					if(!StringUtil.isEmpty(item.getScrfvt())) {
						Date date = ServiceConstant.SDF_DATE_FOR_OUTPUT.parse(item.getScrfvt());
						item.setScrfvt(ServiceConstant.SDF_DATE_FOR_INPUT.format(date));
					}
				} catch(Exception e){}
			}

			dtoPayment.setList((List<IntegralPositionDetailListItemDTO>)bo.findTotals(input));
			Iterator<IntegralPositionDetailListItemDTO> i = dtoPayment.getList().iterator();
			IntegralPositionDetailListItemDTO dto;
			while(i.hasNext()) {
				try{
					dto = i.next();

					// Update medio importes
					updateMedioImporte(dto.getScripd(), dto.getScrdmo(), LABEL_CHEQUESPD);
					updateMedioImporte(dto.getScrice(), dto.getScrdmo(), LABEL_EFECTIVO);
					updateMedioImporte(dto.getScricc(), dto.getScrdmo(), LABEL_CHEQUE48);
					updateMedioImporte(dto.getScricv(), dto.getScrdmo(), LABEL_VALORESTOTALES);
					updateMedioImporte(dto.getScrinc(), dto.getScrdmo(), LABEL_NOTACREDTOTAL);
					updateMedioImporte(dto.getScricr(), dto.getScrdmo(), LABEL_CERTRETTOTAL);
					updateMedioImporte(dto.getScrivc(), dto.getScrdmo(), LABEL_VALCOBTOTAL);
					updateMedioImporte(dto.getScritf(), dto.getScrdmo(), LABEL_TRANSFERENCIA);
					// ----------------------------------------------

					//Update medio cantidad ----------------------------------------------
					updateMedioCantidad(dto.getScrqpd(), dto.getScrdmo(), LABEL_CHEQUESPD);
					updateMedioCantidad(dto.getScrqic(), dto.getScrdmo(), LABEL_CHEQUE48);
					// ----------------------------------------------

					//Update amount summaries ----------------------------------------------
					updateTotal(dto.getScrdmo(), dto.getScritr(), LABEL_IMPORTERECAUDADO, dto.getScrqpd(), dto.getScrqic());
					dto.setScritrExporter(updateTotalForExport(dto.getScritr()));

					updateTotal(dto.getScrdmo(), dto.getScr1im(), LABEL_IMPORTEPUBLICADO, "0", "0");
					dto.setScr1imExporter(updateTotalForExport(dto.getScr1im()));
					// ----------------------------------------------
				} catch(Exception e) {
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT);
				}
			}

			//Print amount summaries ------------------------------------------------
			IntegralPositionDetailResumeListItemDTO item;
			for(String key : currencyMap.keySet()) {
				item = currencyMap.get(key);
				item.setImporte(NumberUtil.amountFormat((item.getAmount())));
				item.setImporteExportador(NumberUtil.amountFormatExporter((item.getAmount())));
				resumen.getList().add(item);
			}
			//------------------------------------------------

			//Print medios summaries ------------------------------------------------
			IntegralPositionDetailPaymentListItemDTO medioItem;
			for(String key : medioCurrencyMap.keySet()) {
				medioItem = medioCurrencyMap.get(key);

				//Validate if the amount is greater than zero
				medioItem.setImporte(NumberUtil.amountFormat((medioItem.getInternalAmount())));

				porcentaje = (medioItem.getInternalAmount() * 100 / medioTotal.get(medioItem.getMoneda()));
				medioItem.setPorcentaje((NumberUtil.amountFormat(porcentaje)));

				medioItem.setAmountForExporter(NumberUtil.amountFormatExporter((medioItem.getInternalAmount())));

				paymentMethods.getList().add(medioItem);
			}
			//------------------------------------------------


			contentToExport.add(dtoItem.getList());
			contentToExport.add(paymentMethods.getList());
			contentToExport.add(resumen.getList());
		}else{
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");		
		
		setDataInVector(8, bo.dtoToXML(resumen));
		setDataInVector(9, bo.dtoToXML(paymentMethods));
		setDataInVector(10, bo.getPagerAsXML());
		setDataInVector(11, filtros.getNroConsulta());
		setDataInVector(7, bo.dtoToXML(dtoItem));
	}

	private String updateTotalForExport(String strAmount) {
		if (strAmount != null){
			return NumberUtil.twoDecimalMax(Float.parseFloat(strAmount));
		}
		return "0";
	}

	/**
	 * This method updates the number of the checks of this check type in a specified payment type
	 * @param strCantidad
	 * @param currency
	 * @param label
	 */
	private void updateMedioCantidad(String strCantidad, String currency, String label) {
		if (strCantidad != null && NumberUtil.isNumeric(strCantidad) && Long.parseLong(strCantidad) > 0) {
			IntegralPositionDetailPaymentListItemDTO item;
			String id = label + " " + currency;

			if(!medioCurrencyMap.containsKey(id)) {
				item = generateNewMedioDTO(label, currency);
				medioCurrencyMap.put(id, item);
			} else {
				item = medioCurrencyMap.get(id);
			}
			item.setCantidad(item.getCantidad() + Long.parseLong(strCantidad));
		}
	}

	/**
	 * This method updates the total amount according to the payment type and currency
	 * EXAMPLES:
	 * 	- Case 1: "Cheques a 48 HS" (payment type) and "PESOS" (currency)
	 * 	- Case 2: "Cheques a 48 HS" (payment type) and "DOLAR" (currency)
	 * @param strAmount
	 * @param currency
	 * @param label
	 * @return
	 */
	private double updateMedioImporte(String strAmount, String currency, String label) {
		if (strAmount == null || !NumberUtil.isNumeric(strAmount) || Double.parseDouble(strAmount) <= 0) {
			return 0;
		}
		IntegralPositionDetailPaymentListItemDTO item;
		String id = label + " " + currency;

		if(!medioCurrencyMap.containsKey(id)) {
			item = generateNewMedioDTO(label, currency);
			medioCurrencyMap.put(id, item);
		} else {
			item = medioCurrencyMap.get(id);
		}
		item.setInternalAmount(item.getInternalAmount() + Double.parseDouble(strAmount));
		medioCurrencyMap.put(id, item);

		updateMedioTotal(currency, Double.parseDouble(strAmount));

		return Double.parseDouble(strAmount);
	}

	/**
	 * This method calculates the total amount according to currency
	 * Examples:
	 * 	- Case 1: "PESOS" Total amount
	 * 	- Case 2: "DOLAR" Total amount
	 * @param currency
	 * @param amount
	 */
	private void updateMedioTotal(String currency, double amount) {
		if(!medioTotal.containsKey(currency)) {
			medioTotal.put(currency, amount);
		} else {
			medioTotal.put(currency, medioTotal.get(currency) + amount);
		}
	}

	/**
	 * This method generates a new payment list item by default to put in the map
	 * @param label
	 * @param currency
	 * @return
	 */
	private IntegralPositionDetailPaymentListItemDTO generateNewMedioDTO(String label, String currency) {
		IntegralPositionDetailPaymentListItemDTO item = new IntegralPositionDetailPaymentListItemDTO();
		item.setDescMedPag(label);
		item.setCantidad(0);
		item.setInternalAmount(0);
		item.setMoneda(currency);
		return item;
	}

	/**
	 * This method updates the total amount according to currency
	 * @param currency
	 * @param strAmount
	 * @param label
	 */
	protected void updateTotal(String currency, String strAmount, String label, String cantidad1, String cantidad2) {
		if (strAmount != null && NumberUtil.isNumeric(strAmount) && Double.parseDouble(strAmount) > 0){
			//Define and initialize variables
			IntegralPositionDetailResumeListItemDTO item;
			String id = label + " " + currency;

			//Get item according to the currency
			if(!currencyMap.containsKey(id)) {
				item = new IntegralPositionDetailResumeListItemDTO();
				item.setAmount(0);
				item.setPorcentaje("100.00");
				item.setCantidad(0);
				item.setDescImp(label);
				item.setMoneda(currency);
				currencyMap.put(id, item);
			} else {
				item = currencyMap.get(id);
			}

			//Update amount
			item.setAmount(item.getAmount() + Double.parseDouble(strAmount));
			item.setCantidad(item.getCantidad() + Integer.parseInt(cantidad1) + Integer.parseInt(cantidad2));
		}
	}
}