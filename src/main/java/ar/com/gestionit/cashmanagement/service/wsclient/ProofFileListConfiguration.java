package ar.com.gestionit.cashmanagement.service.wsclient;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class ProofFileListConfiguration {
	
	private static final String SCHEMA_PACKAGE = "ar.com.gestionit.cashmanagement.service.wsclient";
	
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath(SCHEMA_PACKAGE);
		return marshaller;
	}

	@Bean
	public ProofFileListClient servicioComprobanteServiceClient(Jaxb2Marshaller marshaller) {
		ProofFileListClient client = new ProofFileListClient();
		client.setDefaultUri(ProofFileListClient.SOAP_ACTION);
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}
}