package ar.com.gestionit.cashmanagement.service;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.TransactionBO;
import ar.com.gestionit.cashmanagement.persistence.dto.TransactionDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public class EscrowFileStatusService extends AbstractService{

	/**
	 * Transaction business object
	 */
	private TransactionBO transactionBo;

	/**
	 * DTO
	 */
	private TransactionDTO transactionDto;

	@Override
	protected void validateBefore() throws ServiceException {
		//Validate if the reques is NULL
		if(request == null) {
			try {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTREQUEST);
			} catch(ServiceException e) {
				throw e;
			}
		}
	}
	@Override
	protected void initialize() throws ServiceException {
		transactionDto = new TransactionDTO();
		transactionBo = new TransactionBO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		//Validate transaction ID
		String stqtrs = getDataFromVector(2);
		if(StringUtil.isEmpty(stqtrs)) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTTRANSACTIONID);
		}
		if(!NumberUtil.isNumeric(stqtrs)) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_TRANSACTIONIDNOTNUMERIC);
		}
		transactionDto.setStqtrs(Integer.parseInt(stqtrs));
	}

	@Override
	protected void execute() throws ServiceException {
		transactionDto = transactionBo.findStkestByStqtrs(transactionDto);
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(3, transactionDto.getStkest());
	}
}