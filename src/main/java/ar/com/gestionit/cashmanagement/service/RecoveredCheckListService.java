package ar.com.gestionit.cashmanagement.service;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;

/**
 * Consulta cheques recuperados
 * @author jdigruttola
 */
public class RecoveredCheckListService extends AbstractRecoveredCheckListService {
	
	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		ioDto.setAdherent(getDataFromVector(1));
		
		String a = getDataFromVector(2);
		a = ServiceUtil.validateNumberAndLengthObligatory(a, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYAGREEMENT);
		ioDto.setAgreementNumber(a);
		
		String b = getDataFromVector(3);
		b = ServiceUtil.validateNumberAndLengthObligatory(b,7,ServiceConstant.SERVICERETURN_KEY_INVALIDSUBAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBAGREEMENT);
		ioDto.setAgreementSubnumber(b);
		
		b = getDataFromVector(4);
		b = ServiceUtil.validateNumberAndLength(b,10,ServiceConstant.SERVICERETURN_KEY_NOTVALIDCHECKNUMBER);
		ioDto.setCheckNumber(b);
		
		ioDto.setDateRecoveredFrom(ServiceUtil.validateDateForDepuration(getDataFromVector(5), depurationDate, ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATERECOVFROM));
		ioDto.setDateRecoveredTo(ServiceUtil.validateDate(getDataFromVector(6), ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATERECOVTO));
		
		ioDto.setDateCheckFrom(ServiceUtil.validateDateForDepuration(getDataFromVector(7), depurationDate, ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATERECOVFROM));
		ioDto.setDateCheckTo(ServiceUtil.validateDate(getDataFromVector(8), ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATERECOVTO));
		
		ioDto.setDateAcredFrom(ServiceUtil.validateDateForDepuration(getDataFromVector(9), depurationDate, ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATERECOVFROM));
		ioDto.setDateAcredTo(ServiceUtil.validateDate(getDataFromVector(10), ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATERECOVTO));
		
		String amount = getDataFromVector(11);
		amount = ServiceUtil.validateNumberAndLength(amount, 15, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTFROM);
		ServiceUtil.validateTwoDecimalsMax(amount, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTFROM);
		ioDto.setAmountFrom(amount);
		
		amount = getDataFromVector(12);
		amount = ServiceUtil.validateNumberAndLength(amount, 15, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTTO);
		ServiceUtil.validateTwoDecimalsMax(amount, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTTO);
		ioDto.setAmountTo(amount);
		
		a = getDataFromVector(13);
		if(StringUtil.isEmpty(a)) {
			a = "0";
		} else { if(!a.trim().equalsIgnoreCase("S") && !a.trim().equalsIgnoreCase("N") && !a.trim().equalsIgnoreCase("A")) {
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_INVALIDRECOVEREDP));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDRECOVEREDP);
		}}
		ioDto.setRecoveredStatus(a);

		ioDto.setStatuses(null);
		
		bo.enablePager(getDataFromVector(14));
	}
	
	@Override
	public void execute() throws ServiceException {
		/*
		 * Validate if the request only wants the check in process for the current batch.
		 * In this case, we must get the current batch ID by SP SS0003 and set this result in the DTO,
		 * so, the query will be a batch ID to filter
		 */
		if(ioDto.getRecoveredStatus() != null && ioDto.getRecoveredStatus().trim().equalsIgnoreCase("A")) {
			//Get the current batch ID
			bo.loadRequestNumber(ioDto);
		}
		
		/*
		 * Execute the query
		 */
		super.execute();
	}
	
	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");
		setDataInVector(15, bo.dtoToXML(ioDto));
		setDataInVector(16, String.valueOf(checkTotal));
		String number = NumberUtil.twoDecimalMax(amountTotal).replace(".","").replace(",", ".");
		setDataInVector(17, number);
		setDataInVector(18, bo.getPagerAsXML());
		
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
}