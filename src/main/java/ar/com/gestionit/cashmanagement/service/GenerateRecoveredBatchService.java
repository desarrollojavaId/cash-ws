package ar.com.gestionit.cashmanagement.service;

import java.util.Date;
import java.util.Iterator;

import ar.com.gestionit.cashmanagement.exception.FatalException;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.GeneralRecoveredBO;
import ar.com.gestionit.cashmanagement.persistence.bo.GenerateRecoveredBatchBO;
import ar.com.gestionit.cashmanagement.persistence.bo.RecoveredCheckListBO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredBatchDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.TransactionDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

/**
 * "Generar lote recupero"
 * @author jdigruttola
 *
 */
public class GenerateRecoveredBatchService extends AbstractService {

	/**
	 * DTOs
	 */
	private RecoveredBatchDTO ioDto;

	/**
	 * Business object for this service
	 */
	private GenerateRecoveredBatchBO bo;

	/**
	 * Business object for general usage about the recovered checks
	 */
	private GeneralRecoveredBO generalBo;
	/**
	 * This attribute keeps the current number of the check for this recovered
	 */
	private int checkTotal;

	/**
	 * This attribute keeps the current amount for this recovered
	 */
	private float amountTotal;

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new GenerateRecoveredBatchBO();
		generalBo = new GeneralRecoveredBO();
		ioDto = new RecoveredBatchDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		ioDto.setAdherent(getDataFromVector(1));
		
		String a = getDataFromVector(2);
		 a = ServiceUtil.validateNumberAndLengthObligatory(a, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYAGREEMENT);
		ioDto.setAgreementNumber(a);
		
		String b = getDataFromVector(3);
		b = ServiceUtil.validateNumberAndLengthObligatory(b, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDSUBAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBAGREEMENT);
		ioDto.setAgreementSubnumber(b);
		
		
		Date now = new Date();
		ioDto.setCurrentDate(ServiceConstant.SDF_DATE.format(now));
		ioDto.setCurrentHour(ServiceConstant.SDF_HOUR.format(now));
	}

	@Override
	public void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando DTOs...");
		//Get request number
		ioDto.setRequestNumber(generalBo.executeSS0003(ioDto.getAdherent(), ioDto.getAgreementNumber(), ioDto.getAgreementSubnumber()));

		//Get branch
		RecoveredBatchDTO tmpDto = bo.find(ioDto);
		if(tmpDto == null 
				|| tmpDto.getBranch() == null
				|| tmpDto.getBranch().trim().equals("")) {
			ioDto.setBranch("0");
		} else {
			ioDto.setBranch(tmpDto.getBranch());
		}

		/*
		 * Execute all in atomic mode because there are some updates in the data source
		 * and we must use a transaction (to commit/rollback).
		 */
		bo.executeTransaction(ioDto);
		
		/*
		 * Recovered check list summary
		 */
		RecoveredCheckListDTO rclDto = new RecoveredCheckListDTO();
		rclDto.setAdherent(ioDto.getAdherent());
		rclDto.setAgreementNumber(ioDto.getAgreementNumber());
		rclDto.setAgreementSubnumber(ioDto.getAgreementSubnumber());
		rclDto.setAmountFrom("0");
		rclDto.setAmountTo("0");
		rclDto.setCheckNumber("0");
		rclDto.setDateRecoveredFrom("0");
		rclDto.setDateRecoveredTo("0");
		rclDto.setDateAcredFrom("0");
		rclDto.setDateAcredTo("0");
		rclDto.setDateCheckFrom("0");
		rclDto.setDateCheckTo("0");
		rclDto.setStatuses(null);
		rclDto.setBatchId(ioDto.getRequestNumber());
		
		RecoveredCheckListBO rclBo = new RecoveredCheckListBO();
		rclDto = rclBo.findChecks(rclDto);
		DefinedLogger.SERVICE.debug("Seteo de montos totales...");
		if(rclDto != null && rclDto.getList() != null && rclDto.getList().size() > 0) {
			checkTotal = rclDto.getList().size();
			Iterator<RecoveredCheckListItemDTO> it = rclDto.getList().iterator();
			while(it.hasNext()) {
				try {
					amountTotal += Float.parseFloat(it.next().getAmount());
				} catch(Exception e) {
					DefinedLogger.SERVICE.warn("No se pudo setear el monto total.", e);
				}
			}
			DefinedLogger.SERVICE.warn("No se pudo setear el monto total.");
		}
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");
		setDataInVector(4, ioDto.getRequestNumber());
		setDataInVector(5, String.valueOf(amountTotal));
		setDataInVector(6, String.valueOf(checkTotal));
		setDataInVector(7, String.valueOf(serviceKey));
		setDataInVector(8, ioDto.getBranch());
		
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
	
	/**
	 * This method logs the transactions in the data source
	 * If the application cannot do it, this must be killed
	 * because this will miss the control about the transactions
	 * which is processing
	 * @throws FatalException
	 */
	@Override
	protected TransactionDTO buildTransactionDto() throws ServiceException {
		TransactionDTO transDto = super.buildTransactionDto();
		transDto.setStnars(ioDto.getRequestNumber());
		transDto.setStkcvn(ioDto.getAgreementNumber());
		transDto.setStktbo(ioDto.getAgreementSubnumber());
		//FIXME: realmente aca va a venir el usuario
		transDto.setUser(request.getDrqus());
		transDto.setStjtot(String.valueOf(amountTotal));
		transDto.setStqtot(String.valueOf(checkTotal));
		
		return transDto;
	}
	
}