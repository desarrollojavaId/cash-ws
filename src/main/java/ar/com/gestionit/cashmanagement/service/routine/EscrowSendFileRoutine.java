package ar.com.gestionit.cashmanagement.service.routine;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.mail.MailManager;
import ar.com.gestionit.cashmanagement.persistence.bo.EscrowSendFileBO;
import ar.com.gestionit.cashmanagement.persistence.bo.TransactionBO;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowSendFileDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.TransactionDTO;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManager;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManagerDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public class EscrowSendFileRoutine implements Runnable {

	private String invokedFunction;
	private short channel;
	private String processId;
	private String user;
	private int serviceKey;

	/**
	 * This is a flag to know if the execution finished or not
	 */
	private AtomicBoolean finished = new AtomicBoolean(false);


	/**
	 * Constants
	 * -------------------------
	 * Empty date
	 */
	private static final String DATE_EMPTY = "0001-01-01";

	private static final String STATUS_F = "F";
	private static final String STATUS_C = "C";


	/**
	 * BOs use in this service
	 */
	private EscrowSendFileBO bo;
	private TransactionBO transactionBo;
	private FileManager manager;

	/**
	 * Service DTO
	 */
	private EscrowSendFileDTO dto;

	/**
	 * File Manager DTO
	 */
	private FileManagerDTO fmDto;

	/**
	 * This attribute contains the number of rows maximum that we can use
	 * to persist per each INSERT. 
	 * This is used to persist the file content in the datasource
	 */
	private int rowNumberMax;

	/**
	 * Escrow status
	 */
	private String escrowStatus = "";

	/**
	 * Exception key
	 */
	private String exceptionKey = "";


	/**
	 * This method initializes the attributes of the service
	 */
	private void initialize() {
		bo = new EscrowSendFileBO();
		transactionBo = new TransactionBO();
		manager = new FileManager();
	}

	@Override
	public void run() {
		try{
			initialize();

			/*
			 * This method gets the file name (for example FIDECUO) according to the
			 * group and file type given by inputs
			 */
			loadFileName();

			/*
			 * Get the sent file from SQL Server by the frontend
			 */
			loadFileContent();

			/*
			 * This method validates if the escrow is enabled to send file
			 */
			validateHash();

			/*
			 * Validate if we must update the status in SSFIDEIC table
			 */
			updateStatus();

			/*
			 * The application must trace the file and its status.
			 * So, this registers it in the datasource (SSTRANS)
			 * NOTE: Even though the file is not right, we must
			 * register the tried and its real status
			 */
			registerTransaction();

			/*
			 * Get the library from a global property
			 */
			getLibrary();

			/*
			 * This method executes SP SS0022P to generate the member
			 */
			generateMember();

			/*
			 * This method obtains the table name according to the file name, file type and the escrow number.
			 * This data is the logical table name really.
			 * Obtengo el nombre del Logico que se utilizara como via de acceso para el miembro dentro del archivo correspondiente
			 */
			getTable();

			/*
			 * Update the table name in the register of the escrow in SSTRANS.
			 * The file content will be hosted in this table
			 */
			registerTableName();

			/*
			 * Persist the file in the data source.
			 * For it, we persist the file content in the library and table
			 * given by the process.
			 */
			persistFile();

			/*
			 * After we persisted the file in the data source, a AS400 process
			 * validates if the file has errors and persists the validation result
			 * in the data source. So, 
			 */
			registerStatus();

			/*
			 * Update the upload date according to the file type 
			 */
			updateDate();

			/*
			 * Report the sent file via mails 
			 */
			reportViaEmail();

			/*
			 * This flag indicates that the execution finished
			 */
			finished.set(true);
		} catch(ServiceException e) {
			DefinedLogger.SERVICE.error("Ocurrio un error controlado", e);
			setExceptionKey(e.getKey());			
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("Ocurrio un error inesperado", e);
			setExceptionKey(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT);
		}
	}

	/**
	 * This method gets the file name (for example FIDECUO) according to the
	 * group and file type given by inputs
	 * @throws ServiceException
	 */
	public void loadFileName() throws ServiceException {
		DefinedLogger.SERVICE.info("Obteniendo nombre de archivo desde la DB...");
		dto.setFileName(bo.findFileName(dto));

		if(StringUtil.isEmpty(dto.getFileName())) {
			DefinedLogger.SERVICE.error("No se pudo obtener el nombre del archivo");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFOUNDFILENAME);
		}

		dto.setFileName(dto.getFileName().trim());
	}

	/**
	 * This method loads the ending date in main memory from the datasource
	 * @throws ServiceException
	 */
	private void loadEndingDate() throws ServiceException {
		DefinedLogger.SERVICE.info("Cargando fecha de cierre...");
		String endingDate = bo.findEndingDate();

		//Validate if the ending date could be found
		if(StringUtil.isEmpty(endingDate) || endingDate.length() != 8) {
			DefinedLogger.SERVICE.error(ServiceConstant.SERVICERETURN_KEY_NOTENDINGDATE);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTENDINGDATE);
		}

		//Format ending date
		endingDate = new StringBuffer(endingDate.substring(6))
				.append("-")
				.append(endingDate.substring(4, 6))
				.append("-")
				.append(endingDate.substring(2, 4)).toString();
		dto.setEndingDate(endingDate);
	}

	/**
	 * This method validates if the escrow is available to send file 
	 * @throws ServiceException
	 */
	private void validateHash() throws ServiceException {
		loadEndingDate();

		DefinedLogger.SERVICE.info("Validando fideicomiso...");
		dto.setToday(ServiceConstant.SDF_DATE_FOR_INPUT.format(new Date()));
		bo.validateHash(dto);
	}

	/**
	 * Validate if we must update the status in SSFIDEIC table
	 * @throws ServiceException
	 */
	private void updateStatus() throws ServiceException {
		/*
		 * Try to get the data from the datasource to evaluate if we must modify the data or not
		 * There are the initial parameters for escrow
		 */
		EscrowSendFileDTO data = bo.findDataForStatus(dto);

		//Validate if we could get data from the datasource
		if(data == null || data.getEscrowStatus() == null)
			return;

		//Generate the conditions
		boolean opSent = data.getDateOp() != null && !data.getDateOp().trim().equalsIgnoreCase(DATE_EMPTY) ? true : false;
		boolean pgSent = data.getDatePg() != null && !data.getDatePg().trim().equalsIgnoreCase(DATE_EMPTY) ? true : false;
		boolean clSent = data.getDateCl() != null && !data.getDateCl().trim().equalsIgnoreCase(DATE_EMPTY) ? true : false;
		boolean cuSent = data.getDateCu() != null && !data.getDateCu().trim().equalsIgnoreCase(DATE_EMPTY) ? true : false;
		boolean sgSent = data.getDateSg() != null && !data.getDateSg().trim().equalsIgnoreCase(DATE_EMPTY) ? true : false;

		boolean update = false;
		if(data.getEscrowStatus().trim().equalsIgnoreCase("V")) {
			update = updateStatusV(data,  opSent, pgSent, clSent, cuSent, sgSent);
		} else if(data.getEscrowStatus().trim().equalsIgnoreCase("A") || data.getEscrowStatus().trim().equalsIgnoreCase("C")) {
			update = updateStatusC(data, opSent, pgSent, clSent, cuSent, sgSent);
		}

		if(update) {
			//Update the escrow status in the datasource
			bo.updateStatus(dto);
			//Get current escrow status to add in the output
			escrowStatus = dto.getSskest();
		} else {
			//Get current escrow status to add in the output
			escrowStatus = data.getEscrowStatus();
		}
	}

	/**
	 * 
	 * @param data
	 * @param op
	 * @param pg
	 * @param cl
	 * @param cu
	 * @param sg
	 * @return
	 * @throws ServiceException
	 */
	private boolean updateStatusV(EscrowSendFileDTO data, boolean opSent, boolean pgSent, boolean clSent, boolean cuSent, boolean sgSent) throws ServiceException {
		//Validate if the argument is right
		if(data == null)
			return false;

		//Define variables
		boolean update = false;
		dto.setSskest(STATUS_F);
		dto.setSskests(STATUS_F);
		dto.setSskestr(STATUS_F);

		//Group level
		if(data.getGroup() != null && data.getGroup().trim().equalsIgnoreCase("FAVA")) {
			// FAVA en vuelco inicial solo sube OP
			if(dto.getFileType().trim().equalsIgnoreCase("OP")){
				update=true;
			} else {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT);
			}
		} else {
			// Es un vuelco inicial normal, se analizan los parametros de la SSFIDEIC
			boolean esNecesarioCL = data.getIsNecessaryCl() != null && data.getIsNecessaryCl().trim().equalsIgnoreCase("S") ? true : false;
			boolean esNecesarioPG = data.getIsNecessaryPg() != null && data.getIsNecessaryPg().trim().equalsIgnoreCase("S") ? true : false;
			boolean esNecesarioSG = data.getIsNecessarySg() != null && data.getIsNecessarySg().trim().equalsIgnoreCase("S") ? true : false;

			if(dto.getFileType().trim().equalsIgnoreCase("OP")){
				// Se esta subiendo un archivo de OP
				if( !cuSent
						|| (!clSent && esNecesarioCL)
						|| (!pgSent && esNecesarioPG)
						|| (!sgSent && esNecesarioSG)) {
					update = false;
				} else{
					update = true;
				}
			} if(dto.getFileType().trim().equalsIgnoreCase("CU")){
				// Se esta subiendo un archivo de OP
				if( !opSent
						|| (!clSent && esNecesarioCL)
						|| (!pgSent && esNecesarioPG)
						|| (!sgSent && esNecesarioSG)) {
					update = false;
				} else{
					update = true;
				}
			} else if(dto.getFileType().trim().equalsIgnoreCase("PG")){
				// Se esta subiendo un archivo de PG
				if(!opSent
						|| !cuSent
						|| (!clSent && esNecesarioCL)
						|| (!sgSent && esNecesarioSG)) {
					update = false;
				} else{
					update = true;
				}
			} else if(dto.getFileType().trim().equalsIgnoreCase("CL")){
				// Se esta subiendo un archivo de CL
				if(!opSent
						|| !cuSent
						|| (!pgSent && esNecesarioPG)
						|| (!sgSent && esNecesarioSG)) {
					update = false;
				} else{
					update = true;
				}
			} else if(dto.getFileType().trim().equalsIgnoreCase("SG")){
				// Se esta subiendo un archivo de SG
				if(!opSent
						|| !cuSent
						|| (!pgSent && esNecesarioPG)
						|| (!clSent && esNecesarioCL)) {
					update = false;
				} else{
					update = true;
				}
			}
		}
		return update;
	}

	private boolean updateStatusC(EscrowSendFileDTO data, boolean opSent, boolean pgSent, boolean clSent, boolean cuSent, boolean sgSent) throws ServiceException {
		//Validate is the argument given is right
		if(data == null)
			return false;

		//Define and initialize variables
		boolean update = false;
		//		dto.setUpdateStatus("C");

		if(dto.getFileType().trim().equalsIgnoreCase("PG")){
			// Es una cobranza normal
			update = true;
			dto.setSskest(STATUS_C);
			//TODO preguntarle a Christian por esEnvioSGPeriodico que viene como input
		} else if(true//this.esEnvioSGPeriodico.trim().equalsIgnoreCase("S")
				&& dto.getFileType().trim().equalsIgnoreCase("SG")
				&& data.getIsNecessarySg() != null && data.getIsNecessarySg().trim().equalsIgnoreCase("S")
				&& dto.getSafeStatus() != null && dto.getSafeStatus().trim().equalsIgnoreCase("A")) {
			// Es una subida de Seguros
			update = true;
			dto.setSskests(STATUS_C);
		} else {
			// Es revolving
			boolean esNecesarioCL = dto.getIsNecessaryCl() != null && dto.getIsNecessaryCl().trim().equalsIgnoreCase("S") ? true : false;
			boolean esNecesarioSG = dto.getIsNecessarySg() != null && dto.getIsNecessarySg().trim().equalsIgnoreCase("S") ? true : false;

			if(dto.getFileType().trim().equalsIgnoreCase("OP")) {
				if(!cuSent
						|| (!clSent && esNecesarioCL)
						|| (!sgSent && esNecesarioSG)) {
					update = false;
				} else{
					update = true;
					dto.setSskestr(STATUS_C);
					dto.setSskests(STATUS_C);
				}
			} else if(dto.getFileType().trim().equalsIgnoreCase("CU")){
				if(!opSent
						|| (!clSent && esNecesarioCL)
						|| (!sgSent && esNecesarioSG)) {
					update = false;
				} else{
					update = true;
					dto.setSskestr(STATUS_C);
					dto.setSskests(STATUS_C);
				}
			} else if(dto.getFileType().trim().equalsIgnoreCase("CL")){
				if(!opSent
						|| !cuSent
						|| (!sgSent && esNecesarioSG)) {
					update = false;
				} else{
					update = true;
					dto.setSskestr(STATUS_C);
					dto.setSskests(STATUS_C);
				}
			} else if(dto.getFileType().trim().equalsIgnoreCase("SG")){
				if(!opSent
						|| !cuSent
						|| (!clSent && esNecesarioCL)) {
					update = false;
				} else{
					update = true;
					dto.setSskestr(STATUS_C);
					dto.setSskests(STATUS_C);
				}
			}
		}
		return update;
	}

	/**
	 * This method generates or updates a register in SSTRANS table
	 * @throws ServiceException
	 */
	private void registerTransaction() throws ServiceException {
		//Define and initialize
		Date today = new Date();

		//Load transaction DTO with data by default
		TransactionDTO tdto = this.buildTransactionDto();
		tdto.setStqtrs(serviceKey);

		//Load customized data
		tdto.setStkcnl(channel);
		tdto.setStkest(TransactionDTO.FILE_STATUS_ATRS);
		tdto.setStnarc(fmDto.getFileName());
		tdto.setStqadh(0L);
		tdto.setStunec("1"); //Cantidad de usuarios necesarios
		//Bandera para determinar si es nuevo esquema de firmantes
		//TODO: es necesario setear un usuario en STUGEN? Entiendo que no
		tdto.setStfgen(ServiceConstant.SDF_DATE_FOR_OUTPUT.format(today));
		tdto.setSthgen(ServiceConstant.SDF_HOUR.format(today));
		tdto.setStfau1(TransactionDTO.DATE_EMPTY);
		tdto.setSthau1(TransactionDTO.HOUR_EMPTY);
		tdto.setStfau2(TransactionDTO.DATE_EMPTY);
		tdto.setSthau2(TransactionDTO.HOUR_EMPTY);

		transactionBo.registerTransaction(tdto);
	}

	/**
	 * This method updates the SSTRANS register. Add to the register the table name
	 * where the file will be hosted 
	 * @throws ServiceException
	 */
	private void registerTableName() throws ServiceException {
		DefinedLogger.SERVICE.debug("Registrando nombre de tabla en SSTRANS...");
		TransactionDTO tdto = new TransactionDTO();
		tdto.setStnars(dto.getTable());
		tdto.setStqtrs(serviceKey);
		transactionBo.updateStnars(tdto);
	}

	/**
	 * This method loads the file content in main memory
	 */
	private void loadFileContent() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo contenido de archivo...");

		//Get file
		fmDto = manager.getEscrowFile(fmDto);

		//Validate extension 

		if(!fmDto.getFileName().substring(fmDto.getFileName().length() -4, fmDto.getFileName().length()).equalsIgnoreCase(".txt") && 
				!fmDto.getFileName().substring(fmDto.getFileName().length() -4, fmDto.getFileName().length()).equalsIgnoreCase(".zip") ){
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDEXTENSION);
		}

		//Load the file content as lines
		fmDto.setContenidoMap(fmDto.getContenidoMap().replace("\r", "")); //Fixing to avoid troubles with the line long
		dto.setFileLines(Arrays.asList(fmDto.getContenidoMap().split("\n")));
	}

	/**
	 * This method gets the library where persist the file content. The value is obtained by a global property
	 * of the application
	 * @throws ServiceException
	 */
	private void getLibrary() throws ServiceException {
		//Get library and table where we will persist the file content -----------------------------------------
		DefinedLogger.SERVICE.debug("Obteniendo biblioteca de las propiedades globales de la aplicacion...");
		dto.setLibrary(CashManagementWsApplication.getProperty(ServiceConstant.PROP_ESCROWSENDFILE_LIBRARY));

		//Validate the execution result
		if(StringUtil.isEmpty(dto.getLibrary())) {
			DefinedLogger.SERVICE.error("No se pudo hallar la biblioteca donde persistir el contenido de archivo");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFOUNDLIBRARY);
		}
		DefinedLogger.SERVICE.info(DefinedLogger.TABULATOR + "Biblioteca obtenida: " + dto.getLibrary());
	}

	/**
	 * This method generates the member according to the member name and escrow number given
	 * @throws ServiceException
	 */
	private void generateMember() throws ServiceException {
		DefinedLogger.SERVICE.info("Se ejecutara el SP SS0022P para generar el miembro acorde al alias...");
		bo.generateMember(dto);
	}

	/**
	 * This method obtains the table name according to the file name, file type and the escrow number.
	 * This data is the logical table name really
	 * @throws ServiceException
	 */
	private void getTable() throws ServiceException {
		DefinedLogger.SERVICE.info("Obteniendo nombre de la tabla");
		dto.setTable(bo.findTableName(dto));

		//Validate if we could get the logical name
		if(StringUtil.isEmpty(dto.getTable())) {
			DefinedLogger.SERVICE.error("No se pudo obtener el nombre del Logico que se utilizara como via de acceso para el miembro dentro del archivo correspondiente");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFOUNDLOGICALNAME);
		}

		//Complete the table name to be used
		dto.setTable(dto.getTable() + dto.getEscrowNumber());
	}

	/**
	 * This method persists the file in the data source.
	 * We insert the file content in the table.
	 * @throws ServiceException
	 */
	private void persistFile() throws ServiceException {
		//Persist file content -------------------------------------------------------------------------------------
		DefinedLogger.SERVICE.debug("Persistiendo contenido de archivo...");

		//Validate if the amount of lines is greater than the amount of row that an INSERT can generate 
		List<String> list = dto.getFileLines();
		if(list.size() <= rowNumberMax) {
			DefinedLogger.SERVICE.debug(DefinedLogger.TABULATOR + "El archivo sera persistido en su totalidad con un solo INSERT");
			dto.setFileLines(list);
			bo.insertFileContent(dto);
		} else {
			bo.insertByBatch(dto);
		}
		DefinedLogger.SERVICE.debug(DefinedLogger.TABULATOR + "Se persistio el archivo de forma exitosa.");
	}

	/**
	 * This method update the escrow status to AOKB in SSTRANS.
	 * With this action, the file is ready to be processed 
	 * @throws ServiceException
	 */
	private void registerStatus() throws ServiceException {
		DefinedLogger.SERVICE.info("Actualizando estado de fideicomiso en SSTRANS a AOKB...");

		//Create Transaction DTO to update the table
		TransactionDTO tdto = new TransactionDTO();
		tdto.setStqtrs(serviceKey);
		tdto.setStkest(TransactionDTO.FILE_STATUS_AOKB);

		//Updating SSTRANS...
		if(transactionBo.updateStkest(tdto) == 0) {
			DefinedLogger.SERVICE.warn("No se pudo actualizar el estado de fideicomiso a AOKB");
		}
	}

	/**
	 * This method updates the upload date according to the file type
	 * @throws ServiceException
	 */
	private void updateDate() throws ServiceException {
		DefinedLogger.SERVICE.info("Actualizando fecha de subida del archivo segun tipo de archivo...");
		if(bo.updateDate(dto) == 0) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTUPDATEDATE);
		}
	}

	/**
	 * This method reports that an escrow file was sent
	 * @throws ServiceException
	 */
	private void reportViaEmail() throws ServiceException {
		String subject = "Nuevo archivo de Fideicomiso recibido - Emp: " + dto.getEscrowNumber() + ". Tipo: " + dto.getFileType() + "." ;
		StringBuffer mail = new StringBuffer("Se ha recibido un nuevo archivo del fideicomiso ")
				.append(dto.getEscrowNumber())
				.append(".\nEl mismo es del tipo ")
				.append(dto.getFileType());

		//Validate if the user exists
		if(!StringUtil.isEmpty(user)) {
			mail.append(" y fue enviado por el usuario ")
			.append(user);
		} else {
			DefinedLogger.SERVICE.error("El usuario no fue informado en el campo DRQUS del request para notificarlo en el email");
		}

		mail.append(".\n\nFue ubicado en:\n")
		.append("                  - Libreria: ")
		.append(dto.getLibrary())
		.append("\n")
		.append("                  - Tabla:    " )
		.append(dto.getTable())
		.append("X\n")
		.append("                  - Miembro:   ")
		.append(dto.getMember())
		.append("\n\nEl nombre original dado por el cliente a dicho archivo fue: ")
		.append(fmDto.getFileName())
		.toString();
		MailManager enviarMail = new MailManager();
		enviarMail.postMail(subject, mail.toString());
	}

	/**
	 * THIS IS A WORKAROUND FOR THIS SERVICE BECAUSE OF IT IS NOT ADHERENT NUMBER IN THE REQUEST
	 * This method generates a transaction DTO with the data
	 * by default already loaded
	 * @return
	 * @throws ServiceException
	 */
	protected TransactionDTO buildTransactionDto() throws ServiceException {
		//Generate new transaction
		TransactionDTO dto = new TransactionDTO();

		//Service name --------------------------------------
		String serviceName = this.getClass().getSimpleName();
		if(serviceName != null && serviceName.length() > 60)
			dto.setServiceName(serviceName.substring(0, 59));
		else
			dto.setServiceName(serviceName);

		/*
		 * Invoked function --------------------------------------
		 * 
		 * Validate if the service implementation loads this attribute
		 * (this case should happen only if the service implementation
		 * has more than one option).
		 * If not, this attribute should be NULL always
		 */
		dto.setStktrs(invokedFunction);

		//Other fields --------------------------------------
		dto.setStkcnl(channel);
		dto.setStqadh(0);
		dto.setStidpr(processId);

		return dto;
	}

	public String getInvokedFunction() {
		return invokedFunction;
	}

	public void setInvokedFunction(String invokedFunction) {
		this.invokedFunction = invokedFunction;
	}

	public short getChannel() {
		return channel;
	}

	public void setChannel(short channel) {
		this.channel = channel;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getServiceKey() {
		return serviceKey;
	}

	public void setServiceKey(int serviceKey) {
		this.serviceKey = serviceKey;
	}

	public boolean isFinished() {
		return finished.get();
	}

	public EscrowSendFileDTO getDto() {
		return dto;
	}

	public void setDto(EscrowSendFileDTO dto) {
		this.dto = dto;
	}

	public FileManagerDTO getFmDto() {
		return fmDto;
	}

	public void setFmDto(FileManagerDTO fmDto) {
		this.fmDto = fmDto;
	}

	public String getEscrowStatus() {
		return escrowStatus;
	}

	public void setEscrowStatus(String escrowStatus) {
		this.escrowStatus = escrowStatus;
	}

	public String getExceptionKey() {
		return exceptionKey;
	}

	public void setExceptionKey(String exceptionKey) {
		this.exceptionKey = exceptionKey;
	}


}