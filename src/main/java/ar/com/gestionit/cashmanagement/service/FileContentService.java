package ar.com.gestionit.cashmanagement.service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.cache.ICache;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.persistence.bo.FileContentBO;
import ar.com.gestionit.cashmanagement.persistence.bo.ReferencesRecoveryBO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileContentDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PCFileListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PCFileListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.POFileListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.POFileListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.POFileSummaryDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PagerDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PagerInputDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RFFileListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RFFileListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ReferencesRecoveryDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.ClassUtil;
import ar.com.gestionit.cashmanagement.util.DateUtil;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;

/**
 * Service for "Obtener contenido de archivo"
 * @author jdigruttola
 */
public class FileContentService extends AbstractService {
	
	/**
	 * Constants
	 */
	private static final int BENEFICIARY_NUMBER_INI    = 575;
	private static final int BENEFICIARY_NUMBER_END    = 600;
	private static final String FILE_TYPE_PO = "PO";
	private static final String FILE_TYPE_RF = "RF";
	private static final String FILE_TYPE_PC = "PC";
	
	
	//PO format
	private static final int NRO_BENEFICIARIO_INI    = 575;
	private static final int NRO_BENEFICIARIO_END    = 600;
	private static final int NOMBRE_BENEFICIARIO_INI = 600;
	private static final int NOMBRE_BENEFICIARIO_END = 660;
	private static final int REFERENCIA_CLIENTE_INI  = 2;
	private static final int REFERENCIA_CLIENTE_END  = 27;
	private static final int MOTIVO_PAGO_INI         = 27;
	private static final int MOTIVO_PAGO_END         = 132;
	private static final int FECHA_PAGO_INI          = 132;
	private static final int FECHA_PAGO_END          = 140;
	private static final int MODO_EJEC_INI           = 140;
	private static final int MODO_EJEC_END           = 143;
	private static final int IMPORTE_PAGO_INI        = 143;
	private static final int IMPORTE_PAGO_END        = 158;
	private static final int FECHA_VENC_CHPD_INI     = 161;
	private static final int FECHA_VENC_CHPD_END     = 169;
	private static final int NRO_DOC_BENEF_INI       = 663;
	private static final int NRO_DOC_BENEF_END       = 674;
	private static final int CBU_INI              	 = 947;
	private static final int CBU_END              	 = 969;
	
	//RF format
	private static final int DEUDOR_REF_1_INI                = 2;
	private static final int DEUDOR_REF_1_END                = 27;
	private static final int DEUDOR_REF_2_INI                = 27;
	private static final int DEUDOR_REF_2_END                = 52;
	private static final int DEUDOR_APELL_NOMBRE_INI         = 52;
	private static final int DEUDOR_APELL_NOMBRE_END         = 112;
	private static final int COMPROBANTE_REF_1_INI           = 126;
	private static final int COMPROBANTE_REF_1_END           = 151;
	private static final int COMPROBANTE_REF_2_INI           = 151;
	private static final int COMPROBANTE_REF_2_END           = 176;
	private static final int FECHA_EMISION_INI               = 251;
	private static final int FECHA_EMISION_END               = 259;
	private static final int FECHA_VIGENCIA_INI              = 259;
	private static final int FECHA_VIGENCIA_END              = 267;
	private static final int FECHA_1ER_VTO_COMPROBANTE_INI   = 306;
	private static final int FECHA_1ER_VTO_COMPROBANTE_END   = 314;
	private static final int IMPORTE_1ER_VTO_COMPROBANTE_INI = 314;
	private static final int IMPORTE_1ER_VTO_COMPROBANTE_END = 329;
	private static final int FECHA_2DO_VTO_COMPROBANTE_INI   = 329;
	private static final int FECHA_2DO_VTO_COMPROBANTE_END   = 337;
	private static final int IMPORTE_2DO_VTO_COMPROBANTE_INI = 337;
	private static final int IMPORTE_2DO_VTO_COMPROBANTE_END = 352;
	private static final int IMPORTE_MINIMO_A_PAGAR_INI      = 375;
	private static final int IMPORTE_MINIMO_A_PAGAR_END      = 390;
	DecimalFormat numberFormat = new DecimalFormat("#0.00");
	
	private int POSITION_CONTENT_OUT = 0;

	/**
	 * File content business object
	 */
	private FileContentBO bo;
	
	/**
	 * Service DTO
	 */
	private FileContentDTO dto;
	
	/**
	 * Result to send as result
	 */
	private String xmlContentResult;
	
	/**
	 * File type to send as result
	 */
	private String fileType;
	
	/**
	 * NOTE: In this service, the pager is managed in this level (not by the BO pattern)
	 */
	private PagerInputDTO pagerInput;
	private PagerDTO pagerOutput;
	
	/**
	 * Cache for execution mode description. This evicts that we access
	 * to DB more than one time for each execution mode code
	 */
	private ExecutionModeCache cache;
	
	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new FileContentBO();
		dto = new FileContentDTO();
		cache = new ExecutionModeCache();
		pagerOutput = new PagerDTO();
		pagerOutput.setNext(ServiceConstant.FALSE);
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parámetros de entrada...");
		dto.setAdherent(getDataFromVector(1));
		dto.setFileId(getDataFromVector(2));
		
		if(StringUtil.isEmpty(dto.getFileId())) {
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFILENAME));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFILENAME);
		}
		ServiceUtil.validateLength(dto.getFileId(), 10, ServiceConstant.SERVICERETURN_KEY_INVALIDFILENAME);
		
		//Paging data
		try {
			pagerInput = (PagerInputDTO) bo.xmlToDto(getDataFromVector(3), PagerInputDTO.class);
		} catch(Exception e ) {
			DefinedLogger.SERVICE.warn("Paginado vacío...");
			pagerInput = null;
		}
	}

	@Override
	public void execute() throws ServiceException {
		/*
		 * File status and last invoked function
		 */
		executeFileData();
		
		/*
		 * File library
		 */
		executeFileLibrary();
		
		/*
		 * File content
		 */
		executeFileContent();
	}
	
	/**
	 * This method obtains the status and the last invoked function of the file
	 * @throws ServiceException
	 */
	private void executeFileData() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo estado y ultima invoked function del archivo...");
		/*
		 * File status and last invoked function
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		FileContentDTO aux = (FileContentDTO) bo.find(dto);
		
		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 */
		if(aux == null
				|| StringUtil.isEmpty(aux.getStatus())
				|| StringUtil.isEmpty(aux.getInvokedFunction())) {
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES);
		}
		dto.setStatus(aux.getStatus());
		dto.setInvokedFunction(aux.getInvokedFunction());
	}
	
	/**
	 * This method obtains the library where the file is allocated
	 * @throws ServiceException
	 */
	private void executeFileLibrary() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo librería donde se encuentra el archivo...");
		/*
		 * File library
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		FileContentDTO aux = bo.findLibrary(dto);
		
		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 */
		if(aux == null
				|| StringUtil.isEmpty(aux.getLibrary())) {
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES);
		}
		dto.setLibrary(aux.getLibrary());
	}
	
	/**
	 * This method obtains the beneficiary data
	 * In this method, we get the beneficiary name to use it if the data is not in the file
	 * @throws ServiceException
	 */
	private HashMap<String, String> executeBeneficiaryData() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo Datos del beneficiario...");
		HashMap<String, String> result = new HashMap<String, String>();
		
		/*
		 * Beneficiary data
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		dto.setBeneficiaryNumberIni(BENEFICIARY_NUMBER_INI);
		dto.setBeneficiaryNumberEnd(BENEFICIARY_NUMBER_END - BENEFICIARY_NUMBER_INI);
		List<FileContentDTO> beneficiaryNames = bo.findBeneficiaryData(dto);
		
		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 */
		if(beneficiaryNames != null && beneficiaryNames.size() > 0) {
			Iterator<FileContentDTO> i = beneficiaryNames.iterator();
			FileContentDTO item;
			while(i.hasNext()) {
				item = i.next();
				if(item.getSatBeneficiaryName() != null && item.getSatBeneficiaryNumber() != null)
					result.put(item.getSatBeneficiaryNumber().trim().toLowerCase(), item.getSatBeneficiaryName().trim());
			}
		}
		
		return result;
	}
	
	/**
	 * This method obtains the file content from the data source
	 * and processes it.
	 * @throws ServiceException
	 */
	private void executeFileContent() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo y procesando contenido...");
		/*
		 * File content
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		List<String> list = bo.findFile(dto);
		
		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 */
		if(list == null
				|| list.size() == 0) {
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_FILEEMPTY));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FILEEMPTY);
		}
		
		//Get file type
		fileType = list.get(0).substring(2, 4);
		
		if(StringUtil.isEmpty(fileType)) {
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_FILETYPENOTFOUND));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FILETYPENOTFOUND);
		}
		
		if(fileType.equalsIgnoreCase(FILE_TYPE_PO)) {
			formatPO(list);
			POSITION_CONTENT_OUT = 5;
		} else if(fileType.equalsIgnoreCase(FILE_TYPE_RF)) {
			formatRF(list);
			POSITION_CONTENT_OUT = 7;
		} else if(fileType.equalsIgnoreCase(FILE_TYPE_PC)) {
			formatPC(list);
			POSITION_CONTENT_OUT = 6;
		}
	}
	
	/**
	 * This method generates a PO list according to the argument given
	 * @param content
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	private void formatPO(List<String> content) throws ServiceException {
		DefinedLogger.SERVICE.debug("Generando lista de POs...");
		//Define and initialize variables
		String line;
		String aux;
		double totalAmount = 0;
		double auxDouble;
		POFileListItemDTO r;
		POFileListItemDTO exportDTO;
		POFileListDTO list = new POFileListDTO();
		POFileListDTO exportList = new POFileListDTO();
		POFileSummaryDTO s = new POFileSummaryDTO();
		POFileSummaryDTO exportSummary = new POFileSummaryDTO(); 
		double totalAmountLot = 0;
		String contentSize = "0";
		//totalAmountLot = content.get(content.size()-1).substring(2, 25).;
		if(content.size() > 1)
			totalAmountLot = Double.parseDouble(content.get(content.size()-1).substring(2, 25) + "." + content.get(content.size()-1).substring(25, 27)) ;
		
		//The first and last lines are useless for this format
		content.remove(content.size() - 1);
		if(content.size() > 0)
			content.remove(0);
		
		contentSize = String.valueOf(content.size());
		//Get beneficiary name from SAT (MTPROVEE table)
		HashMap<String, String> beneficiaryNameMap = executeBeneficiaryData();
		
		//Paginate the content
		content = (List<String>) paginate(content);
		
		//Process lines
		for(int i = 0; i < content.size(); i++) {
			line = content.get(i);
			r = new POFileListItemDTO();
			
			//Client reference
			r.setClientReference(line.substring(REFERENCIA_CLIENTE_INI, REFERENCIA_CLIENTE_END));

			//Beneficiary number
			r.setBeneficiaryNumber(dto.getSatBeneficiaryNumber());
			aux = line.substring(NRO_BENEFICIARIO_INI, NRO_BENEFICIARIO_END).trim();
			if (!StringUtil.isEmpty(aux))
				r.setBeneficiaryNumber(aux);
			
			//Beneficiary name
			r.setBeneficiaryName(dto.getSatBeneficiaryName());
			aux = line.substring(NOMBRE_BENEFICIARIO_INI, NOMBRE_BENEFICIARIO_END).trim();
			if (!StringUtil.isEmpty(aux))
				r.setBeneficiaryName(aux);
			else if (!StringUtil.isEmpty(r.getBeneficiaryNumber()) && beneficiaryNameMap.containsKey(r.getBeneficiaryNumber().toLowerCase()))
				r.setBeneficiaryName(beneficiaryNameMap.get(r.getBeneficiaryNumber()));
				
			
			//Beneficiary document
			r.setBeneficiaryDocument(line.substring(NRO_DOC_BENEF_INI, NRO_DOC_BENEF_END));
			
			//Payment amount and amount total
			aux = line.substring(IMPORTE_PAGO_INI, IMPORTE_PAGO_END);
			if(aux != null) {
				try {
					//Format
					aux =  aux.substring(0, aux.length()-2).replaceAll(" ", "0")
							+ "."
							+ aux.substring(aux.length()-2);
					auxDouble = Double.parseDouble(aux);
					r.setAmount(numberFormat.format(auxDouble).replace(",","."));
					
					//Update amount total
					totalAmount += auxDouble;
				} catch(Exception e) {
					r.setAmount("0");
				}
			} else {
				r.setAmount("0");
			}
			
			//Execution mode description
			aux = line.substring(MODO_EJEC_INI, MODO_EJEC_END);
			if( !StringUtil.isEmpty(aux) ) {
				r.setExecutionModeDescription(cache.getValue(aux));
			} else {
				r.setExecutionModeDescription("");
			}
			
			r.setPaymentDate(DateUtil.numericDateForOutput(line.substring(FECHA_PAGO_INI, FECHA_PAGO_END).trim()));
			r.setCheckExipirationDate(DateUtil.dateForOutput(line.substring(FECHA_VENC_CHPD_INI, FECHA_VENC_CHPD_END).trim()));
			r.setPaymentCause(line.substring(MOTIVO_PAGO_INI, MOTIVO_PAGO_END).trim());
			
			//CBU
			aux = line.substring(CBU_INI, CBU_END).trim();
			if(StringUtil.isEmpty(aux) || !NumberUtil.isNumeric(aux))
				r.setCbu("");
			else
				r.setCbu(aux);
			try{
				exportDTO = (POFileListItemDTO) ClassUtil.copyInstance(r);
				exportDTO.setAmount(NumberUtil.twoDecimalMax(Float.parseFloat(exportDTO.getAmount())));
				if (exportDTO.getBeneficiaryDocument() != null 	&& !exportDTO.getBeneficiaryDocument().equalsIgnoreCase("")	&& exportDTO.getBeneficiaryDocument().length() == 11){
					exportDTO.setBeneficiaryDocument(exportDTO.getBeneficiaryDocument().substring(0,2) + "-" + exportDTO.getBeneficiaryDocument().substring(2,10) + "-" + exportDTO.getBeneficiaryDocument().substring(10,11));
				}
				exportList.addItem(exportDTO);
			}catch(Exception e){
				e.printStackTrace();
			}			
			list.addItem(r);
		}
		
		//Generate the list summary
		/*
		s.setTotalAmount(numberFormat.format(totalAmount).replace(",","."));
		s.setRegisterNumber(String.valueOf(content.size()));
		*/
		s.setTotalAmount(String.valueOf(totalAmountLot));
		s.setRegisterNumber(contentSize);
		try{
			exportSummary = (POFileSummaryDTO) ClassUtil.copyInstance(s);
			exportSummary.setTotalAmount(NumberUtil.twoDecimalMax(Float.parseFloat(exportSummary.getTotalAmount())));
		}catch(Exception e){
			e.printStackTrace();
		}
		List<POFileSummaryDTO> summaryExportList = new ArrayList<POFileSummaryDTO>();
		summaryExportList.add(exportSummary);
		
		contentToExport.add(exportList.getList());
		contentToExport.add(summaryExportList);
		//Generate the result
		xmlContentResult = bo.dtoToXML(list) + bo.dtoToXML(s);
	}
	
	/**
	 * This method generates a RF list according to the argument given
	 * @param content
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	private void formatRF(List<String> content) throws ServiceException {
		DefinedLogger.SERVICE.debug("Generando lista de RFs...");
		//Define and initialize variables
		RFFileListDTO list = new RFFileListDTO();
		RFFileListDTO exportList = new RFFileListDTO();
		RFFileListItemDTO exportDTO;
		RFFileListItemDTO r;
		String line;
		int size;
		String aux;
		
		/*
		 * FIXME: Esto solo lo usa el exportador, por ende, habria que hacer una mejora para evitar
		 * que se ejecute cuando el servicio no sea llamado por el exportador
		 */
		HashMap<String, String> referenceName = getReferences(content);
		
		//Remove the first and last lines
		content.remove(content.size() - 1);
		if(content.size() > 0)
			content.remove(0);
		
		//Paginate the content
		content = (List<String>) paginate(content);
		
		size = content.size();
		for (int i = 0; i < size; i++) {
			line = content.get(i);
			r = new RFFileListItemDTO();
			
			//For the exporter: here, I load the column titles 
			loadReferenceTitles(r, referenceName);
			
			r.setDebtorRef1(line.substring(DEUDOR_REF_1_INI,DEUDOR_REF_1_END).trim());
			r.setDebtorRef2(line.substring(DEUDOR_REF_2_INI,DEUDOR_REF_2_END).trim());
			r.setDebtorName(line.substring(DEUDOR_APELL_NOMBRE_INI,DEUDOR_APELL_NOMBRE_END).trim());
			r.setProofRef1(line.substring(COMPROBANTE_REF_1_INI,COMPROBANTE_REF_1_END).trim());
			r.setProofRef2(line.substring(COMPROBANTE_REF_2_INI,COMPROBANTE_REF_2_END).trim());
			r.setEmisionDate(line.substring(FECHA_EMISION_INI,FECHA_EMISION_END).trim());
			r.setEffectiveDate(line.substring(FECHA_VIGENCIA_INI,FECHA_VIGENCIA_END).trim());
			
			
			//Expiration 1
			r.setExpirationDate1(line.substring(FECHA_1ER_VTO_COMPROBANTE_INI,FECHA_1ER_VTO_COMPROBANTE_END).trim());
			aux = line.substring(IMPORTE_1ER_VTO_COMPROBANTE_INI,IMPORTE_1ER_VTO_COMPROBANTE_END).trim();
			if(aux != null && !aux.equals("")) {
				aux = aux.substring(0, aux.length()-2) + "." + aux.substring(aux.length()-2);
				aux = NumberUtil.doubleFormat(aux);
			} else {
				aux = "0";
			}
			r.setExpirationAmount1(aux.replace(".","").replace(",","."));
			
			//Expiration 2
			r.setExpirationDate2(line.substring(FECHA_2DO_VTO_COMPROBANTE_INI, FECHA_2DO_VTO_COMPROBANTE_END).trim());
			aux = line.substring(IMPORTE_2DO_VTO_COMPROBANTE_INI, IMPORTE_2DO_VTO_COMPROBANTE_END).trim();
			if(aux != null && !aux.equals("")) {
				aux = aux.substring(0, aux.length()-2) + "." + aux.substring(aux.length()-2);
				aux = NumberUtil.doubleFormat(aux);
			} else {
				aux = "0";
			}
			r.setExpirationAmount2(aux.replace(".","").replace(",","."));
			
			//Minimum amount
			aux = line.substring(IMPORTE_MINIMO_A_PAGAR_INI, IMPORTE_MINIMO_A_PAGAR_END).trim();
			if(aux != null && !aux.equals("")) {
				aux = aux.substring(0, aux.length()-2) + "." + aux.substring(aux.length()-2);
				aux = NumberUtil.doubleFormat(aux);
			} else {
				aux = "0";
			}
			r.setMinAmount(aux.replace(".","").replace(",","."));
			try{
				exportDTO = (RFFileListItemDTO) ClassUtil.copyInstance(r);
				exportDTO.setMinAmount(NumberUtil.twoDecimalMax(Float.parseFloat(exportDTO.getMinAmount())));
				exportDTO.setExpirationAmount1(NumberUtil.twoDecimalMax(Float.parseFloat(exportDTO.getExpirationAmount1())));
				exportDTO.setExpirationAmount2(NumberUtil.twoDecimalMax(Float.parseFloat(exportDTO.getExpirationAmount2())));
				exportList.addItem(exportDTO);
			}catch(Exception e){
				e.printStackTrace();
			}
			list.addItem(r);				
		}
		contentToExport.add(exportList.getList());
		//Generate the result
		xmlContentResult = bo.dtoToXML(list);
	}
	
	/**
	 * This method generates a PC list according to the argument given
	 * @param content
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	private void formatPC(List<String> content) throws ServiceException {
		DefinedLogger.SERVICE.debug("Generando lista de PCs...");
		//Define and initialize variables
		PCFileListDTO list = new PCFileListDTO();
		List<PCFileListItemDTO> itemList = new ArrayList<PCFileListItemDTO>();
		String line;
		int size;
		PCFileListItemDTO dto = null;
		boolean flag = false;
		StringBuffer register = new StringBuffer();
		
		//Remove the first and last lines
		content.remove(content.size() - 1);
		if(content.size() > 0)
			content.remove(0);
		
		size = content.size();
		for (int i = 0; i < size; i++) {
			
			line = content.get(i);
			if (line.startsWith("H1")) {
				flag = true;
				continue;
			} 
			if (line.startsWith("T1")) {
				dto = new PCFileListItemDTO();
				flag = false;
				dto.setDetail(register.toString());
				register = new StringBuffer("");
			}
			if (flag) {
				register.append(line.substring(10) + "\r\n");
			}
			if(dto != null && dto.getDetail() != null){
				itemList.add(dto);
				dto = null;
			}
		}
		//Paginate the content
		itemList = (List<PCFileListItemDTO>) paginate(itemList);
		list.setList(itemList);
		contentToExport.add(list.getList());
		
		//Generate the result
		xmlContentResult = bo.dtoToXML(list);
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");
		setDataInVector(4, fileType);
		if(POSITION_CONTENT_OUT != 0){
			setDataInVector(POSITION_CONTENT_OUT, xmlContentResult);
		}
		
		setDataInVector(8, bo.dtoToXML(pagerOutput));
		
		reportQueryNumber(bo.getQueryCounter());
	}
	
	/**
	 * This method paginates the content according to the page data given
	 * by argument 
	 * @param content
	 * @return the content already paginated
	 */
	@SuppressWarnings("rawtypes")
	private List<?> paginate(List<?> content) {
		DefinedLogger.SERVICE.debug("Paginando...");
		//Validate if the list exists
		if(content == null)
			return null;
		
		/*
		 * Validate if PagerInput does not exists. In this case, the content is not
		 * paginated (the service processes all the registers)
		 */
		if(pagerInput == null)
			return content;
		
		/*
		 * Calculate the from and to of the page.
		 * - Note that these values contemplate the first element as 0 (not 1).
		 * - Also, the to value is increased by 1 to validate if there is next page
		 */
		int from = ((pagerInput.getPageNumber() - 1) * pagerInput.getAmount());
		int to = pagerInput.getPageNumber() * pagerInput.getAmount() + 1;
		
		/*
		 * If the from is greater than the list size, this page is empty
		 * because the registers of the list are in the previous pages
		 */
		if(content.size() < from)
			return new ArrayList();
		
		/*
		 * If the to is greater than the list size, the page has content
		 * but it is not full (in this case, the page is the last)
		 */
		if(content.size() < to)
			to = content.size();
		else {
			/*
			 * In this case, assuming the to is increased by 1, the next page
			 * exists and we subtract 1 to get the real to for this page
			 */
			//pagerOutput.setNext(Integer.parseInt(ServiceConstant.TRUE));
			pagerOutput.setNext((content.size() / pagerInput.getAmount())+1);
			to--;
		}
			
		return content.subList(from, to);
	}
	
	/**
	 * This cache keeps the execution mode description in main memory
	 * to evict to access to DB more than one time
	 * @author jdigruttola
	 */
	private class ExecutionModeCache implements ICache<String, String> {

		/**
		 * This map keeps the values already obtained from the data source
		 */
		private HashMap<String, String> map = new HashMap<String, String>();
		
		@Override
		public String getValue(String key) {
			if(!map.containsKey(key))
				map.put(key, getValueFromDataSource(key));
			return map.get(key);
		}
		
		/**
		 * This method accesses to the data source to get the
		 * value
		 * @param key
		 * @return
		 */
		private String getValueFromDataSource(String key) {
			try {
				return bo.findExecutionMode(key);
			} catch (ServiceException e) {
				return "";
			}
		}
	}
	
	/**
	 * This method returns a map with the reference configured by the adherent
	 * @param content
	 * @return
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	private HashMap<String, String> getReferences(List<String> content) throws ServiceException {
		HashMap<String, String> referenceName = new HashMap<String, String>();
		
		//Get agreement and sub-agreement from the file content
		String agreement = content.get(0).substring(46, 53);
		String subagreement = content.get(0).substring(61, 68);
		
		//Get the references configured by the adherent
		ReferencesRecoveryDTO referenceRecoveryDto = new ReferencesRecoveryDTO();
		referenceRecoveryDto.setMrecnv(agreement);
		referenceRecoveryDto.setMretbo(subagreement);
		ReferencesRecoveryBO referencesRecoveryBo = new ReferencesRecoveryBO();
		List<ReferencesRecoveryDTO> referenceList = (List<ReferencesRecoveryDTO>) referencesRecoveryBo.findList(referenceRecoveryDto);
		
		//Load the references in the map
		for(ReferencesRecoveryDTO dto : referenceList) {
			referenceName.put(dto.getMreccg(), dto.getMredcg());
		}
		return referenceName;
	}
	
	/**
	 * This method loads the reference names in the specified DTO for the column titles in the exporter
	 * @param r
	 * @param referenceName
	 */
	private void loadReferenceTitles(RFFileListItemDTO r, HashMap<String, String> referenceName) {
		if(referenceName.containsKey(ReferencesRecoveryDTO.REF_D1))
			r.setDebtorRef1Title(referenceName.get(ReferencesRecoveryDTO.REF_D1));
		if(referenceName.containsKey(ReferencesRecoveryDTO.REF_D2))
			r.setDebtorRef2Title(referenceName.get(ReferencesRecoveryDTO.REF_D2));
		
		boolean firstWasFound = false;
		if(referenceName.containsKey(ReferencesRecoveryDTO.REF_C1)) {
			r.setProofRef1Title(referenceName.get(ReferencesRecoveryDTO.REF_C1));
			firstWasFound = true;
		}
		if(referenceName.containsKey(ReferencesRecoveryDTO.REF_C2)) {
			if(!firstWasFound) {
				r.setProofRef1Title(referenceName.get(ReferencesRecoveryDTO.REF_C2));
				firstWasFound = true;
			} else {
				r.setProofRef2Title(referenceName.get(ReferencesRecoveryDTO.REF_C2));
			}
		}
		if(referenceName.containsKey(ReferencesRecoveryDTO.REF_C3)) {
			if(!firstWasFound) {
				r.setProofRef1Title(referenceName.get(ReferencesRecoveryDTO.REF_C3));
				firstWasFound = true;
			} else {
				r.setProofRef2Title(referenceName.get(ReferencesRecoveryDTO.REF_C3));
			}
		}
		if(referenceName.containsKey(ReferencesRecoveryDTO.REF_C4)) {
			if(!firstWasFound) {
				r.setProofRef1Title(referenceName.get(ReferencesRecoveryDTO.REF_C4));
				firstWasFound = true;
			} else {
				r.setProofRef2Title(referenceName.get(ReferencesRecoveryDTO.REF_C4));
			}
		}
		if(referenceName.containsKey(ReferencesRecoveryDTO.REF_C5)) {
			if(!firstWasFound) {
				r.setProofRef1Title(referenceName.get(ReferencesRecoveryDTO.REF_C5));
				firstWasFound = true;
			} else {
				r.setProofRef2Title(referenceName.get(ReferencesRecoveryDTO.REF_C5));
			}
		}
	}
}