package ar.com.gestionit.cashmanagement.service;

import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.RecoveredCheckDetailBO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckDetailListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckDetailStatusEventDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckDetailStatusListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckDetailStatusListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DateUtil;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

/** Detalle de recupero CPD  **/
public class RecoveredCheckDetailService extends AbstractService {
	
	private RecoveredCheckDetailStatusListItemDTO input;
	private RecoveredCheckDetailStatusListItemDTO recoveredCheckDetailDTO;
	private RecoveredCheckDetailBO bo;
	private String xmlStatusList;
	private String xmlCheqList;

	@Override
	protected void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando listas...");
		/*
		 * Executing query....
		 * ------------------------------------------------------------------------
		 */
		@SuppressWarnings("unchecked")
		List<RecoveredCheckDetailStatusListItemDTO> list = (List<RecoveredCheckDetailStatusListItemDTO>) bo.findList(input);
		RecoveredCheckDetailStatusListDTO statusList = new RecoveredCheckDetailStatusListDTO();
		
		if(list != null && list.size() > 0) {
			Iterator<RecoveredCheckDetailStatusListItemDTO> i = list.iterator();
			RecoveredCheckDetailStatusListItemDTO dto1;
			RecoveredCheckDetailStatusEventDTO event;
			RecoveredCheckDetailListItemDTO cheque;
			String prevStatus = "";
			while (i.hasNext()) {
				dto1 = i.next();
								
				//Validate if the register is an event or a new status
				if(!prevStatus.equals(dto1.getEstId())) {
					//This is a new status
					
					//Convert to XML the previous status (if it exists...)
					if(recoveredCheckDetailDTO != null) {
						statusList.setStatus(recoveredCheckDetailDTO);
					}
					
					//Create a new status
					recoveredCheckDetailDTO = dto1;
					
					//Update the variable of control
					prevStatus = recoveredCheckDetailDTO.getEstId();
				}
				
				//Create a new event
				event = new RecoveredCheckDetailStatusEventDTO();
				event.setHs(DateUtil.hourForOutput(dto1.getTmpHs()));
				event.setFechaEvento(DateUtil.dateForOutput(dto1.getTmpFechaEvento()));
				event.setDescEvento(dto1.getTmpDescEvento());
				
				
				
				//Load event in the status
				recoveredCheckDetailDTO.setEvent(event);
				
			}
			
			cheque = new RecoveredCheckDetailListItemDTO();
			cheque.setNroCheq(recoveredCheckDetailDTO.getNroCheq());
			cheque.setNroLote(input.getNroLote());
			cheque.setDocLibrador(recoveredCheckDetailDTO.getDocLibrador());
			cheque.setCodPostalCheq(recoveredCheckDetailDTO.getCodPostalCheq());
			cheque.setNroCtaGirada(recoveredCheckDetailDTO.getNroCtaGirada());
			cheque.setFecEnvio(recoveredCheckDetailDTO.getFecEnvio());
			cheque.setFecVenc(recoveredCheckDetailDTO.getFecVenc());
			cheque.setSucursalCheq(recoveredCheckDetailDTO.getSucursalCheq());
			cheque.setBcoCheq(recoveredCheckDetailDTO.getBcoCheq());
			cheque.setImpCheq(recoveredCheckDetailDTO.getImpCheq());
			cheque.setFecEntregaCli(recoveredCheckDetailDTO.getFecEntregaCli());
			cheque.setSucursalEnv(recoveredCheckDetailDTO.getSucursalEnv());
			cheque.setFecGenPedido(recoveredCheckDetailDTO.getFecGenPedido());
			cheque.setNomLibrador(recoveredCheckDetailDTO.getNomLibrador());
			cheque.setTipoDocLibrador(recoveredCheckDetailDTO.getTipoDocLibrador());
			
			recoveredCheckDetailDTO.setCheq(cheque);
			
			
			statusList.setStatus(recoveredCheckDetailDTO);		
			xmlCheqList = bo.dtoToXML(cheque);
		}else{
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
		xmlStatusList = bo.dtoToXML(statusList);
		
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		String a = getDataFromVector(4);
		a = ServiceUtil.validateNumberAndLengthObligatory(a, 10, ServiceConstant.SERVICERETURN_KEY_INVALIDBATCHID, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYBATCHID);
		input.setNroLote(a);
		
		String b = getDataFromVector(5);
		b = ServiceUtil.validateNumberAndLengthObligatory(b, 10, ServiceConstant.SERVICERETURN_KEY_INVALIDNROCHEQXSAT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYCHECKNUMBER);
		input.setNroCheq(b);
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");
		if(recoveredCheckDetailDTO == null)
			   recoveredCheckDetailDTO = new RecoveredCheckDetailStatusListItemDTO();
		
		setDataInVector(6, xmlStatusList);
		setDataInVector(7, xmlCheqList);
				
		reportQueryNumber(bo.getQueryCounter());
	}

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		input = new RecoveredCheckDetailStatusListItemDTO();
		bo = new RecoveredCheckDetailBO();
	}
}
