package ar.com.gestionit.cashmanagement.service;

import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.CompanyListBO;
import ar.com.gestionit.cashmanagement.persistence.dto.CompanyListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.CompanyListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class CompanyListService extends AbstractService{

	CompanyListBO bo;
	CompanyListDTO dto;
	
	@Override
	protected void initialize() throws ServiceException {
		bo = new CompanyListBO();
		dto = new CompanyListDTO();
	}
	@Override
	protected void loadInputs() throws ServiceException {
		String cuit = getDataFromVector(2);
		cuit = ServiceUtil.validateNumberAndLengthObligatory(cuit, 11, ServiceConstant.SERVICERETURN_KEY_INVALIDCUIT , ServiceConstant.SERVICERETURN_KEY_OBLIGATORYCUIT);
		dto.setCuit(cuit);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void execute() throws ServiceException {
		dto.setList((List<CompanyListItemDTO>) bo.findList(dto));
	}
	
	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(3, bo.dtoToXML(dto));
		
	}

}
