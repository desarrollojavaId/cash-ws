package ar.com.gestionit.cashmanagement.service;



import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.CashAccountsListBO;
import ar.com.gestionit.cashmanagement.persistence.bo.GeneralAccountBO;
import ar.com.gestionit.cashmanagement.persistence.dto.CashAccountsListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.CashAccountsListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public class CashAccountsListService extends AbstractAccountManagerService {
	
	private static final String TRUE = "TRUE";
	
	/**
	 * This attribute is used to load the input parameters
	 */
	private CashAccountsListItemDTO input;

	/**
	 * Business object for this service
	 */
	private CashAccountsListBO bo;

	/**
	 * This is the payment list result to send
	 */
	private String xmlCashAccountsList;

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new CashAccountsListBO();
		input = new CashAccountsListItemDTO();
		accountBO = new GeneralAccountBO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parámetros de entrada...");
		input.setAdherent(getDataFromVector(1));	
		DefinedLogger.SERVICE.debug("Validaciones de adherente...");
		//Validate if the adherent is right
		if(input.getAdherent() == null
				|| input.getAdherent().trim().equals("")
				|| !NumberUtil.isNumeric(input.getAdherent())) {
			DefinedLogger.SERVICE.error("Falló el adherente, el mismo no fue ingresado o tiene formato incorrecto");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando listas...");
		CashAccountsListDTO cashAccountsList = new CashAccountsListDTO();
		cashAccountsList.setList((List<CashAccountsListItemDTO>) bo.findList(input));
		if (cashAccountsList == null || cashAccountsList.getList().size() == 0) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}

		//This method does validations and loads the internal data account if this is empty
		validateResult(cashAccountsList.getList(), input.getAdherent());
		
		//Identify which account is by default in the list
		indicateAccountByDefault(cashAccountsList.getList(), input.getAdherent());

		contentToExport.add(cashAccountsList.getList());
		xmlCashAccountsList = bo.dtoToXML(cashAccountsList);
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");
		setDataInVector(2, xmlCashAccountsList);

		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
	
	/**
	 * This method indicates which is the account by default in the list given by argument
	 * @param list
	 * @param adherent
	 * @throws ServiceException
	 */
	public void indicateAccountByDefault(List<CashAccountsListItemDTO> list, String adherent) throws ServiceException {
		//Validate if the arguments are valid
		if(list == null || list.size() == 0 || StringUtil.isEmpty(adherent))
			return;
		
		//Get the account by default
		GeneralAccountBO accountBo = new GeneralAccountBO();
		String accountByDefault = accountBo.findDefaultCBU(adherent);
		
		//Iterate accounts
		for(CashAccountsListItemDTO item : list) {
			//Validate if this is the account by default
			if(accountByDefault.trim().equals(item.getCbu().trim())) {
				item.setIsDefault(TRUE);
				break;
			}
		}
	}

}