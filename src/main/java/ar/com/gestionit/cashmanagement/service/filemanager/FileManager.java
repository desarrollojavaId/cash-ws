package ar.com.gestionit.cashmanagement.service.filemanager;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.EncoderUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;
import ar.com.gestionit.cashmanagement.util.ZIPUtil;

/**
 * This class is used to manage files.
 * We can get or persist files by this class in the specified data source.
 * @author jdigruttola
 */
public class FileManager {

	/**
	 * This method persists the file in the repository
	 * @param fileId
	 * @param file
	 * @return the data from 
	 */
//	public int persistFile(HashMap<String, byte[]> dto) throws ServiceException {
//		try {
//			SqlSession session = CashManagementWsApplication.fileManagerSessionFactory.openSession(true);
//			FileManagerMapper mapper = session.getMapper(FileManagerMapper.class);
//			int result = mapper.insert(dto);
//			session.commit();
//			session.close();
//			return result;
//		} catch(Exception e) {
//			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
//		}
//	}
	
	/**
	 * This method updated the file in the repository
	 * @param fileId
	 * @param file
	 * @return the data from 
	 */
	public int updateFile(FileManagerDTO dto) throws ServiceException {
		try {
			
			dto.setTokenId(fixNewlineForSQLServer(dto.getTokenId()));
			
			SqlSession session = CashManagementWsApplication.fileManagerSessionFactory.openSession(true);
			FileManagerMapper mapper = session.getMapper(FileManagerMapper.class);
			int result = mapper.update(dto);
			session.commit();
			session.close();
			
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		}
	}
	
	/**
	 * This method updated the file in the repository
	 * @param fileId
	 * @param file
	 * @return the data from 
	 */
	public FileManagerDTO getFile(FileManagerDTO dto) throws ServiceException {
		try {
			String token = dto.getTokenId();
			dto.setTokenId(fixNewlineForSQLServer(dto.getTokenId()));
			SqlSession session = CashManagementWsApplication.fileManagerSessionFactory.openSession(true);
			FileManagerMapper mapper = session.getMapper(FileManagerMapper.class);
			dto = mapper.getFile(dto);
			session.commit();
			session.close();
			
			if(dto == null)
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFOUNDFILEBYTOKEN);
			
			dto.setTokenId(token);
			
			//Convert from binary (hexa) to string
			if(dto.getContenidoMap() != null) {
				dto.setContenidoMap(EncoderUtil.hexToString(dto.getContenidoMap()));
			}
			
			return dto;
		} catch(ServiceException e) {
			throw e;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		}
	}
	
	/**
	 * FIXME: This method should be merged with getFile method but today there is not time
	 * This method updated the file in the repository
	 * @param fileId
	 * @param file
	 * @return the data from 
	 */
	public FileManagerDTO getEscrowFile(FileManagerDTO dto) throws ServiceException {
		try {
			String token = dto.getTokenId();
			dto.setTokenId(fixNewlineForSQLServer(dto.getTokenId()));
			
			DefinedLogger.SERVICE.info(DefinedLogger.TABULATOR + "Obteniendo archivo segun token " + token + "...");
			SqlSession session = CashManagementWsApplication.fileManagerSessionFactory.openSession(true);
			FileManagerMapper mapper = session.getMapper(FileManagerMapper.class);
			dto = mapper.getEscrowFile(dto);
			session.commit();
			session.close();
			DefinedLogger.SERVICE.info(DefinedLogger.TABULATOR + "Archivo obtenido para token " + token);
			
			//Validate if the file could be obtained
			if(dto == null)
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFOUNDFILEBYTOKEN);
			
			//Validate if the file name could be obtained
			if(StringUtil.isEmpty(dto.getFileName())) {
				DefinedLogger.SERVICE.error("No se pudo obtener el nombre del archivo.");
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFOUNDFILENAME);
			}
			
			//Persist file in the filesystem to do tests
//			FileOutputStream fos = new FileOutputStream("C:\\javi\\" + dto.getFileName());
//			fos.write(dto.getFileContent());
//			fos.close();
			
			// Validate if the file is zipped. In this case, we must uncompress the file
			if(ZIPUtil.isZipFile(dto.getFileName())) {
				// So, we must uncompress the file and get the file content sent
				try {
					dto.setContenidoMap(ZIPUtil.uncompress(dto.getFileContent()));
				} catch(IllegalArgumentException e) {
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDCOMPRESSEDFILENAME, e);
				} catch (ServiceException e) {
					throw e;
				} catch (Exception e) {
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FAILEDUNZIPFILE, e);
				}
			} else {
				//Convert from binary (hexa) to string
				if(dto.getFileContent() != null) {
					dto.setContenidoMap(new String(dto.getFileContent()));
					dto.setFileContent(null);
//					dto.setContenidoMap(EncoderUtil.hexToString(new String(dto.getFileContent())));
				}
			}
			
			//Fixing...
			dto.setTokenId(token);
			
			return dto;
		} catch(ServiceException e) {
			throw e;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		}
	}
	
	/**
	 * This method fixes the problem to persist the newline character in SQL Server
	 * by MyBatis 
	 * @param obj
	 * @return
	 */
	private static String fixNewlineForSQLServer(String obj) {
		if(obj == null)
			return null;
		return obj
				//.replace("\r", "' + CHAR(13) + '")
				.replace("\n", "' + CHAR(10) + '")
				//.replace("\\r", "' + CHAR(13) + '")
				.replace("\\n", "' + CHAR(10) + '");
	}
	

}