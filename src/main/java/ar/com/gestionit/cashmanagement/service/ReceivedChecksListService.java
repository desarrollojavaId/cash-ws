package ar.com.gestionit.cashmanagement.service;

import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.ReceivedChecksListBO;
import ar.com.gestionit.cashmanagement.persistence.bo.ReferencesRecoveryBO;
import ar.com.gestionit.cashmanagement.persistence.dto.ReceivedChecksListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ReceivedChecksListInputDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ReceivedChecksListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ReferencesRecoveryDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;


public class ReceivedChecksListService extends AbstractService {

	private ReceivedChecksListDTO dto;
	private ReceivedChecksListBO bo;
	private ReceivedChecksListInputDTO listaEntrada;
	private final static String TIPO1 = "Al día";
	private final static String TIPO2 = "A futuro";

	/**
	 * This attribute is used only for the file exporter 
	 * This contains a list of the dynamic titles
	 */
	List<ReferencesRecoveryDTO> referenceList = null;

	@Override
	protected void initialize() throws ServiceException {
		bo = new ReceivedChecksListBO();	
		dto = new ReceivedChecksListDTO();
		listaEntrada = new ReceivedChecksListInputDTO();

	}

	@Override
	protected void loadInputs() throws ServiceException {

		listaEntrada = (ReceivedChecksListInputDTO)bo.xmlToDto(getDataFromVector(4), ReceivedChecksListInputDTO.class);

		if(listaEntrada == null) {
			DefinedLogger.SERVICE.error(ServiceConstant.SERVICERETURN_KEY_EMPTYENTRYSTRUCTURE);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_EMPTYENTRYSTRUCTURE);
		}

		String convenio = getDataFromVector(2);
		String subconvenio = getDataFromVector(3);
		String tipoCheque = listaEntrada.getTipocheque();
		String numCheque = listaEntrada.getNumcheque();
		String fechaAcredDesde = listaEntrada.getFechaacreddesde();
		String fechaAcredHasta = listaEntrada.getFechaacredhasta();
		String fechaRecDesde = listaEntrada.getFecharecdesde();
		String fechaRecHasta = listaEntrada.getFecharechasta();
		String ref1deud = listaEntrada.getRef1deudor();
		String ref2deud = listaEntrada.getRef2deudor();
		String ref1comp = listaEntrada.getRef1comp();
		String ref2comp = listaEntrada.getRef2comp();
		String ref3comp = listaEntrada.getRef3comp();
		String ref4comp = listaEntrada.getRef4comp();
		String ref5comp = listaEntrada.getRef5comp();
		String numrecaud = listaEntrada.getNumrecaud();

		convenio = ServiceUtil.validateNumberAndLengthObligatory(convenio, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYAGREEMENT);
		listaEntrada.setNroconvenio(convenio);				

		subconvenio = ServiceUtil.validateNumberAndLengthObligatory(subconvenio, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDSUBAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBAGREEMENT);
		listaEntrada.setNrosubconv(subconvenio);

		ServiceUtil.validateNumberAndLengthObligatory(tipoCheque, 1, ServiceConstant.SERVICERETURN_KEY_INVALIDCHEQTYPE, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYCHEQTYPE);
		listaEntrada.setNumcheque(ServiceUtil.validateNumberAndLength(numCheque, 10, ServiceConstant.SERVICERETURN_KEY_NOTVALIDCHECKNUMBER));


		listaEntrada.setFechaacreddesde(ServiceUtil.validateDateForDepuration(fechaAcredDesde, depurationDate, ServiceConstant.SERVICERETURN_KEY_NOTDATEFROM));
		listaEntrada.setFechaacredhasta(ServiceUtil.validateDate(fechaAcredHasta, ServiceConstant.SERVICERETURN_KEY_NOTDATETO));
		ServiceUtil.validateDateInterval(fechaAcredDesde, fechaAcredHasta, ServiceConstant.SDF_AMERICAN_DATE);

		listaEntrada.setFecharecdesde(ServiceUtil.validateDateForDepuration(fechaRecDesde, depurationDate, ServiceConstant.SERVICERETURN_KEY_NOTDATEFROM));
		listaEntrada.setFecharechasta(ServiceUtil.validateDate(fechaRecHasta, ServiceConstant.SERVICERETURN_KEY_NOTDATETO));
		ServiceUtil.validateDateInterval(fechaRecDesde, fechaRecHasta,ServiceConstant.SDF_AMERICAN_DATE);		

		listaEntrada.setRef1deudor(ServiceUtil.validateLength(ref1deud, 25, ServiceConstant.SERVICERETURN_KEY_NOTVALIDREFERENCE));
		listaEntrada.setRef2deudor(ServiceUtil.validateLength(ref2deud, 25, ServiceConstant.SERVICERETURN_KEY_NOTVALIDREFERENCE));
		listaEntrada.setRef1comp(ServiceUtil.validateLength(ref1comp, 25, ServiceConstant.SERVICERETURN_KEY_NOTVALIDREFERENCE));
		listaEntrada.setRef2comp(ServiceUtil.validateLength(ref2comp, 25, ServiceConstant.SERVICERETURN_KEY_NOTVALIDREFERENCE));
		listaEntrada.setRef3comp(ServiceUtil.validateLength(ref3comp, 25, ServiceConstant.SERVICERETURN_KEY_NOTVALIDREFERENCE));
		listaEntrada.setRef4comp(ServiceUtil.validateLength(ref4comp, 25, ServiceConstant.SERVICERETURN_KEY_NOTVALIDREFERENCE));
		listaEntrada.setRef5comp(ServiceUtil.validateLength(ref5comp, 25, ServiceConstant.SERVICERETURN_KEY_NOTVALIDREFERENCE));

		listaEntrada.setNumrecaud(ServiceUtil.validateNumberAndLength(numrecaud, 8, ServiceConstant.SERVICERETURN_KEY_INVALIDNUMRECAUD));

		if (!tipoCheque.equals("1") && !tipoCheque.equals("2") && !tipoCheque.equals("3")){
			throw new ServiceException("Coloque un tipo de cheque válido (1, 2 o 3)");
		}

		bo.enablePager(getDataFromVector(5));
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void execute() throws ServiceException {
		listaEntrada.setFechaacreddesde("0");
		listaEntrada.setFecharecdesde("0");
		listaEntrada.setRef1deudor("0");
		dto.setList((List<ReceivedChecksListItemDTO>) bo.findList(listaEntrada));
		if(dto != null && dto.getList() != null && dto.getList().size() > 0) {
			Iterator<ReceivedChecksListItemDTO> i = dto.getList().iterator();
			ReceivedChecksListItemDTO item;
			while(i.hasNext()) {
				item = i.next();
				if (item.getFormacobro().equals("12") || item.getFormacobro().equals("13")){
					item.setTipo("2");
					item.setTipoExporter(TIPO1);
				}else{
					item.setTipo("1");
					item.setTipoExporter(TIPO2);
				}
				item.setImporteExporter(NumberUtil.amountFormatExporter(item.getImporte()));

				//Assign dynamic titles ONLY if the service was called by the file exporter
				//if(super.isFileExporterActive) {
				if(1 == 1) {
					loadDynamicTitle(item);
				}
			}

			contentToExport.add(dto.getList());
		}else{
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
	}
	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(6, bo.dtoToXML(dto));
		setDataInVector(8, bo.getPagerAsXML());

	}

	/**
	 * This method is only used for the file exporter
	 * @return the dynamic title list.
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	private List<ReferencesRecoveryDTO> loadTitleList() throws ServiceException {
		ReferencesRecoveryDTO referenceRecoveryDto = new ReferencesRecoveryDTO();
		referenceRecoveryDto.setMrecnv(listaEntrada.getNroconvenio());
		referenceRecoveryDto.setMretbo(listaEntrada.getNrosubconv());
		ReferencesRecoveryBO referencesRecoveryBo = new ReferencesRecoveryBO();
		return (List<ReferencesRecoveryDTO>) referencesRecoveryBo.findList(referenceRecoveryDto);
	}

	/**
	 * This method loads the dynamic titles in the specified DTO given by argument
	 * This is used only the file exporter 
	 * @param dto
	 * @throws ServiceException 
	 */
	private void loadDynamicTitle(ReceivedChecksListItemDTO dto) throws ServiceException {
		//Validate if the title list was already loaded. If not we must load it
		if(referenceList == null) {
			referenceList = loadTitleList();
		}

		//Loading dynamic titles...
		for(ReferencesRecoveryDTO ref : referenceList) {
			if(ref.getMreccg() != null && ref.getMreccg().trim().equals("1")) {
				dto.setRef1deudorTitle(ref.getMredcg());
			} else if(ref.getMreccg() != null && ref.getMreccg().trim().equals("2")) {
				dto.setRef2deudorTitle(ref.getMredcg());
			} else if(ref.getMreccg() != null && ref.getMreccg().trim().equals("11")) {
				dto.setRef1compTitle(ref.getMredcg());
			} else if(ref.getMreccg() != null && ref.getMreccg().trim().equals("12")) {
				dto.setRef2compTitle(ref.getMredcg());
			} else if(ref.getMreccg() != null && ref.getMreccg().trim().equals("13")) {
				dto.setRef3compTitle(ref.getMredcg());
			} else if(ref.getMreccg() != null && ref.getMreccg().trim().equals("14")) {
				dto.setRef4compTitle(ref.getMredcg());
			} else if(ref.getMreccg() != null && ref.getMreccg().trim().equals("15")) {
				dto.setRef5compTitle(ref.getMredcg());
			}
		}
	}
}
