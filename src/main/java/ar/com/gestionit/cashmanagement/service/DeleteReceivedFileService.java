package ar.com.gestionit.cashmanagement.service;

import java.util.Iterator;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.persistence.bo.DeleteReceivedFileBO;
import ar.com.gestionit.cashmanagement.persistence.dto.DeleteReceivedFileListDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.service.util.ServiceReturn;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class DeleteReceivedFileService extends AbstractService {
	/**
	 * This attribute is used to load the input parameters
	 */
	private DeleteReceivedFileListDTO input;

	/**
	 * Business object for this service
	 */
	private DeleteReceivedFileBO bo;

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new DeleteReceivedFileBO();
		input = new DeleteReceivedFileListDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		DeleteReceivedFileListDTO ids = (DeleteReceivedFileListDTO)
				bo.xmlToDto(getDataFromVector(5), DeleteReceivedFileListDTO.class);
		if(ids != null && ids.getIdArchivo() != null && ids.getIdArchivo().size() > 0) {
			input.setIdArchivo(ids.getIdArchivo());
			Iterator<String> i = ids.getIdArchivo().iterator();
			String item;
			while(i.hasNext()) {
				item = i.next();
				ServiceUtil.validateLength(item, 10, ServiceConstant.SERVICERETURN_KEY_NOTFILELIST);		
			}
			
		}else{
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFILENAME));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFILENAME);
		}
	}


	@Override
	public void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Eliminando archivos...");
		bo.update(input);
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");
		
		setDataInVector(6, String.valueOf(serviceKey));
		
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());

	}
	
	@Override
	protected void reportResponse(ServiceReturn r, int level) {
		super.reportResponse(r, level);
		if(r == null || r.getDescription() == null)
			return;
		setDataInVector(2, r.getDescription());
	}
}