package ar.com.gestionit.cashmanagement.service.filemanager;

public interface FileManagerMapper {
	public FileManagerDTO find(FileManagerDTO dto);
	public int insert(FileManagerDTO dto);
	public int update(FileManagerDTO dto);
	public FileManagerDTO getFile(FileManagerDTO dto);
	public FileManagerDTO getEscrowFile(FileManagerDTO dto);
}