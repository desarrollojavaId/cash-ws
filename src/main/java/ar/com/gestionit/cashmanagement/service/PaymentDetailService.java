package ar.com.gestionit.cashmanagement.service;

import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.PaymentDetailBO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentErrorListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentErrorListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ProofListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ProofListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StatusEventDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StatusListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StatusListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DateUtil;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;
import ar.com.gestionit.cashmanagement.util.XMLUtil;

public class PaymentDetailService extends AbstractService {

	/**
	 * This attribute is used to load the input parameters
	 */
	private PaymentDTO input;

	/**
	 * Business object for this service
	 */
	private PaymentDetailBO bo;

	/**
	 * This is the status list result to send
	 */
	private String xmlStatusList;
	
	/**
	 * This is the proof list result to send
	 */
	private String xmlProofList;
	
	/**
	 * This is the payment detail result
	 */
	private PaymentDTO paymentDetail;
	
	/**
	 * This is the payment detail as structure
	 */
	private String xmlPaymentStructure;
	
	/**
	 * This is the status list result to send
	 */
	private String xmlErrorsList;

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new PaymentDetailBO();
		input = new PaymentDTO();
		xmlPaymentStructure = "";
		
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		String c = getDataFromVector(1);
		c = ServiceUtil.validateNumberAndLengthObligatory(c, 7, ServiceConstant.SERVICERETURN_KEY_ADHERENTLENGTHERROR, ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);
		input.setAdherent(c);
		
		String a = getDataFromVector(2);
		a = ServiceUtil.validateNumberAndLengthObligatory(a, 8, ServiceConstant.SERVICERETURN_KEY_INVALIDPAYORDER, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYPAYORDER);
		input.setPaymentNumber(a);
		
		String b = getDataFromVector(3);
		b = ServiceUtil.validateNumberAndLengthObligatory(b, 3, ServiceConstant.SERVICERETURN_KEY_INVALIDSUBPAYORDER, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBPAYORDER);
		input.setPaymentSubnumber(b);
	}

	@Override
	public void execute() throws ServiceException {
		/*
		 * Payment detail
		 */
		executePaymentDetail();
		
		/*
		 * Statuses
		 */
		executeStatusList();
		
		/*
		 * Beneficiary email
		 */
		executeBeneficiaryEmail();
		
		/*
		 * Proof list
		 */
		executeProofList();
		
		/*
		 * Proof dates
		 */
		executeProofDates();
		
		/*
		 * Get Payment detail again as structure similar of the PaymentListService
		 * to send in the response
		 */
		executePaymentStructure();
		
		/*
		 * Errors
		 */
		executeErrorsList();
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando lista de outputs...");
		setDataInVector(4, paymentDetail.getMtmcbu());
		setDataInVector(5, paymentDetail.getMail());
		setDataInVector(6, paymentDetail.getMtlinv());
		setDataInVector(7, paymentDetail.getDec());
		setDataInVector(8, paymentDetail.getMtlcin());
		setDataInVector(9, paymentDetail.getMtlrrp());
		setDataInVector(10, paymentDetail.getMtlaco());
		setDataInVector(11, xmlProofList);
		setDataInVector(12, xmlStatusList);
		setDataInVector(13, paymentDetail.getMtafec());
		setDataInVector(14, paymentDetail.getMtafim());
		setDataInVector(15, xmlPaymentStructure);
		setDataInVector(16, xmlErrorsList);
		
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
	
	/**
	 * This method processes the payment status 
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	private void executeStatusList() throws ServiceException {
		DefinedLogger.SERVICE.debug("Procesando estados de OP...");
		/*
		 * Status list
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		List<StatusListItemDTO> list = (List<StatusListItemDTO>) bo.findList(input);

		/*	
		 * processing information....
		 */
		DefinedLogger.SERVICE.debug("Validando eventos y estados...");
		StatusListDTO statusList = new StatusListDTO();
		if(list != null && list.size() > 0) {
			Iterator<StatusListItemDTO> i = list.iterator();
			/*
			 * El query devuelve una lista de registros. Cada uno contiene un estado y un evento.
			 * Un estado puede tener N eventos. Por ende, hay que validar si cada registro presenta
			 * solo un nuevo evento o tambien un nuevo estado.
			 */
			StatusListItemDTO status = null;
			StatusListItemDTO dto;
			StatusEventDTO event;
			String prevStatusId = "";
			while (i.hasNext()) {
				dto = i.next();
			
				//Unifico el contenido de Id y SubId separados por un guion
				dto.setEstn(dto.getEstn() + "-" + dto.getEstsn());
				
				//Validate if the register is an event or a new status
				if(!prevStatusId.equals(dto.getEstn())) {
					//This is a new status
					
					//Convert to XML the previous status (if it exists...)
					if(status != null) {
						statusList.setStatus(status);
					}
					
					//Create a new status02
					status = dto;
					
					//Update the variable of control
					prevStatusId = dto.getEstn();
				}
				
				//Create a new event
				event = new StatusEventDTO();
				event.setDate(dto.getMtqhmv());
				event.setDescription(dto.getKev());
				
				//Load event in the status
				status.setEvent(event);
			}
			statusList.setStatus(status);
		
		}
		xmlStatusList = bo.dtoToXML(statusList);
	}
	
	/**
	 * This method processes the payment detail
	 * @throws ServiceException
	 */
	private void executePaymentDetail() throws ServiceException {
		DefinedLogger.SERVICE.debug("Procesando detalle de OP...");
		/*
		 * Payment details
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		paymentDetail = (PaymentDTO)bo.find(input);
		
		/*	
		 * processing information....
		 */
		//Validate if the payment detail exists according to the data given
		if(paymentDetail == null)
			paymentDetail = new PaymentDTO();
		
		//Name
		if(paymentDetail.getOppronom() == null || paymentDetail.getOppronom().equals("")) {
			paymentDetail.setOppronom(paymentDetail.getProveenom());
		}
		
		//Communication via
		if(paymentDetail.getDec() != null
				&& paymentDetail.getDec().equals(ServiceConstant.LABEL_COMMVIA)) {
			paymentDetail.setDec(ServiceConstant.LABEL_COMMVIA_EXT);
		}
	}
	
	/**
	 * This method processes the beneficiary email
	 * @throws ServiceException
	 */
	private void executeBeneficiaryEmail() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obtreniendo mail del beneficiario...");
		/*
		 * Beneficiary Email
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		PaymentDTO output = bo.findMail(input);
		
		/*
		 * Processing information...
		 */
		if(output != null && output.getMail() != null && !output.getMail().trim().equals("")) {
			paymentDetail.setMail(output.getMail());
		} else {
			output = bo.findNoMail(input);
			if(output != null)
				paymentDetail.setMail(output.getMail());
		}
	}
	
	/**
	 * This method processes the proof list
	 * @throws ServiceException
	 */
	private void executeProofList() throws ServiceException {
		DefinedLogger.SERVICE.debug("Lista de comprobantes...");
		/*
		 * Proof list
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		//Define and initialize variables
		ProofListItemDTO proofDto = new ProofListItemDTO();
		
		//Get customer number and reference number. These are used to get the proof list
		proofDto.setCustomerNumber(paymentDetail.getMtlqca());
		proofDto.setReferenceNumber(paymentDetail.getMtlqrc());
		
		//Query execution
		ProofListDTO proofList = new ProofListDTO();
		List<ProofListItemDTO> list = bo.findProof(proofDto);
		
		/*
		 * Processing information...
		 */
		/*
		 * Pongo un numero autoincremental que van a usar desde el frontend para
		 * enlazar las listas
		 */
		if(list != null && list.size() > 0)
			for(int i = 0; i < list.size(); i++)
				list.get(i).setPid(i + 1);
		proofList.setList(list);
		xmlProofList = bo.dtoToXML(proofList);
	}
	
	/**
	 * This method processes the proof dates (sent date and printed date)
	 * @throws ServiceException
	 */
	private void executeProofDates() throws ServiceException {
		DefinedLogger.SERVICE.debug("Fechas de comprobantes...");
		/*
		 * Proof dates
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		PaymentDTO output = bo.findProofDates(input);
		
		/*
		 * Processing information...
		 */
		if(output != null) {
			//Sent date
			if(output.getMtafec() != null)
				paymentDetail.setMtafec(output.getMtafec());
			else
				paymentDetail.setMtafec("");
			
			//Printed date
			if(output.getMtafim() != null)
				paymentDetail.setMtafim(output.getMtafim());
			else
				paymentDetail.setMtafim(output.getMtafim());
		}
	}
	
	/**
	 * This method gets the payment detail as structure
	 * @throws ServiceException
	 */
	private void executePaymentStructure() throws ServiceException {
		PaymentListItemDTO result = bo.findPaymentStructure(input);
									
		if(result == null ) {
			DefinedLogger.SERVICE.error("No se pudo hallar el detalle de la orden de pago para estructura");
			return;
		}
		result.setMtlfpa(DateUtil.normalizeDateFormat(result.getMtlfpa()));
		result.setFecha(DateUtil.normalizeDateFormat(result.getFecha()));
		result.setMtovto(DateUtil.normalizeDateFormat(result.getMtovto()));
		
		
		//Unifico el contenido de Id y SubId separados por un guion
		result.setStatusNumber(result.getStatusNumber() + "-" + result.getStatusSubnumber());
		result.setMtlkme(result.getMtlkme() + "-" + result.getSubmedio());

		xmlPaymentStructure = XMLUtil.dtoToXML(result);
	}
	
	/**
	 * This method returns errors of payment 
	 * @throws ServiceException
	 */
	@SuppressWarnings("unchecked")
	private void executeErrorsList() throws ServiceException {
		DefinedLogger.SERVICE.debug("Procesando errores de OP...");
		/*
		 * Status list
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		List<PaymentErrorListItemDTO> listItem = (List<PaymentErrorListItemDTO>) bo.findErrorsList(input);

		/*	
		 * processing information....
		 */
		DefinedLogger.SERVICE.debug("Validando errores...");
		PaymentErrorListDTO list = new PaymentErrorListDTO();
		list.setList(listItem);
		xmlErrorsList = XMLUtil.dtoToXML(list);
		
	}
}