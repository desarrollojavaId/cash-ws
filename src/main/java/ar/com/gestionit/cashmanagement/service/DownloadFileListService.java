package ar.com.gestionit.cashmanagement.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.persistence.bo.FileContentBO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileContentDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileInputListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.TokenInputListDTO;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManager;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManagerDTO;
import ar.com.gestionit.cashmanagement.service.filemanager.FileManagerOutputDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.StringUtil;
import ar.com.gestionit.cashmanagement.util.XMLUtil;

public class DownloadFileListService extends AbstractService {

	/**
	 * File content business object
	 */
	private FileContentBO bo;

	/**
	 * Service DTO
	 */
	private FileManagerDTO dtoInput;

	/**
	 * This DTO is used for each file given in the list
	 */
	private FileContentDTO dtoFileContent;

	/**
	 * This attribute keeps the token for each file to process 
	 */
	private String currentToken;

	/**
	 * This attribute keeps the file name for each file to process 
	 */
	private String currentFileName;

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new FileContentBO();
		dtoInput = new FileManagerDTO();
		dtoInput.setList(new ArrayList<FileManagerOutputDTO>());
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		//Get file name list
		FileInputListDTO lstFiles = (FileInputListDTO) bo.xmlToDto(getDataFromVector(3), FileInputListDTO.class);
		if(lstFiles == null || lstFiles.getFiles() == null || lstFiles.getFiles().size() == 0) {
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES);
		}

		//Get token list
		TokenInputListDTO lstToken = (TokenInputListDTO) bo.xmlToDto(getDataFromVector(4), TokenInputListDTO.class);
		if(lstToken == null || lstToken.getTokens() == null || lstToken.getTokens().size() == 0) {
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFINDTOKENS));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDTOKENS);
		}

		//Validate if the size of the linked lists is different
		if(lstFiles.getFiles().size() != lstToken.getTokens().size()) {
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_FILETOKENNUMBER));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FILETOKENNUMBER);
		}

		//Validate if any element of the both lists is empty
		for(int i = 0; i < lstFiles.getFiles().size(); i++) {
			if(StringUtil.isEmpty(lstFiles.getFiles().get(i))
					|| StringUtil.isEmpty(lstToken.getTokens().get(i))) {
				DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_LISTELEMENTEMPTY));
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_LISTELEMENTEMPTY);
			}
		}

		dtoInput.setFiles(lstFiles.getFiles());
		dtoInput.setTokens(lstToken.getTokens());
	}

	@Override
	public void execute() throws ServiceException {
		/*
		 * Por cada id del archivo realizo cada una de las siguientes operaciones.
		 */
		DefinedLogger.SERVICE.debug("Recorriendo archivos...");
		FileManagerOutputDTO output;
		for(int i = 0; i < dtoInput.getFiles().size(); i++) {
			dtoFileContent = new FileContentDTO();
			DefinedLogger.SERVICE.debug("Obteniendo nombres de archivo y tokens...");
			//Get the file name (file id) and its token
			dtoFileContent.setFileId(dtoInput.getFiles().get(i));
			currentToken = dtoInput.getTokens().get(i);
			currentFileName = dtoInput.getFiles().get(i);

			/*
			 * File status and last invoked function
			 */
			executeFileData();

			/*
			 * File library
			 */
			executeFileLibrary();

			/*
			 * File content
			 */
			executeFileContent();

			/*
			 * Persist the file for the ws client (frontend)
			 */
			executeDownloader();

			/*
			 * Generate the list for the response
			 */
			output = new FileManagerOutputDTO();
			output.setFileId(dtoInput.getFiles().get(i));
			output.setTokenId(dtoInput.getTokens().get(i));
			dtoInput.getList().add(output);
		}
	}

	/**
	 * This method obtains the status and the last invoked function of the file
	 * @throws ServiceException
	 */
	
	private void executeFileData() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo estado y última invoked function del archivo...");
		/*
		 * File status and last invoked function
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		FileContentDTO aux = (FileContentDTO) bo.find(dtoFileContent);

		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 */
		if(aux == null
				|| StringUtil.isEmpty(aux.getStatus())
				|| StringUtil.isEmpty(aux.getInvokedFunction())) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES);
		}
		dtoFileContent.setStatus(aux.getStatus());
		dtoFileContent.setInvokedFunction(aux.getInvokedFunction());
	}

	/**
	 * This method obtains the library where the file is allocated
	 * @throws ServiceException
	 */
	private void executeFileLibrary() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo librería donde se encuentra el archi...");
		/*
		 * File library
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		FileContentDTO aux = bo.findLibrary(dtoFileContent);

		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 */
		if(aux == null
				|| StringUtil.isEmpty(aux.getLibrary())) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES);
		}
		dtoFileContent.setLibrary(aux.getLibrary());
	}

	/**
	 * This method obtains the file content from the data source
	 * and processes it.
	 * @throws ServiceException
	 */
	private void executeFileContent() throws ServiceException {
		DefinedLogger.SERVICE.debug("Obteniendo contenido de archivo y procesando...");
		/*
		 * File content
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		List<String> list = bo.findFile(dtoFileContent);

		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 */
		//Define and initialize variables
		Iterator<String> i = list.iterator();
		String fileContent = "";

		//Parse to string the file content
		while(i.hasNext())
			fileContent += i.next().trim() + "\n";

		dtoFileContent.setFileContent(fileContent);
	}

	/**
	 * This method persists the file in the appropriate data source.
	 * The WS client (frontend) will access it by this data source.
	 */
	private void executeDownloader() {
		try {
			dtoInput.setFileContent(dtoFileContent.getFileContent().getBytes());
			dtoInput.setTokenId(currentToken);
			dtoInput.setFileName(currentFileName + ".txt");
			FileManager manager = new FileManager();
			manager.updateFile(dtoInput);


		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");

		setDataInVector(5, XMLUtil.dtoToXML(dtoInput));

		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
}