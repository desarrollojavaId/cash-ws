package ar.com.gestionit.cashmanagement.service;

import java.util.ArrayList;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.EscrowFileTypeBO;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowFileTypeDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowFileTypeItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;
import ar.com.gestionit.cashmanagement.util.XMLUtil;

public class EscrowFileTypeService extends AbstractService{

	private static final String GROUP_FAVA = "FAVA";

	/**
	 * Business object for this service
	 */
	private EscrowFileTypeBO bo;

	/**
	 * DTO
	 */
	private EscrowFileTypeDTO dto;

	@Override
	protected void validateBefore() throws ServiceException {
		//Validate if the reques is NULL
		if(request == null) {
			try {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTREQUEST);
			} catch(ServiceException e) {
				throw e;
			}
		}
	}
	@Override
	protected void initialize() throws ServiceException {
		bo = new EscrowFileTypeBO();
		dto = new EscrowFileTypeDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		//Load inputs
		dto.setSsgcod(getDataFromVector(2));
		dto.setSelectedInterface(getDataFromVector(4));

		//Validate inputs
		if(StringUtil.isEmpty(dto.getSsgcod())) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTESCROWID);
		}
		dto.setSsgcod(dto.getSsgcod().trim()); //Fixing...
		if(!NumberUtil.isNumeric(dto.getSsgcod())) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWID);
		}

		if(StringUtil.isEmpty(dto.getSelectedInterface())) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTESCROWINTERFACE);
		}
		dto.setSelectedInterface(dto.getSelectedInterface().trim().toUpperCase());//Fixing...
		if(!ServiceConstant.EscrowInterface.contains(dto.getSelectedInterface())) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTVALIDESCROWINTERFACE);
		}
	}

	@Override
	protected void execute() throws ServiceException {

		//Define and initialize variables
		String member = ServiceConstant.EscrowMember.ALTAS.getExternalReference();
		String selectedInterface = dto.getSelectedInterface();

		//Get escrow by PGCOD
		dto = (EscrowFileTypeDTO) bo.find(dto);
		
		//Reload selected interface to DTO because we lost it in the previous assignment
		dto.setSelectedInterface(selectedInterface);

		//Validate if the status is Vuelco Habilitado (V)
		if(dto.getSskest().equalsIgnoreCase(ServiceConstant.EscrowStatus.VUELCO_HABILITADO.getCode())
				&& dto.getSelectedInterface().equals(ServiceConstant.EscrowInterface.VUELVO_INICIAL.getCode())) {

			//Validate if the group is FAVA
			if(dto.getSskfid().equalsIgnoreCase(GROUP_FAVA)) {
				dto.addIncludedFileTypes(ServiceConstant.EscrowFileType.OP.getCode());
			} else {
				//Validate HISP OPER
				if(dto.getSskhis().equalsIgnoreCase(ServiceConstant.NO)) {
					dto.addExcludedFileTypes(ServiceConstant.EscrowFileType.PG.getCode());
				}

				//Validate SEGUROS interface
				if(dto.getSskseg().equalsIgnoreCase(ServiceConstant.NO)) {
					dto.addExcludedFileTypes(ServiceConstant.EscrowFileType.SG.getCode());
				}

				//Validate VUELCACLI
				if(dto.getSskcli().equals(ServiceConstant.NO)) {
					dto.addExcludedFileTypes(ServiceConstant.EscrowFileType.CL.getCode());
				}
			}
			
			//Get file list
			addFileList(member);
		} else if (dto.getSskest().equalsIgnoreCase(ServiceConstant.EscrowStatus.ACTIVO.getCode())
				&& dto.getSelectedInterface().equals(ServiceConstant.EscrowInterface.COBRANZA.getCode())){
			dto.addIncludedFileTypes(ServiceConstant.EscrowFileType.PG.getCode());
			member = ServiceConstant.EscrowMember.COBRA.getExternalReference();

			//Get file list
			addFileList(member);
		}

		//Validate the statuses
		if(dto.getSskest().equalsIgnoreCase(ServiceConstant.EscrowStatus.ACTIVO.getCode())
				|| dto.getSskest().equalsIgnoreCase(ServiceConstant.EscrowStatus.COBRANZA.getCode())) {
			//SEGUROS interface
			if(dto.getSskests().equalsIgnoreCase(ServiceConstant.EscrowStatus.ACTIVO.getCode())
					&& dto.getSskseg().trim().equalsIgnoreCase(ServiceConstant.YES)
					&& dto.getSelectedInterface().equals(ServiceConstant.EscrowInterface.SEGUROS.getCode())) {
				//Add condition
				dto.addIncludedFileTypes(ServiceConstant.EscrowFileType.SG.getCode());

				//Get file list
				addFileList(ServiceConstant.EscrowMember.COBRA.getExternalReference());
			}

			//REVOLVING interface
			if(dto.getSskestr().equalsIgnoreCase(ServiceConstant.EscrowStatus.ACTIVO.getCode())
					&& dto.getSskrev().trim().equalsIgnoreCase(ServiceConstant.YES)
					&& dto.getSelectedInterface().equals(ServiceConstant.EscrowInterface.REVOLVING.getCode())) {
				//Validate is the group is FAVA
				if(dto.getSskfid().equalsIgnoreCase(GROUP_FAVA)) {
					//Validate SEGUROS interface
					dto.addIncludedFileTypes(ServiceConstant.EscrowFileType.OP.getCode());
				} else {
					dto.addExcludedFileTypes(ServiceConstant.EscrowFileType.PG.getCode());

					//Validate SEGUROS interface
					if(dto.getSskseg().equals(ServiceConstant.NO)) {
						dto.addExcludedFileTypes(ServiceConstant.EscrowFileType.SG.getCode());
					}

					//Validate VUELCACLI
					if(dto.getSskcli().equals(ServiceConstant.NO)) {
						dto.addExcludedFileTypes(ServiceConstant.EscrowFileType.CL.getCode());
					}
				}
				
				//Add file list
				addFileList(ServiceConstant.EscrowMember.ALTAS.getExternalReference());
			}
		}
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(5, XMLUtil.dtoToXML(dto));
	}

	/**
	 * This method updates each item of the list with the member specified by argument
	 * @param list
	 * @param member
	 */
	private void updateMember(List<EscrowFileTypeItemDTO> list, String member) {
		if(list == null)
			return;

		for(EscrowFileTypeItemDTO item : list) {
			item.setMember(member);
		}
	}

	@SuppressWarnings("unchecked")
	private void addFileList(String member) throws ServiceException {
		List<EscrowFileTypeItemDTO> fileList = (List<EscrowFileTypeItemDTO>) bo.findList(dto);

		//Update member
		updateMember(fileList, member);

		//Update main file list
		dto.getFileList().addAll(fileList);

		dto.setExcludedFileTypes(new ArrayList<String>());
		dto.setIncludedFileTypes(new ArrayList<String>());
	}
}