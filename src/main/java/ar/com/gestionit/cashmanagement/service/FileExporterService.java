package ar.com.gestionit.cashmanagement.service;


import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.exporter.CSVExporter;
import ar.com.gestionit.cashmanagement.exporter.ExcelExporter;
import ar.com.gestionit.cashmanagement.exporter.IExporter;
import ar.com.gestionit.cashmanagement.exporter.TXTExporter;
import ar.com.gestionit.cashmanagement.factory.ServiceFactory;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.message.ExecuteRequest;
import ar.com.gestionit.cashmanagement.persistence.dto.FileExporterDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.XMLUtil;

/**
 * This service is used to generate a report about other services.
 * @author jdigruttola
 */
public class FileExporterService extends AbstractService {

	/**
	 * Position where the export information is hosted
	 */
	private static final int POSITION = 19;
	private static final String FILE_NAME = "Reporte";
	private static final String FILE_EXT_XLS = ".xls";
	private static final String FILE_EXT_CSV = ".csv";
	private static final String FILE_EXT_TXT = ".txt";

	/**
	 * Exporter type
	 */
	private FileExporterDTO dto;

	@Override
	protected void initialize() throws ServiceException {
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		//Get information for exportation
		dto = (FileExporterDTO) XMLUtil.xmlToDto(getDataFromVector(POSITION), FileExporterDTO.class);
		if(dto == null) {
			DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_EXPORTINFORMATION));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_EXPORTINFORMATION);
		}

		//Validate inputs -----------------------------------------------------------------------

		if(dto.getChannelNumber() == null || !NumberUtil.isNumeric(dto.getChannelNumber())) {
			DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_CHANNELNOTVALID));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_CHANNELNOTVALID);
		}

		if(dto.getOperationNumber() == null || !NumberUtil.isNumeric(dto.getOperationNumber())) {
			DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_OPERATIONNOTVALID));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_OPERATIONNOTVALID);
		}

		if(dto.getVariantNumber() == null || !NumberUtil.isNumeric(dto.getVariantNumber())) {
			DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_VARIANTNOTVALID));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VARIANTNOTVALID);
		}

		if(dto.getToken() == null) {
			DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_TOKENNOTVALID));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_TOKENNOTVALID);
		}

		if(dto.getFormat() == null) {
			DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_FORMATTOEXPORTNOTVALID));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FORMATTOEXPORTNOTVALID);
		}

		if(!dto.getFormat().trim().equals(String.valueOf(IExporter.FORMAT_EXCEL))
				&& !dto.getFormat().trim().equals(String.valueOf(IExporter.FORMAT_TXT))
				&& !dto.getFormat().trim().equals(String.valueOf(IExporter.FORMAT_CSV))) {
			DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_FORMATTOEXPORTNOTVALID));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_FORMATTOEXPORTNOTVALID);
		}
	}

	@Override
	public void execute() throws ServiceException {
		/*
		 * Change the request ID (channel/operation/variant) to invoke the service factory
		 * with the real service (service what be exported)
		 * 
		 * NOTE: I generate other request to do these chances to avoid to modify the real request
		 * Perhaps, in the future, this avoids problems my dear friend...
		 */
		ExecuteRequest serviceRequest = request;
		try {
			serviceRequest.setAcnco(Short.parseShort(dto.getChannelNumber()));
			serviceRequest.setBopco(Short.parseShort(dto.getOperationNumber()));
			serviceRequest.setCvart(Short.parseShort(dto.getVariantNumber()));
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_EXPORTINFORMATION);
		}

		IService service = ServiceFactory.build(serviceRequest);
		
		//Indicate to the service that it is called by the file exporter
		service.setExporterIndicator(true);

		//Execute real service
		service.execute(serviceRequest);

		//Get content to export
		List<List<? extends IDTO>> content = service.getContentToExport();

		//Validate if there is not content to export
		if(content == null || content.size() == 0) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_EXPORTCONTENTEMPTY);
		}

		//Exporting...
		IExporter exporter;
		switch(Integer.parseInt(dto.getFormat())) {
		case IExporter.FORMAT_EXCEL:
			dto.setFileName(FILE_NAME + FILE_EXT_XLS);
			exporter = new ExcelExporter(content, dto);
			break;
		case IExporter.FORMAT_CSV:
			dto.setFileName(FILE_NAME + FILE_EXT_CSV);
			exporter = new CSVExporter(content, dto);
			break;
		case IExporter.FORMAT_TXT:
			dto.setFileName(FILE_NAME + FILE_EXT_TXT);
			exporter = new TXTExporter(content, dto);
			break;
		default:
			dto.setFileName(FILE_NAME + FILE_EXT_TXT);
			exporter = new TXTExporter(content, dto);
		}

		exporter.export();
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		int i;
		for (i=2;i<18;i++){
			setDataInVector(i, "");
		}
	}
}