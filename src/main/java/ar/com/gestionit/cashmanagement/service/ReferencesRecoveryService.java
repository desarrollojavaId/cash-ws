package ar.com.gestionit.cashmanagement.service;

import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.ReferencesRecoveryBO;
import ar.com.gestionit.cashmanagement.persistence.dto.ReferencesRecoveryDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;


public class ReferencesRecoveryService extends AbstractService {
	
	/**
	 * This attribute is used to load the input parameters
	 */
	private ReferencesRecoveryDTO input;
	
	/**
	 * Business object for this service
	 */
	private ReferencesRecoveryBO bo;
	

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new ReferencesRecoveryBO();
	}
	
	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		input = new ReferencesRecoveryDTO();
		
		String a = getDataFromVector(2);
		a = ServiceUtil.validateNumberAndLengthObligatory(a,7, ServiceConstant.SERVICERETURN_KEY_INVALIDAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYAGREEMENT);
		input.setMrecnv(a);//Convenio
		
		String b = getDataFromVector(3);
		b = ServiceUtil.validateNumberAndLengthObligatory(b, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBAGREEMENT);
		input.setMretbo(b);//Tipo de boleta (Subconvenio)
	}
	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando querys y listas...");
		List<ReferencesRecoveryDTO> dtoList = (List<ReferencesRecoveryDTO>) bo.findList(input);

		//Valido si obtuve datos
		if(dtoList == null || dtoList.size() == 0) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}

		Iterator<ReferencesRecoveryDTO> i = dtoList.iterator();
		String mreccg;
		String mredcg;
		//Establezco contador d para deudor y c para comprobante
		int dContador = 4;
		int cContador = 6;
		//Itero los datos traídos por el Query que pueden ser máximo 7.
		while(i.hasNext()) {
			input = i.next();
			mreccg = input.getMreccg();
			mredcg = input.getMredcg();
			
			// Establece en deudor casos 1 y 2, comprobante para el 11, 12, 13, 14 y 15
			if(mreccg.equalsIgnoreCase("1") || mreccg.equalsIgnoreCase("2")) {
				setDataInVector(dContador, mredcg);
				dContador++;
			} else if(mreccg.equalsIgnoreCase("11")  || mreccg.equalsIgnoreCase("12") || mreccg.equalsIgnoreCase("13") ||
					mreccg.equalsIgnoreCase("14") || mreccg.equalsIgnoreCase("15")) {
				setDataInVector(cContador, mredcg);
				cContador++;
			}
		}
		//Aseguro tener completa hasta la posición 10, aunque sea con vacío ""
		if(cContador < 10) {
			setDataInVector(9, "");
		}
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
}