package ar.com.gestionit.cashmanagement.service;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

/**
 * Consulta cheques recuperados
 * @author jdigruttola
 */
public class RecoveredHistoryService extends AbstractRecoveredCheckListService {

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parámetros de entrada...");
		/*
		 * Get from vector
		 */
		ioDto.setAdherent(getDataFromVector(1));

		String b = getDataFromVector(2);
		b = ServiceUtil.validateNumberAndLengthObligatory(b, 7, ServiceConstant.SERVICERETURN_KEY_INVALIDAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYAGREEMENT);
		ioDto.setAgreementNumber(b);

		String c = getDataFromVector(3);
		c = ServiceUtil.validateNumberAndLengthObligatory(c,7,ServiceConstant.SERVICERETURN_KEY_INVALIDSUBAGREEMENT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYSUBAGREEMENT);
		ioDto.setAgreementSubnumber(c);

		String a = getDataFromVector(4);
		a = ServiceUtil.validateNumberAndLengthObligatory(a, 10, ServiceConstant.SERVICERETURN_KEY_INVALIDBATCHID, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYBATCHID);
		ioDto.setBatchId(a);

		bo.enablePager(getDataFromVector(5));
		
		//Autocompleto el DTO con "valores vacios" ya que el query a ejecutar sirve para otros fuentes
		ioDto.setAmountFrom("0");
		ioDto.setAmountTo("0");
		ioDto.setDateAcredFrom("0");
		ioDto.setDateAcredTo("0");
		ioDto.setDateCheckFrom("0");
		ioDto.setDateCheckTo("0");
		ioDto.setDateRecoveredFrom("0");
		ioDto.setDateRecoveredTo("0");
		ioDto.setStatuses(null);
		ioDto.setCheckNumber("0");
		ioDto.setRecoveredStatus("0");
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Outputs...");
		setDataInVector(6, bo.dtoToXML(ioDto));
		setDataInVector(7, bo.getPagerAsXML());

		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}
}