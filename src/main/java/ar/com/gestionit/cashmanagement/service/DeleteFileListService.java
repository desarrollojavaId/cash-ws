package ar.com.gestionit.cashmanagement.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.FatalException;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.DeleteFileListBO;
import ar.com.gestionit.cashmanagement.persistence.bo.TransactionBO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.TransactionDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;

public class DeleteFileListService extends AbstractService {

	/*
	 * Constants
	 */
	private static final String INVOKED_FUNCTION = "Archivo_eliminado";
	private static final String STATUS_PENDING_VALIDATION = "APVI"; //Pendings
	private static final String STATUS_PROCESSED = "AOKB"; //this file already was processed by the bank
	private static final String STATUS_DELETED = "AELI"; //this file already was deleted

	/**
	 * This attribute is used to load the input parameters
	 */
	private FileListDTO input;

	/**
	 * Business object for this service
	 */
	private DeleteFileListBO bo;

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new DeleteFileListBO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Carga parametros de entrada...");
		input = (FileListDTO) bo.xmlToDto(getDataFromVector(2), FileListDTO.class);
		input.setAdherent(getDataFromVector(1));
		if(input == null || input.getList() == null || input.getList().size() == 0) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFILELIST);
		}

		input.setUser(request.getDrqus());
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ServiceException {
		/*
		 * Current file status list
		 * 
		 * NOTE: This point is used to validate if we can operate about the
		 * specified files. If the file/s is/are as validation pending/s
		 * we cannot delete it/them 
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		DefinedLogger.SERVICE.debug("Ejecutando querys...");
		List<FileListItemDTO> currentStatusList = (List<FileListItemDTO>) bo.findList(input);

		/*	
		 * processing information....
		 */
		/*
		 * Validate if all the files were found according to the specification given
		 */
		if(currentStatusList == null
				|| currentStatusList.size() == 0
				|| currentStatusList.size() < input.getList().size()) {
			reportWarning(ServiceConstant.SERVICERETURN_KEY_NOTFINDFILES);
			return;
		}

		//Set general fields for the file list
		Date now = new Date();
		input.setDate(ServiceConstant.SDF_DATE.format(now));
		input.setHour(ServiceConstant.SDF_HOUR.format(now));
		DefinedLogger.SERVICE.debug("Cargando listas y realizando validaciones...");
		Iterator<FileListItemDTO> i = currentStatusList.iterator();
		FileListItemDTO file;
		while(i.hasNext()) {
			file = i.next();

			if (file.getStkest() != null) {
				//Validate if there are files as validation pendings. In this case, the files cannot be deleted
				if(file.getStkest().equals(STATUS_PENDING_VALIDATION)) {
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_PENDINGFILES);
				}

				//Validate if there are files which already were processed. In this case, the files cannot be deleted
				if(file.getStkest().equals(STATUS_PROCESSED)) {
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_PROCESSEDFILE);
				}

				//Validate if there are files which already were deleted. In this case, the files cannot be deleted
				if(file.getStkest().equals(STATUS_DELETED)) {
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_DELETEDFILE);
				}
			} else {
				//Inconsistent data in the data source....
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_STATUSNOTFOUND);
			}
		}
		input.setList(currentStatusList);

		/*
		 * Delete file list
		 */
		bo.deleteFileList(input);
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		//Nothing here...

		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());

		setInvokedFunction(INVOKED_FUNCTION);
	}

	/**
	 * This method logs the transactions in the data source
	 * If the application cannot do it, this must be killed
	 * because this will miss the control about the transactions
	 * which is processing
	 * @throws FatalException
	 */
	@Override
	protected void reportTransaction() throws ServiceException {
		try {
			TransactionBO bo = new TransactionBO();
			TransactionDTO dto = super.buildTransactionDto();

			//Generate a register for each deleted file
			if(input != null && input.getList() != null && input.getList().size() > 0) {
				Iterator<FileListItemDTO> i = input.getList().iterator();
				while(i.hasNext()) {
					dto.setStnars(i.next().getStnars());
					bo.registerTransaction(dto);
				}
			} else {
				bo.registerTransaction(dto);
			}
		} catch(Exception e) {
			DefinedLogger.SERVICE.error("No se pudo registrar la ejecucion de este servicio en la fuente de datos");
			DefinedLogger.SERVICE.error(e.getMessage(), e);
		}
	}
}