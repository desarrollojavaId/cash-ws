package ar.com.gestionit.cashmanagement.service;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.persistence.bo.ArchiveValidationStatusBO;
import ar.com.gestionit.cashmanagement.persistence.dto.ArchiveValidationStatusArchListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ArchiveValidationStatusListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ArchiveValidationStatusListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileAccountListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.POFileReport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class ArchiveValidationStatusService extends AbstractService{
	/**
	 * This attribute is used to load the input parameters
	 */
	private ArchiveValidationStatusListItemDTO input;
	/**
	 * Business object for this service
	 */
	private ArchiveValidationStatusBO bo;

	private String xmlArchiveStatus;
	
	private long impTotal;
	private long cantCts=0;
	private String fileName="";
	private String totalRegistros;

	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		input = new ArchiveValidationStatusListItemDTO();
		bo = new ArchiveValidationStatusBO();

	}


	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando parametros de entrada...");
		String a = getDataFromVector(1);
		a = ServiceUtil.validateNumberAndLengthObligatory(a, 7, ServiceConstant.SERVICERETURN_KEY_NOTADHERENT, ServiceConstant.SERVICERETURN_KEY_ADHERENTLENGTHERROR);
		input.setAdherent(a);

		DefinedLogger.SERVICE.debug("Cargando lista de archivos a utilizar...");
		ArchiveValidationStatusArchListDTO IdArchivo = (ArchiveValidationStatusArchListDTO)
				bo.xmlToDto(getDataFromVector(2), ArchiveValidationStatusArchListDTO.class);
		if(IdArchivo != null && IdArchivo.getIdArchivo() != null && IdArchivo.getIdArchivo().size() > 0) {
			input.setIdArchivo(IdArchivo.getIdArchivo());
			Iterator<String> i = IdArchivo.getIdArchivo().iterator();
			String item;
			while(i.hasNext()) {
				item = i.next();
				ServiceUtil.validateLength(item, 10, ServiceConstant.SERVICERETURN_KEY_NOTFILELIST);		
			}

		}else{
			DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_NOTFILENAME));
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTFILENAME);

		}
	}		


	@SuppressWarnings("unchecked")
	@Override
	protected void execute() throws ServiceException {

		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando listas...");		
		List<ArchiveValidationStatusListItemDTO> archivos = (List<ArchiveValidationStatusListItemDTO>) bo.findList(input);
	
		if(archivos != null && archivos.size() > 0){
			Iterator<ArchiveValidationStatusListItemDTO> i = archivos.iterator();
			ArchiveValidationStatusListItemDTO archivo;
			List<FileAccountListItemDTO> accounts;
			POFileReport reporter = new POFileReport();
			HashMap<String, FileAccountListItemDTO> accountMap = new HashMap<String, FileAccountListItemDTO>();
			
			while(i.hasNext()) {
				archivo = i.next();
				//Validate if the file type is PO
				if(archivo.getStnars() != null
						&& archivo.getStnars().length() >= 2
						&& archivo.getStnars().substring(0, 2).equalsIgnoreCase("PO")) {
					//Obtain the accounts from the datasource
					archivo.setAdherent(input.getAdherent());
					accounts = bo.findAccounts(archivo);
					
					//Generate the PO report (Payment ways and Accounts)
					reporter.loadReportForArchiveValidationStatus(accounts);
					
					//Get results from the reporter
					impTotal += reporter.getAmountTotalAsLong();
					accountMap.putAll(reporter.getAccountMap());
					archivo.setPaymentWays(reporter.getPaymentWayList());
				}
				/**
				 * For cases where the batch is "PD" and the state is APVI, 
				 * the state APA1 is returned. by bank request
				 */				
				if(archivo.getStnars() != null
						&& archivo.getStnars().length() >= 2
						&& archivo.getStnars().substring(0, 2).equalsIgnoreCase("PD") && "APVI".equals(archivo.getStkest())) {
					DefinedLogger.SERVICE.debug("The batch is PD with APVI status, APA1 is returned");
					archivo.setStkest("APA1");
				}
				cantCts = accountMap.size();
			}
			
			
		}else{
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
		if(archivos.size()==1){
			fileName=archivos.get(0).getStnarc();
			totalRegistros = archivos.get(0).getStqtot();
		}
		
		ArchiveValidationStatusListDTO root = new ArchiveValidationStatusListDTO();
		root.setList(archivos);
		

		xmlArchiveStatus = bo.dtoToXML(root);
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");	
		setDataInVector(3, xmlArchiveStatus);
		setDataInVector(5, fileName);
		setDataInVector(6, POFileReport.formatNumber(String.valueOf(impTotal)));
		setDataInVector(7, Long.toString(cantCts));
		setDataInVector(9, totalRegistros);
	}

}
