package ar.com.gestionit.cashmanagement.service;

import java.util.ArrayList;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.PortalPaymentListBO;
import ar.com.gestionit.cashmanagement.persistence.dto.PortalPaymentListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PortalPaymentListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StatusInputListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StatusInputListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DateUtil;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public class PortalPaymentListService extends AbstractService{
	PortalPaymentListBO bo;
	PortalPaymentListDTO dto;
	
	@Override
	protected void initialize() throws ServiceException {
		bo = new PortalPaymentListBO();
		dto =  new PortalPaymentListDTO();
	}
	
	@Override
	protected void loadInputs() throws ServiceException {
		
		String cuit = getDataFromVector(2);
		cuit = ServiceUtil.validateNumberAndLength(cuit, 11, ServiceConstant.SERVICERETURN_KEY_INVALIDCUIT);
		dto.setCuit(cuit);
		
		String cuitBenef = getDataFromVector(3);
		cuitBenef = ServiceUtil.validateNumberAndLengthObligatory(cuitBenef, 11, ServiceConstant.SERVICERETURN_KEY_INVALIDCUIT, ServiceConstant.SERVICERETURN_KEY_OBLIGATORYCUIT);
		dto.setCuitBeneficiario(cuitBenef);
		
		String formaPago = getDataFromVector(4);
		formaPago = ServiceUtil.validateNumberAndLength(formaPago, 3, ServiceConstant.SERVICERETURN_KEY_PAYMENTTYPELENGTHERROR);
		dto.setFormaPago(formaPago);
		
		String impDesde = getDataFromVector(5);
		impDesde = ServiceUtil.validateNumberAndLength(impDesde, 15, ServiceConstant.SERVICERETURN_KEY_AMOUNTFROM);
		dto.setImpDesde(impDesde);
		
		String impHasta = getDataFromVector(6);
		impHasta = ServiceUtil.validateNumberAndLength(impHasta, 15, ServiceConstant.SERVICERETURN_KEY_AMOUNTTO);
		dto.setImpHasta(impHasta);

		String fechaEmisionDesde = getDataFromVector(7);
		fechaEmisionDesde = ServiceUtil.validateDateForDepuration(fechaEmisionDesde, depurationDate, ServiceConstant.SERVICERETURN_KEY_NOTDATEFROM);
		dto.setFechaEmisionDesde(fechaEmisionDesde);
		
		String fechaEmisionHasta = getDataFromVector(8);
		fechaEmisionHasta = ServiceUtil.validateDate(fechaEmisionHasta, ServiceConstant.SERVICERETURN_KEY_NOTDATETO);
		dto.setFechaEmisionHasta(fechaEmisionHasta);
		
		String fechaPagoDesde = getDataFromVector(9);
		fechaPagoDesde = ServiceUtil.validateDateForDepuration(fechaPagoDesde, depurationDate, ServiceConstant.SERVICERETURN_KEY_NOTDATEFROM);
		dto.setFechaPagoDesde(fechaPagoDesde);
		
		String fechaPagoHasta = getDataFromVector(10);
		fechaPagoHasta = ServiceUtil.validateDate(fechaPagoHasta, ServiceConstant.SERVICERETURN_KEY_NOTDATETO);
		dto.setFechaPagoHasta(fechaPagoHasta);
		
		
		DefinedLogger.SERVICE.debug("Cargando, formateando y validando estados...");
		StatusInputListDTO statuses = (StatusInputListDTO) bo.xmlToDto(getDataFromVector(11), StatusInputListDTO.class);
		
		if(statuses != null && statuses.getStatuses() != null) {
			dto.setLstEstados(statuses.getStatuses());
		} else {
			dto.setLstEstados(new ArrayList<StatusInputListItemDTO>());
		}
		
		//dto.setEstado("0");
		/*
		 * Validating...
		 * ------------------------------------------------------------------------
		 */

		for(StatusInputListItemDTO lstStatus:dto.getLstEstados()){
			String estado = lstStatus.getNumber().trim();			
			if (estado != null && estado.length() == 3)
				estado.replace(estado.substring(1,estado.length()), "");
			estado = ServiceUtil.validateNumberAndLength(estado, 1, ServiceConstant.SERVICERETURN_KEY_INVALIDSTATUS);

			if(!"0".equals(estado) && !"1".equals(estado) && !"2".equals(estado) && !"3".equals(estado) && 
			!"4".equals(estado) && !"5".equals(estado) && !"6".equals(estado) && !"8".equals(estado))
				throw new ServiceException ("El estado debe quedar o vacío, o estar entre 1, 2, 3, 4, 5, 6 u 8");
			
		}
		
		bo.enablePager(getDataFromVector(12));		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void execute() throws ServiceException {
		dto.setList((List<PortalPaymentListItemDTO>) bo.findList(dto));
		
		if(dto != null && dto.getList() != null && dto.getList().size() > 0) {
			for(PortalPaymentListItemDTO item : dto.getList()) {

				item.setMtlfal(DateUtil.normalizeDateFormat(item.getMtlfal()));
				if("2".equals(item.getMtlkme()) || "3".equals(item.getMtlkme()))
					if(item.getFecCheq() != null && !"".equals(item.getFecCheq().trim()))
						item.setMtlfer(DateUtil.normalizeDateFormat(item.getFecCheq()));
				else
					item.setMtlfer(DateUtil.normalizeDateFormat(item.getMtlfer()));
	
				//concatenar id estado con id de subEstado
				item.setEstCod(item.getEstCod() + "-" + item.getEstSCod());
				
				//Get beneficiary mail
				executeBeneficiaryEmail(item);
				
				//Get mark beneficiary mail
				executeMarkBeneficiaryEmail(item);
				
			}
		}
			
	}
	
	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(13, bo.dtoToXML(dto));	
		setDataInVector(14, bo.getPagerAsXML()); 
	}
	
	/**
	 * This method processes the beneficiary email
	 * @throws ServiceException
	 */
	private void executeBeneficiaryEmail(PortalPaymentListItemDTO item) throws ServiceException {
		DefinedLogger.SERVICE.debug("Obtreniendo mail del beneficiario...");
		/*
		 * Beneficiary Email
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		String mail = bo.findMail(item);
		
		/*
		 * Processing information...
		 */
		if(!StringUtil.isEmpty(mail)) {
			item.setMail(mail.trim());
		} else {
			mail = bo.findNoMail(item);
			if(!StringUtil.isEmpty(mail))
				item.setMail(mail.trim());
		}
	}
	
	/**
	 * I override this method to avoid the adherent validation
	 */
	@Override
	protected void validateBefore() throws ServiceException {
		//Validate if the reques is NULL
		if(request == null) {
			try {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTREQUEST);
			} catch(ServiceException e) {
				throw e;
			}
		}
	}
	
	
	/**
	 * This method processes the beneficiary email
	 * @throws ServiceException
	 */
	private void executeMarkBeneficiaryEmail(PortalPaymentListItemDTO item) throws ServiceException {
		DefinedLogger.SERVICE.debug("Obtreniendo la marca de mail del beneficiario...");
		/*
		 * Mark Beneficiary Email
		 * ------------------------------------------------------------------------
		 * query execution...
		 */
		String mark = bo.findMarkMail(item);
		/*
		 * Processing information...
		 */
		if(!StringUtil.isEmpty(mark) && mark.trim().equals("4")) {
			item.setMarkMail("S");
		}
		else
			item.setMarkMail("N");
	}
}
