package ar.com.gestionit.cashmanagement.service;

import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.bo.GeneratedTicketDetailBO;
import ar.com.gestionit.cashmanagement.persistence.dto.GeneratedTicketDetailDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.GeneratedTicketDetailListItemDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;

public class GeneratedTicketDetailService extends AbstractService{

	GeneratedTicketDetailBO bo;
	GeneratedTicketDetailDTO dto;

	@Override
	protected void initialize() throws ServiceException {
		bo = new GeneratedTicketDetailBO();
		dto = new GeneratedTicketDetailDTO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		String cuit = getDataFromVector(2);
		cuit = ServiceUtil.validateNumberAndLengthObligatory(cuit, 11,ServiceConstant.SERVICERETURN_KEY_INVALIDCUIT , ServiceConstant.SERVICERETURN_KEY_OBLIGATORYCUIT);
		dto.setCuit(cuit);
		
		String nroBoleta = getDataFromVector(3);
		nroBoleta = ServiceUtil.validateNumberAndLengthObligatory(nroBoleta, 15, ServiceConstant.SERVICERETURN_KEY_INVALIDNROBOLETA , ServiceConstant.SERVICERETURN_KEY_OBLIGATORYNROBOLETA);
		dto.setNroBoleta(nroBoleta);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void execute() throws ServiceException {
		dto.setList(((List<GeneratedTicketDetailListItemDTO>)bo.findList(dto)));
	}
	
	@Override
	protected void loadOutputs() throws ServiceException {
		setDataInVector(4, bo.dtoToXML(dto));
		
	}

	
}
