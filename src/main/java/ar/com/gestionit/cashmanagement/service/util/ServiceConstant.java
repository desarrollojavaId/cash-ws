package ar.com.gestionit.cashmanagement.service.util;

import java.text.SimpleDateFormat;

public interface ServiceConstant {
	
	/**
	 * This enumeration defines all the possible members for escrow files.
	 * @author jdigruttola
	 */
	public enum EscrowMember {
		ALTAS("ALTAS", "1"),
		COBRA("COBRA", "2");
		
		private String member;
		private String externalReference;
		
		EscrowMember(String member, String externalReference) {
			this.member = member;
			this.externalReference = externalReference;
		}
		public String getMember() {
			return this.member;
		}
		public String getExternalReference() {
			return this.externalReference;
		}
	}
	
	/**
	 * This enumeration defines all the possible file types for escrow
	 * @author jdigruttola
	 */
	public enum EscrowFileType {
		PG("PG", "PAGOS"),
		CU("CU", "CUOTAS"),
		OP("OP", "OPERACIONES"),
		CL("CL", "CLIENTES"),
		SG("SG", "SEGUROS");
		
		private String code;
		private String description;
		
		EscrowFileType(String code, String description) {
			this.code = code;
			this.description = description;
		}
		public String getCode() {
			return this.code;
		}
		public String getDescription() {
			return this.description;
		}
	}
	
	/**
	 * This enumeration defines all the possible file statuses
	 * @author jdigruttola
	 */
	public enum EscrowStatus {
		BAJA("B", "Baja"),
		FIN_VUELCO("F", "Fin Vuelco"),
		VUELCO_HABILITADO("V", "Vuelco Habilitado"),
		ACTIVO("A", "Activo"),
		COBRANZA("C", "Cobranza"),
		NO_HABILITADO("", "No habilitado");
		
		private String code;
		private String description;
		
		EscrowStatus(String code, String description) {
			this.code = code;
			this.description = description;
		}
		
		public String getCode() {
			return this.code;
		}
		public String getDescription() {
			return this.description;
		}
	}
	
	/**
	 * This enumeration defines all the possible interfaces for file sending
	 * @author jdigruttola
	 */
	public enum EscrowInterface {
		VUELVO_INICIAL("V", "Vuelvo inicial"),
		SEGUROS("S", "Seguros"),
		REVOLVING("R", "Revolving"),
		COBRANZA("C", "Cobranza");
		
		private String code;
		private String description;
		
		EscrowInterface(String code, String description) {
			this.code = code;
			this.description = description;
		}
		
		public String getCode() {
			return this.code;
		}
		public String getDescription() {
			return this.description;
		}
		
		public static boolean contains(String interf) {
			return interf != null
					&& (interf.equals(VUELVO_INICIAL.getCode())
							|| interf.equals(COBRANZA.getCode())
							|| interf.equals(REVOLVING.getCode())
							|| interf.equals(SEGUROS.getCode()));
		}
	}
	
	public static final String FIELD_ID_SEPARATOR = "-";

	public static final String TAG_STATUS2 = "ESTADO";
	public static final String TAG_ID = "ID";
	public static final String TAG_PAYMENTTYPE_ROOT = "MEDIOSPAGO";
	public static final String TAG_PAYMENTTYPE = "MPAGO";
	public static final String TAG_PAYMENTTYPE_ID = "IDMPAGO";
	public static final String TAG_STATUS_ROOT = "ESTADOS";
	public static final String TAG_STATUS = "EST";
	public static final String TAG_STATUS_ID = "IDEST";
	public static final String TAG_STATUS_SID = "SIDESTADO";
	public static final String TAG_STATUS_DESC = "DESCESTADO";
	public static final String TAG_PAGER_INPUT = "PAGINADO";
	public static final String TAG_PAGER_INPUT_NUMBER = "NROPAGINA";
	public static final String TAG_PAGER_INPUT_AMOUNT = "CANTPAGINA";
	public static final String TAG_PAGER = "PROXPAG";
	public static final String TAG_PAGER_NEXT = "CONTINUAR";
	public static final String TAG_EVENT_ROOT = "EVENTOS";
	public static final String TAG_EVENT = "EVENTO";
	public static final String TAG_EVENT_DATE = "FECHA";
	public static final String TAG_EVENT_DESC = "DESCEVENTO";
	public static final String TAG_PROOF_ROOT = "COMPROBANTES";
	public static final String TAG_PROOF = "COMPROBANTE";
	public static final String TAG_PROOF_ID = "IDCOMPROBANTE";
	public static final String TAG_PROOF_DESC = "DESC";
	public static final String TAG_PROOF_SIGNATURE = "FIRMA";
	public static final String TAG_TOKEN = "TOKEN";

	public static final String DTO_PAYMENT_ROOT = "OSP";
	public static final String DTO_PAYMENT = "OP";
	public static final String DTO_PAYMENT_MTLQOP = "NOP"; //Nro de orden de pago asignado
	public static final String DTO_PAYMENT_MTLQSO = "SNOP"; //Sub-nro de orden de pago asignado
	public static final String DTO_PAYMENT_REFCLI = "REFCLI"; //Numero de referencia interno dado por cliente
	public static final String DTO_PAYMENT_STATUSNRO = "IDEST"; //Codigo de estado de orden de pago
	public static final String DTO_PAYMENT_STATUSSUBNRO = "SUBIDEST"; //Sub-Codigo de estado de orden de pago
	public static final String DTO_PAYMENT_STATUSDESC = "ESTDESC"; //Descripcion de estado de orden de pago
	public static final String DTO_PAYMENT_OPPRONOM = "DENBEN"; //Denominacion del beneficiario
	public static final String DTO_PAYMENT_MTOVTO = "FVCHE"; //Fecha de vencimiento solo para diferidos
	public static final String DTO_PAYMENT_FECHA = "FEST"; //Fecha de ultimo cambio de estado
	public static final String DTO_PAYMENT_MTLFPA = "FECEX"; //Fecha de ejecución ordenada o Fecha de emision solicitada
	public static final String DTO_PAYMENT_MTLJPA = "IMP"; //Importe neto a pagar
	public static final String DTO_PAYMENT_MTLCUI = "DOCBEN"; //(CUIL o CUIT) Documento del beneficiario
	public static final String DTO_PAYMENT_SCKDER = "TDOCBEN"; //Tipo de doc (CUIT, DNI, CUIL,...)
	public static final String DTO_PAYMENT_MTLQIP = "NROINSTPAGO"; //Nro de instrumento de pago
	public static final String DTO_PAYMENT_MTLKME = "IDMP"; //Codigo de medio de ejecución
	public static final String DTO_PAYMENT_SUC = "IDSUCDES"; //ID de sucursal destino
	public static final String DTO_PAYMENT_MTXDEN = "DSUC"; //Detalle de sucursal de destino
	//public static final String DTO_PAYMENT_MTLQCT = "NROCTACASH"; //Nro de cuenta desde el que se originarán los fondos para el pago 
	public static final String DTO_PAYMENT_DBE = "AVISO"; //Descripcion de aviso (Avisar por email)
	public static final String DTO_PAYMENT_KFP ="FORMAPAGO"; //Forma de pago
	public static final String DTO_PAYMENT_MODULO = "MODCTAORIGEN";
	public static final String DTO_PAYMENT_SUCURSAL = "SUCORIGEN";
	public static final String DTO_PAYMENT_MONEDA = "MONEDAORIGEN";
	public static final String DTO_PAYMENT_SUBOPERA = "SUBOPERACIONORIGEN";
	public static final String DTO_PAYMENT_CUENTA = "NROCTACASH";
	public static final String DTO_PAYMENT_CBU = "CBU";
	public static final String DTO_PAYMENT_MOTIVORECH = "MOTIVORECHAZO";
	public static final String DTO_PAYMENT_CODERROR = "CODERROR";  
	public static final String DTO_PAYMENT_DESCERROR = "DESCERROR";
	public static final String DTO_PAYMENT_FECHAERROR = "FECHAERROR";
	public static final String DTO_PAYMENT_HORAERROR = "HORAERROR";
	public static final String DTO_PAYMENT_MONEDALOTE = "MONEDALOTE"; //Moneda del Lote
	
	public static final String DTO_RECEIVEDCHECK_ROOT ="CHEQUES";
	public static final String DTO_RECEIVEDCHECK = "CHEQUE";
	public static final String DTO_RECEIVEDCHECK_TIPO = "TIPO";
	public static final String DTO_RECEIVEDCHECK_NUMERO = "NUMERO";
	public static final String DTO_RECEIVEDCHECK_ESTADO = "ESTADO";
	public static final String DTO_RECEIVEDCHECK_BANCO = "BANCO";
	public static final String DTO_RECEIVEDCHECK_SUCURSAL = "SUCURSAL";
	public static final String DTO_RECEIVEDCHECK_CODPOS = "CODPOS";
	public static final String DTO_RECEIVEDCHECK_IMPORTE = "IMPORTE";	
	public static final String DTO_RECEIVEDCHECK_TIPODOCLIBRADOR = "TIPODOCLIBRADOR";
	public static final String DTO_RECEIVEDCHECK_DOCLIBRADOR = "DOCLIBRADOR";
	public static final String DTO_RECEIVEDCHECK_FECHAACREDIT = "FECHAACREDIT";
	public static final String DTO_RECEIVEDCHECK_FECHACHEQUE = "FECHACHEQUE";
	public static final String DTO_RECEIVEDCHECK_NUMRECAUDACION = "NUMRECAUDACION";
	public static final String DTO_RECEIVEDCHECK_CTAGIRADA = "CTAGIRADA";
	public static final String DTO_RECEIVEDCHECK_FECHARECAUD = "FECHARECAUD";
	public static final String DTO_RECEIVEDCHECK_REF1DEUDOR = "REF1DEUDOR";
	public static final String DTO_RECEIVEDCHECK_REF2DEUDOR = "REF2DEUDOR";
	public static final String DTO_RECEIVEDCHECK_REF1COMP = "REF1COMP";
	public static final String DTO_RECEIVEDCHECK_REF2COMP = "REF2COMP";
	public static final String DTO_RECEIVEDCHECK_REF3COMP = "REF3COMP";
	public static final String DTO_RECEIVEDCHECK_REF4COMP = "REF4COMP";
	public static final String DTO_RECEIVEDCHECK_REF5COMP = "REF5COMP";
	public static final String DTO_RECEIVEDCHECK_ENTRADA = "ENTRADA";
	public static final String DTO_RECEIVEDCHECK_NROADHERENTE = "NROADHERENTE";
	public static final String DTO_RECEIVEDCHECK_NROCONVENIO = "NROCONVENIO";
	public static final String DTO_RECEIVEDCHECK_NROSUBCONV = "NROSUBCONV";
	public static final String DTO_RECEIVEDCHECK_FECHARECDESDE = "FECHARECDESDE";
	public static final String DTO_RECEIVEDCHECK_FECHARECHASTA = "FECHARECHASTA";
	public static final String DTO_RECEIVEDCHECK_FECHAACREDDESDE = "FECHAACREDDESDE";
	public static final String DTO_RECEIVEDCHECK_FECHAACREDHASTA = "FECHAACREDHASTA";
	public static final String DTO_RECEIVEDCHECK_TIPOCHEQUE = "TIPOCHEQUE";
	public static final String DTO_RECEIVEDCHECK_NUMCHEQUE = "NUMCHEQUE";
	public static final String DTO_RECEIVEDCHECK_BANCHEID = "BANCHEID";
	public static final String DTO_RECEIVEDCHECK_NUMRECAUD = "NUMRECAUD";
	public static final String DTO_RECEIVEDCHECK_MONEDA = "MONEDA";


	public static final String DTO_ERRORSLIST_ROOT = "ERRORES";
	public static final String DTO_ERRORSLIST = "ERROR";
	public static final String DTO_ERRORSLIST_ROW = "LINEA"; //Linea en la que se encuentra el error 
	public static final String DTO_ERRORSLIST_DESCRIPTION = "DESCRIPCION"; //Descripción del error

	public static final String DTO_FILERECEPTION = "ARCH"; 
	public static final String DTO_FILERECEPTION_ROOT = "ARCHIVOS";
	public static final String DTO_FILERECEPTION_NAME ="NOMBRE";
	public static final String DTO_FILERECEPTION_FECGENEC = "FECGENERAC"; 
	public static final String DTO_FILERECEPTION_HORGENEC = "HORAGENERAC"; 
	public static final String DTO_FILERECEPTION_STATE = "ESTADO";
	public static final String DTO_FILERECEPTION_TYPE = "TIPO";
	public static final String DTO_FILERECEPTION_DESCRIPTION = "DESCRIPCION";
	public static final String DTO_FILERECEPTION_STATUSID = "ESTADOARCHIVO";
	public static final String DTO_FILERECEPTION_TYPESID = "TIPOSARCHIVO";
	public static final String DTO_FILERECEPTION_ID = "ID";
	public static final String DTO_FILERECEPTION_SANAOR = "NOMBREORIG";

	public static final String DTO_FILE_ROOT = "ARCHIVOS";
	public static final String DTO_FILE = "ARCHIVO";
	public static final String DTO_FILE_ID = "IDARCHIVO";

	public static final String DTO_CARD_ROOT = "TARJETAS";
	public static final String DTO_CARD = "TARJETA";
	public static final String DTO_CARD_NROTAR = "NROTAR";
	public static final String DTO_CARD_REF1DEUDOR = "REF1DEUDOR";
	public static final String DTO_CARD_REF2DEUDOR = "REF2DEUDOR";
	public static final String DTO_CARD_REF1COMPROBANTE = "REF1COMPROBANTE";
	public static final String DTO_CARD_REF2COMPROBANTE = "REF2COMPROBANTE";
	public static final String DTO_CARD_REF3COMPROBANTE = "REF3COMPROBANTE";
	public static final String DTO_CARD_REF4COMPROBANTE = "REF4COMPROBANTE";
	public static final String DTO_CARD_REF5COMPROBANTE = "REF5COMPROBANTE";
	public static final String DTO_CARD_RAZSOCDEUD = "RAZSOCDEUD";
	public static final String DTO_CARD_ESTTARFEC = "ESTTARFEC";
	public static final String DTO_CARD_FECALTA = "FECALTA";
	public static final String DTO_CARD_FECULTMOV = "FECULTMOV";

	public static final String DTO_CARD_SUMMARY_ROOT = "RESUMENES";
	public static final String DTO_CARD_SUMMARY = "RESUMEN";
	public static final String DTO_CARD_SUMMARY_DESC = "DESCEST";
	public static final String DTO_CARD_SUMMARY_AMOUNT = "CANTIDAD";
	public static final String DTO_CARD_SUMMARY_PERCENTAGE = "PORCENTAJE";

	public static final String DTO_SIGNATORY_ROOT = "FIRMANTES";
	public static final String DTO_SIGNATORY = "FIRMANTE";
	public static final String DTO_SIGNATORY_COUNTRY = "PAIS";
	public static final String DTO_SIGNATORY_FIRSTNAME = "NOMBRE";
	public static final String DTO_SIGNATORY_LASTNAME = "APELLIDO";
	public static final String DTO_SIGNATORY_DATE = "FECHAFIRMA";
	public static final String DTO_SIGNATORY_HOUR = "HRAFIRMA";
	public static final String DTO_SIGNATORY_DOCUMENTTYPE = "TPODOC";
	public static final String DTO_SIGNATORY_DOCUMENTNUMBER = "NRODOC";
	public static final String DTO_SIGNATORY_ACTION = "ACCFIRMA";

	public static final String DTO_REFERENCE = "REFERENCIAS";
	public static final String DTO_REFERENCE_DEU1 = "REF1DEUDOR";
	public static final String DTO_REFERENCE_DEU2 = "REF2DEUDOR";
	public static final String DTO_REFERENCE_COMP1 = "REF1COMPROBANTE";
	public static final String DTO_REFERENCE_COMP2 = "REF2COMPROBANTE";
	public static final String DTO_REFERENCE_COMP3 = "REF3COMPROBANTE";
	public static final String DTO_REFERENCE_COMP4 = "REF4COMPROBANTE";
	public static final String DTO_REFERENCE_COMP5 = "REF5COMPROBANTE";

	public static final String DTO_PAYMENT_SUC_LABEL = "Multisucursal";
	public static final String LABEL_COMMVIA = "Avisar p/e-mail";
	public static final String LABEL_COMMVIA_EXT = "Avisar por e-mail + Call Center disponible";

	public static final String DTO_MONEDA = "MONEDA";
	public static final String DTO_SUCURSAL = "SUCURSAL";
	public static final String DTO_MODULO = "MODULO";
	public static final String DTO_CUENTA = "CUENTA";
	public static final String DTO_CTA = "CTA";
	public static final String DTO_SBOP = "SBOP";
	public static final String DTO_IMPMAX = "IMPMAX";
	public static final String DTO_IMPTOT = "IMPTOT";
	public static final String DTO_FORMAPAGO = "FORMAPAGO";
	public static final String DTO_MDAIMP = "MDAIMP";
	public static final String DTO_CUENTAS = "CUENTAS";


	//Consulta situacion cheque (Pagos).
	public static final String DTO_RECOVCHECK_ROOT = "CHEQUES";
	public static final String DTO_RECOVCHECK = "CHEQ";
	public static final String DTO_RECOVCHECK_REQNUM = "NROPEDIDO";
	public static final String DTO_RECOVCHECK_CHENUM = "NROCHEQUE";
	public static final String DTO_RECOVCHECK_CHENUM2 = "NROCHE";
	public static final String DTO_RECOVCHECK_SATCHENUM = "NROCHEINT";
	public static final String DTO_RECOVCHECK_STATUS = "ESTADO";
	public static final String DTO_RECOVCHECK_STATUS2 = "DESCEST";
	public static final String DTO_RECOVCHECK_DATEGENER = "FECGEN";
	public static final String DTO_RECOVCHECK_BRDESC = "SUCDESC";
	public static final String DTO_RECOVCHECK_BRDESC2 = "SUCDES";
	public static final String DTO_RECOVCHECK_AMOUNT = "IMPORTE";
	public static final String DTO_RECOVCHECK_BANCHECOD = "BANCHECOD";
	public static final String DTO_RECOVCHECK_BANKDESC = "BANCHEDESC";
	public static final String DTO_RECOVCHECK_BANKDESC2 = "BANORIGDES";
	public static final String DTO_RECOVCHECK_DATEEXPIR = "FECVTO";
	public static final String DTO_RECOVCHECK_DATEEXPIR2 = "FECVEN";
	public static final String DTO_RECOVCHECK_STATUSCODE = "CODEST";
	public static final String DTO_RECOVCHECK_TIPODOCLIBR = "TIPODOCLIBR";
	public static final String DTO_RECOVCHECK_DOCLIBR = "DOCLIBR";
	public static final String DTO_RECOVCHECK_FECACRE = "FECACRE";
	public static final String DTO_RECOVCHECK_FECCHE = "FECCHE";
	public static final String DTO_RECOVCHECK_CODPOS = "CODPOS";
	public static final String DTO_RECOVCHECK_FECREC = "FECREC";
	public static final String DTO_RECOVCHECK_RECENPROC = "RECENPROC";
	public static final String DTO_RECOVCHECK_FECACRED = "FECACRED";
	public static final String DTO_RECOVCHECK_DOCLIBRADOR = "DOCLIBRADOR";
	public static final String DTO_RECOVCHECK_NUMCTAGIRADA = "NUMCTAGIRADA";
	public static final String DTO_RECOVCHECK_SUCENV = "SUCENV";


	//DATOS DE SALIDA (LISTA DE CHEQUES)
	public static final String DTO_CHECK_STATUS_LIST_ROOT = "CHEQUES";
	public static final String DTO_CHECK_STATUS_LIST_ITEM = "CHEQ";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_NROCHQ = "NROCHEQ";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_CODEST = "CODEST";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_NOMBEN = "NOMBEN";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_DOCBEN = "DOCBEN";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_IMPORTE = "IMPORTE"; 	
	public static final String DTO_CHECK_STATUS_LIST_ITEM_REFCLIENTE = "REFCLI";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_OPSEGUNBANCO = "OPSEGBAN";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_SOPSEGUNBANCO = "SOPSEGBAN";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_NROCTACASH = "NROCTACASH";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_SERIE = "SERIE";
	public static final String DTO_CHECK_STATUS_LIST_CANT_CHEQ = "CANTCHEQSIMP";	
	public static final String DTO_CHECK_STATUS_LIST_CANT_CHEQTOT = "CANTCHEQTOT";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_SUCURSAL ="SUCORIGEN";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_MONEDA = "MONEDAORIGEN";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_SUBOPERACION = "SUBOPERACIONORIGEN";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_CBU = "CBU";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_CUENTA = "CUENTA";
	public static final String DTO_CHECK_STATUS_LIST_ITEM_MODULO = "MODCTAORIGEN";
	

	//Detalle de recupero CPD (RecoveredCheckDetailDTO)
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ROOT = "ESTADOS";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ESTADO = "ESTADO";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_ID = "ID";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_DESCRIPESTADO = "DESCESTADO";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_LISTEVENTOS = "LISTEVENTOS";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_EVENTO = "EVENTO";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_FECHA = "FEC";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_HS = "HS";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_DESCRIPEVENT = "DESCEVENT";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_CHEQ = "CHEQUE";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_NOMLIBRADOR = "NOMLIBRADOR";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_FECGENPED = "FECGENPED";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_SUCENV = "SUCENV";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_FECENTCLI = "FECENTCLI";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_IMPCHEQUE = "IMPCHEQUE";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_BANCOCHE = "BANCOCHE";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_SUCCHE = "SUCCHE";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_CODPOSCHE = "CODPOSCHE";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_FECVENC = "FECVENC";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_FECENVSUCLIB = "FECENVSUCLIB";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_DOCLIBRADOR = "DOCLIBRADOR";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_NUMCTAGIRADA = "NUMCTAGIRADA";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_NROCHEQOUT = "NROCHEQOUT";
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_NROLOTEOUT = "NROLOTEOUT";	
	public static final String DTO_RECOVERED_CHECK_DETAIL_STATUS_LIST_ITEM_TIPODOCLIBRADOR = "TIPODOCLIBRADOR";
	
	public static final String DTO_CONVLIST_ROOT = "CONVENIOS";
	public static final String DTO_CONVLIST = "CONVENIO";
	public static final String DTO_CONVLIST_DESCRIPCION = "DESCRIPCION";
	public static final String DTO_CONVLIST_NROCONV = "NROCONVENIO";
	public static final String DTO_CONVLIST_NROSUBCONV = "NROSUBCONVENIO";

	public static final String DTO_CASHACCOUNTSLIST_ROOT = "CUENTAS";
	public static final String DTO_CASHACCOUNTSLIST = "CUENTA";
	public static final String DTO_CASHACCOUNTSLIST_CBU = "CBU";
	public static final String DTO_CASHACCOUNTSLIST_MOD = "MOD";
	public static final String DTO_CASHACCOUNTSLIST_MDA = "MDA";
	public static final String DTO_CASHACCOUNTSLIST_SUC = "SUC";
	public static final String DTO_CASHACCOUNTSLIST_CTA = "CTA";
	public static final String DTO_CASHACCOUNTSLIST_SUBCTA = "SUBCTA";
	public static final String DTO_CASHACCOUNTSLIST_TRAMDA = "TRAMDA";
	public static final String DTO_CASHACCOUNTSLIST_PAGFOR = "PAGFOR";
	public static final String DTO_CASHACCOUNTSLIST_IMPMAX = "IMPMAX";
	public static final String DTO_CASHACCOUNTSLIST_IMPTOT = "IMPTOT";
	public static final String DTO_CASHACCOUNTSLIST_PAPER = "PAP";
	public static final String DTO_CASHACCOUNTSLIST_COMPANY = "EMP";
	public static final String DTO_CASHACCOUNTSLIST_OPERATION = "OPE";
	public static final String DTO_CASHACCOUNTSLIST_OPERATIONTYPE = "OPET";
	public static final String DTO_CASHACCOUNTSLIST_ISDEFAULT = "DEFAULT";

	//PO file
	public static final String DTO_POFILE_ROOT = "PAGOS";
	public static final String DTO_POFILE = "PAGO";
	public static final String DTO_POFILE_BENEFICIARYNAME = "NOMB";
	public static final String DTO_POFILE_BENEFICIARYNUMBER = "NROB";
	public static final String DTO_POFILE_CLIENTREFERENCE = "REFC";
	public static final String DTO_POFILE_BENEFICIARYDOCUMENT = "DOCB";
	public static final String DTO_POFILE_AMOUNT = "IMP";
	public static final String DTO_POFILE_EXEMODEDESC = "MEJEC";
	public static final String DTO_POFILE_PAYMENTDATE = "FECP"; 
	public static final String DTO_POFILE_CHECKEXPDATE = "CHVTO";
	public static final String DTO_POFILE_PAYMENTCAUSE = "MOTP";
	public static final String DTO_POFILE_CBU = "CBU";
	public static final String DTO_POFILESUMMARY = "RESUMEN";
	public static final String DTO_POFILESUMMARY_AMOUNT = "IMP";
	public static final String DTO_POFILESUMMARY_REGNUMBER = "CANT";

	//RF file
	public static final String DTO_RFFILE_ROOT = "COBROS";
	public static final String DTO_RFFILE = "COBRO";
	public static final String DTO_RFFILE_DEBTORREF1 = "DR1";
	public static final String DTO_RFFILE_DEBTORREF2 = "DR2";
	public static final String DTO_RFFILE_DEBTORNAME = "DAN";
	public static final String DTO_RFFILE_PROOFREF1 = "CR1";
	public static final String DTO_RFFILE_PROOFREF2 = "CR2";
	public static final String DTO_RFFILE_EMISIONDATE = "FECE";
	public static final String DTO_RFFILE_EFFECTIVEDATE = "FECV";
	public static final String DTO_RFFILE_EXPDATE1 = "FVTO1";
	public static final String DTO_RFFILE_EXPAMOUNT1 = "IMP1";
	public static final String DTO_RFFILE_EXPDATE2 = "FVTO2";
	public static final String DTO_RFFILE_EXPAMOUNT2 = "IMP2";
	public static final String DTO_RFFILE_MINAMOUNT = "IMPM";

	public static final String DTO_RECOVCHECKCRUD_CHECK_ROOT = "CHEQUES";
	public static final String DTO_RECOVCHECKCRUD_CHECK = "IDCHEQUE";

	//Check Reception
	public static String DTO_CHECKRECEPTION_IN_ROOT = "ENTRADA";
	//PC file
	public static final String DTO_PCFILE_ROOT = "COMPS";
	public static final String DTO_PCFILE = "COMP";
	public static final String DTO_PCFILE_DETAIL = "DETALLE";

	public static final String DEFAULT_SEPARATOR = "-";
	
	//Integral Position
	public static final String DTO_INTEGRALPOSITION_REFERENCES="REFERENCIAS";
	public static final String DTO_INTEGRALPOSITION_FORMASCOBRO="FORMASCOBRO";
	public static final String DTO_INTEGRALPOSITION_FORMASFCOB="FCOB";
	public static final String DTO_INTEGRALPOSITION_INPUT="FILTROS";
	public static final String DTO_INTEGRALPOSITION_NROCONSULTA="NROCONSULTA";
	public static final String DTO_INTEGRALPOSITION_CANALES="TIPORECAUDACIONES";
	public static final String DTO_INTEGRALPOSITION_CANAL="TIPOREC";
	public static final String DTO_INTEGRALPOSITION_FECHACOBRODESDE="FECHACOBRODESDE";
	public static final String DTO_INTEGRALPOSITION_FECHACOBROHASTA="FECHACOBROHASTA";
	public static final String DTO_INTEGRALPOSITION_MONTODESDE="MONTODESDE";
	public static final String DTO_INTEGRALPOSITION_MONTOHASTA="MONTOHASTA";
	public static final String DTO_INTEGRALPOSITION_FORMACOBRO="FORMACOBRO";
	public static final String DTO_INTEGRALPOSITION_CANALBOLETA="CANALBOLETA";
	public static final String DTO_INTEGRALPOSITION_SUCURSAL="SUCURSAL";
	public static final String DTO_INTEGRALPOSITION_ESTADORECAUD="ESTADORECAUD";
	public static final String DTO_INTEGRALPOSITION_FECHAPUBLICDESDE="FECHAPUBLICDESDE";
	public static final String DTO_INTEGRALPOSITION_FECHAPUBLICHASTA="FECHAPUBLICHASTA";
	public static final String DTO_INTEGRALPOSITION_IMPORTEPUBLICDESDE="IMPORTEPUBLICDESDE";
	public static final String DTO_INTEGRALPOSITION_IMPORTEPUBLICHASTA="IMPORTEPUBLICHASTA";
	public static final String DTO_INTEGRALPOSITION_FECHAVENCDESDE="FECHAVENCDESDE";
	public static final String DTO_INTEGRALPOSITION_FECHAVENCHASTA="FECHAVENCHASTA";
	public static final String DTO_INTEGRALPOSITION_ESTADODELPOSITIVO="ESTADODELPOSITIVO";
	public static final String DTO_INTEGRALPOSITION_REFENT="REFENT";
	public static final String DTO_INTEGRALPOSITION_TIPOREC_ROOT = "TIPORECAUDACIONES";
	public static final String DTO_INTEGRALPOSITION_TIPOREC="TIPORE";
	public static final String DTO_INTEGRALPOSITION_TIPOREC_DESC="TIPORECDESC";
	public static final String DTO_INTEGRALPOSITION_DESCCANAL = "DESCCANAL";
	public static final String DTO_INTEGRALPOSITION_TIPORECA = "TIPORECID";
	public static final String DTO_INTEGRALPOSITION_MONTOREC = "MONTOREC";
	public static final String DTO_INTEGRALPOSITION_MEDIOS = "MEDIOSPAGO";
	public static final String DTO_INTEGRALPOSITION_COBRO_ROOT = "COBROS";
	public static final String DTO_INTEGRALPOSITION_COBRO = "COBRO";
	public static final String DTO_INTEGRALPOSITION_COBRO_CONV = "NROCON";
	public static final String DTO_INTEGRALPOSITION_COBRO_SUBCONV = "NROSUBCON";
	public static final String DTO_INTEGRALPOSITION_COBRO_CONVDESC = "DENOMCONV";
	public static final String DTO_INTEGRALPOSITION_COBRO_IMP = "IMPORTE";
	public static final String DTO_INTEGRALPOSITION_FCOB1 = "FCOB1";
	public static final String DTO_INTEGRALPOSITION_FCOB2 = "FCOB2";
	public static final String DTO_INTEGRALPOSITION_FCOB3 = "FCOB3";
	public static final String DTO_INTEGRALPOSITION_FCOB4 = "FCOB4";
	public static final String DTO_INTEGRALPOSITION_FCOB5 = "FCOB5";
	public static final String DTO_INTEGRALPOSITION_FCOB6 = "FCOB6";
	
	
	public static final String DTO_INTEGRALPOSITION_ROOT="COBROS";
	public static final String DTO_INTEGRALPOSITION="COBRO";
	public static final String DTO_INTEGRALPOSITION_FECCOB="FECCOB";
	public static final String DTO_INTEGRALPOSITION_REF1DEUDOR="REF1DEUDOR";
	public static final String DTO_INTEGRALPOSITION_REF2DEUDOR="REF2DEUDOR";
	public static final String DTO_INTEGRALPOSITION_REF1COMPROBANTE="REF1COMPROBANTE";
	public static final String DTO_INTEGRALPOSITION_REF2COMPROBANTE="REF2COMPROBANTE";
	public static final String DTO_INTEGRALPOSITION_REF3COMPROBANTE="REF3COMPROBANTE";
	public static final String DTO_INTEGRALPOSITION_REF4COMPROBANTE="REF4COMPROBANTE";
	public static final String DTO_INTEGRALPOSITION_REF5COMPROBANTE="REF5COMPROBANTE";
	public static final String DTO_INTEGRALPOSITION_IMPREC="IMPREC";
	public static final String DTO_INTEGRALPOSITION_IMPACRE= "IMPACRE";
	public static final String DTO_INTEGRALPOSITION_IMPRECH="IMPRECH";
	public static final String DTO_INTEGRALPOSITION_FCOBRODESC="FCOBRODESC";
	public static final String DTO_INTEGRALPOSITION_ESTCOBDESC="ESTCOBDESC";
	public static final String DTO_INTEGRALPOSITION_VALCONFIRM="VALCONFIRM";
	public static final String DTO_INTEGRALPOSITION_EFECTIVO="EFECTIVO";
	public static final String DTO_INTEGRALPOSITION_CHEDIA="CHEDIA";
	public static final String DTO_INTEGRALPOSITION_CHEPAGDIFE="CHEPAGDIFE";
	public static final String DTO_INTEGRALPOSITION_NOTACRED="NOTACRED";
	public static final String DTO_INTEGRALPOSITION_CERTRETEN="CERTRETEN";
	public static final String DTO_INTEGRALPOSITION_TIPOATM="TIPOATM";
	public static final String DTO_INTEGRALPOSITION_SUCCOBDESC="SUCCOBDESC";	
	public static final String DTO_INTEGRALPOSITION_IMPPUBLI="IMPPUBLI";
	public static final String DTO_INTEGRALPOSITION_VENCPUBLI="VENCPUBLI";
	public static final String DTO_INTEGRALPOSITION_IMPADEU= "IMPOADEU";
	public static final String DTO_INTEGRALPOSITION_FILTROS = "FILTROS";
	public static final String DTO_INTEGRALPOSITION_TIPODOC = "TIPODOC";
	public static final String DTO_INTEGRALPOSITION_NRODOC = "NRODOC";
	public static final String DTO_INTEGRALPOSITION_TIPODOCDEUDOR = "TIPODOCDEUDOR";
	public static final String DTO_INTEGRALPOSITION_NRODOCDEUDOR = "NRODOCDEUDOR";
	public static final String DTO_INTEGRALPOSITION_FECHARECDESDE = "FECHARECDESDE";
	public static final String DTO_INTEGRALPOSITION_FECHARECHASTA = "FECHARECHASTA";
	public static final String DTO_INTEGRALPOSITION_IMPORTEDESDE = "IMPORTEDESDE";
	public static final String DTO_INTEGRALPOSITION_IMPORTEHASTA = "IMPORTEHASTA";
	public static final String DTO_INTEGRALPOSITION_NROBANELCO = "NROBANELCO";
	public static final String DTO_INTEGRALPOSITION_CHEVALCOB="CHEVALCOB";
	public static final String DTO_INTEGRALPOSITION_NROREC="NROREC";
	public static final String DTO_INTEGRALPOSITION_MONEDA="MONEDA";
	public static final String DTO_INTEGRALPOSITION_TRANSFERENCIA="TRANSFERENCIA";
	public static final String DTO_INTEGRALPOSITION_BCOORIGENTRANSF="BCOORIGENTRANSF";
	public static final String DTO_INTEGRALPOSITION_BOLETA_COBINPRO="BOLETACOBINPRO";
	
	public static final String DTO_INTEGRALPOSITION_RESUMEROOT="RESUMENES";
	public static final String DTO_INTEGRALPOSITION_RESUME="RESUMEN";
	public static final String DTO_INTEGRALPOSITION_DESCIMP="DESCIMP";
	public static final String DTO_INTEGRALPOSITION_IMPORTE="IMPORTE";
	
	public static final String DTO_INTEGRALPOSITION_MEDIOSPAGOROOT="MEDIOSPREC";
	public static final String DTO_INTEGRALPOSITION_DESCMEDPAG="DESCMEDPAG";
	public static final String DTO_INTEGRALPOSITION_MEDIOSPAGO="MEDIO";
	public static final String DTO_INTEGRALPOSITION_PORCENTAJE="PORCENTAJE";
	public static final String DTO_INTEGRALPOSITION_CANTIDAD="CANTIDAD";
	
	
	//Egress outflow DTOs
	public static final String DTO_EGRESS_CHECK_ROOT = "CHEQUES";
	public static final String DTO_EGRESS_CHECK = "CHEQUE";
	public static final String DTO_EGRESS_ROOT = "FLUJOEGRESOS";
	public static final String DTO_EGRESS = "EGRESO";
	public static final String DTO_MEDIOPAGODESC = "MEDIOPAGODESC";
	public static final String DTO_IMPORTESANTERIORES = "IMPORTESANTERIORES";
	public static final String DTO_IMPORTEHOY = "IMPORTEHOY";
	public static final String DTO_IMPORTEHOYMAS1 = "IMPORTEHOYMAS1";
	public static final String DTO_IMPORTEHOYMAS2 = "IMPORTEHOYMAS2";
	public static final String DTO_IMPORTEHOYMAS3 = "IMPORTEHOYMAS3";
	public static final String DTO_IMPORTEHOYMAS4 = "IMPORTEHOYMAS4";
	public static final String DTO_IMPORTEMAY5 = "IMPORTEMAY5";
	public static final String DTO_IMPORTEHOYFECHA = "IMPORTEHOYFECHA";
	public static final String DTO_IMPORTEHOYMAS1FECHA = "IMPORTEHOYMAS1FECHA";
	public static final String DTO_IMPORTEHOYMAS2FECHA = "IMPORTEHOYMAS2FECHA";
	public static final String DTO_IMPORTEHOYMAS3FECHA = "IMPORTEHOYMAS3FECHA";
	public static final String DTO_IMPORTEHOYMAS4FECHA = "IMPORTEHOYMAS4FECHA";

	//File Exporter DTO
	public static final String DTO_EXPORT = "ARCHIVOS";
	public static final String DTO_EXPORT_CHANNEL = "CANAL";
	public static final String DTO_EXPORT_OPERATION = "OPERACION";
	public static final String DTO_EXPORT_VARIANT = "VARIANTE";
	public static final String DTO_EXPORT_FORMAT = "FORMATO";

	//Escrow List Item DTO
	public static final String DTO_ESCROWS_ROOT = "FIDEICOMISOS";
	public static final String DTO_ESCROWS = "FIDEICOMISO";
	
	public static final String DTO_ESCROWS_FILE_ROOT = "ARCHIVOS";
	public static final String DTO_ESCROWS_FILE = "ARCHIVO";
	public static final String DTO_ESCROWS_FILE_ID = "IDARCHIVO";
	public static final String DTO_ESCROWS_FILE_NAME = "NOMBREARCHIVO";

	public static final String DTO_ESCROWS_CUENTAS = "CUENTAS";
	public static final String DTO_ESCROWS_NROCUENTA = "NROCUENTA"; 

	public static final String DTO_ESCROWS_ID = "ID";
	public static final String DTO_ESCROWS_CUENTA = "CUENTA";
	public static final String DTO_ESCROWS_NOMBRE = "NOMBRE";
	public static final String DTO_ESCROWS_SERIE = "SERIE";
	public static final String DTO_ESCROWS_GRUPO = "GRUPO";
	public static final String DTO_ESCROWS_FECHACORTE = "FECHACORTE";
	public static final String DTO_ESCROWS_CAPITALCORTE = "CAPITALCORTE";
	public static final String DTO_ESCROWS_ESTADO = "ESTADO";
	public static final String DTO_ESCROWS_HISTOPER = "HISTOPER";
	public static final String DTO_ESCROWS_VUELCACLI = "VUELCACLI";
	public static final String DTO_ESCROWS_SEGUROS = "SEGUROS";
	public static final String DTO_ESCROWS_REVOLVING = "REVOLVING";
	public static final String DTO_ESCROWS_INTSEGUROS = "INTSEGUROS";
	public static final String DTO_ESCROWS_INTREVOLVING = "INTREVOLVING";
	public static final String DTO_ESCROWS_INTCOBRANZA = "INTCOBRANZA";
	public static final String DTO_ESCROWS_INTVUELCOINICIAL = "INTVUELCOINICIAL";
	public static final String DTO_ESCROWS_ADMINSEG = "ADMINSEG";
	public static final String DTO_ESCROWS_MIEMBRO = "MIEMBRO";
	public static final String DTO_ESCROWS_ARCHNOMBRE = "ARCHNOMBRE";
	public static final String DTO_ESCROWS_ARCHTIPO = "ARCHTIPO";
	public static final String DTO_ESCROWS_VALIDARLONG = "VALIDARLONG";
	public static final String DTO_ESCROWS_CONDESC = "CONDESC";
	
	//GeneratedTicketDTO
	public static final String DTO_GENERATEDTICKET_ROOT = "BOLETAS";
	public static final String DTO_GENERATEDTICKET = "BOLETA";
	public static final String DTO_GENERATEDTICKET_NROCONVENIO = "NROCONVENIO";
	public static final String DTO_GENERATEDTICKET_NROBOLETA = "NROBOLETA";
	public static final String DTO_GENERATEDTICKET_EMPRESADESC = "EMPRESADESC";
	public static final String DTO_GENERATEDTICKET_FECHAVENC = "FECHAVENC";
	public static final String DTO_GENERATEDTICKET_MONTOADEUDADO = "MONTOADEUDADO";
	public static final String DTO_GENERATEDTICKET_MONTO = "MONTO";
	public static final String DTO_GENERATEDTICKET_MONEDA = "MONEDA";
	public static final String DTO_GENERATEDTICKET_EMPRESA = "EMPRESA";
	public static final String DTO_GENERATEDTICKET_TIPOCOMPROB = "TIPOCOMPROB";
	public static final String DTO_GENERATEDTICKET_DETAIL = "DOCUMENTO";
	public static final String DTO_GENERATEDTICKET_DETAILROOT = "DOCUMENTOS";
	
	//DebtListItemDTO
	public static final String DTO_DEBTLIST_ROOT = "DEUDAS";
	public static final String DTO_DEBTLIST = "DEUDA";
	public static final String DTO_DEBTLIST_FECHAVENC = "FECHAVENC";
	public static final String DTO_DEBTLIST_EMPRESADESC = "EMPRESADESC";
	public static final String DTO_DEBTLIST_MONEDA = "MONEDA";
	public static final String DTO_DEBTLIST_IMPORTE = "IMPORTE";
	public static final String DTO_DEBTLIST_PAGOSREALIZADOS = "PAGOSREALIZADOS";
	public static final String DTO_DEBTLIST_TIPOCOMPROBANTE = "TIPOCOMPROBANTE";
	public static final String DTO_DEBTLIST_REF1 = "REF1";
	public static final String DTO_DEBTLIST_REF2 = "REF2";
	public static final String DTO_DEBTLIST_REF3 = "REF3";
	public static final String DTO_DEBTLIST_REF4 = "REF4";
	public static final String DTO_DEBTLIST_REF5 = "REF5";
	
	//Company List
	public static final String DTO_COMPANYLIST_ROOT = "EMPRESAS";
	public static final String DTO_COMPANYLIST = "EMPRESA";
	public static final String DTO_COMPANYLIST_EMPRESADESC = "EMPRESADESC";
	public static final String DTO_COMPANYLIST_CONVENIO="CONVENIO";
	public static final String DTO_COMPANYLIST_SUBCONVENIO="SUBCONVENIO";
	public static final String DTO_COMPANYLIST_MONEDA="MONEDA";
	
	
	//MadePaymentListDTO
	public static final String DTO_MADEPAYMENT_ROOT = "PAGOS";
	public static final String DTO_MADEPAYMENT = "PAGO";
	public static final String DTO_MADEPAYMENT_COMPANYID = "EMPRESAID";
	public static final String DTO_MADEPAYMENT_COMPANYDESC = "EMPRESADESC";
	public static final String DTO_MADEPAYMENT_AMOUNT = "IMPORTE";
	public static final String DTO_MADEPAYMENT_OPERATIONNUMBER = "NROOPERACION"; 
	public static final String DTO_MADEPAYMENT_TYPE = "FORMAPAGO";
	public static final String DTO_MADEPAYMENT_DATE = "FECHAPAGO";  
	
	//PortalPaymentListItemDTO
	public static final String DTO_PORTALPAYMENT_ROOT = "OSP";
	public static final String DTO_PORTALPAYMENT = "OP";
	public static final String DTO_PORTALPAYMENT_NOP = "NOP";
	public static final String DTO_PORTALPAYMENT_SNOP = "SNOP";
	public static final String DTO_PORTALPAYMENT_FECPAGO = "FECPAGO";
	public static final String DTO_PORTALPAYMENT_EMISOR = "EMISOR";
	public static final String DTO_PORTALPAYMENT_TDOCEMISOR = "TDOCEMISOR";
	public static final String DTO_PORTALPAYMENT_DOCEMISOR = "DOCEMISOR";
	public static final String DTO_PORTALPAYMENT_IDFP = "IDFP";
	public static final String DTO_PORTALPAYMENT_IMP = "IMP";
	public static final String DTO_PORTALPAYMENT_FECEMISION = "FECEMISION";
	public static final String DTO_PORTALPAYMENT_IDEST = "IDEST";
	public static final String DTO_PORTALPAYMENT_ESTDESC = "ESTDESC";
	public static final String DTO_PORTALPAYMENT_BENMAIL = "MAILBEN";
	public static final String DTO_PORTALPAYMENT_ADHERENTE = "ADHERENTE";
	public static final String DTO_PORTALPAYMENT_ENVIAMAIL = "ENVIAMAIL";
	public static final String DTO_PORTALPAYMENT_MEDPAGO = "MEDPAGO";
	public static final String DTO_PORTALPAYMENT_MONEDA = "MONEDA";
	
	
	//Dictionary
	public static final String DTO_DICTIONARY_ROOT= "CARACTERES";
	public static final String DTO_DICTIONARY = "CARACTER";
	public static final String DTO_DICTIONARY_CLAVE = "CLAVE";
	public static final String DTO_DICTIONARY_VALOR = "VALOR";
	
	//AdherentListForMepDTO
	public static final String DTO_ADHERENTLISTFORMEP = "ADHERENTE"; 
	public static final String DTO_ADHERENTLISTFORMEP_ROOT = "ADHERENTES";
	public static final String DTO_ADHERENTLISTFORMEP_ID = "NROADHERENTE";
	public static final String DTO_ADHERENTLISTFORMEP_ENABLED = "HABILITADOMEP";
	/*
	 * Default properties  
	 */
	public static final int FALSE = 0;
	public static final String TRUE = "1";
	public static final String YES = "S";
	public static final String NO = "N";

	public static final SimpleDateFormat SDF_DATE = new SimpleDateFormat("dd/MM/yyyy");
	public static final SimpleDateFormat SDF_AMERICAN_DATE = new SimpleDateFormat("yyyy/MM/dd");
	public static final SimpleDateFormat SDF_DATE_FOR_OUTPUT = new SimpleDateFormat("yyyy-MM-dd");
	public static final SimpleDateFormat SDF_DATE_FOR_INPUT = new SimpleDateFormat("dd/MM/yy");
	public static final SimpleDateFormat SDF_HOUR = new SimpleDateFormat("HH:mm:ss");
	public static final SimpleDateFormat SDF_HOUR_FOR_INPUT = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
	public static final SimpleDateFormat SDF_DATE_FULL = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss.S000");
	public static final SimpleDateFormat SDF_YYYYMMDD = new SimpleDateFormat("yyyyMMdd");
	public static final SimpleDateFormat SDF_DDMMYYYY = new SimpleDateFormat("ddMMyyyy");

	public static final String USER_BY_DEFAULT = "CashManagementWS";

	/*
	 * Mapped global properties of the application 
	 */
	public static final String PROP_TRACEEVENT_MAXATTEMPTS = "service.traceEvent.generateKey.attempts";
	public static final String PROP_TRACEEVENT_TIME = "service.traceEvent.generateKey.time";

	public static final String PROP_SENDFILE_INSERT_MAX = "service.sendFileService.numberOfRegisters";
	public static final String PROP_SENDFILE_FILESTATUS_ATTEMPTS = "service.sendFileService.getFileStatus.attempts";
	public static final String PROP_SENDFILE_FILESTATUS_TIME = "service.sendFileService.getFileStatus.time";
	
	public static final String PROP_SENDFILE_PAYMENTTYPE_1_CODE = "service.sendFileService.poReport.paymentType.1.code";
	public static final String PROP_SENDFILE_PAYMENTTYPE_1_DESCRIPTION = "service.sendFileService.poReport.paymentType.1.description";
	public static final String PROP_SENDFILE_PAYMENTTYPE_2_CODE = "service.sendFileService.poReport.paymentType.2.code";
	public static final String PROP_SENDFILE_PAYMENTTYPE_2_DESCRIPTION = "service.sendFileService.poReport.paymentType.2.description";
	public static final String PROP_SENDFILE_PAYMENTTYPE_3_CODE = "service.sendFileService.poReport.paymentType.3.code";
	public static final String PROP_SENDFILE_PAYMENTTYPE_3_DESCRIPTION = "service.sendFileService.poReport.paymentType.3.description";
	
	public static final String PROP_ESCROWSENDFILE_LIBRARY = "service.escrowSendFileService.library";
	public static final String PROP_ESCROWSENDFILE_TIMEOUT = "service.escrowSendFileService.timeout";
	
	public static final String PROP_GENERAL_LANGUAGEDEFAULT = "application.language.default";
	
	public static final String PROP_MAIL_SMTP_HOST = "mail.smtp.host";
	public static final String PROP_MAIL_SMTP_AUTH = "mail.smtp.auth";
	public static final String PROP_MAIL_SMTP_DEBUG = "mail.smtp.debug";
	public static final String PROP_MAIL_SMTP_PORT = "mail.smtp.port";
	public static final String PROP_MAIL_SMTP_USER = "mail.smtp.user";
	public static final String PROP_MAIL_SMTP_PASS = "mail.smtp.pass";
	public static final String PROP_MAIL_FROM_ADDRESS = "mail.from.address";
	public static final String PROP_MAIL_TO_ADDRESS = "mail.to.address";
	
	public static final String PROP_DATASOURCE_APP_USER = "application.datasource.username";
	public static final String PROP_DATASOURCE_APP_PASS = "application.datasource.password";
	public static final String PROP_DATASOURCE_FM_USER = "filemanager.datasource.username";
	public static final String PROP_DATASOURCE_FM_PASS = "filemanager.datasource.password";
	
	public static final String DTO_PAYMENT_WAY_FORMASDEPAGO = "FORMASDEPAGO";
	public static final String DTO_PAYMENT_WAY_FORMADEPAGO = "FORMADEPAGO";
	public static final String DTO_PAYMENT_WAY_IDFORMADEPAGO = "IDFORMADEPAGO";
	public static final String DTO_PAYMENT_WAY_DESCFORMADEPAGO = "DESCFORMADEPAGO";
	public static final String DTO_PAYMENT_WAY_IMPMAX= "IMPMAX";
	public static final String DTO_PAYMENT_WAY_IMPTOT = "IMPTOT";
	public static final String DTO_PAYMENT_WAY_CUENTAS = "CUENTAS";
	
	/*
	 * Mapped service returns
	 */
	public static final String SERVICERETURN = "RESULTADO";
	public static final String SERVICERETURN_FIELD_CODE = "CODIGO";
	public static final String SERVICERETURN_FIELD_DESC = "DESCRIPCION";
	public static final String SERVICERETURN_KEY_OK = "ok";
	public static final String SERVICERETURN_KEY_NOTREQUEST = "error.notRequest";
	public static final String SERVICERETURN_KEY_NOTADHERENT = "error.notAdherent";
	public static final String SERVICERETURN_KEY_ERRORDTOTOXML = "error.dtoToXML";
	public static final String SERVICERETURN_KEY_QUERYNOTEXEC = "error.queryNotExecution";
	public static final String SERVICERETURN_KEY_NOTSERVICE = "error.notService";
	public static final String SERVICERETURN_KEY_NOTDATAINVECTOR = "error.notDataInVector";
	public static final String SERVICERETURN_KEY_NOTPERSISTTRANS = "error.notPersistTransaction";
	public static final String SERVICERETURN_KEY_VALPENDING = "ok.validationPending";
	public static final String SERVICERETURN_KEY_WITHOUTERRORS = "ok.withoutErrors";
	public static final String SERVICERETURN_KEY_AMOUNTFROM = "error.amountFromInvalidFormat";
	public static final String SERVICERETURN_KEY_AMOUNTTO = "error.amountToInvalidFormat";
	public static final String SERVICERETURN_KEY_NOTFINDFILES= "error.notFindFiles";
	public static final String SERVICERETURN_KEY_PENDINGFILES= "error.pendingFiles";
	public static final String SERVICERETURN_KEY_NOTDELETEFILES= "error.notDeleteFiles";
	public static final String SERVICERETURN_KEY_NOTDELETEFILESSAT= "error.notDeleteFilesSAT";
	public static final String SERVICERETURN_KEY_PROCESSEDFILE= "error.processedFile";
	public static final String SERVICERETURN_KEY_STATUSNOTFOUND= "error.statusNotFound";
	public static final String SERVICERETURN_KEY_DELETEDFILE = "error.deletedFile";
	public static final String SERVICERETURN_KEY_NOTFILENAME = "error.notFileName";
	public static final String SERVICERETURN_KEY_ERRORBYDEFAULT = "error.errorByDefault";
	public static final String SERVICERETURN_KEY_NOCHECKSFORRECOVERED = "error.noChecksForRecovered";
	public static final String SERVICERETURN_KEY_ERRORNOTACTION = "error.notActionProcessOrRejection";
	public static final String SERVICERETURN_KEY_NOTPAYMENTNUMBER = "error.notPaymentNumber";
	public static final String SERVICERETURN_KEY_NOTPAYMENTSUBNUMBER = "error.notPaymentSubnumber";
	public static final String SERVICERETURN_KEY_NOTFILELIST = "error.notFileList";
	public static final String SERVICERETURN_KEY_NOTVALIDDATESTARTFROM = "error.notValidDateStartFrom";
	public static final String SERVICERETURN_KEY_NOTVALIDDATESTARTTO = "error.notValidDateStartTo";
	public static final String SERVICERETURN_KEY_NOTVALIDDATELASTMOVFROM = "error.notValidDateLastMovFrom";
	public static final String SERVICERETURN_KEY_NOTVALIDDATELASTMOVTO = "error.notValidDateLastMovTo";
	public static final String SERVICERETURN_KEY_NOTVALIDDATERECOVFROM = "error.notValidDateRecoveredFrom";
	public static final String SERVICERETURN_KEY_NOTVALIDDATERECOVTO = "error.notValidDateRecoveredTo";
	public static final String SERVICERETURN_KEY_NOTVALIDDATEEXECFROM = "error.notExecutionDateFrom";
	public static final String SERVICERETURN_KEY_NOTVALIDDATEEXECTO = "error.notExecutionDateTo";
	public static final String SERVICERETURN_KEY_OBLIGATORYAGREEMENT ="error.obligatoryAgreement";
	public static final String SERVICERETURN_KEY_OBLIGATORYSUBAGREEMENT ="error.obligatorySubAgreement";
	public static final String SERVICERETURN_KEY_INVALIDAGREEMENT ="error.invalidAgreement";
	public static final String SERVICERETURN_KEY_INVALIDSUBAGREEMENT ="error.invalidSubAgreement";
	public static final String SERVICERETURN_KEY_ADHERENTLENGTHERROR = "error.adherentLengthError";
	public static final String SERVICERETURN_KEY_INVALIDACCOUNT = "error.invalidAccount";
	public static final String SERVICERETURN_KEY_INVALIDBENEFICIARYNAME = "error.beneficiaryNameLengthError";
	public static final String SERVICERETURN_KEY_INVALIDBENEFICIARYDOC= "error.beneficiaryDocLengthError";
	public static final String SERVICERETURN_KEY_AMOUNTLENGTHERROR = "error.amountLengthError";
	public static final String SERVICERETURN_KEY_PAYMENTNUMBERLENGTHERROR = "error.paymentNumberLengthError";
	public static final String SERVICERETURN_KEY_PAYMENTSUBNUMBERLENGTHERROR = "error.paymentSubNumberLengthError";
	public static final String SERVICERETURN_KEY_INVALIDPAYMENTTYPE = "error.invalidPaymentType";
	public static final String SERVICERETURN_KEY_PAYMENTTYPELENGTHERROR = "error.paymentTypeLengthError";
	public static final String SERVICERETURN_KEY_INVALIDSTATUS = "error.invalidStatus";
	public static final String SERVICERETURN_KEY_STATUSLENGHTERROR = "error.statusLengthError";
	public static final String SERVICERETURN_KEY_HYPHENFORMAT = "error.hyphenFormat";
	public static final String SERVICERETURN_KEY_DEBREFERENCELENGTHERROR = "error.debReferenceLengthError";
	public static final String SERVICERETURN_KEY_COMPREFERENCELENGTHERROR = "error.compReferenceLengthError";
	public static final String SERVICERETURN_KEY_DATEINTERVALNOTVALID = "error.dateIntervalNotValid";
	public static final String SERVICERETURN_KEY_AMOUNTINTERVALNOTVALID = "error.amountIntervalNotValid";
	public static final String SERVICERETURN_KEY_OBLIGATORYACCOUNT = "error.obligatoryAccount"; 
	public static final String SERVICERETURN_KEY_INVALIDSUBPAYORDER = "error.invalidSubPayOrder";
	public static final String SERVICERETURN_KEY_OBLIGATORYSUBPAYORDER = "error.obligatorySubPayOrder";
	public static final String SERVICERETURN_KEY_OBLIGATORYPAYORDER = "error.obligatoryPayOrder";
	public static final String SERVICERETURN_KEY_INVALIDPAYORDER = "error.invalidPayOrder";
	public static final String SERVICERETURN_KEY_PAYORDERFROMNOTVALID = "error.payOrderFromNotValid";
	public static final String SERVICERETURN_KEY_PAYORDERTONOTVALID = "error.payOrderToNotValid";
	public static final String SERVICERETURN_KEY_PAYORDERRANGE = "error.payOrderRange";
	public static final String SERVICERETURN_KEY_NOTDATEFROM = "error.notDateFrom";
	public static final String SERVICERETURN_KEY_NOTDATETO = "error.notDateTo";
	public static final String SERVICERETURN_KEY_FILETYPENOTFOUND = "error.fileTypeNotFound";
	public static final String SERVICERETURN_KEY_FILEEMPTY = "error.fileEmpty";
	public static final String SERVICERETURN_KEY_OBLIGATORYFIELD = "error.obligatoryField"; 
	public static final String SERVICERETURN_KEY_OBLIGATORYPAGER = "error.obligatoryPager";
	public static final String SERVICERETURN_KEY_INVALIDPAGER = "error.invalidPager";
	public static final String SERVICERETURN_KEY_INVALIDFILENAME = "error.invalidFileName";
	public static final String SERVICERETURN_KEY_CHECKFROM = "error.checkFromInvalid";
	public static final String SERVICERETURN_KEY_CHECKTO = "error.checktoInvalid";
	public static final String SERVICERETURN_KEY_CHECKORDERRANGE = "error.checkOrderRange";
	public static final String SERVICERETURN_KEY_INVALIDAMOUNTFROM = "error.amountFromInvalid";
	public static final String SERVICERETURN_KEY_INVALIDAMOUNTTO = "error.amountToInvalid";
	public static final String SERVICERETURN_KEY_STATUSSUBNUMBERLENGTHERROR = "error.statusSubNumberLengthError";
	public static final String SERVICERETURN_KEY_INVALIDCBU = "error.invalidCbu";
	public static final String SERVICERETURN_KEY_NOTFINDTOKENS = "error.notFindTokens";
	public static final String SERVICERETURN_KEY_FILETOKENNUMBER = "error.fileTokenNumber";
	public static final String SERVICERETURN_KEY_LISTELEMENTEMPTY = "error.listElementEmpty";
	public static final String SERVICERETURN_KEY_INVALIDBATCHID = "error.invalidBatchId";
	public static final String SERVICERETURN_KEY_INVALIDNROCHEQXSAT = "error.invalidNroCheqXSat";
	public static final String SERVICERETURN_KEY_OBLIGATORYBATCHID = "error.obligatoryBatchId";
	public static final String SERVICERETURN_KEY_NOTFINDACTION = "error.notFindAction";
	public static final String SERVICERETURN_KEY_ACTIONNOTVALID = "error.actionNotValid";
	public static final String SERVICERETURN_KEY_OBLIGATORYCHECKNUMBER = "error.obligatoryCheckNumber";
	public static final String SERVICERETURN_KEY_NULLDTO = "error.nullDto";
	public static final String SERVICERETURN_KEY_CHECKEXISTS = "error.checkExists";
	public static final String SERVICERETURN_KEY_VOIDCHECKLIST = "error.voidCheckList";
	public static final String SERVICERETURN_KEY_NOTVALIDCHECKNUMBER = "error.notValidCheckNumber";
	public static final String SERVICERETURN_KEY_NOTVALIDCHECKLIST = "error.checkListNotValid";
	public static final String SERVICERETURN_KEY_CONVERTBASE64 = "error.convertBase64";
	public static final String SERVICERETURN_KEY_INVALIDRECOVEREDP = "error.invalidRecoveredP";
	public static final String SERVICERETURN_KEY_PAYMENTORDEBT = "error.noValidOption";
	public static final String SERVICERETURN_KEY_EXPORTINFORMATION = "error.exportInformation";
	public static final String SERVICERETURN_KEY_CHANNELNOTVALID = "error.channelNotValid"; 
	public static final String SERVICERETURN_KEY_OPERATIONNOTVALID = "error.operarionNotValid"; 
	public static final String SERVICERETURN_KEY_VARIANTNOTVALID = "error.variantNotValid"; 
	public static final String SERVICERETURN_KEY_TOKENNOTVALID = "error.tokenNotValid";
	public static final String SERVICERETURN_KEY_FORMATTOEXPORTNOTVALID = "error.formatToExportNotValid";
	public static final String SERVICERETURN_KEY_EXPORTHEADERS = "error.exportHeaders";
	public static final String SERVICERETURN_KEY_EXPORTCONTENT = "error.exportContent";
	public static final String SERVICERETURN_KEY_EXPORTCONTENTEMPTY = "ok.exportContentEmpty";
	public static final String SERVICERETURN_KEY_FAILEDTOGETFILE = "error.failedToGetFile";
	public static final String SERVICERETURN_KEY_NOTFILETYPE = "error.notFileType";
	public static final String SERVICERETURN_KEY_NOTVALIDFILETYPE = "error.notValidFileType";
	public static final String SERVICERETURN_KEY_NOTVALIDADHERENT = "error.notValidAdherent";
	public static final String SERVICERETURN_KEY_NOTFOUNDLIBRARY = "error.notFoundLibrary";
	public static final String SERVICERETURN_KEY_NOTFOUNDTABLE = "error.notFoundTable";
	public static final String SERVICERETURN_KEY_INVALIDCHEQTYPE ="error.invalidCheqType";
	public static final String SERVICERETURN_KEY_OBLIGATORYCHEQTYPE ="error.obligatoryCheqType";
	public static final String SERVICERETURN_KEY_NOTVALIDREFERENCE ="error.notValidReference";
	public static final String SERVICERETURN_KEY_NOTPERSISTS = "error.notPersists";
	public static final String SERVICERETURN_KEY_NOTFILEID = "error.notFileId";
	public static final String SERVICERETURN_KEY_NOTNEWSCHEMA = "error.notNewSchema";
	public static final String SERVICERETURN_KEY_NOTACTION = "error.notAction";
	public static final String SERVICERETURN_KEY_SIGNATORIESMANDATORY = "error.signatoryListMandatory";
	public static final String SERVICERETURN_KEY_SIGNATORIESDATAMANDATORY = "error.signatoryDataMandatory";
	public static final String SERVICERETURN_KEY_NOTFILESTATUS = "error.notFileStatus";
	public static final String SERVICERETURN_KEY_FILESTATUSAPVI = "ok.fileStatusAPVI";
	public static final String SERVICERETURN_KEY_NOTREJECTFILE = "error.notRejectFile";
	public static final String SERVICERETURN_KEY_NOTAUTORIZEDFILE = "error.notAutorizedFile";
	public static final String SERVICERETURN_KEY_NOTFOUNDFILENAME = "error.notFoundFileName";
	public static final String SERVICERETURN_KEY_NOTFOUNDNEWSCHEMA = "error.notFoundNewSchema";
	public static final String SERVICERETURN_KEY_INVALIDQUERYNUMBER = "error.invalidQueryNumber";
	public static final String SERVICERETURN_KEY_OBLIGATORYQUERYNUMBER = "error.obligatoryQueryNumber";
	public static final String SERVICERETURN_KEY_INVALIDCHANNEL = "error.invalidChannel";
	public static final String SERVICERETURN_KEY_OBLIGATORYCHANNEL = "error.obligatoryChannel";
	public static final String SERVICERETURN_KEY_INVALIDPAYMENTMETHOD = "error.invalidPaymentMethod";
	public static final String SERVICERETURN_KEY_INVALIDBRANCH = "error.invalidBranch";
	public static final String SERVICERETURN_KEY_INVALIDINCOME = "error.invalidIncome";
	public static final String SERVICERETURN_KEY_INVALIDSTATE = "error.invalidState";
	public static final String SERVICERETURN_KEY_INVALIDENTITYREFERENCE = "error.invalidEntityReference";
	public static final String SERVICERETURN_KEY_INVALIDATMTYPE = "error.invalidATMType";
	public static final String SERVICERETURN_KEY_VOIDLIST = "error.voidList";
	public static final String SERVICERETURN_KEY_INVALIDCUIT= "error.invalidCuit";
	public static final String SERVICERETURN_KEY_OBLIGATORYCUIT= "error.obligatoryCuit";
	public static final String SERVICERETURN_KEY_INVALIDNROBOLETA = "error.invalidNroBoleta";
	public static final String SERVICERETURN_KEY_OBLIGATORYNROBOLETA = "error.obligatoryNroBoleta";
	public static final String SERVICERETURN_KEY_EMPTYFILTERS = "error.emptyFilters";
	public static final String SERVICERETURN_KEY_EMPTYENTRYSTRUCTURE = "error.emptyEntryStructure";
	public static final String SERVICERETURN_KEY_INVALIDCOMPANY = "error.invalidCompany";
	public static final String SERVICERETURN_KEY_NOTENDINGDATE = "error.notEndingDate";
	public static final String SERVICERETURN_KEY_ESCROWNOTENABLED = "error.escrowNotEnabled";
	public static final String SERVICERETURN_KEY_OBLIGATORYRECTYPE = "error.obligatoryRecType";
	public static final String SERVICERETURN_KEY_INVALIDRECTYPE = "error.invalidRecType";
	public static final String SERVICERETURN_KEY_NOTGENERATEDMEMBER = "error.notGeneratedMember";
	public static final String SERVICERETURN_KEY_NOTFOUNDLOGICALNAME = "error.notFoundLogicalName";
	public static final String SERVICERETURN_KEY_NOTUPDATEDATE = "error.notUpdateDate";
	public static final String SERVICERETURN_KEY_MAILPROPTROUBLES = "error.mailPropertiesTroubles";
	public static final String SERVICERETURN_KEY_NOTSENDMAIL = "error.notSendMail";
	public static final String SERVICERETURN_KEY_NOTVALIDMEMBER = "error.notValidMember";
	public static final String SERVICERETURN_KEY_NOTVALIDGROUP = "error.notValidGroup";
	public static final String SERVICERETURN_KEY_NOTVALIDFILENAME = "error.notValidFileName";
	public static final String SERVICERETURN_KEY_NOTCHECKSUMDATA = "error.notChecksumData";
	public static final String SERVICERETURN_KEY_NOTCHECKSUMINFILE= "error.notChecksumInFile";
	public static final String SERVICERETURN_KEY_NOTGENERATEDCHECKSUM = "error.notGeneratedChecksum";
	public static final String SERVICERETURN_KEY_INVALIDCHECKSUM = "error.invalidChecksum";
	public static final String SERVICERETURN_KEY_CBUNOTVALID = "error.cbunotvalid";
	public static final String SERVICERETURN_KEY_NOTVALIDESCROWCODE="error.notValidEscrowCode";
	public static final String SERVICERETURN_KEY_NOTVALIDESCROWACCOUNT="error.notValidEscrowAccount";
	public static final String SERVICERETURN_KEY_NOTVALIDESCROWSERIE="error.notValidEscrowSerie";
	public static final String SERVICERETURN_KEY_NOTVALIDESCROWGROUP="error.notValidEscrowGroup";
	public static final String SERVICERETURN_KEY_NOTVALIDESCROWALTSUM="error.notValidEscrowAltsum";
	public static final String SERVICERETURN_KEY_NOTVALIDESCROWCLISUM="error.notValidEscrowClisum";
	public static final String SERVICERETURN_KEY_NOTVALIDESCROWCUOSUM="error.notValidEscrowCuosum";
	public static final String SERVICERETURN_KEY_NOTVALIDESCROWPAGSUM="error.notValidEscrowPagsum";
	public static final String SERVICERETURN_KEY_NOTVALIDESCROWSEGSUM="error.notValidEscrowSegsum";
	public static final String SERVICERETURN_KEY_NOTESCROWCHECKSUMS="error.notEscrowChecksums";
	public static final String SERVICERETURN_KEY_NOTUPDATEESCROWCHECKSUM = "error.notUpdateEscrowChecksum";
	public static final String SERVICERETURN_KEY_NOTINSERTESCROWCHECKSUM = "error.notInsertEscrowChecksum";
	public static final String SERVICERETURN_KEY_NOTFOUNDFILEBYTOKEN = "error.notFoundFileByToken";
	public static final String SERVICERETURN_KEY_NOTVALIDREQUESTID = "error.notValidRequestId";
	public static final String SERVICERETURN_KEY_NOTFOUNDAUXILIARYTABLE = "error.notFoundAuxiliaryTable";
	public static final String SERVICERETURN_KEY_NOTFINDTOKENINDB = "error.notFindTokenInDB";
	public static final String SERVICERETURN_KEY_NOTADHERENTFORMEP = "error.notAdherentForMep";
	public static final String SERVICERETURN_KEY_NOTRESULTFORMEP = "error.notResultForMep";
	public static final String SERVICERETURN_KEY_FILENOTFOUNDINLIBRARY = "error.fileNotFoundInLibrary";
	public static final String SERVICERETURN_KEY_FAILEDUNZIPFILE = "error.failedUnzipFile";
	public static final String SERVICERETURN_KEY_ZIPMORETHANONE = "error.zipMoreThanOne";
	public static final String SERVICERETURN_KEY_INVALIDNUMRECAUD = "error.invalidNumRecaud";
	public static final String SERVICERETURN_KEY_INVALIDFILEDATA = "error.invalidFileData";
	public static final String SERVICERETURN_KEY_NOTPDFILEID = "error.notPDFileId";
	public static final String SERVICERETURN_KEY_NOTVALIDAGREEMENT = "error.notValidAgreement";
	public static final String SERVICERETURN_KEY_NOTVALIDAGREEMENTINFILE = "error.notValidAgreementInFile";
	public static final String SERVICERETURN_KEY_NOTFILEVALID = "error.notFileValid";
	public static final String SERVICERETURN_KEY_NOTCONFIRMFILE = "error.notConfirmFile";
	public static final String SERVICERETURN_KEY_PONOTVALIDPAYMENTTYPE = "error.poNotValidPaymentType";
	public static final String SERVICERETURN_KEY_NOTVALIDFILETYPELENGTH = "error.notValidFileTypeLength";
	public static final String SERVICERETURN_KEY_NOTVALIDCOMPRESSEDFILENAME = "error.notValidCompressedFileName";
	public static final String SERVICERETURN_KEY_NOTESCROWID = "error.notEscrowId";
	public static final String SERVICERETURN_KEY_NOTVALIDESCROWID = "error.notValidEscrowId";
	public static final String SERVICERETURN_KEY_NOTESCROWINTERFACE = "error.notEscrowInterface";
	public static final String SERVICERETURN_KEY_NOTVALIDESCROWINTERFACE = "error.notValidEscrowInterface";
	public static final String SERVICERETURN_KEY_NOTTRANSACTIONID = "error.notTransactionId";
	public static final String SERVICERETURN_KEY_TRANSACTIONIDNOTNUMERIC = "error.transactionIdNotNumeric";
	public static final String SERVICERETURN_KEY_NOTVALIDEXTENSION = "error.notValidExtension";
	public static final String SERVICERETURN_KEY_NOVOUCHERFORRECOVERED = "error.noVoucherForRecovered";
}