package ar.com.gestionit.cashmanagement.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.persistence.bo.PaymentListBO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentCbuInputListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentStatusInputListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentStatusInputListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentTypeListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentTypeListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DateUtil;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.NumberUtil;
import ar.com.gestionit.cashmanagement.util.ServiceUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public class PaymentListService extends AbstractService {

	/**
	 * This attribute is used to load the input parameters
	 */
	private PaymentListItemDTO input;

	/**
	 * Business object for this service
	 */
	private PaymentListBO bo;

	/**
	 * This is the payment list result to send
	 */
	private String xmlPaymentList;

	/**
	 * Pager attributes
	 */
	private String xmlPager;
	
	/***
	 * 
	 */
	private String counTotal;
	
	/**
	 * */
	private String sumTotal;


	@Override
	protected void initialize() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		bo = new PaymentListBO();
	}

	@Override
	protected void loadInputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Inicializando...");
		input = new PaymentListItemDTO();
		input.setAdherent(getDataFromVector(1));

		DefinedLogger.SERVICE.debug("Cargando lista de CBUs...");
		//Get payment cbu's list
		PaymentCbuInputListDTO cbuList = (PaymentCbuInputListDTO) bo.xmlToDto(getDataFromVector(2), PaymentCbuInputListDTO.class);
		if(cbuList != null && cbuList.getCbu() != null) {
			input.setLstCbus(cbuList.getCbu());
		} else {
			input.setLstCbus(new ArrayList<String>());
		}
		
		if(cbuList != null && cbuList.getCbu() != null && cbuList.getCbu().size() > 0) {
			Iterator<String> i = cbuList.getCbu().iterator();
			String item;
			while(i.hasNext()) {
				item = i.next();
				
				item = ServiceUtil.validateNumberAndLengthObligatory(item, 22, ServiceConstant.SERVICERETURN_KEY_INVALIDACCOUNT, ServiceConstant.SERVICERETURN_KEY_INVALIDACCOUNT);
				
				if(!NumberUtil.isNumeric(item)) {
					DefinedLogger.SERVICE.error(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_INVALIDCBU));
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDCBU);
				}
			}
		}
		
		DefinedLogger.SERVICE.debug("Cargando fechas...");
		String dateFrom = getDataFromVector(3);
		String dateTo = getDataFromVector(4);
		input.setDateFrom(ServiceUtil.validateDateDDMMYYYY(dateFrom, ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATEEXECFROM));
		input.setDateTo(ServiceUtil.validateDateDDMMYYYY(dateTo, ServiceConstant.SERVICERETURN_KEY_NOTVALIDDATEEXECTO));
		ServiceUtil.validateDateInterval(input.getDateFrom(), input.getDateTo());

		String b = getDataFromVector(5);
		b = ServiceUtil.validateNumberAndLength(b, 46, ServiceConstant.SERVICERETURN_KEY_PAYORDERFROMNOTVALID);
		input.setPayOrderFrom(b);
		
		String c = getDataFromVector(6);
		c = ServiceUtil.validateNumberAndLength(c, 46, ServiceConstant.SERVICERETURN_KEY_PAYORDERTONOTVALID);
		input.setPayOrderTo(c);
		
		ServiceUtil.validateNumberInterval(b, c);
		
		String d = getDataFromVector(7);
		ServiceUtil.validateLength(d, 46, ServiceConstant.SERVICERETURN_KEY_INVALIDBENEFICIARYNAME);
		if (d == null || "".equals(d)){
			d =  "0";
		}
		/*
		if (d != null){
			d = "%" + d + "%";
		}else{ 
			d =  "0";
		}
		*/
		
		input.setBeneficiaryName(d);
		
		DefinedLogger.SERVICE.debug("Cargando y procesando documento del beneficiario...");
		String beneficiaryDocument = getDataFromVector(8);
		beneficiaryDocument = ServiceUtil.validateNumberAndLength(beneficiaryDocument, 11, ServiceConstant.SERVICERETURN_KEY_INVALIDBENEFICIARYDOC);
		input.setBeneficiaryDocument(beneficiaryDocument);
		
		if(StringUtil.isEmpty(beneficiaryDocument) || beneficiaryDocument.trim().equals("0")) {
			beneficiaryDocument = "0";
			input.setBeneficiaryDocumentForLike(beneficiaryDocument);
		} else {
			input.setBeneficiaryDocumentForLike("%" + beneficiaryDocument + "%");
		}
					
		DefinedLogger.SERVICE.debug("Cargando y procesando nro. de op...");
		String payOrder = getDataFromVector(9);
		payOrder = ServiceUtil.validateNumberAndLength(payOrder, 46, ServiceConstant.SERVICERETURN_KEY_INVALIDPAYORDER);
		if(StringUtil.isEmpty(payOrder) || payOrder.trim().equals("0")) {
			input.setPayOrder("0");
		} else {
			input.setPayOrder(payOrder);
		}
		
		DefinedLogger.SERVICE.debug("Cargando y procesando montos...");
		String amount = getDataFromVector(10);
		amount = ServiceUtil.validateNumberAndLengthForAmounts(amount, 16, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTFROM);
		ServiceUtil.validateTwoDecimalsMax(amount, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTFROM);
		input.setAmountFrom(amount);
		
		amount = getDataFromVector(11);
		amount = ServiceUtil.validateNumberAndLengthForAmounts(amount, 16, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTTO);
		ServiceUtil.validateTwoDecimalsMax(amount, ServiceConstant.SERVICERETURN_KEY_INVALIDAMOUNTTO);
		input.setAmountTo(amount);
		
		ServiceUtil.validateNumberInterval(input.getAmountFrom(), input.getAmountTo());
		
		DefinedLogger.SERVICE.debug("Cargando tipos de pago...");
		//Get payment method list
		PaymentTypeListDTO ptl = (PaymentTypeListDTO)
				bo.xmlToDto(getDataFromVector(12), PaymentTypeListDTO.class);
		if(ptl != null && ptl.getList() != null) {
			input.setPaymentTypes(ptl.getList());
		} else {
			input.setPaymentTypes(new ArrayList<PaymentTypeListItemDTO>());
		}
		if(ptl != null && ptl.getList() != null && ptl.getList().size() > 0) {
			Iterator<PaymentTypeListItemDTO> i = ptl.getList().iterator();
			PaymentTypeListItemDTO item;
			while(i.hasNext()) {
				item = i.next();
				//obtenemos el indice del - y reemplazamos el contenido del atributo por el id
				int indice = item.number.indexOf("-");
				if(indice > 0){
					item.setSubnumber(item.number.substring(indice+1,item.number.length()).trim());
					item.setNumber(item.number.substring(0,indice).trim());					
				}
				else{
					DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_HYPHENFORMAT));
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_HYPHENFORMAT);
				}
				if(!NumberUtil.isNumeric(item.getNumber())) {
					DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_INVALIDPAYMENTTYPE));
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDPAYMENTTYPE);
				}
				if(!NumberUtil.isNumeric(item.getSubnumber())) {
					DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_INVALIDPAYMENTTYPE));
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDPAYMENTTYPE);
				}
			}
		}

		DefinedLogger.SERVICE.debug("Cargando estados...");
		//Get payment status list
		PaymentStatusInputListDTO psil = (PaymentStatusInputListDTO) bo.xmlToDto(getDataFromVector(13), PaymentStatusInputListDTO.class);
		if(psil != null && psil.getList() != null) {
			input.setStatuses(psil.getList());
		} else {
			input.setStatuses(new ArrayList<PaymentStatusInputListItemDTO>());
		}

		if(psil != null && psil.getList() != null && psil.getList().size() > 0) {
			Iterator<PaymentStatusInputListItemDTO> i = psil.getList().iterator();
			PaymentStatusInputListItemDTO item;
			while(i.hasNext()) {
				item = i.next();
				//obtenemos el indice del - y reemplazamos el contenido del atributo por el id
				int indice = item.number.indexOf("-");
				if(indice > 0){
					item.setSubnumber(item.number.substring(indice+1,item.number.length()).trim());
					item.setNumber(item.number.substring(0,indice).trim());					
				}
				else{
					DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_HYPHENFORMAT));
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_HYPHENFORMAT);
				}
				if(!NumberUtil.isNumeric(item.getNumber())) {
					DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_INVALIDSTATUS));
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDSTATUS);
				}
				if(!NumberUtil.isNumeric(item.getSubnumber())) {
					DefinedLogger.SERVICE.debug(ServiceReturnFactory.build(ServiceConstant.SERVICERETURN_KEY_INVALIDSTATUS));
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_INVALIDSTATUS);
				}
			}
		}

		

		//Enable pager in business object level
		bo.enablePager(getDataFromVector(14));

		/*
		 * Validating...
		 * ------------------------------------------------------------------------
		 */
		DefinedLogger.SERVICE.debug("Realizando validaciones...");
		if(input.getAccountNumber() == null || input.getAccountNumber().trim().equals("")) {
			input.setAccountNumber("0");
		}

		if(input.getAdherent() == null
				|| input.getAdherent().trim().equals(""))
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTADHERENT);

		if(input.getBeneficiaryDocument() == null
				|| input.getBeneficiaryDocument().trim().equals("")) {
			DefinedLogger.SERVICE.info("No se especifico Numero de Documento del beneficiario");
			input.setBeneficiaryDocument("0");
		}
		input.setBeneficiaryDocument(input.getBeneficiaryDocument().replaceAll("-", ""));

		if(input.getPayOrderFrom() == null
				|| input.getPayOrderFrom().trim().equals("")) {
			DefinedLogger.SERVICE.info("No se especifico Numero de Orden Desde");
			input.setPayOrderFrom("0");
		}

		if(input.getPayOrderTo() == null
				|| input.getPayOrderTo().trim().equals("")) {
			DefinedLogger.SERVICE.info("No se especifico Numero de Orden Hasta");
			input.setPayOrderTo("0");
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ServiceException {
		DefinedLogger.SERVICE.debug("Ejecutando querys y cargando listas...");
		/*
		 * Executing query....
		 * ------------------------------------------------------------------------
		 */
		//List<PaymentListItemDTO> list = (List<PaymentListItemDTO>) bo.findList(input);
		DefinedLogger.SERVICE.debug("Se invoca al Stored es ssconorp");
		String id = bo.executeSSCONORP(input);
		System.out.println("El Id obtenido por el Stored es: " + id);
		DefinedLogger.SERVICE.debug("El Id obtenido por el Stored es: " + id);
		input.setExecutionId(id);
		List<PaymentListItemDTO> list = null;
		if(!"".equals(id)) {
			list = bo.findSSCONORP(input);
			PaymentListItemDTO findCountSumTotal = bo.findCountSumTotal(input);
			setCounTotal(findCountSumTotal.getCountNum());
			setSumTotal(findCountSumTotal.getSumAmount());
		}

		/*
		 * Processing information....
		 * ------------------------------------------------------------------------
		 */
		
		
		
		if(list != null && list.size() > 0) {
			contentToExport.add(list);			
		}else {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_VOIDLIST);
		}
		PaymentListDTO pList = new PaymentListDTO();
		pList.setList(list);
		xmlPaymentList = bo.dtoToXML(pList);

		xmlPager = bo.getPagerAsXML();
	}

	@Override
	protected void loadOutputs() throws ServiceException {
		DefinedLogger.SERVICE.debug("Cargando outputs...");
		setDataInVector(15, xmlPaymentList);
		setDataInVector(16, xmlPager);
		setDataInVector(17, sumTotal);
		setDataInVector(18, counTotal);
		//Notify number of executed queries
		reportQueryNumber(bo.getQueryCounter());
	}

	public String getCounTotal() {
		return counTotal;
	}

	public void setCounTotal(String counTotal) {
		this.counTotal = counTotal;
	}

	public String getSumTotal() {
		return sumTotal;
	}

	public void setSumTotal(String sumTotal) {
		this.sumTotal = sumTotal;
	}
}