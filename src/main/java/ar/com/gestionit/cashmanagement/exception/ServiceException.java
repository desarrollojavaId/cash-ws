package ar.com.gestionit.cashmanagement.exception;

/**
 * This exception was created to notify that some problem exists
 * with some specified service
 * @author jdigruttola
 */
public class ServiceException extends Exception {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Service Return key
	 */
	private String key;
	
	/**
	 * Parameterized constructor
	 * @param key
	 */
	public ServiceException(String key) {
		super();
		this.key = key;
	}
	
	/**
	 * Parameterized constructor
	 * @param key
	 * @param cause
	 */
	public ServiceException(String key, Throwable cause) {
		super(cause);
		this.key = key;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}
}