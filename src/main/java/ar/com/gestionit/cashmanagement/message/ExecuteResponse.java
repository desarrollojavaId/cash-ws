//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2015.07.29 a las 10:41:55 AM ART 
//


package ar.com.gestionit.cashmanagement.message;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

import ar.com.gestionit.cashmanagement.message.entity.ArrayOfstring20;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Edatos" type="{http://spring.io/guides/gs-producing-web-service}ArrayOfstring_20"/>
 *         &lt;element name="Etdats" type="{http://spring.io/guides/gs-producing-web-service}ArrayOfstring_20"/>
 *         &lt;element name="Evalcs" type="{http://spring.io/guides/gs-producing-web-service}ArrayOfstring_20"/>
 *         &lt;element name="Evalls" type="{http://spring.io/guides/gs-producing-web-service}ArrayOfstring_20"/>
 *         &lt;element name="Ecantlin" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Dfesv" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="Dhosv" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Drqsv" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Grsco" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Drpgc" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Drsuc" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Drmod" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Drtrn" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Drrel" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Drmnc" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Drsdo" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Drsdd" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="Xml" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "edatos",
    "etdats",
    "evalcs",
    "evalls",
    "ecantlin",
    "dfesv",
    "dhosv",
    "drqsv",
    "grsco",
    "drpgc",
    "drsuc",
    "drmod",
    "drtrn",
    "drrel",
    "drmnc",
    "drsdo",
    "drsdd",
    "xml"
})
@XmlRootElement(name = "executeResponse", namespace = "")
public class ExecuteResponse {

    @XmlElement(name = "Edatos", required = true, namespace = "")
    protected ArrayOfstring20 edatos;
    @XmlElement(name = "Etdats", required = true, namespace = "")
    protected ArrayOfstring20 etdats;
    @XmlElement(name = "Evalcs", required = true, namespace = "")
    protected ArrayOfstring20 evalcs;
    @XmlElement(name = "Evalls", required = true, namespace = "")
    protected ArrayOfstring20 evalls;
    @XmlElement(name = "Ecantlin", namespace = "")
    protected short ecantlin;
    @XmlElement(name = "Dfesv", required = true, namespace = "")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dfesv;
    @XmlElement(name = "Dhosv", required = true, namespace = "")
    protected String dhosv;
    @XmlElement(name = "Drqsv", namespace = "")
    protected int drqsv;
    @XmlElement(name = "Grsco", required = true, namespace = "")
    protected String grsco;
    @XmlElement(name = "Drpgc", namespace = "")
    protected short drpgc;
    @XmlElement(name = "Drsuc", namespace = "")
    protected short drsuc;
    @XmlElement(name = "Drmod", namespace = "")
    protected short drmod;
    @XmlElement(name = "Drtrn", namespace = "")
    protected short drtrn;
    @XmlElement(name = "Drrel", namespace = "")
    protected short drrel;
    @XmlElement(name = "Drmnc", namespace = "")
    protected short drmnc;
    @XmlElement(name = "Drsdo", namespace = "")
    protected double drsdo;
    @XmlElement(name = "Drsdd", namespace = "")
    protected double drsdd;
    @XmlElement(name = "Xml", required = true, namespace = "")
    protected String xml;

    /**
     * Obtiene el valor de la propiedad edatos.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public ArrayOfstring20 getEdatos() {
        return edatos;
    }

    /**
     * Define el valor de la propiedad edatos.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public void setEdatos(ArrayOfstring20 value) {
        this.edatos = value;
    }

    /**
     * Obtiene el valor de la propiedad etdats.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public ArrayOfstring20 getEtdats() {
        return etdats;
    }

    /**
     * Define el valor de la propiedad etdats.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public void setEtdats(ArrayOfstring20 value) {
        this.etdats = value;
    }

    /**
     * Obtiene el valor de la propiedad evalcs.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public ArrayOfstring20 getEvalcs() {
        return evalcs;
    }

    /**
     * Define el valor de la propiedad evalcs.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public void setEvalcs(ArrayOfstring20 value) {
        this.evalcs = value;
    }

    /**
     * Obtiene el valor de la propiedad evalls.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public ArrayOfstring20 getEvalls() {
        return evalls;
    }

    /**
     * Define el valor de la propiedad evalls.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfstring20 }
     *     
     */
    public void setEvalls(ArrayOfstring20 value) {
        this.evalls = value;
    }

    /**
     * Obtiene el valor de la propiedad ecantlin.
     * 
     */
    public short getEcantlin() {
        return ecantlin;
    }

    /**
     * Define el valor de la propiedad ecantlin.
     * 
     */
    public void setEcantlin(short value) {
        this.ecantlin = value;
    }

    /**
     * Obtiene el valor de la propiedad dfesv.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDfesv() {
        return dfesv;
    }

    /**
     * Define el valor de la propiedad dfesv.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDfesv(XMLGregorianCalendar value) {
        this.dfesv = value;
    }

    /**
     * Obtiene el valor de la propiedad dhosv.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDhosv() {
        return dhosv;
    }

    /**
     * Define el valor de la propiedad dhosv.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDhosv(String value) {
        this.dhosv = value;
    }

    /**
     * Obtiene el valor de la propiedad drqsv.
     * 
     */
    public int getDrqsv() {
        return drqsv;
    }

    /**
     * Define el valor de la propiedad drqsv.
     * 
     */
    public void setDrqsv(int value) {
        this.drqsv = value;
    }

    /**
     * Obtiene el valor de la propiedad grsco.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGrsco() {
        return grsco;
    }

    /**
     * Define el valor de la propiedad grsco.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGrsco(String value) {
        this.grsco = value;
    }

    /**
     * Obtiene el valor de la propiedad drpgc.
     * 
     */
    public short getDrpgc() {
        return drpgc;
    }

    /**
     * Define el valor de la propiedad drpgc.
     * 
     */
    public void setDrpgc(short value) {
        this.drpgc = value;
    }

    /**
     * Obtiene el valor de la propiedad drsuc.
     * 
     */
    public short getDrsuc() {
        return drsuc;
    }

    /**
     * Define el valor de la propiedad drsuc.
     * 
     */
    public void setDrsuc(short value) {
        this.drsuc = value;
    }

    /**
     * Obtiene el valor de la propiedad drmod.
     * 
     */
    public short getDrmod() {
        return drmod;
    }

    /**
     * Define el valor de la propiedad drmod.
     * 
     */
    public void setDrmod(short value) {
        this.drmod = value;
    }

    /**
     * Obtiene el valor de la propiedad drtrn.
     * 
     */
    public short getDrtrn() {
        return drtrn;
    }

    /**
     * Define el valor de la propiedad drtrn.
     * 
     */
    public void setDrtrn(short value) {
        this.drtrn = value;
    }

    /**
     * Obtiene el valor de la propiedad drrel.
     * 
     */
    public short getDrrel() {
        return drrel;
    }

    /**
     * Define el valor de la propiedad drrel.
     * 
     */
    public void setDrrel(short value) {
        this.drrel = value;
    }

    /**
     * Obtiene el valor de la propiedad drmnc.
     * 
     */
    public short getDrmnc() {
        return drmnc;
    }

    /**
     * Define el valor de la propiedad drmnc.
     * 
     */
    public void setDrmnc(short value) {
        this.drmnc = value;
    }

    /**
     * Obtiene el valor de la propiedad drsdo.
     * 
     */
    public double getDrsdo() {
        return drsdo;
    }

    /**
     * Define el valor de la propiedad drsdo.
     * 
     */
    public void setDrsdo(double value) {
        this.drsdo = value;
    }

    /**
     * Obtiene el valor de la propiedad drsdd.
     * 
     */
    public double getDrsdd() {
        return drsdd;
    }

    /**
     * Define el valor de la propiedad drsdd.
     * 
     */
    public void setDrsdd(double value) {
        this.drsdd = value;
    }

    /**
     * Obtiene el valor de la propiedad xml.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXml() {
        return xml;
    }

    /**
     * Define el valor de la propiedad xml.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXml(String value) {
        this.xml = value;
    }

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ExecuteResponse [edatos=" + edatos + ", etdats=" + etdats + ", evalcs=" + evalcs + ", evalls=" + evalls
				+ ", ecantlin=" + ecantlin + ", dfesv=" + dfesv + ", dhosv=" + dhosv + ", drqsv=" + drqsv + ", grsco="
				+ grsco + ", drpgc=" + drpgc + ", drsuc=" + drsuc + ", drmod=" + drmod + ", drtrn=" + drtrn + ", drrel="
				+ drrel + ", drmnc=" + drmnc + ", drsdo=" + drsdo + ", drsdd=" + drsdd + ", xml=" + xml + "]";
	}

}
