package ar.com.gestionit.cashmanagement.exporter;

import ar.com.gestionit.cashmanagement.exception.ServiceException;

public interface IExporter {
	/**
	 * Exporter formats
	 */
	public static final int FORMAT_EXCEL = 0;
	public static final int FORMAT_TXT = 1;
	public static final int FORMAT_CSV = 2;
	
	public static final String DYNAMIC_TITLE_SUFIX = "Title";
	
	/**
	 * This method is implemented by each exporter implementation
	 * according to the exporter type
	 */
	public void export() throws ServiceException;
}
