package ar.com.gestionit.cashmanagement.exporter;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IOUtils;

import ar.com.gestionit.cashmanagement.ServiceExporterLabel;
import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.FileExporterDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.PathUtil;
import ar.com.gestionit.cashmanagement.util.StringUtil;

/**
 * Excel exporter
 * @author jdigruttola
 */
@SuppressWarnings("deprecation")
public class ExcelExporter extends AbstractExporter {
	/**
	 * Default name
	 */
	protected static final String NAME = "Reporte";
	
	/**
	 * Font by default to the file to export
	 */
	protected static final String FONT_DEFAULT = "Calibri";
	
	/**
	 * This constant is used to generate the title key and get the real title
	 */
	protected static final String TITLE_KEY_SUFFIX = ".title";

	/**
	 * This attribute contains the current row number enabled
	 * to write in the Excel sheet
	 */
	protected int rowCounter = 4;
	
	/**
	 * POI entities
	 */
	protected HSSFWorkbook wb;
	protected Sheet sheet;
	protected Map<String, CellStyle> styles;

	/**
	 * Parameterized constructor
	 * @param list
	 */
	public ExcelExporter(List<List<? extends IDTO>> list, FileExporterDTO dto) {
		super(list, dto);
	}

	/**
	 * This method generates the grid
	 */
	private void generateGrid() throws ServiceException {
		for(int i = 0; i < list.size(); i++) {
			generateGridHeader(headerList.get(i));
			generateGridContent(list.get(i));
		}
	}

	/**
	 * This method generates the grid header
	 * @param headers
	 */
	private void generateGridHeader(List<String> headers) throws ServiceException {
		if(headers == null)
			return;
		
		Row row;
		Cell cell;
		//Generate header row
		row = sheet.createRow(rowCounter++);
		row.setHeightInPoints(45);
		for(int i = 0; i < headers.size(); i++) {
			cell = row.createCell(i);
			cell.setCellValue(StringUtil.notNull(headers.get(i)));
			cell.setCellStyle(styles.get("header"));
		}
	}

	/**
	 * This method generates the grid content
	 * @param content
	 */
	private void generateGridContent(List<? extends IDTO> content) throws ServiceException {
		try {
			if(content == null || content.size() == 0)
				return;

			Row row;
			Cell cell;
			Field[] fields;
			Field field;
			String fieldValue;
			Method method;
			int cellCounter;
			Class<? extends IDTO> c = content.get(0).getClass();

			//Generate row
			for (int i = 0; i < content.size(); i++) {
				row = sheet.createRow(rowCounter++);
				row.setHeightInPoints(20);

				fields = c.getDeclaredFields();

				cellCounter = 0;

				//Generate each column in the row
				for(int j = 0; j < fields.length; j++) {
					field = fields[j];

					//Validate if the field is to be exported
					if(isFieldToExport(field)) {
						method = getMethod(c, field);
						if(method != null
								&& isValid(field.getAnnotation(FieldToExport.class), c, field, content.get(i))) {
							cell = row.createCell(cellCounter++);
							cell.setCellType(Cell.CELL_TYPE_STRING);
							cell.setCellStyle(styles.get("cell"));
							
							//Execute the attribute setter
							fieldValue = StringUtil.notNull(
									String.valueOf(
											method.invoke(content.get(i))));

							//Validate if the value is a code to be interpreted
							fieldValue = validateMappedValue(c, field, fieldValue);

							cell.setCellValue(fieldValue);
						}
					}
				}
			}
			sheet.createRow(rowCounter++);
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_EXPORTCONTENT, e);
		}
	}

	/**
	 * This method generates the title cell
	 */
	private void generateTitle() {
		String title = null;
		
		//Try to get the title
		if(list != null
				&& list.size() > 0
				&& list.get(0) != null
				&& list.get(0).size() > 0) {
			String titleKey = list.get(0).get(0).getClass().getSimpleName() + TITLE_KEY_SUFFIX;
			title = StringUtil.notNull(
							ServiceExporterLabel.getProperty(titleKey));
		}
		
		//Validate if the title was obtained
		if(title == null) {
			DefinedLogger.SERVICE.error("ExcelExporter.generateTitle: No esta definido el titulo en service.export.label.properties para esta exportacion");
			title = NAME;
		}
		
		//Generate the cell with the title
		Row row = sheet.createRow(rowCounter++);
		row.setHeightInPoints(45);
		Cell cell = row.createCell(0);
		cell.setCellValue(title);
		cell.setCellStyle(styles.get("title"));
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, headerList.size() - 1));
		
		rowCounter++;
	}
	
	/**
	 * This method initializes all you need to export by Excel
	 */
	private void initialize() {
		//Define and initialize
		wb = new HSSFWorkbook();
		styles = createStyles(wb);
		sheet = wb.createSheet(NAME);

		//Set sheet properties
		sheet.setFitToPage(true);
		sheet.setHorizontallyCenter(true);
	}
	
	/**
	 * This method generate the bank's logo and attach it in the sheet
	 * @throws IOException
	 */
	private void generateLogo() throws IOException {
		InputStream is = new FileInputStream(PathUtil.getResourcePath() + "/images/exporter.excel.logo.jpg");
		
		byte [] bytes = IOUtils.toByteArray(is); 
		int pictureIndex = wb.addPicture(bytes, Workbook.PICTURE_TYPE_JPEG);
		is.close();
		
		CreationHelper helper = wb.getCreationHelper();
		Drawing drawingPatriarch = sheet.createDrawingPatriarch();
		
		ClientAnchor anchor = helper.createClientAnchor();
		anchor.setCol1(0);
		anchor.setCol2(1);
		anchor.setRow1(0);
		anchor.setRow2(1);

		Picture pict = drawingPatriarch.createPicture(anchor, pictureIndex);
		pict.resize(0.9);
	}
	
	/**
	 * This method loads the variable fileContent
	 * @throws IOException
	 */
	private void persist() throws IOException {
//		ByteArrayOutputStream out = new ByteArrayOutputStream();
//		wb.write(out);
//		out.flush();
//		fileContent = out.toByteArray();
//		out.close();
		FileOutputStream out = new FileOutputStream("C:/javi/test.xls");
		wb.write(out);
		out.flush();
		out.close();
	}

	@Override
	public void generateContent() throws ServiceException {
		try {
			//Initialize Excel sheet and all you need
			initialize();

			//Generate file logo
			generateLogo();
			
			//Generate title row
			generateTitle();
			
			//Generate all the grid according to the content in the lists
			generateGrid();
			
			//Persist file content object (bytes)
			persist();
		}catch (Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_EXPORTCONTENT, e);
		}
	}
	
	/**
	 * Create a library of cell styles
	 */
	protected static Map<String, CellStyle> createStyles(Workbook wb){
		Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
		CellStyle style;
		Font titleFont = wb.createFont();
		titleFont.setFontName(FONT_DEFAULT);
		titleFont.setFontHeightInPoints((short)12);
		titleFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFont(titleFont);
		styles.put("title", style);

		Font monthFont = wb.createFont();
		monthFont.setFontHeightInPoints((short)11);
		monthFont.setColor(IndexedColors.WHITE.getIndex());
		monthFont.setFontName(FONT_DEFAULT);
		monthFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(monthFont);
		style.setWrapText(true);
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		styles.put("header", style);

		Font filterFont = wb.createFont();
		filterFont.setFontHeightInPoints((short)11);
		filterFont.setColor(IndexedColors.WHITE.getIndex());
		filterFont.setFontName(FONT_DEFAULT);
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(filterFont);
		style.setWrapText(true);
		styles.put("headerFilter", style);

		Font borderFont = wb.createFont();
		borderFont.setFontHeightInPoints((short)10);
		borderFont.setColor(IndexedColors.WHITE.getIndex());
		borderFont.setFontName(FONT_DEFAULT);
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(borderFont);
		style.setWrapText(true);
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		styles.put("headerBorder", style);

		Font cellFont = wb.createFont();
		cellFont.setFontHeightInPoints((short)11);
		cellFont.setColor(IndexedColors.BLACK.getIndex());
		cellFont.setFontName(FONT_DEFAULT);
		style = wb.createCellStyle();
		style.setFont(cellFont);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setWrapText(true);
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		styles.put("cell", style);

		Font errorFont = wb.createFont();
		errorFont.setFontHeightInPoints((short)11);
		errorFont.setColor(IndexedColors.WHITE.getIndex());
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.RED.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(errorFont);
		style.setWrapText(true);
		styles.put("error", style);

		Font yellowBack = wb.createFont();
		style = wb.createCellStyle();
		style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(yellowBack);
		style.setWrapText(true);
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		styles.put("yellowBack", style);

		return styles;
	}
}

