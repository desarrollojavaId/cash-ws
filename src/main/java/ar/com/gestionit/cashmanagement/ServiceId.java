package ar.com.gestionit.cashmanagement;

import java.util.Properties;

import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.FactoryUtil;

/**
 * This class is used to get the service identifier by default
 * 
 * NOTE: in SSTRANS table, we trace each request of the web service.
 * For each traced register, we must specify an identifier in STKTRS field.
 * The service implementation can change it, but the identifiers by default are here
 * 
 * @author jdigruttola
 */
public class ServiceId {
	
	/**
	 * This constant contains the properties file name
	 */
	public static final String PROPERTIES_FILE = "service.id.properties";
	
	/**
	 * This static variable contains all the Service IDs possibles
	 */
	public static Properties properties = null;

	/**
	 * This method loads the application properties in main memory
	 * @throws Exception
	 */
	public static void loadPorperties() throws Exception {
		if(properties != null)
			return;

		if(properties != null) {
			DefinedLogger.CONTEXT.warn("Las propiedades ya estan cargadas en la aplicacion");
		} else {
			properties = FactoryUtil.loadProperties(PROPERTIES_FILE);
		}
	}

	/**
	 * This method gets a property of the properties
	 * of the application according to the key given by argument
	 * @param key
	 * @return
	 */
	public static String getProperty(String key) {
		if(properties == null)
			return null;
		return properties.getProperty(key);
	}

	/**
	 * This method gets a property of the properties
	 * of the application according to the key given by argument
	 * as integer
	 * @param key
	 * @return
	 */
	public static int getPropertyAsInteger(String key) {
		return Integer.parseInt(getProperty(key));
	}

}
