package ar.com.gestionit.cashmanagement.cache;

/**
 * This interface is used to standardize the cache implementations
 * @author jdigruttola
 * @param <T>
 * @param <U>
 */
public interface ICache<T, U> {
	
	/**
	 * This method returns a value according to the specified key
	 * 
	 * NOTE: Please, remember the cache concept! This method must
	 * resolve the request. If the cache has not the value,
	 * this must have the logic to obtain the value and cached it.
	 * @param key
	 * @return
	 */
	public T getValue(U key);
}