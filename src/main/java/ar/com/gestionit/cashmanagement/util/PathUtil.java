package ar.com.gestionit.cashmanagement.util;

import java.io.File;
import java.net.URL;

/**
 * Path Related utilities
 * @author jdigruttola
 *
 */
public class PathUtil {
	
	/**
	 * This method builds the project root path
	 * @return
	 */
	public static String getRootPath() {
		URL url = PathUtil.class.getResource(PathUtil.class.getSimpleName() + ".class");
		File file = new File( url.getFile().replaceAll("%20", " "));
		file = new File(file.getParent());
		file = new File(file.getParent());
		file = new File(file.getParent());
		file = new File(file.getParent());
		file = new File(file.getParent());
		file = new File(file.getParent());
		file = new File(file.getParent());
		file = new File(file.getParent());

		return file.toString();
	}
	
	/**
	 * This method builds the project root path
	 * @return
	 */
	public static String getResourcePath() {
		return getRootPath() + "/WEB-INF/classes";
	}

}
