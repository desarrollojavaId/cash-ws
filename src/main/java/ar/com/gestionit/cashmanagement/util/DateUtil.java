package ar.com.gestionit.cashmanagement.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

/**
 * This class is used for date management 
 * @author jdigruttola
 */
public class DateUtil {
	
	/**
	 * This method converts a given date to XMLGregorianCalendar object 
	 * @param date
	 * @return XMLGregorianCalendar
	 * @throws DatatypeConfigurationException
	 */
	public static XMLGregorianCalendar getAsXMLGregorianCalendar(Date date) 
			throws DatatypeConfigurationException {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(new Date());
		return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
	}
	
	/**
	 * Devuelve un string con la fecha formateada para los campos de "fecha" especificados.
	 * @param date
	 * @return
	 */
	public static String dateForOutput(String date) {
		if(StringUtil.isEmpty(date))
			return "";
		
		String out;
		try{
			SimpleDateFormat sdfInput = ServiceConstant.SDF_DATE_FOR_INPUT;
			SimpleDateFormat sdfOutput = ServiceConstant.SDF_DATE_FOR_OUTPUT;
			Date convertedCurrentDate = sdfInput.parse(date);
			out =sdfOutput.format(convertedCurrentDate);	
		}
		 catch(Exception e){
			 return "";
		 }
	    return 	out;
	}
	/**
	 * Devuelve un string con la fecha formateada para los campos de "fecha" especificados.
	 * @param date
	 * @return
	 */
	public static String numericDateForOutput(String date) {
		if(StringUtil.isEmpty(date))
			return "";
		
		String out;
		try{
			SimpleDateFormat sdfInput = ServiceConstant.SDF_YYYYMMDD;
			SimpleDateFormat sdfOutput = ServiceConstant.SDF_DATE;
			Date convertedCurrentDate = sdfInput.parse(date);
			out =sdfOutput.format(convertedCurrentDate);	
		}
		 catch(Exception e){
			 return "";
		 }
	    return 	out;
	}
	
	/**
	 * Devuelve un string con la fecha formateada para los campos de "fecha" 
	 * según el formato de parametros de entrada y salida recibidos en el método.
	 * @param date
	 * @param patternInput
	 * @param patternOutput
	 * @return
	 */
	public static String changeFormat(String date, String patternInput, String patternOutput) {
		String out;
		try{
			SimpleDateFormat sdfInput = new SimpleDateFormat(patternInput);
			SimpleDateFormat sdfOutput = new SimpleDateFormat(patternOutput);
			Date convertedCurrentDate = sdfInput.parse(date);
			out =sdfOutput.format(convertedCurrentDate);	
		}
		 catch(Exception e){
			 return "";
		 }
	    return 	out;
	}
	
	/**
	 * This method returns a date with the specified format for the response
	 * @param date
	 * @return
	 */
	public static String normalizeDateFormat(String date){
		String dateFormat="";
		if (date != null){
			try {
				Date dateFecEmi = ServiceConstant.SDF_DATE_FOR_INPUT.parse(date);	
				dateFormat = ServiceConstant.SDF_DATE.format(dateFecEmi);
			} catch (ParseException e) {}
		}
		return dateFormat;
	}
	
	/**
	 * Devuelve un string con la hora formateada para los campos "Horas" especificados
	 * @param hour
	 * @return
	 */
	public static String hourForOutput(String hour) {
		String out;
		try{
			SimpleDateFormat sdfInput = ServiceConstant.SDF_HOUR_FOR_INPUT;
			SimpleDateFormat sdfOutput = ServiceConstant.SDF_HOUR;			
			Date convertedCurrentDate = sdfInput.parse(hour);
			out =sdfOutput.format(convertedCurrentDate);
	    }
		catch(Exception e){
			return "";
		}
	return 	out;
		
	}
	
	/**
	 * Devuelve un string con la hora formateada para los campos de "Hora" 
	 * según el formato de parametros de entrada y salida recibidos en el método. 
	 * @param hour
	 * @param patternInput
	 * @param patternOutput
	 * @return
	 */
	public static String hourForOutput(String hour, String patternInput, String patternOutput) {
		String out;
		try{
			SimpleDateFormat sdfInput = new SimpleDateFormat(patternInput);
			SimpleDateFormat sdfOutput = new SimpleDateFormat(patternOutput);
			Date convertedCurrentDate = sdfInput.parse(hour);
			out =sdfOutput.format(convertedCurrentDate);
	    }
		catch(Exception e){
			return "";
		}
		return 	out;
	}
	
	/**
	 * This static method validates if the string given by argument is a date
	 * according to the format loaded in the specified SimpleDateFormat given.
	 * If the string is NULL, the method also returns TRUE because of this
	 * is used by non mandatory inputs
	 * @param date
	 * @param sdf
	 * @return TRUE if the string is a date or NULL and FALSE if not
	 */
	public static boolean isDateOrNull(String date, SimpleDateFormat sdf) {
		if(date == null )
			return true;
		return isDate(date, sdf);
	}
	
	/**
	 * This static method validates if the string given by argument is a date
	 * according to the format loaded in the specified SimpleDateFormat given.
	 * @param date
	 * @param sdf
	 * @return TRUE if the string is a date and FALSE if not or if this is NULL
	 */
	public static boolean isDate(String date, SimpleDateFormat sdf) {
		if(date == null )
			return false;
		try {
			sdf.parse(date);
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	/**
	 * This static method returns a date according to the input format
	 * @param date
	 * @param sdf
	 * @return
	 */
	public static Date getDate(String date, SimpleDateFormat sdf) {
		if(date == null )
			return null;
		try {
			String dateTest = date.substring(8);
			if (!NumberUtil.isNumeric(dateTest)){
				return null;}
			sdf.setLenient(false);
			return sdf.parse(date);
		} catch(Exception e) {
			return null;
		}
	}
	
	/**
	 * This method validates if the date interval is right
	 * @param from
	 * @param to
	 * @return
	 */
	public static boolean validateDateInterval(Date from, Date to) {
		if(from == null || to == null)
			return true;
		
		return !from.after(to);
	}
}