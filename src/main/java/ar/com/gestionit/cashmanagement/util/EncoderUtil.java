package ar.com.gestionit.cashmanagement.util;

import org.apache.commons.codec.binary.Base64;

public class EncoderUtil {
	
	/**
	 * This method converts a string to base 64
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public static String stringToBase64(String source) throws Exception {
		//Validate if the file content exists
		if(StringUtil.isEmpty(source))
			return "";
		
		//Encoding...
		byte[] bytes = source.getBytes();
		bytes = Base64.encodeBase64(bytes);
		return new String(bytes);
	}
	
	 public static String hexToString(String hex) {
		  StringBuilder sb = new StringBuilder();
		  StringBuilder temp = new StringBuilder();
		  
		  for( int i=0; i<hex.length()-1; i+=2 ){
			  
		      //grab the hex in pairs
		      String output = hex.substring(i, (i + 2));
		      //convert hex to decimal
		      int decimal = Integer.parseInt(output, 16);
		      //convert the decimal to character
		      sb.append((char)decimal);
		      temp.append(decimal);
		  }
		  return sb.toString();
	  }
	
	/**
	 * This method converts base 64 to string
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public static String base64ToString(String source) throws Exception {
		//Validate if the file content exists
		if(StringUtil.isEmpty(source))
			return "";
		
		//Decoding...
		byte[] bytes = Base64.decodeBase64(source);
		return new String(bytes);
	}
}