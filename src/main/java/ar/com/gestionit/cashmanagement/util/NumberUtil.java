package ar.com.gestionit.cashmanagement.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

/**
 * This class is used for number management 
 * @author jdigruttola
 */


public class NumberUtil {

	/**
	 * This method formats the number given by argument as String
	 * @param s
	 * @return
	 */
	public static boolean twoDecimalMax(String s){
		if (s == null) 
			return false;
		if (s.equals("-1"))
			return true;
		return s.matches("[0-9]+([.][0-9]{1,2})?");

	}

	public static String twoDecimalMax(float n){
		NumberFormat df = NumberFormat.getInstance();			
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		return df.format(n);

	}


	/**
	 * This static method validates if the value given by argument
	 * is number or not according to this pattern
	 * @param s
	 * @return
	 */
	public static boolean isNumeric(String s) {
		if(s == null)
			return false;
		return s.matches("[-+]?\\d*\\.?\\d+");  
	}

	/**
	 * This static method validates if the value given by argument
	 * is number or not according to this pattern
	 * @param s
	 * @return TRUE is the given argument is a number or NULL. FALSE if not
	 */
	public static boolean isNumericOrNull(String s) {
		if(s == null)
			return true;
		return isNumeric(s);  
	} 

	/**
	 * This static method validates if the value given by argument
	 * is number or not according to this pattern
	 * @param s
	 * @return
	 */
	public static String getNumberOrZero(String s, String errorKey) throws ServiceException {
		if(StringUtil.isEmpty(s))
			return "0";
		if(!isNumeric(s))
			throw new ServiceException(errorKey);
		return s;
	}
	/**
	 * This static method validates if the value given by argument
	 * is number or not according to this pattern. This is used
	 * in specific cases for amounts.
	 * @param s
	 * @return
	 */
	public static String getNumberOrZeroForAmounts(String s, String errorKey) throws ServiceException {
		if(StringUtil.isEmpty(s))
			return "-1";
		if(!isNumeric(s))
			throw new ServiceException(errorKey);
		return s;
	}

	/**
	 * This method validates if the number interval is right
	 * @param f
	 * @param t
	 * @return TRUE if the limit "from" is less or equal than the limit "to"
	 */
	public static boolean validateNumberInterval(float f, float t) {
		return f <= t;
	}

	/**
	 * This method validates if the number interval is right
	 * @param f
	 * @param t
	 * @return TRUE if the limit "from" is less or equal than the limit "to"
	 */
	public static boolean validateNumberInterval(int f, int t) {
		return validateNumberInterval((float)f, (float)t);
	}

	/**
	 * This method validates if the number interval is right
	 * @param f
	 * @param t
	 * @return TRUE if the limit "from" is less or equal than the limit "to"
	 */
	public static boolean validateNumberInterval(long f, long t) {
		return validateNumberInterval((float)f, (float)t);
	}

	/**
	 * This method formats the number to #.NN
	 * @param n
	 * @return a string with format #.NN (always two decimals)
	 */
	public static String doubleFormat(double n) {
		NumberFormat df = NumberFormat.getInstance();			
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		return df.format(n);
	}

	/**
	 * This method formats the number to #.NN
	 * @param n
	 * @return a string with format #.NN (always two decimals)
	 */
	public static String doubleFormat(String s) {
		double n;
		try {
			n = Double.parseDouble(s);
		} catch (Exception e) {
			n = 0;
		}
		NumberFormat df = NumberFormat.getInstance();			
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		return df.format(n);
	}

	/**
	 * This method convert a string to number with format #.NN
	 * @param n
	 * @return
	 */
	public static double toNumber(String n) throws ServiceException {
		NumberFormat df = NumberFormat.getInstance();			
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		try {
			return df.parse(n).doubleValue();
		} catch (ParseException e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		}
	}
	
	public static String amountFormat(String n) {
		if(n == null || !NumberUtil.isNumeric(n))
			return "0.00";
		double number = Double.parseDouble(n);
		return amountFormat(number);
	}
	
	public static String amountFormat(float n) {
		DecimalFormat format = new DecimalFormat("#0.00");
		String number = format.format(n);
		number = number.replace(".", "");
		number = number.replace(",", "");
		if(number != null && number.length() > 2) {
			number = number.substring(0, number.length()-2) + "." + number.substring(number.length()-2);
		}
		return number;
	}
	
	public static String amountFormat(double n) {
		DecimalFormat format = new DecimalFormat("#0.00");
		String number = format.format(n);
		number = number.replace(".", "");
		number = number.replace(",", "");
		if(number != null && number.length() > 2) {
			number = number.substring(0, number.length()-2) + "." + number.substring(number.length()-2);
		}
		return number;
	}
	
	/**
	 * SOLO PARA DOS DECIMALES
	 * @param n
	 * @return
	 */
	public static String amountFormatExporter(String n) {
		if(n == null)
			return "0,00";
		if(n.length() >= 3) {
			n = n.substring(0, n.length()-3) + n.substring(n.length()-3).replace(",", ".");
			n = n.substring(0, n.length()-3).replace(",", "").replace(".", "") + n.substring(n.length()-3);
		}
		double number = Double.parseDouble(n);
		return amountFormatExporter(number);
	}
	
	public static String amountFormatExporter(float n) {
		DecimalFormat format = new DecimalFormat("#0.00");
		String number = format.format(n);
		number = number.replace(".", "");
		number = number.replace(",", "");
		if(number != null && number.length() > 2) {
			number = number.substring(0, number.length()-2) + "," + number.substring(number.length()-2);
		}
		if(number != null && number.length() > 6) {
			number = number.substring(0, number.length()-6) + "." + number.substring(number.length()-6);
		}
		if(number != null && number.length() > 10) {
			number = number.substring(0, number.length()-10) + "." + number.substring(number.length()-10);
		}
		if(number != null && number.length() > 14) {
			number = number.substring(0, number.length()-14) + "." + number.substring(number.length()-14);
		}
		return number;
	}
	
	public static String amountFormatExporter(double n) {
		DecimalFormat format = new DecimalFormat("#0.00");
		String number = format.format(n);
		number = number.replace(".", "");
		number = number.replace(",", "");
		if(number != null && number.length() > 2) {
			number = number.substring(0, number.length()-2) + "," + number.substring(number.length()-2);
		}
		if(number != null && number.length() > 6) {
			number = number.substring(0, number.length()-6) + "." + number.substring(number.length()-6);
		}
		if(number != null && number.length() > 10) {
			number = number.substring(0, number.length()-10) + "." + number.substring(number.length()-10);
		}
		if(number != null && number.length() > 14) {
			number = number.substring(0, number.length()-14) + "." + number.substring(number.length()-14);
		}
		return number;
	}
}