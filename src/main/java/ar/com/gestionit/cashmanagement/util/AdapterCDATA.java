package ar.com.gestionit.cashmanagement.util;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class AdapterCDATA extends XmlAdapter<String, String> {

	@Override
	public String marshal(String arg0) throws Exception {
		System.out.println(arg0);
		if(!arg0.contains("CDATA")) {
			return "<![CDATA[" + arg0 + "]]>";
		}
		return arg0;
	}
	@Override
	public String unmarshal(String arg0) throws Exception {
		return arg0;
	}
}