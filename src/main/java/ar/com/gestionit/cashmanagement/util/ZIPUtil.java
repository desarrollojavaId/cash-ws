package ar.com.gestionit.cashmanagement.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class ZIPUtil {


	private static final String LOG_UNZIP_PREFIX = "Unzip Process: ";

	/**
	 * This constant is used to know the extension length for ZIP
	 */
	private static final int ZIP_EXTENSION_OFFSET = 3;

	/**
	 * This constant is the file extension for a ZIP file
	 */
	private static final String ZIP_EXTENSION = "ZIP";

	/**
	 * This method validate if the file type is ZIP according to the file name specified by argument.
	 * For example:
	 * 	- helloWorld.zip => this returns TRUE
	 * 	- helloWorld.pepe => this returns FALSE
	 * @param fileName
	 * @return
	 */
	public static boolean isZipFile(String fileName) {
		if(fileName.length() >= ZIP_EXTENSION_OFFSET
				&& fileName.substring(fileName.length() - ZIP_EXTENSION_OFFSET).equalsIgnoreCase(ZIP_EXTENSION)) {
			return true;
		}
		return false;
	}

	public static String uncompress(byte[] file) throws Exception {
		if(file == null)
			return null;
		return uncompress(new ByteArrayInputStream(file));
	}
	
	
	/**
	 * This method uncompresses a file and returns its content as String object
	 * 
	 *  IMPORTANT NOTE: now, we are using java.util.zip.ZipEntry to uncompress, but there is a better way if we will use
	 *  Apache Commons-compress API. This dependency contemplates more cases to uncompress than the current implementation.
	 *  At present, we don't do the changes because all is running well and it seems unnecessary at the moment.
	 *  But, if you have problems in the future, you contemplate this option. Below, I write Stackoverflow link about this.
	 *  http://stackoverflow.com/questions/15521966/zipinputstream-getnextentry-returns-null-on-some-zip-files
	 * 
	 * 
	 * @param is
	 * @return
	 * @throws Exception
	 */
	public static String uncompress(InputStream is) throws Exception {
		//Validate if the argument is right
		if(is == null)
			throw new Exception("InputStream is NULL in ZIPUtil.uncompress() and this method does not allow the argument in NULL");

		//Define and initialize variables
		byte[] buffer = new byte[1024];
		byte[] fileContentBytes = null;
		ByteArrayOutputStream baos;
		int len;
		int iterationNumber = 0;

		//get the zip file content
		ZipInputStream zis = new ZipInputStream(is);
		//get the zipped file list entry
		ZipEntry ze = zis.getNextEntry();

		while(ze!=null){
			//Validate if there are more than one file in the ZIP file
			if(iterationNumber > 0) {
				zis.closeEntry();
				zis.close();
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ZIPMORETHANONE);
			}
			
			DefinedLogger.SERVICE.info(LOG_UNZIP_PREFIX + "Descomprimiendo archivo " + ze.getName());
			baos = new ByteArrayOutputStream((int) ze.getSize());

			while ((len = zis.read(buffer)) > 0) {
				baos.write(buffer, 0, len);
			}

			fileContentBytes = baos.toByteArray();
			baos.close();
			
			iterationNumber++;
			ze = zis.getNextEntry();
		}

		zis.closeEntry();
		zis.close();
		
		DefinedLogger.SERVICE.info(LOG_UNZIP_PREFIX + "se descomprimio con exito el archivo");
		return new String(fileContentBytes);
	}
}