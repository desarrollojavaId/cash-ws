package ar.com.gestionit.cashmanagement.util;

/**
 * This class is used for date management 
 * @author jdigruttola
 */
public class StringUtil {
	
	/**
	 * End of line types
	 */
	public static final String EOL01 = "\r\n";
	public static final String EOL02 = "\n";
	
	/**
	 * This static validates if the string given by argument
	 * is interpreted as empty or not
	 * @param s
	 * @return
	 */
	public static boolean isEmpty(String s) {
		return s == null || s.trim().equals("");
	}
	
	/**
	 * This method is used to avoid NULL
	 * @param s
	 * @return
	 */
	public static String notNull(String s) {
		return s == null || s.trim().equalsIgnoreCase("null") ? "" : s;
	}
	
	/**
	 * This method validates the value given by argument.
	 * If this value is empty or is NULL, so the method will return 0.
	 * In the other hand, the method will return the value. 
	 * @param s
	 * @return
	 */
	public static String valueOrZero(String s) {
		return s == null || s.trim().equalsIgnoreCase("null") || s.trim().equals("") ? "0" : s;
	}
	
	/**
	 * This static validates if the string given by argument
	 * is interpreted as empty or not
	 * @param s
	 * @return
	 */
	public static boolean isEmptyOrZero(String s) {
		return s == null || s.trim().equals("") || s.trim().equals("0");
	}

	/**
	 * This method autocompletes a given string with the specified value
	 * to the indicated size
	 * @param obj
	 * @param size
	 * @param value
	 * @return
	 */
	public static String autocomplete(String obj, int size, char value) {
		//Validate size
		if(size <= 0)
			return "";

		StringBuffer result = new StringBuffer();
		int difference;

		//Validate if the string is NULL
		if(obj == null) {
			difference = size;
		} else {
			difference = size - obj.length();
			
			if(difference <= 0)
				return obj;
		}
		
		//Autocomplete the string
		for(int i = 0; i < difference; i++)
			result.append(value);

		//Add the real string
		if(difference < size)
			result.append(obj);

		return result.toString();
	}
	
	/**
	 * This method splits a string by end of line
	 * @param content
	 * @return
	 */
	public static String[] splitByEOL(String content) {
		if(content.contains(EOL01))
			return content.split(EOL01);
		return content.split(EOL02);
	}
}