package ar.com.gestionit.cashmanagement.webservice;

import java.util.ArrayList;

import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import ar.com.gestionit.cashmanagement.factory.ServiceFactory;
import ar.com.gestionit.cashmanagement.factory.ServiceReturnFactory;
import ar.com.gestionit.cashmanagement.message.ExecuteRequest;
import ar.com.gestionit.cashmanagement.message.ExecuteResponse;
import ar.com.gestionit.cashmanagement.message.entity.ArrayOfstring20;
import ar.com.gestionit.cashmanagement.service.IService;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.service.util.ServiceReturn;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.StringUtil;
import ar.com.gestionit.cashmanagement.util.XMLUtil;;

@Endpoint
public class NsbtEndpoint {
	public static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

	/**
	 * Execute endpoint
	 * @param request
	 * @return response
	 */
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "executeRequest")
	@ResponsePayload
	public ExecuteResponse execute(@RequestPayload ExecuteRequest request) {
		IService service = null;
		ExecuteResponse response;
		try {
			//Get specified service
			service = ServiceFactory.build(request);

			//Consume service and return the response
			response = service.execute(request);
		} catch (Throwable e) {
			DefinedLogger.SERVICE.error(e.getMessage(), e);
			response = createResponse(request, ServiceConstant.SERVICERETURN_KEY_NOTSERVICE);
		}
		return response;
	}

	/**
	 * This method generates the response if any error happens in this level
	 * @param request
	 * @param returnKey
	 * @return
	 */
	private ExecuteResponse createResponse(ExecuteRequest request, String returnKey) {
		try {
			ExecuteResponse response = new ExecuteResponse();

			//Load data from request
			response.setDrmod(request.getDcmod());
			response.setDrpgc(request.getDrpgc());
			response.setDrrel(request.getDrrel());
			response.setDrsuc(request.getDrsuc());
			response.setDrtrn(request.getDrtrn());
			response.setEcantlin(request.getEcantlin());
			response.setEdatos(request.getEdatos());
			response.setEtdats(request.getEtdats());
			response.setEvalcs(request.getEvalcs());
			response.setEvalls(request.getEvalls());

			//Validate return key
			if(!StringUtil.isEmpty(returnKey)) {
				ServiceReturn sr = ServiceReturnFactory.build(returnKey);
				//Validate return
				if(sr != null) {
					response.setXml(XMLUtil.dtoToXML(sr));

					if(response.getEvalcs() == null)
						response.setEvalcs(new ArrayOfstring20());
					if(response.getEvalcs().getItem() != null)
						response.getEvalcs().setItem(new ArrayList<String>());
					if(response.getEvalcs().getItem().size() == 0)
						response.getEvalcs().getItem().add(sr.getCode());
					else
						response.getEvalcs().getItem().set(0, sr.getCode());
				}
			}

			return response;
		} catch(Exception e) {
			return new ExecuteResponse();
		}
	}
}