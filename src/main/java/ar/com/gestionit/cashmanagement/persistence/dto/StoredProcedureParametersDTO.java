package ar.com.gestionit.cashmanagement.persistence.dto;

public class StoredProcedureParametersDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	
	private String param1;
	private String param2;
	private String param3;
	private String param4;
	private String param5;
	private String param6;
	private String param7;
	private String param8;
	private String param9;
	private String param10;
	private String param11;
	private String param12;
	private String param13;
	private String param14;
	private String param15;
	private String param16;
	private String param17;
	private String param18;
	private String param19;
	private String param20;
	private String param21;
	private String param22;
	private String param23;
	private String param24;
	private String param25;
	private String param26;
	private String param27;
	private String param28;
	/**
	 * @return the param1
	 */
	public String getParam1() {
		return param1;
	}
	/**
	 * @param param1 the param1 to set
	 */
	public void setParam1(String param1) {
		this.param1 = param1;
	}
	/**
	 * @return the param2
	 */
	public String getParam2() {
		return param2;
	}
	/**
	 * @param param2 the param2 to set
	 */
	public void setParam2(String param2) {
		this.param2 = param2;
	}
	/**
	 * @return the param3
	 */
	public String getParam3() {
		return param3;
	}
	/**
	 * @param param3 the param3 to set
	 */
	public void setParam3(String param3) {
		this.param3 = param3;
	}
	/**
	 * @return the param4
	 */
	public String getParam4() {
		return param4;
	}
	/**
	 * @param param4 the param4 to set
	 */
	public void setParam4(String param4) {
		this.param4 = param4;
	}
	/**
	 * @return the param5
	 */
	public String getParam5() {
		return param5;
	}
	/**
	 * @param param5 the param5 to set
	 */
	public void setParam5(String param5) {
		this.param5 = param5;
	}
	/**
	 * @return the param6
	 */
	public String getParam6() {
		return param6;
	}
	/**
	 * @param param6 the param6 to set
	 */
	public void setParam6(String param6) {
		this.param6 = param6;
	}
	/**
	 * @return the param7
	 */
	public String getParam7() {
		return param7;
	}
	/**
	 * @param param7 the param7 to set
	 */
	public void setParam7(String param7) {
		this.param7 = param7;
	}
	/**
	 * @return the param8
	 */
	public String getParam8() {
		return param8;
	}
	/**
	 * @param param8 the param8 to set
	 */
	public void setParam8(String param8) {
		this.param8 = param8;
	}
	/**
	 * @return the param9
	 */
	public String getParam9() {
		return param9;
	}
	/**
	 * @param param9 the param9 to set
	 */
	public void setParam9(String param9) {
		this.param9 = param9;
	}
	/**
	 * @return the param10
	 */
	public String getParam10() {
		return param10;
	}
	/**
	 * @param param10 the param10 to set
	 */
	public void setParam10(String param10) {
		this.param10 = param10;
	}
	/**
	 * @return the param11
	 */
	public String getParam11() {
		return param11;
	}
	/**
	 * @param param11 the param11 to set
	 */
	public void setParam11(String param11) {
		this.param11 = param11;
	}
	/**
	 * @return the param12
	 */
	public String getParam12() {
		return param12;
	}
	/**
	 * @param param12 the param12 to set
	 */
	public void setParam12(String param12) {
		this.param12 = param12;
	}
	/**
	 * @return the param13
	 */
	public String getParam13() {
		return param13;
	}
	/**
	 * @param param13 the param13 to set
	 */
	public void setParam13(String param13) {
		this.param13 = param13;
	}
	/**
	 * @return the param14
	 */
	public String getParam14() {
		return param14;
	}
	/**
	 * @param param14 the param14 to set
	 */
	public void setParam14(String param14) {
		this.param14 = param14;
	}
	/**
	 * @return the param15
	 */
	public String getParam15() {
		return param15;
	}
	/**
	 * @param param15 the param15 to set
	 */
	public void setParam15(String param15) {
		this.param15 = param15;
	}
	/**
	 * @return the param16
	 */
	public String getParam16() {
		return param16;
	}
	/**
	 * @param param16 the param16 to set
	 */
	public void setParam16(String param16) {
		this.param16 = param16;
	}
	/**
	 * @return the param17
	 */
	public String getParam17() {
		return param17;
	}
	/**
	 * @param param17 the param17 to set
	 */
	public void setParam17(String param17) {
		this.param17 = param17;
	}
	/**
	 * @return the param18
	 */
	public String getParam18() {
		return param18;
	}
	/**
	 * @param param18 the param18 to set
	 */
	public void setParam18(String param18) {
		this.param18 = param18;
	}
	/**
	 * @return the param19
	 */
	public String getParam19() {
		return param19;
	}
	/**
	 * @param param19 the param19 to set
	 */
	public void setParam19(String param19) {
		this.param19 = param19;
	}
	/**
	 * @return the param20
	 */
	public String getParam20() {
		return param20;
	}
	/**
	 * @param param20 the param20 to set
	 */
	public void setParam20(String param20) {
		this.param20 = param20;
	}
	/**
	 * @return the param21
	 */
	public String getParam21() {
		return param21;
	}
	/**
	 * @param param21 the param21 to set
	 */
	public void setParam21(String param21) {
		this.param21 = param21;
	}
	/**
	 * @return the param22
	 */
	public String getParam22() {
		return param22;
	}
	/**
	 * @param param22 the param22 to set
	 */
	public void setParam22(String param22) {
		this.param22 = param22;
	}
	/**
	 * @return the param23
	 */
	public String getParam23() {
		return param23;
	}
	/**
	 * @param param23 the param23 to set
	 */
	public void setParam23(String param23) {
		this.param23 = param23;
	}
	/**
	 * @return the param24
	 */
	public String getParam24() {
		return param24;
	}
	/**
	 * @param param24 the param24 to set
	 */
	public void setParam24(String param24) {
		this.param24 = param24;
	}
	/**
	 * @return the param25
	 */
	public String getParam25() {
		return param25;
	}
	/**
	 * @param param25 the param25 to set
	 */
	public void setParam25(String param25) {
		this.param25 = param25;
	}
	/**
	 * @return the param26
	 */
	public String getParam26() {
		return param26;
	}
	/**
	 * @param param26 the param26 to set
	 */
	public void setParam26(String param26) {
		this.param26 = param26;
	}
	/**
	 * @return the param27
	 */
	public String getParam27() {
		return param27;
	}
	/**
	 * @param param27 the param27 to set
	 */
	public void setParam27(String param27) {
		this.param27 = param27;
	}
	/**
	 * @return the param28
	 */
	public String getParam28() {
		return param28;
	}
	/**
	 * @param param28 the param28 to set
	 */
	public void setParam28(String param28) {
		this.param28 = param28;
	}
}