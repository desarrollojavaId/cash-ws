package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckCrudDTO;

public interface RecoveredCheckCRUDMapper extends IMapper<RecoveredCheckCrudDTO> {
	
	/**
	 * This method finds a check according to the specified check ID
	 * @param checkId
	 * @return
	 */
	public String findCheck(String checkId);
	
	/**
	 * This method deletes the event about the deleted check
	 * @param dto
	 * @return
	 */
	public int insertEventByDeleted(RecoveredCheckCrudDTO dto);
	
	/**
	 * This method registers the event about the inserted check
	 * @param dto
	 * @return
	 */
	public int insertEventByCreated(RecoveredCheckCrudDTO dto);
}