package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.Date;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.TransactionIsolationLevel;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.TransactionDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.TransactionMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public class TransactionBO extends AbstractBO<TransactionDTO, TransactionMapper> {

	/**
	 * Constants
	 * -----------------------------------------------------
	 * Possible values for STKEST field
	 */
	public static final String STKEST_ATRS = "ATRS"; //Archivo en transferencia
	public static final String STKEST_APVI = "APVI"; //Pendiente de validacion
	public static final String STKEST_AERR = "AERR";
	public static final String STKEST_APA1 = "APA1"; //Ahora, representa un archivo listo para procesar o rechazar
	public static final String STKEST_AREC = "AREC"; //Reachazar archivo. Eliminar archivo
	public static final String STKEST_AOKB = "AOKB"; //Autorizar
	/*
	 * -----------------------------------------------------
	 * Possible values for STKTRS field
	 */
	public static final String STKTRS_FILE_SENT = "Envio_archivo%";
	public static final String STKTRS_FILE_DELETED = "Archivo_eliminado";
	public static final String STKTRS_FILE_AUTORIZED = "Archivo_autorizado";
	public static final String SKTRS_FILE_REJECTED = "Archivo_rechazado";
	/*
	 * -----------------------------------------------------
	 * Possible values for STDOBS field
	 */
	public static final String STDOBS_PROCESSFILESERVICE = "AutorizarRechazarArchivo";

	/**
	 * Maximum number of attempts to get the key 
	 */
	private static final int ATTEMPT_MAX_BY_DEFAULT = 4;

	/**
	 * Time between an attempt and the following attempt
	 */
	private static final int ATTEMPT_TIME_BY_DEFAULT = 50;

	/**
	 * Maximum number of attempts to get the key 
	 */
	private int attemptMax;

	/**
	 * Time between an attempt and the following attempt
	 */
	private int timeBetweenAttempt;

	/**
	 * Constructor by default
	 */
	public TransactionBO() {
		super();
		try {
			this.attemptMax = CashManagementWsApplication
					.getPropertyAsInteger(ServiceConstant.PROP_TRACEEVENT_MAXATTEMPTS);
			this.timeBetweenAttempt = CashManagementWsApplication
					.getPropertyAsInteger(ServiceConstant.PROP_TRACEEVENT_TIME);
		} catch (Exception e) {
			this.attemptMax = ATTEMPT_MAX_BY_DEFAULT;
			this.timeBetweenAttempt = ATTEMPT_TIME_BY_DEFAULT;
		}
	}

	@Override
	protected void specifyMapper() {
		mapperInterface = TransactionMapper.class;
	}

	/**
	 * This method logs each request/response of the web service
	 * in the data source
	 * @throws ServiceException
	 */
	public void registerTransaction(TransactionDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			Date now = new Date();
			dto.setStfgen(ServiceConstant.SDF_DATE.format(now));
			dto.setSthgen(ServiceConstant.SDF_HOUR.format(now));

			//Open session and get the mapper
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			TransactionMapper mapper = session.getMapper(TransactionMapper.class);

			//Generate new transaction register
			mapper.insert(dto);

			session.commit();
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	/**
	 * This method updates the file status in the data source (SSTRANS table)
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public int updateStkest(TransactionDTO dto) throws ServiceException {
		SqlSession session = null;
		int result = 0;
		try {
			//Open session and get the mapper
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			TransactionMapper mapper = session.getMapper(TransactionMapper.class);

			//Generate new transaction register
			result = mapper.updateStkest(dto);

			session.commit();

			return result;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	/**
	 * This method updates STNARS field in the data source (SSTRANS table)
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public int updateStnars(TransactionDTO dto) throws ServiceException {
		SqlSession session = null;
		int result = 0;
		try {
			//Open session and get the mapper
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			TransactionMapper mapper = session.getMapper(TransactionMapper.class);

			//Generate new transaction register
			result = mapper.updateStnars(dto);

			session.commit();

			return result;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public TransactionDTO getTransactionData() throws ServiceException {
		return getTransactionData(0);
	}

	/**
	 * This method generates a new key to log the events of the transactions
	 * 
	 * NOTE: The session must do a commit in manual mode and lock the table to reading level
	 * to evict that other thread gets the same key and duplicates the key when it will persist
	 * in the transaction table
	 * @return
	 * @throws ServiceException
	 */
	private TransactionDTO getTransactionData(int attemptNumber) throws ServiceException {
		SqlSession session = null;
		TransactionDTO result = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(TransactionIsolationLevel.REPEATABLE_READ);

			//Get mapper
			TransactionMapper mapper = session.getMapper(TransactionMapper.class);

			//Generate new transaction ID
			TransactionDTO dto = mapper.find();
			if(dto == null)
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTPERSISTTRANS);
			dto.setMtvutr(dto.getMtvutr() + 1);

			//Update key
			mapper.update(dto);

			session.commit();
			session.close();
			result = dto;
		} catch(Exception e) {

			DefinedLogger.SERVICE.error("Transaction data: attempt numbner: " + attemptNumber);

			/*
			 * I suppose that the exception was thrown because of
			 * other thread is locking MTCONVAL table.
			 * So, I wait and try again according to the parameters...
			 */
			if(session != null) {
				DefinedLogger.SERVICE.error("Haciendo rollback...");
				session.rollback();
				session.close();
				DefinedLogger.SERVICE.error("Rollback y close");
			}

			if(attemptNumber < attemptMax) {
				//Sleeping...
				try {
					Thread.sleep(timeBetweenAttempt);
				} catch (InterruptedException e1) {}

				//Try again my dear slave...
				result = getTransactionData(attemptNumber + 1);
			} else {
				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
			}
		} finally {
			if(session != null)
				session.close();
		}
		return result;
	}

	/**
	 * This is a shortcut
	 * This method is used by Send File service to update SSTRANS
	 * if the file still was not validated by the AS400 process
	 * @param stqtrs
	 * @param stjtot
	 * @param stqtot
	 * @throws ServiceException
	 */
	public void updateForSendFileService(int stqtrs, String stjtot, String stqtot) throws ServiceException {
		String out= stqtot==null?"0":"".equals(stqtot.trim())?"0":stqtot;
		updateForSendFileService(stqtrs, stjtot, out, null);
	}

	/**
	 * This method is used by Send File service to update SSTRANS
	 * if the file still was not validated by the AS400 process
	 * @param stqtrs
	 * @param stjtot
	 * @param stqtot
	 * @param stkest
	 * @throws ServiceException
	 */
	public void updateForSendFileService(int stqtrs, String stjtot, String stqtot, String stkest) throws ServiceException {
		SqlSession session = null;
		TransactionMapper mapper = null;

		try {
			//Validate if the method must get the STKEST (file status) of the data source
			if(StringUtil.isEmpty(stkest)) {
				session = CashManagementWsApplication.sqlSessionFactory.openSession(TransactionIsolationLevel.REPEATABLE_READ);

				//Get mapper
				mapper = session.getMapper(TransactionMapper.class);

				//Generate new transaction ID
				stkest = mapper.findStkestForUpdate(String.valueOf(stqtrs));
				if(StringUtil.isEmpty(stkest))
					throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOTPERSISTTRANS);
			} else {
				session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
				mapper = session.getMapper(TransactionMapper.class);
			}

			TransactionDTO dto = new TransactionDTO();
			dto.setStqtrs(stqtrs);
			dto.setStjtot(stjtot);
			dto.setStqtot(stqtot);

			if(!stkest.trim().equalsIgnoreCase(STKEST_ATRS)){
				dto.setStkest(STKEST_ATRS);
			} else {
				dto.setStkest(STKEST_APVI);
			}
			mapper.updateForSendFile(dto);

			session.commit();
			session.close();
		} catch(Exception e) {
			if(session != null) {
				session.rollback();
				session.close();
			}
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		}
	}

	/**
	 * This method finds a file status according to the file ID and a description
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public TransactionDTO findStkest(TransactionDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			TransactionMapper mapper = session.getMapper(mapperInterface);
			String result = mapper.findStkest(dto);
			queryCounter++;
			dto.setStkest(result);
			return dto;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	/**
	 * This method finds a file status according to the file ID and a description
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public TransactionDTO findStkestByStqtrs(TransactionDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			TransactionMapper mapper = session.getMapper(mapperInterface);
			String result = mapper.findStkestByStqtrs(dto);
			queryCounter++;
			dto.setStkest(result);
			return dto;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public TransactionDTO findStqtrs(TransactionDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			TransactionMapper mapper = session.getMapper(mapperInterface);
			int result = Integer.parseInt(mapper.findStqtrs(dto));
			queryCounter++;
			session.close();
			dto.setStqtrs(result);

			return dto;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	/**
	 * This method rejects a file to process
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public int updateFile(TransactionDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			TransactionMapper mapper = session.getMapper(mapperInterface);
			int result = mapper.updateFile(dto);
			session.commit();
			queryCounter++;
			return result;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	/**
	 * This method rejects a file to process
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public int confirmFile(TransactionDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			TransactionMapper mapper = session.getMapper(mapperInterface);
			int result = mapper.confirmFile(dto);
			session.commit();
			queryCounter++;
			return result;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	/**
	 * This method rejects a PD file to process
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public int updateFilePD(TransactionDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			TransactionMapper mapper = session.getMapper(mapperInterface);
			int result = mapper.updateFilePD(dto);
			session.commit();
			queryCounter++;
			return result;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

}