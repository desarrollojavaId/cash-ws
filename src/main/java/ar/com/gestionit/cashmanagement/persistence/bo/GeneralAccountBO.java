package ar.com.gestionit.cashmanagement.persistence.bo;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.IAccount;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.AccountMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.StringUtil;

public class GeneralAccountBO extends AbstractBO<IDTO, AccountMapper>{

	@Override
	protected void specifyMapper() {
		mapperInterface = AccountMapper.class;

	}

	public IAccount findAccount(IAccount dto) throws ServiceException {
		SqlSession session = null;
		try {
			//Generate the stored procedure parameters
			StoredProcedureParametersDTO spDto = new StoredProcedureParametersDTO();
			String cbu = dto.getCbu();
			
			while(cbu.length() < 22)
				cbu = "0" + cbu;
			
			spDto.setParam1("0");
			spDto.setParam2("0");
			spDto.setParam3("0");
			spDto.setParam4("0");
			spDto.setParam5("0");
			spDto.setParam6(cbu.substring(0, 8));
			spDto.setParam7(cbu.substring(8));
			
			//Call stored procedure
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			AccountMapper mapper = session.getMapper(AccountMapper.class);
			mapper.findAccount(spDto);
			queryCounter++;
			session.commit();

			if(spDto != null) {
				dto.setCuenta(spDto.getParam1());
				dto.setSucursal(spDto.getParam2());
				dto.setSuboperacion(spDto.getParam3());
				dto.setModulo(spDto.getParam4());
				dto.setMoneda(spDto.getParam5());
			}

			return dto;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}	
	}

	/**
	 * This method returns the account by default according to adherent
	 * @param adherent
	 * @return
	 * @throws ServiceException
	 */
	public String findDefaultCBU(String adherent) throws ServiceException {
		SqlSession session = null;
		try {
			//Validate if the argument is empty
			if(StringUtil.isEmpty(adherent)) {
				return null;
			}
			
			//Access to the datasource
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			AccountMapper mapper = session.getMapper(AccountMapper.class);
			return mapper.findDefaultCBU(adherent);
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}