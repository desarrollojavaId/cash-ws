package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlTransient;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name=ServiceConstant.DTO_GENERATEDTICKET_ROOT)
public class GeneratedTicketDTO extends AbstractDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3463715463884647934L;
	
	//Input
	@XmlTransient
	private String cuit;
	
	//Output
	@XmlElement(name=ServiceConstant.DTO_GENERATEDTICKET, defaultValue="")
	private List<GeneratedTicketListItemDTO> list;

	public String getCuit() {
		return cuit;
	}

	public void setCuit(String cuit) {
		this.cuit = sanitateString(cuit);
	}

	public List<GeneratedTicketListItemDTO> getList() {
		return list;
	}

	public void setList(List<GeneratedTicketListItemDTO> list) {
		this.list = list;
	}
}
