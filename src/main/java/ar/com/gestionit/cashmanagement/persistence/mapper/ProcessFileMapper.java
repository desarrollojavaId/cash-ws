package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.ProcessFileDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;

public interface ProcessFileMapper extends IMapper<ProcessFileDTO> {
	public void moveFilePD(StoredProcedureParametersDTO dto);
}