package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType(propOrder={"nroadherente",
		"nroconvenio",
		"nrosubconv",
		"fecharecdesde",
		"fecharechasta",
		"fechaacreddesde",
		"fechaacredhasta",
		"tipocheque",
		"numcheque",
		"ref1deudor",
		"ref2deudor",
		"ref1comp",
		"ref2comp",
		"ref3comp",
		"ref4comp",
		"ref5comp",
		"numrecaud"
		})
@XmlRootElement(name = ServiceConstant.DTO_RECEIVEDCHECK_ENTRADA)
public class ReceivedChecksListInputDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4722620207304071214L;
	
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_NROADHERENTE, defaultValue="")
	private String nroadherente;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_NROCONVENIO, defaultValue="")
	private String nroconvenio;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_NROSUBCONV, defaultValue="")
	private String nrosubconv;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_FECHARECDESDE, defaultValue="")
	private String fecharecdesde;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_FECHARECHASTA, defaultValue="")
	private String fecharechasta;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_FECHAACREDDESDE, defaultValue="")
	private String fechaacreddesde;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_FECHAACREDHASTA, defaultValue="")
	private String fechaacredhasta;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_TIPOCHEQUE, defaultValue="")
	private String tipocheque;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_NUMCHEQUE, defaultValue="")
	private String numcheque;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF1DEUDOR, defaultValue="")
	private String ref1deudor;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF2DEUDOR, defaultValue="")
	private String ref2deudor;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF1COMP, defaultValue="")
	private String ref1comp;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF2COMP, defaultValue="")
	private String ref2comp;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF3COMP, defaultValue="")
	private String ref3comp;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF4COMP, defaultValue="")
	private String ref4comp;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF5COMP, defaultValue="")
	private String ref5comp;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_NUMRECAUD, defaultValue="")
	private String numrecaud;
	
	public String getNroadherente() {
		return nroadherente;
	}
	public void setNroadherente(String nroadherente) {
		this.nroadherente = sanitateString(nroadherente);
	}
	public String getNroconvenio() {
		return nroconvenio;
	}
	public void setNroconvenio(String nroconvenio) {
		this.nroconvenio = sanitateString(nroconvenio);
	}
	public String getNrosubconv() {
		return nrosubconv;
	}
	public void setNrosubconv(String nrosubconv) {
		this.nrosubconv = sanitateString(nrosubconv);
	}
	public String getFecharecdesde() {
		return fecharecdesde;
	}
	public void setFecharecdesde(String fecharecdesde) {
		this.fecharecdesde = sanitateString(fecharecdesde);
	}
	public String getFecharechasta() {
		return fecharechasta;
	}
	public void setFecharechasta(String fecharechasta) {
		this.fecharechasta = sanitateString(fecharechasta);
	}
	public String getFechaacreddesde() {
		return fechaacreddesde;
	}
	public void setFechaacreddesde(String fechaacreddesde) {
		this.fechaacreddesde = sanitateString(fechaacreddesde);
	}
	public String getFechaacredhasta() {
		return fechaacredhasta;
	}
	public void setFechaacredhasta(String fechaacredhasta) {
		this.fechaacredhasta = sanitateString(fechaacredhasta);
	}
	public String getNumcheque() {
		return numcheque;
	}
	public void setNumcheque(String numcheque) {
		this.numcheque = sanitateString(numcheque);
	}
	public String getTipocheque() {
		return tipocheque;
	}
	public void setTipocheque(String tipocheque) {
		this.tipocheque = tipocheque;
	}
	public String getRef1deudor() {
		return ref1deudor;
	}
	public void setRef1deudor(String ref1deudor) {
		this.ref1deudor = sanitateString(ref1deudor);
	}
	public String getRef2deudor() {
		return ref2deudor;
	}
	public void setRef2deudor(String ref2deudor) {
		this.ref2deudor = sanitateString(ref2deudor);
	}
	public String getRef1comp() {
		return ref1comp;
	}
	public void setRef1comp(String ref1comp) {
		this.ref1comp = sanitateString(ref1comp);
	}
	public String getRef2comp() {
		return ref2comp;
	}
	public void setRef2comp(String ref2comp) {
		this.ref2comp = sanitateString(ref2comp);
	}
	public String getRef3comp() {
		return ref3comp;
	}
	public void setRef3comp(String ref3comp) {
		this.ref3comp = sanitateString(ref3comp);
	}
	public String getRef4comp() {
		return ref4comp;
	}
	public void setRef4comp(String ref4comp) {
		this.ref4comp = sanitateString(ref4comp);
	}
	public String getRef5comp() {
		return ref5comp;
	}
	public void setRef5comp(String ref5comp) {
		this.ref5comp = sanitateString(ref5comp);
	}
	public String getNumrecaud() {
		return numrecaud;
	}
	public void setNumrecaud(String numrecaud) {
		this.numrecaud = sanitateString(numrecaud);
	}
}


