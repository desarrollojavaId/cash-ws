package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

public class SendFileDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	
	private String adherent;
	private String agreement;
	private String subagreement;
	private String cbu;
	private String fileType;
	private String table;
	private String library;
	private String initialHour;
	private String initialDate;
	private List<String> fileLines;
	private String fileStatus;
	private String registerNumber;
	private String amountTotal;
	private ErrorListDTO errorList;
	private String newSchema;
	private String checksumKey;
	private String checksumRequired;
	private String cuit;
	
	/**
	 * These attributes contain the locations where the data is allocated in the file
	 */
	private String locationRegisterNumber;
	private String locationAmountTotal;
	
	/**
	 * @return the adherent
	 */
	public String getAdherent() {
		return adherent;
	}
	/**
	 * @param adherent the adherent to set
	 */
	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}
	/**
	 * @return the cbu
	 */
	public String getCbu() {
		return cbu;
	}
	/**
	 * @param cbu the cbu to set
	 */
	public void setCbu(String cbu) {
		this.cbu = cbu;
	}
	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}
	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	/**
	 * @return the table
	 */
	public String getTable() {
		return table;
	}
	/**
	 * @param table the table to set
	 */
	public void setTable(String table) {
		this.table = table;
	}
	/**
	 * @return the library
	 */
	public String getLibrary() {
		return library;
	}
	/**
	 * @param library the library to set
	 */
	public void setLibrary(String library) {
		this.library = library;
	}
	/**
	 * @return the initialHour
	 */
	public String getInitialHour() {
		return initialHour;
	}
	/**
	 * @param initialHour the initialHour to set
	 */
	public void setInitialHour(String initialHour) {
		this.initialHour = initialHour;
	}
	/**
	 * @return the initialDate
	 */
	public String getInitialDate() {
		return initialDate;
	}
	/**
	 * @param initialDate the initialDate to set
	 */
	public void setInitialDate(String initialDate) {
		this.initialDate = initialDate;
	}
	/**
	 * @return the fileLines
	 */
	public List<String> getFileLines() {
		return fileLines;
	}
	/**
	 * @param fileLines the fileLines to set
	 */
	public void setFileLines(List<String> fileLines) {
		this.fileLines = fileLines;
	}
	/**
	 * @return the fileStatus
	 */
	public String getFileStatus() {
		return fileStatus;
	}
	/**
	 * @param fileStatus the fileStatus to set
	 */
	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}
	/**
	 * @return the locationRegisterNumber
	 */
	public String getLocationRegisterNumber() {
		return locationRegisterNumber;
	}
	/**
	 * @param locationRegisterNumber the locationRegisterNumber to set
	 */
	public void setLocationRegisterNumber(String locationRegisterNumber) {
		this.locationRegisterNumber = locationRegisterNumber;
	}
	/**
	 * @return the locationAmountTotal
	 */
	public String getLocationAmountTotal() {
		return locationAmountTotal;
	}
	/**
	 * @param locationAmountTotal the locationAmountTotal to set
	 */
	public void setLocationAmountTotal(String locationAmountTotal) {
		this.locationAmountTotal = locationAmountTotal;
	}
	/**
	 * @return the registerNumber
	 */
	public String getRegisterNumber() {
		return registerNumber;
	}
	/**
	 * @param registerNumber the registerNumber to set
	 */
	public void setRegisterNumber(String registerNumber) {
		this.registerNumber = registerNumber;
	}
	/**
	 * @return the amountTotal
	 */
	public String getAmountTotal() {
		return amountTotal;
	}
	/**
	 * @param amountTotal the amountTotal to set
	 */
	public void setAmountTotal(String amountTotal) {
		this.amountTotal = amountTotal;
	}
	/**
	 * @return the errorList
	 */
	public ErrorListDTO getErrorList() {
		return errorList;
	}
	/**
	 * @param errorList the errorList to set
	 */
	public void setErrorList(ErrorListDTO errorList) {
		this.errorList = errorList;
	}
	/**
	 * @return the newSchema
	 */
	public String getNewSchema() {
		return newSchema;
	}
	/**
	 * @param newSchema the newSchema to set
	 */
	public void setNewSchema(String newSchema) {
		this.newSchema = newSchema;
	}
	/**
	 * @return the checksumKey
	 */
	public String getChecksumKey() {
		return checksumKey;
	}
	/**
	 * @param checksumKey the checksumKey to set
	 */
	public void setChecksumKey(String checksumKey) {
		this.checksumKey = checksumKey;
	}
	/**
	 * @return the checksumRequired
	 */
	public String getChecksumRequired() {
		return checksumRequired;
	}
	/**
	 * @param checksumRequired the checksumRequired to set
	 */
	public void setChecksumRequired(String checksumRequired) {
		this.checksumRequired = checksumRequired;
	}
	public String getAgreement() {
		return agreement;
	}
	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}
	public String getSubagreement() {
		return subagreement;
	}
	public void setSubagreement(String subagreement) {
		this.subagreement = subagreement;
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
}