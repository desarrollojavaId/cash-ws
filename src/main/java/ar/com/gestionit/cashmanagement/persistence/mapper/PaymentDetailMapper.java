package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentErrorListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ProofListItemDTO;

public interface PaymentDetailMapper extends IMapper<IDTO> {
	
	/**
	 * This method returns a proof list according to DTO given by argument
	 * @param dto
	 * @return
	 */
	public List<ProofListItemDTO> findProof(ProofListItemDTO dto);
	
	/**
	 * This method returns the beneficiary email
	 * @param dto
	 * @return
	 */
	public PaymentDTO findMail(PaymentDTO dto);
	
	/**
	 * This method is used if the email is not found
	 * @param dto
	 * @return
	 */
	public PaymentDTO findNoMail(PaymentDTO dto);
	
	/**
	 * This method is used for the proof dates (sent date and printed date)
	 * @param dto
	 * @return
	 */
	public PaymentDTO findProofDates(PaymentDTO dto);

	public PaymentDTO findOps(PaymentDTO dto);

	public List<ProofListItemDTO> findPayment(IntegralPositionListDTO dto);
	
	/**
	 * This method gets the payment again to send as structure (the same structure of PaymentListService)
	 * in the response
	 * @param dto
	 * @return
	 */
	public PaymentListItemDTO findPaymentStructure(PaymentDTO dto);
	
	/**
	 * This method returns errors of payment
	 * @param dto
	 * @return
	 */
	public List<PaymentErrorListItemDTO> findErrorsList(PaymentDTO dto);
	
}