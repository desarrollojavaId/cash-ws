package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.CodeMapped;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"cbu",
		"modulo",
		"sucursal",
		"moneda",
		"cuenta",
		"suboperacion",
		"isDefault"
})
@XmlRootElement(name = ServiceConstant.DTO_CONVLIST)
public class CashAccountsListItemDTO extends AbstractDTO implements IAccount{

	private static final long serialVersionUID = 6076853825083306594L;

	//Input
	@XmlTransient
	private String adherent; 
	@XmlTransient
	private String scuqcl; 
	
	//Output
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_CBU, defaultValue="")
	private String cbu;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_MOD, defaultValue="")
	private String modulo;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_SUC, defaultValue="")
	private String sucursal;
	@CodeMapped
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_MDA, defaultValue="")
	private String moneda;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_CTA, defaultValue="")
	private String cuenta;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_SUBCTA, defaultValue="")
	private String suboperacion;
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST_ISDEFAULT, defaultValue="")
	private String isDefault;

	public String getAdherent() {
		return adherent;
	}

	public void setAdherent(String adherent) {
		this.adherent = sanitateString(adherent);
	}

	public String getScuqcl() {
		return scuqcl;
	}

	public void setScuqcl(String scuqcl) {
		this.scuqcl = sanitateString(scuqcl);
	}

	/**
	 * @return the cbu
	 */
	public String getCbu() {
		return cbu;
	}

	/**
	 * @param cbu the cbu to set
	 */
	public void setCbu(String cbu) {
		this.cbu = sanitateString(cbu);
	}

	/**
	 * @return the modulo
	 */
	public String getModulo() {
		return modulo;
	}

	/**
	 * @param modulo the modulo to set
	 */
	public void setModulo(String modulo) {
		this.modulo = sanitateString(modulo);
	}

	/**
	 * @return the sucursal
	 */
	public String getSucursal() {
		return sucursal;
	}

	/**
	 * @param sucursal the sucursal to set
	 */
	public void setSucursal(String sucursal) {
		this.sucursal = sanitateString(sucursal);
	}

	/**
	 * @return the moneda
	 */
	public String getMoneda() {
		return moneda;
	}

	/**
	 * @param moneda the moneda to set
	 */
	public void setMoneda(String moneda) {
		this.moneda = sanitateString(moneda);
	}

	/**
	 * @return the cuenta
	 */
	public String getCuenta() {
		return cuenta;
	}

	/**
	 * @param cuenta the cuenta to set
	 */
	public void setCuenta(String cuenta) {
		this.cuenta = sanitateString(cuenta);
	}

	/**
	 * @return the suboperacion
	 */
	public String getSuboperacion() {
		return suboperacion;
	}

	/**
	 * @param suboperacion the suboperacion to set
	 */
	public void setSuboperacion(String suboperacion) {
		this.suboperacion = sanitateString(suboperacion);
	}

	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = sanitateString(isDefault);
	}
}