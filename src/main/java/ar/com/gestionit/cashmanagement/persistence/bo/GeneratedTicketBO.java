package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.GeneratedTicketMapper;

public class GeneratedTicketBO extends AbstractBO<IDTO, GeneratedTicketMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = GeneratedTicketMapper.class;
	}
}