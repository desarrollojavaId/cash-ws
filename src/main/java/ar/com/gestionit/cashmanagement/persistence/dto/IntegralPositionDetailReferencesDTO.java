package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"ref1deudor",
		"ref2deudor",
		"ref1comprobante",
		"ref2comprobante",
		"ref3comprobante",
		"ref4comprobante",
		"ref5comprobante"
})
@XmlRootElement(name = ServiceConstant.DTO_INTEGRALPOSITION_REFERENCES)
public class IntegralPositionDetailReferencesDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6349663295058167366L;
	
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF1DEUDOR, defaultValue="")
	private String ref1deudor;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF2DEUDOR, defaultValue="")
	private String ref2deudor;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF1COMPROBANTE, defaultValue="")
	private String ref1comprobante;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF2COMPROBANTE, defaultValue="")
	private String ref2comprobante;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF3COMPROBANTE, defaultValue="")
	private String ref3comprobante;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF4COMPROBANTE, defaultValue="")
	private String ref4comprobante;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF5COMPROBANTE, defaultValue="")
	private String ref5comprobante;
	public String getRef1deudor() {
		return ref1deudor;
	}
	public void setRef1deudor(String ref1deudor) {
		this.ref1deudor = sanitateString(ref1deudor);
	}
	public String getRef2deudor() {
		return ref2deudor;
	}
	public void setRef2deudor(String ref2deudor) {
		this.ref2deudor = sanitateString(ref2deudor);
	}
	public String getRef1comprobante() {
		return ref1comprobante;
	}
	public void setRef1comprobante(String ref1comprobante) {
		this.ref1comprobante = sanitateString(ref1comprobante);
	}
	public String getRef2comprobante() {
		return ref2comprobante;
	}
	public void setRef2comprobante(String ref2comprobante) {
		this.ref2comprobante = sanitateString(ref2comprobante);
	}
	public String getRef3comprobante() {
		return ref3comprobante;
	}
	public void setRef3comprobante(String ref3comprobante) {
		this.ref3comprobante = sanitateString(ref3comprobante);
	}
	public String getRef4comprobante() {
		return ref4comprobante;
	}
	public void setRef4comprobante(String ref4comprobante) {
		this.ref4comprobante = sanitateString(ref4comprobante);
	}
	public String getRef5comprobante() {
		return ref5comprobante;
	}
	public void setRef5comprobante(String ref5comprobante) {
		this.ref5comprobante = sanitateString(ref5comprobante);
	}
	
	
}
