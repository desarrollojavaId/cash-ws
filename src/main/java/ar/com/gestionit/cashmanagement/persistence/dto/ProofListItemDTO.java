package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"pid",
		"mtkdes",
		"mtbifi"})
@XmlRootElement(name = ServiceConstant.TAG_PROOF)
public class ProofListItemDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	//Input
	@XmlTransient
	private String customerNumber;
	@XmlTransient
	private String referenceNumber;
	
	//Ouput
	@XmlElement(name=ServiceConstant.TAG_PROOF_ID, defaultValue="")
	private int pid; //Proof number
	@XmlElement(name=ServiceConstant.TAG_PROOF_DESC, defaultValue="")
	private String mtkdes; //Proof description
	@XmlElement(name=ServiceConstant.TAG_PROOF_SIGNATURE, defaultValue="")
	private String mtbifi; //Proof signature (si tiene firma)
	/**
	 * @return the customerNumber
	 */
	public String getCustomerNumber() {
		return customerNumber;
	}
	/**
	 * @param customerNumber the customerNumber to set
	 */
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	/**
	 * @return the referenceNumber
	 */
	public String getReferenceNumber() {
		return referenceNumber;
	}
	/**
	 * @param referenceNumber the referenceNumber to set
	 */
	public void setReferenceNumber(String referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	/**
	 * @return the mtkdes
	 */
	public String getMtkdes() {
		return mtkdes;
	}
	/**
	 * @param mtkdes the mtkdes to set
	 */
	public void setMtkdes(String mtkdes) {
		this.mtkdes = sanitateString(mtkdes);
	}
	/**
	 * @return the mtbifi
	 */
	public String getMtbifi() {
		return mtbifi;
	}
	/**
	 * @param mtbifi the mtbifi to set
	 */
	public void setMtbifi(String mtbifi) {
		this.mtbifi = sanitateString(mtbifi);
	}
	/**
	 * @return the pid
	 */
	public int getPid() {
		return pid;
	}
	/**
	 * @param pid the pid to set
	 */
	public void setPid(int pid) {
		this.pid = pid;
	}
}