package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PortalPaymentListItemDTO;

public interface PortalPaymentListMapper extends IMapper<IDTO> {
	
	public String findMail(PortalPaymentListItemDTO item);
	
	public String findNoMail(PortalPaymentListItemDTO item);
	
	public String findMarkMail(PortalPaymentListItemDTO item);
	
}