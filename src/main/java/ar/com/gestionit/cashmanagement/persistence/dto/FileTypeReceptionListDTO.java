package ar.com.gestionit.cashmanagement.persistence.dto;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_FILERECEPTION_ROOT)

public class FileTypeReceptionListDTO extends AbstractDTO{

	private static final long serialVersionUID = -6323165447987349115L;

	@XmlElement(name=ServiceConstant.DTO_FILERECEPTION)
	private List<FileTypeReceptionListItemDTO> list;

	public List<FileTypeReceptionListItemDTO> getList() {
		return list;
	}

	public void setList(List<FileTypeReceptionListItemDTO> list) {
		this.list = list;
	}
}