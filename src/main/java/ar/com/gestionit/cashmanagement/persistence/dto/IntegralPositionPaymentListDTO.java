package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name=ServiceConstant.DTO_INTEGRALPOSITION_MEDIOS)
public class IntegralPositionPaymentListDTO extends AbstractDTO{

	
	private static final long serialVersionUID = -748360737517952806L;
	
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_MEDIOSPAGO,  defaultValue="")
	private List<IntegralPositionPaymentItemDTO> list;

	public List<IntegralPositionPaymentItemDTO> getList() {
		return list;
	}

	public void setList(List<IntegralPositionPaymentItemDTO> list) {
		this.list = list;
	}
	
	

}
