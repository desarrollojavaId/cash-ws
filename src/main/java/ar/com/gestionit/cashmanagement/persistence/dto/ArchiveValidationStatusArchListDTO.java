package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"idArchivo"
})
@XmlRootElement(name = ServiceConstant.DTO_FILERECEPTION_ROOT)
public class ArchiveValidationStatusArchListDTO extends AbstractDTO{

	private static final long serialVersionUID = -1414758864739578889L;

	@XmlElement(name=ServiceConstant.DTO_FILE_ID, defaultValue="")
	private List<String> idArchivo;

	public List<String> getIdArchivo() {
		return idArchivo;
	}

	public void setIdArchivo(List<String> idArchivo) {
		this.idArchivo = idArchivo;
	}
}