package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.AdherentListForMepMapper;

public class AdherentListForMepBO extends AbstractBO<IDTO, AdherentListForMepMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = AdherentListForMepMapper.class;
	}
}