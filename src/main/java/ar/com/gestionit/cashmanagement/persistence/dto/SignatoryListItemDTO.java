package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"country",
		"documentType",
		"documentNumber",
		"firstname",
		"lastname",
		"date",
		"hour",
		"action"
})
@XmlRootElement(name = ServiceConstant.DTO_SIGNATORY)
public class SignatoryListItemDTO extends AbstractDTO {
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -1600726894333083824L;

	@XmlTransient
	private String secuencia;
	@XmlElement(name=ServiceConstant.DTO_SIGNATORY_COUNTRY, defaultValue="")
	private String country;
	@XmlElement(name=ServiceConstant.DTO_SIGNATORY_DOCUMENTTYPE, defaultValue="")
	private String documentType;
	@XmlElement(name=ServiceConstant.DTO_SIGNATORY_DOCUMENTNUMBER, defaultValue="")
	private String documentNumber;
	@XmlElement(name=ServiceConstant.DTO_SIGNATORY_FIRSTNAME, defaultValue="")
	private String firstname;
	@XmlElement(name=ServiceConstant.DTO_SIGNATORY_LASTNAME, defaultValue="")
	private String lastname;
	@XmlElement(name=ServiceConstant.DTO_SIGNATORY_DATE, defaultValue="")
	private float date;
	@XmlElement(name=ServiceConstant.DTO_SIGNATORY_HOUR, defaultValue="")
	private float hour;
	@XmlElement(name=ServiceConstant.DTO_SIGNATORY_ACTION, defaultValue="")
	private float action;
	
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the documentType
	 */
	public String getDocumentType() {
		return documentType;
	}
	/**
	 * @param documentType the documentType to set
	 */
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	/**
	 * @return the documentNumber
	 */
	public String getDocumentNumber() {
		return documentNumber;
	}
	/**
	 * @param documentNumber the documentNumber to set
	 */
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}
	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}
	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	/**
	 * @return the date
	 */
	public float getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(float date) {
		this.date = date;
	}
	/**
	 * @return the hour
	 */
	public float getHour() {
		return hour;
	}
	/**
	 * @param hour the hour to set
	 */
	public void setHour(float hour) {
		this.hour = hour;
	}
	/**
	 * @return the action
	 */
	public float getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(float action) {
		this.action = action;
	}
	public String getSecuencia() {
		return secuencia;
	}
	public void setSecuencia(String secuencia) {
		this.secuencia = secuencia;
	}
	
}