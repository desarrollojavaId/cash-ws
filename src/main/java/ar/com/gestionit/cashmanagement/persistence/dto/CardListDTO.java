package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name=ServiceConstant.DTO_CARD_ROOT)
public class CardListDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	
	//Inputs
	@XmlTransient
	private String adherent;
	@XmlTransient
	private String agreementNumber;
	@XmlTransient
	private String agreementSubnumber;
	@XmlTransient
	private String cardNumber;
	@XmlTransient
	private ReferenceDTO references;
	@XmlTransient
	private String deuBusinessName;
	@XmlTransient
	private String dateStartFrom;
	@XmlTransient
	private String dateStartTo;
	@XmlTransient
	private String dateLastMovFrom;
	@XmlTransient
	private String dateLastMovTo;
	@XmlTransient
	private List<String> statuses;
	
	//Outputs
	@XmlElement(name=ServiceConstant.DTO_CARD, defaultValue="")
	private List<CardListItemDTO> list;

	/**
	 * @return the list
	 */
	public List<CardListItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<CardListItemDTO> list) {
		this.list = list;
	}

	/**
	 * @return the adherent
	 */
	public String getAdherent() {
		return adherent;
	}

	/**
	 * @param adherent the adherent to set
	 */
	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}
	
	public String getAgreementNumber() {
		return agreementNumber;
	}

	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}

	public String getAgreementSubnumber() {
		return agreementSubnumber;
	}

	public void setAgreementSubnumber(String agreementSubnumber) {
		this.agreementSubnumber = agreementSubnumber;
	}

	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * @return the references
	 */
	public ReferenceDTO getReferences() {
		return references;
	}

	/**
	 * @param references the references to set
	 */
	public void setReferences(ReferenceDTO references) {
		this.references = references;
	}

	/**
	 * @return the deuBusinessName
	 */
	public String getDeuBusinessName() {
		return deuBusinessName;
	}

	/**
	 * @param deuBusinessName the deuBusinessName to set
	 */
	public void setDeuBusinessName(String deuBusinessName) {
		this.deuBusinessName = deuBusinessName;
	}

	/**
	 * @return the dateStartFrom
	 */
	public String getDateStartFrom() {
		return dateStartFrom;
	}

	/**
	 * @param dateStartFrom the dateStartFrom to set
	 */
	public void setDateStartFrom(String dateStartFrom) {
		this.dateStartFrom = dateStartFrom;
	}

	/**
	 * @return the dateStartTo
	 */
	public String getDateStartTo() {
		return dateStartTo;
	}

	/**
	 * @param dateStartTo the dateStartTo to set
	 */
	public void setDateStartTo(String dateStartTo) {
		this.dateStartTo = dateStartTo;
	}

	/**
	 * @return the dateLastMovFrom
	 */
	public String getDateLastMovFrom() {
		return dateLastMovFrom;
	}

	/**
	 * @param dateLastMovFrom the dateLastMovFrom to set
	 */
	public void setDateLastMovFrom(String dateLastMovFrom) {
		this.dateLastMovFrom = dateLastMovFrom;
	}

	/**
	 * @return the dateLastMovTo
	 */
	public String getDateLastMovTo() {
		return dateLastMovTo;
	}

	/**
	 * @param dateLastMovTo the dateLastMovTo to set
	 */
	public void setDateLastMovTo(String dateLastMovTo) {
		this.dateLastMovTo = dateLastMovTo;
	}

	/**
	 * @return the statuses
	 */
	public List<String> getStatuses() {
		return statuses;
	}

	/**
	 * @param statuses the statuses to set
	 */
	public void setStatuses(List<String> statuses) {
		this.statuses = statuses;
	}
}