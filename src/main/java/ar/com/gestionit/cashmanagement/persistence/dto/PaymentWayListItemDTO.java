package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"idFormasDePago",
		"descFormaPago",
		"strAmount",
		"strAmountTotal",
		"accounts"
})
@XmlRootElement(name = ServiceConstant.DTO_PAYMENT_WAY_FORMADEPAGO)
public class PaymentWayListItemDTO extends AbstractDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7216995175981339991L;
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_WAY_IDFORMADEPAGO, defaultValue="")
	private String idFormasDePago;
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_WAY_DESCFORMADEPAGO, defaultValue="")
	private String descFormaPago;
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_WAY_IMPMAX, defaultValue="")
	private String strAmount;
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_WAY_IMPTOT, defaultValue="")
	private String strAmountTotal;
	@XmlElementWrapper(name=ServiceConstant.DTO_PAYMENT_WAY_CUENTAS)
	@XmlElement(name=ServiceConstant.DTO_CASHACCOUNTSLIST, defaultValue="")
	private List<FileAccountListItemDTO> accounts;
	@XmlTransient
	private HashMap<String, FileAccountListItemDTO> accountMap;
	@XmlTransient
	private long amount;
	@XmlTransient
	private long amountTotal;
	
	public String getIdFormasDePago() {
		return idFormasDePago;
	}
	public void setIdFormasDePago(String idFormasDePago) {
		this.idFormasDePago = idFormasDePago;
	}
	public String getDescFormaPago() {
		return descFormaPago;
	}
	public void setDescFormaPago(String descFormaPago) {
		this.descFormaPago = descFormaPago;
	}
	/**
	 * @return the accounts
	 */
	public List<FileAccountListItemDTO> getAccounts() {
		return accounts;
	}
	/**
	 * @param accounts the accounts to set
	 */
	public void setAccounts(List<FileAccountListItemDTO> accounts) {
		this.accounts = accounts;
	}
	/**
	 * @return the amount
	 */
	public long getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(long amount) {
		this.amount = amount;
	}
	/**
	 * @return the amountTotal
	 */
	public long getAmountTotal() {
		return amountTotal;
	}
	/**
	 * @param amountTotal the amountTotal to set
	 */
	public void setAmountTotal(long amountTotal) {
		this.amountTotal = amountTotal;
	}
	/**
	 * @return the accountMap
	 */
	public HashMap<String, FileAccountListItemDTO> getAccountMap() {
		return accountMap;
	}
	/**
	 * @param accountMap the accountMap to set
	 */
	public void setAccountMap(HashMap<String, FileAccountListItemDTO> accountMap) {
		this.accountMap = accountMap;
	}
	/**
	 * @return the strAmount
	 */
	public String getStrAmount() {
		return strAmount;
	}
	/**
	 * @param strAmount the strAmount to set
	 */
	public void setStrAmount(String strAmount) {
		this.strAmount = strAmount;
	}
	/**
	 * @return the strAmountTotal
	 */
	public String getStrAmountTotal() {
		return strAmountTotal;
	}
	/**
	 * @param strAmountTotal the strAmountTotal to set
	 */
	public void setStrAmountTotal(String strAmountTotal) {
		this.strAmountTotal = strAmountTotal;
	}
}