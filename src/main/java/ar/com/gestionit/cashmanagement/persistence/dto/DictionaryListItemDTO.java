package ar.com.gestionit.cashmanagement.persistence.dto;

public class DictionaryListItemDTO extends AbstractDTO {

	private static final long serialVersionUID = -7078349700523248148L;
	
	private String sschar;
	private String ssvalu;
	
	public String getSschar() {
		return sschar;
	}
	public void setSschar(String sschar) {
		this.sschar = sanitateString(sschar);
	}
	public String getSsvalu() {
		return ssvalu;
	}
	public void setSsvalu(String ssvalu) {
		this.ssvalu = sanitateString(ssvalu);
	}
}