package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"checkList"})
@XmlRootElement(name=ServiceConstant.DTO_RECOVCHECKCRUD_CHECK_ROOT)
public class RecoveredCheckCrudDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	
	//Inputs
	@XmlTransient
	private String adherent;
	@XmlTransient
	private String agreementNumber;
	@XmlTransient
	private String agreementSubnumber;
	@XmlTransient
	private String action;
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECKCRUD_CHECK)
	private List<String> checkList;
	
	//Inputs for queries
	@XmlTransient
	private String requestId;
	@XmlTransient
	private String timeStamp;
	@XmlTransient
	private String date;
	@XmlTransient
	private String hour;
	
	//Temporal inputs for each check to process
	@XmlTransient
	private String checkId;
	
	/**
	 * @return the agreementNumber
	 */
	public String getAgreementNumber() {
		return agreementNumber;
	}
	/**
	 * @param agreementNumber the agreementNumber to set
	 */
	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}
	/**
	 * @return the agreementSubnumber
	 */
	public String getAgreementSubnumber() {
		return agreementSubnumber;
	}
	/**
	 * @param agreementSubnumber the agreementSubnumber to set
	 */
	public void setAgreementSubnumber(String agreementSubnumber) {
		this.agreementSubnumber = agreementSubnumber;
	}
	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}
	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}
	/**
	 * @return the checkList
	 */
	public List<String> getCheckList() {
		return checkList;
	}
	/**
	 * @param checkList the checkList to set
	 */
	public void setCheckList(List<String> checkList) {
		this.checkList = checkList;
	}
	/**
	 * @return the adherent
	 */
	public String getAdherent() {
		return adherent;
	}
	/**
	 * @param adherent the adherent to set
	 */
	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}
	/**
	 * @return the requestId
	 */
	public String getRequestId() {
		return requestId;
	}
	/**
	 * @param requestId the requestId to set
	 */
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	/**
	 * @return the checkId
	 */
	public String getCheckId() {
		return checkId;
	}
	/**
	 * @param checkId the checkId to set
	 */
	public void setCheckId(String checkId) {
		this.checkId = checkId;
	}
	/**
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}
	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	/**
	 * @return the hour
	 */
	public String getHour() {
		return hour;
	}
	/**
	 * @param hour the hour to set
	 */
	public void setHour(String hour) {
		this.hour = hour;
	}
}