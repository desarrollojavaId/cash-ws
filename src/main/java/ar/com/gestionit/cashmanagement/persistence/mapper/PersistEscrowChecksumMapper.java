package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.EscrowChecksumDTO;

public interface PersistEscrowChecksumMapper extends IMapper<EscrowChecksumDTO> {
}
