package ar.com.gestionit.cashmanagement.persistence.mapper;


import ar.com.gestionit.cashmanagement.persistence.dto.FileListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface ErrorsQueryMapper extends IMapper<IDTO> {
	
	public FileListItemDTO findStatus(FileListItemDTO dto2);
}