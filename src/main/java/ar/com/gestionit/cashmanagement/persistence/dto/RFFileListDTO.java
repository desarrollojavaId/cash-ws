package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_RFFILE_ROOT)
public class RFFileListDTO extends AbstractDTO{

	private static final long serialVersionUID = -4991324892118658096L;
	
	//Output
	@XmlElement(name=ServiceConstant.DTO_RFFILE, defaultValue="")
	private List<RFFileListItemDTO> list;

	/**
	 * @return the list
	 */
	public List<RFFileListItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<RFFileListItemDTO> list) {
		this.list = list;
	}

	/**
	 * This method adds a new item in the list
	 * @param item
	 */
	public void addItem(RFFileListItemDTO item) {
		if(list == null)
			list = new ArrayList<RFFileListItemDTO>();
		list.add(item);
	}
}