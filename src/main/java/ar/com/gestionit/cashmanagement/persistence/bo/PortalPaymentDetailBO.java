package ar.com.gestionit.cashmanagement.persistence.bo;


import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.PortalPaymentDetailMapper;



public class PortalPaymentDetailBO extends AbstractBO<IDTO, PortalPaymentDetailMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = PortalPaymentDetailMapper.class;
	}
}