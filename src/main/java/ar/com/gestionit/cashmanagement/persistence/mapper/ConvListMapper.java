package ar.com.gestionit.cashmanagement.persistence.mapper;


import ar.com.gestionit.cashmanagement.persistence.dto.ConvListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface ConvListMapper extends IMapper<IDTO> {
	
	public ConvListItemDTO findList(ConvListItemDTO dto);
}