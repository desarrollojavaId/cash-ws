package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.SecurityTicketDTO;

public interface SecurityTicketMapper extends IMapper<SecurityTicketDTO> {
	
	/**
	 * This method is used to get the "sucursal"
	 * description according to the "sucursal" ID
	 * @param dto
	 * @return
	 */
	public SecurityTicketDTO findSuc(SecurityTicketDTO dto);
}