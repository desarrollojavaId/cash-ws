package ar.com.gestionit.cashmanagement.persistence.dto;

public interface IAccount {
		
	public String getCuenta();
	public void setCuenta(String cuenta);
	
	public String getSucursal();
	public void setSucursal(String sucursal);

	public String getMoneda();
	public void setMoneda(String moneda);

	public String getSuboperacion();
	public void setSuboperacion(String suboperacion );

	public String getModulo();
	public void setModulo(String modulo);

	public String getCbu();
	public void setCbu (String cbu);
	
	public String getAdherent();
	public void setAdherent (String adherent);
}
