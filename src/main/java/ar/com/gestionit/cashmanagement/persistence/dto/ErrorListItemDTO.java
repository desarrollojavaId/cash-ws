package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"rowError",
		"description",
})
@XmlRootElement(name = ServiceConstant.DTO_ERRORSLIST)
public class ErrorListItemDTO extends AbstractDTO{

	private static final long serialVersionUID = -4422316280008537113L;
	
	//Input
	@XmlTransient
	private String mtksko; 
	@XmlTransient
	private String nomArch;
	@XmlTransient
	private int pageFrom;
	@XmlTransient
	private int pageTo;
	
	//Output
	@FieldToExport(name=ServiceConstant.DTO_ERRORSLIST_ROW)
	@XmlElement(name=ServiceConstant.DTO_ERRORSLIST_ROW, defaultValue="")
	private String rowError; 
	@FieldToExport(name=ServiceConstant.DTO_ERRORSLIST_DESCRIPTION)
	@XmlElement(name=ServiceConstant.DTO_ERRORSLIST_DESCRIPTION, defaultValue="")
	private String description; 
	@XmlTransient
	private String detdes;
	@XmlTransient
	private String mtkdes;
	@XmlTransient
	private String detnre; 
	
	public String getMtksko() {
		return mtksko;
	}
	public void setMtksko(String mtksko) {
		this.mtksko = sanitateString(mtksko);
	}
	public String getNomArch() {
		return nomArch;
	}
	public void setNomArch(String nomArch) {
		this.nomArch = sanitateString(nomArch);
	}
	public String getDetdes() {
		return detdes;
	}
	public void setDetdes(String detdes) {
		this.detdes = sanitateString(detdes);
	}
	public String getMtkdes() {
		return mtkdes;
	}
	public void setMtkdes(String mtkdes) {
		this.mtkdes = sanitateString(mtkdes);
	}
	public String getRowError() {
		return rowError;
	}
	public void setRowError(String rowError) {
		this.rowError = sanitateString(rowError);
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = sanitateString(description);
	}
	public int getPageFrom() {
		return pageFrom;
	}
	public void setPageFrom(int pageFrom) {
		this.pageFrom = pageFrom;
	}
	public int getPageTo() {
		return pageTo;
	}
	public void setPageTo(int pageTo) {
		this.pageTo = pageTo;
	}
	public String getDetnre() {
		return detnre;
	}
	public void setDetnre(String detnre) {
		this.detnre = sanitateString(detnre);
	}
}