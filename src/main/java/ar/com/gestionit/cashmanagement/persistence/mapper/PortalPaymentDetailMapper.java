package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface PortalPaymentDetailMapper extends IMapper<IDTO> {}