package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.Date;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.ProcessOrRejectionCheckDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.ProcessOrRejectionCheckMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;

public class ProcessOrRejectionCheckBO extends AbstractBO<ProcessOrRejectionCheckDTO, ProcessOrRejectionCheckMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = ProcessOrRejectionCheckMapper.class;
	}

	/**
	 * Ejecuta las actualizaciones dependiendo si la acción es rechazar o procesar
	 * @param dto
	 * @throws ServiceException	 
	 */
	public void executeTransaction(ProcessOrRejectionCheckDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			//TODO: Se crean las variables cantidadMrdrecpd, cantidadMrlrecpd y cantidadSstrans para luego validar si 
			// el update que se realizan previo a la asignación de los eventos son relativos para utilizar
			// un rollback
			//			int cantidadMrdrecpd = -1;
			//			int cantidadMrlrecpd = -1;
			//			int cantidadSstrans = -1;
//			int cantidadNonRecuperable = -1;

			GeneralRecoveredBO grlBo = new GeneralRecoveredBO();
			
			//seteo la fecha actual para el MRLRECPD
			Date fecha = new Date();
			dto.setFechaActual(fecha);
			dto.setDate(ServiceConstant.SDF_DATE.format(fecha));
			dto.setHour(ServiceConstant.SDF_HOUR.format(fecha));
			
			session = CashManagementWsApplication.sqlSessionFactory.openSession(false);
			ProcessOrRejectionCheckMapper mapper = session.getMapper(ProcessOrRejectionCheckMapper.class);

			if(dto.getAccion().equalsIgnoreCase("P")){
				// Actualizo MRDRECPD
				//				cantidadMrdrecpd = 
				mapper.updateProMrdrecpd(dto);

				// Actualizo MRLRECPD (Fecha Actual)
				//				cantidadMrlrecpd =
				mapper.updateMrlrecpd(dto);

				//Delete non recovered checks
				grlBo.deleteNonRecuperableChecks(dto.getIdLote(),
						dto.getNroConv(),
						dto.getNroSubConv(),
						session,
						false);

				// Inserto el evento 5 – 4 Pedido anulado automáticamente desde Web
				grlBo.addEvent(dto.getIdLote(), "5", "4", session, false);

				// Inserto el evento 1 - 4 Autorización
				grlBo.addEvent(dto.getIdLote(), "1", "4", session, false);

				// Registro el evento que indica que el lote ya esta aprobado
				grlBo.addEvent(dto.getIdLote(), "2", "1", session, false);
			} else if(dto.getAccion().equalsIgnoreCase("R")) {
				//Actualizo el sstrans para indicar la fecha de rechazo
				//				cantidadSstrans =
				mapper.updateSstrans(dto);
				// Actualizo MRDRECPD
				//				cantidadMrdrecpd = 
				mapper.updateRecMrdrecpd(dto);

				// Inserto el evento 5 – 3 Pedido NO autorizado
				grlBo.addEvent(dto.getIdLote(), "5", "3", session, false);

			}

			//Valido si quedan cheques para recuerpo
//			if (cantidadNonRecuperable <= 0) {
//				throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_NOCHECKSFORRECOVERED);
//			}

			//TODO: Si no se pudo realizar alguna de estas actualizaciones, se hace un ROLLBACK??
			//if(cantidadMrdrecpd == 0 || cantidadMrlrecpd == 0 || cantidadSstrans == 0)

			session.commit();

			/*
			 * Esta linea es para contabilizar la cantidad de queries que se corrieron en total
			 * ya que el servicio despues lo solicitara
			 */
			queryCounter += grlBo.getQueryCounter();

		} catch (ServiceException e) {
			if(session != null)
				session.rollback();
			throw e;
		} catch (Exception e) {
			if(session != null)
				session.rollback();
			DefinedLogger.SERVICE.error(e.getMessage(), e);
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}