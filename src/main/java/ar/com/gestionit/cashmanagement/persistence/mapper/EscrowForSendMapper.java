package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.EscrowInputListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.EscrowListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface EscrowForSendMapper extends IMapper<IDTO> {
	
	public EscrowListItemDTO findList(EscrowInputListDTO dto);
}
