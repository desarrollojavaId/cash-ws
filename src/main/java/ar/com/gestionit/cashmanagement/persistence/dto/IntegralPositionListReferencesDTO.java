package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"refDebtor1",
				"refProof1"})
@XmlRootElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REFERENCES)
public class IntegralPositionListReferencesDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF1DEUDOR, defaultValue="")
	private String refDebtor1;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF1COMPROBANTE, defaultValue="")
	private String refProof1;

	/**
	 * @return the refDebtor1
	 */
	public String getRefDebtor1() {
		return refDebtor1;
	}
	/**
	 * @param refDebtor1 the refDebtor1 to set
	 */
	public void setRefDebtor1(String refDebtor1) {
		this.refDebtor1 = refDebtor1;
	}
	/**
	 * @return the refProof1
	 */
	public String getRefProof1() {
		return refProof1;
	}
	/**
	 * @param refProof1 the refProof1 to set
	 */
	public void setRefProof1(String refProof1) {
		this.refProof1 = refProof1;
	}
}