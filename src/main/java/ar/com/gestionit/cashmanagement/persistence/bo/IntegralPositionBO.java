package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionListInputDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionListReferencesDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IntegralPositionPaymentItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.IntegralPositionMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DefinedLogger;

public class IntegralPositionBO extends AbstractBO<IDTO, IntegralPositionMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = IntegralPositionMapper.class;
	}

	/**
	 * This method executes the stored procedure SSRECCON to full in the data source
	 * the report data
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public IntegralPositionListDTO loadData(IntegralPositionListDTO dto) throws ServiceException {
		//Validate if the method was called rightly
		if(dto == null || dto.getInput() == null || dto.getReferences() == null) {
			DefinedLogger.SERVICE.error("El metodo IntegralPositionBO.loadData() recibio como argumento un DTO en NULL");
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT);
		}

		SqlSession session = null;
		try {
			IntegralPositionListInputDTO input = dto.getInput();
			IntegralPositionListReferencesDTO references = dto.getReferences();

			//Create and load the SP DTO
			StoredProcedureParametersDTO spDto = new StoredProcedureParametersDTO();
			spDto.setParam1(input.getRequestNumber());
			spDto.setParam2(CashManagementWsApplication.getProperty(ServiceConstant.PROP_GENERAL_LANGUAGEDEFAULT));
			spDto.setParam3(dto.getAdherent());
			spDto.setParam4(dto.getAgreementNumber());
			spDto.setParam5(dto.getAgreementSubnumber());
			spDto.setParam6(references.getRefDebtor1());
//			spDto.setParam7(references.getRefDebtor2());
			spDto.setParam8(references.getRefProof1());
//			spDto.setParam9(references.getRefProof2());
//			spDto.setParam10(references.getRefProof3());
//			spDto.setParam11(references.getRefProof4());
//			spDto.setParam12(references.getRefProof5());
//			spDto.setParam13(input.getDocumentType());
//			spDto.setParam14(input.getDocumentNumber());
			spDto.setParam15(input.getDateRecFrom());
			spDto.setParam16(input.getDateRecTo());
//			spDto.setParam17(input.getDebtorDocumentType());
//			spDto.setParam18(input.getDebtorDocumentNumber());
//			spDto.setParam19(input.getDateExpirationFrom());
//			spDto.setParam20(input.getDateExpirationTo());
//			spDto.setParam21(input.getAmountFrom());
//			spDto.setParam22(input.getAmountTo());
//			spDto.setParam23(input.getBanelcoNumber());

			//Execute SP
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			IntegralPositionMapper mapper = session.getMapper(IntegralPositionMapper.class);
			mapper.executeSSRECCON(spDto);
			queryCounter++;

			//Update information in the main DTO
			dto.getInput().setRequestNumber(spDto.getParam1());

			return dto;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	public List<IntegralPositionPaymentItemDTO> findPayment(IntegralPositionListDTO dto) throws ServiceException{
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			IntegralPositionMapper mapper = session.getMapper(IntegralPositionMapper.class);
			List<IntegralPositionPaymentItemDTO> result = mapper.findPayment(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}