package ar.com.gestionit.cashmanagement.persistence.dto;

/**
 * This is the DTO interface. This must be implemented
 * by all the DTO implementations
 * @author jdigruttola
 */
public interface IDTO {
	
	/**
	 * This method is used by the DTOs which need 
	 * to paginate
	 * 
	 * NOTE: this is implemented in AbstractDTO
	 * (So, implement it in each service is needless)
	 * @return
	 */
	public int getRownumber();
	
	public void setPageFrom(int from);
	public void setPageTo(int to);
}