package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.CheckStatusListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.CheckStatusListItemDTO;

public interface CheckStatusListMapper extends IMapper<CheckStatusListItemDTO> {

	/**
	 * This method is used to get the "sucursal"
	 * description according to the "sucursal" ID
	 * @param dto
	 * @return
	 */
	public List<CheckStatusListItemDTO> findChk(CheckStatusListDTO dto);
	
	/**
	 * Obtiene la cantidad de cheques sin imprimir  
	 * @param dto
	 * @return
	 */
	public String findChkUnPrinted(CheckStatusListDTO dto);
	public String findTotChk(CheckStatusListDTO dto);
}