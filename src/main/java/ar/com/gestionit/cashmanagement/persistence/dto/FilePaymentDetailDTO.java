package ar.com.gestionit.cashmanagement.persistence.dto;

public class FilePaymentDetailDTO extends AbstractDTO {
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -1600726894333083824L;

	private String registroId;
	private String refCliente;
	private String motivoPago;
	private String fechaEjeOrden;
	private String tipoPago; // key
	private Long impAPagar;
	private String monedaPago; // key
	private String fechaVtoCHPD;
	private String requerirRecibo;
	private String clausulaNoALaOrden;
	private String incluFirma;
	private String acompaniamientoCompAdjuntos;
	private String txtRefAsoc01;
	private String txtRefAsoc02;
	private String txtRefAsoc03;
	private String instrCustomServ;
	private String dateLastMov;
	
	//Proveedor
	private String provNumero;
	private String provNombre;
	private String provTipoDocum;
	private String provCuitCuilCdi;
	private String provDomicilio;
	private String provCodPostal;
	private String provEmail;
	private String provFax;
	private String provMediosComunic;
	private String provCbu;
	private String provSistemaCtaAAcreditar;
	private String provMonedaCtaAAcreditar;
	
	//Cuenta
	private String cuentaTipoDePago;
	private String cuentaMoneda;
	private String cuentaCbu;
	
	/**
	 * @return the registroId
	 */
	public String getRegistroId() {
		return registroId;
	}
	/**
	 * @param registroId the registroId to set
	 */
	public void setRegistroId(String registroId) {
		this.registroId = registroId;
	}
	/**
	 * @return the refCliente
	 */
	public String getRefCliente() {
		return refCliente;
	}
	/**
	 * @param refCliente the refCliente to set
	 */
	public void setRefCliente(String refCliente) {
		this.refCliente = refCliente;
	}
	/**
	 * @return the motivoPago
	 */
	public String getMotivoPago() {
		return motivoPago;
	}
	/**
	 * @param motivoPago the motivoPago to set
	 */
	public void setMotivoPago(String motivoPago) {
		this.motivoPago = motivoPago;
	}
	/**
	 * @return the fechaEjeOrden
	 */
	public String getFechaEjeOrden() {
		return fechaEjeOrden;
	}
	/**
	 * @param fechaEjeOrden the fechaEjeOrden to set
	 */
	public void setFechaEjeOrden(String fechaEjeOrden) {
		this.fechaEjeOrden = fechaEjeOrden;
	}
	/**
	 * @return the tipoPago
	 */
	public String getTipoPago() {
		return tipoPago;
	}
	/**
	 * @param tipoPago the tipoPago to set
	 */
	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}
	/**
	 * @return the impAPagar
	 */
	public Long getImpAPagar() {
		return impAPagar;
	}
	/**
	 * @param impAPagar the impAPagar to set
	 */
	public void setImpAPagar(Long impAPagar) {
		this.impAPagar = impAPagar;
	}
	/**
	 * @return the monedaPago
	 */
	public String getMonedaPago() {
		return monedaPago;
	}
	/**
	 * @param monedaPago the monedaPago to set
	 */
	public void setMonedaPago(String monedaPago) {
		this.monedaPago = monedaPago;
	}
	/**
	 * @return the fechaVtoCHPD
	 */
	public String getFechaVtoCHPD() {
		return fechaVtoCHPD;
	}
	/**
	 * @param fechaVtoCHPD the fechaVtoCHPD to set
	 */
	public void setFechaVtoCHPD(String fechaVtoCHPD) {
		this.fechaVtoCHPD = fechaVtoCHPD;
	}
	/**
	 * @return the requerirRecibo
	 */
	public String getRequerirRecibo() {
		return requerirRecibo;
	}
	/**
	 * @param requerirRecibo the requerirRecibo to set
	 */
	public void setRequerirRecibo(String requerirRecibo) {
		this.requerirRecibo = requerirRecibo;
	}
	/**
	 * @return the clausulaNoALaOrden
	 */
	public String getClausulaNoALaOrden() {
		return clausulaNoALaOrden;
	}
	/**
	 * @param clausulaNoALaOrden the clausulaNoALaOrden to set
	 */
	public void setClausulaNoALaOrden(String clausulaNoALaOrden) {
		this.clausulaNoALaOrden = clausulaNoALaOrden;
	}
	/**
	 * @return the incluFirma
	 */
	public String getIncluFirma() {
		return incluFirma;
	}
	/**
	 * @param incluFirma the incluFirma to set
	 */
	public void setIncluFirma(String incluFirma) {
		this.incluFirma = incluFirma;
	}
	/**
	 * @return the acompaniamientoCompAdjuntos
	 */
	public String getAcompaniamientoCompAdjuntos() {
		return acompaniamientoCompAdjuntos;
	}
	/**
	 * @param acompaniamientoCompAdjuntos the acompaniamientoCompAdjuntos to set
	 */
	public void setAcompaniamientoCompAdjuntos(String acompaniamientoCompAdjuntos) {
		this.acompaniamientoCompAdjuntos = acompaniamientoCompAdjuntos;
	}
	/**
	 * @return the txtRefAsoc01
	 */
	public String getTxtRefAsoc01() {
		return txtRefAsoc01;
	}
	/**
	 * @param txtRefAsoc01 the txtRefAsoc01 to set
	 */
	public void setTxtRefAsoc01(String txtRefAsoc01) {
		this.txtRefAsoc01 = txtRefAsoc01;
	}
	/**
	 * @return the txtRefAsoc02
	 */
	public String getTxtRefAsoc02() {
		return txtRefAsoc02;
	}
	/**
	 * @param txtRefAsoc02 the txtRefAsoc02 to set
	 */
	public void setTxtRefAsoc02(String txtRefAsoc02) {
		this.txtRefAsoc02 = txtRefAsoc02;
	}
	/**
	 * @return the txtRefAsoc03
	 */
	public String getTxtRefAsoc03() {
		return txtRefAsoc03;
	}
	/**
	 * @param txtRefAsoc03 the txtRefAsoc03 to set
	 */
	public void setTxtRefAsoc03(String txtRefAsoc03) {
		this.txtRefAsoc03 = txtRefAsoc03;
	}
	/**
	 * @return the instrCustomServ
	 */
	public String getInstrCustomServ() {
		return instrCustomServ;
	}
	/**
	 * @param instrCustomServ the instrCustomServ to set
	 */
	public void setInstrCustomServ(String instrCustomServ) {
		this.instrCustomServ = instrCustomServ;
	}
	/**
	 * @return the dateLastMov
	 */
	public String getDateLastMov() {
		return dateLastMov;
	}
	/**
	 * @param dateLastMov the dateLastMov to set
	 */
	public void setDateLastMov(String dateLastMov) {
		this.dateLastMov = dateLastMov;
	}
	/**
	 * @return the provNumero
	 */
	public String getProvNumero() {
		return provNumero;
	}
	/**
	 * @param provNumero the provNumero to set
	 */
	public void setProvNumero(String provNumero) {
		this.provNumero = provNumero;
	}
	/**
	 * @return the provNombre
	 */
	public String getProvNombre() {
		return provNombre;
	}
	/**
	 * @param provNombre the provNombre to set
	 */
	public void setProvNombre(String provNombre) {
		this.provNombre = provNombre;
	}
	/**
	 * @return the provTipoDocum
	 */
	public String getProvTipoDocum() {
		return provTipoDocum;
	}
	/**
	 * @param provTipoDocum the provTipoDocum to set
	 */
	public void setProvTipoDocum(String provTipoDocum) {
		this.provTipoDocum = provTipoDocum;
	}
	/**
	 * @return the provCuitCuilCdi
	 */
	public String getProvCuitCuilCdi() {
		return provCuitCuilCdi;
	}
	/**
	 * @param provCuitCuilCdi the provCuitCuilCdi to set
	 */
	public void setProvCuitCuilCdi(String provCuitCuilCdi) {
		this.provCuitCuilCdi = provCuitCuilCdi;
	}
	/**
	 * @return the provDomicilio
	 */
	public String getProvDomicilio() {
		return provDomicilio;
	}
	/**
	 * @param provDomicilio the provDomicilio to set
	 */
	public void setProvDomicilio(String provDomicilio) {
		this.provDomicilio = provDomicilio;
	}
	/**
	 * @return the provCodPostal
	 */
	public String getProvCodPostal() {
		return provCodPostal;
	}
	/**
	 * @param provCodPostal the provCodPostal to set
	 */
	public void setProvCodPostal(String provCodPostal) {
		this.provCodPostal = provCodPostal;
	}
	/**
	 * @return the provEmail
	 */
	public String getProvEmail() {
		return provEmail;
	}
	/**
	 * @param provEmail the provEmail to set
	 */
	public void setProvEmail(String provEmail) {
		this.provEmail = provEmail;
	}
	/**
	 * @return the provFax
	 */
	public String getProvFax() {
		return provFax;
	}
	/**
	 * @param provFax the provFax to set
	 */
	public void setProvFax(String provFax) {
		this.provFax = provFax;
	}
	/**
	 * @return the provMediosComunic
	 */
	public String getProvMediosComunic() {
		return provMediosComunic;
	}
	/**
	 * @param provMediosComunic the provMediosComunic to set
	 */
	public void setProvMediosComunic(String provMediosComunic) {
		this.provMediosComunic = provMediosComunic;
	}
	/**
	 * @return the provCbu
	 */
	public String getProvCbu() {
		return provCbu;
	}
	/**
	 * @param provCbu the provCbu to set
	 */
	public void setProvCbu(String provCbu) {
		this.provCbu = provCbu;
	}
	/**
	 * @return the provSistemaCtaAAcreditar
	 */
	public String getProvSistemaCtaAAcreditar() {
		return provSistemaCtaAAcreditar;
	}
	/**
	 * @param provSistemaCtaAAcreditar the provSistemaCtaAAcreditar to set
	 */
	public void setProvSistemaCtaAAcreditar(String provSistemaCtaAAcreditar) {
		this.provSistemaCtaAAcreditar = provSistemaCtaAAcreditar;
	}
	/**
	 * @return the provMonedaCtaAAcreditar
	 */
	public String getProvMonedaCtaAAcreditar() {
		return provMonedaCtaAAcreditar;
	}
	/**
	 * @param provMonedaCtaAAcreditar the provMonedaCtaAAcreditar to set
	 */
	public void setProvMonedaCtaAAcreditar(String provMonedaCtaAAcreditar) {
		this.provMonedaCtaAAcreditar = provMonedaCtaAAcreditar;
	}
	/**
	 * @return the cuentaTipoDePago
	 */
	public String getCuentaTipoDePago() {
		return cuentaTipoDePago;
	}
	/**
	 * @param cuentaTipoDePago the cuentaTipoDePago to set
	 */
	public void setCuentaTipoDePago(String cuentaTipoDePago) {
		this.cuentaTipoDePago = cuentaTipoDePago;
	}
	/**
	 * @return the cuentaMoneda
	 */
	public String getCuentaMoneda() {
		return cuentaMoneda;
	}
	/**
	 * @param cuentaMoneda the cuentaMoneda to set
	 */
	public void setCuentaMoneda(String cuentaMoneda) {
		this.cuentaMoneda = cuentaMoneda;
	}
	/**
	 * @return the cuentaCbu
	 */
	public String getCuentaCbu() {
		return cuentaCbu;
	}
	/**
	 * @param cuentaCbu the cuentaCbu to set
	 */
	public void setCuentaCbu(String cuentaCbu) {
		this.cuentaCbu = cuentaCbu;
	}
}