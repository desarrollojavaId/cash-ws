package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.FileTypeReceptionMapper;

public class FileTypeReceptionBO extends AbstractBO<IDTO, FileTypeReceptionMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = FileTypeReceptionMapper.class;
	}
}