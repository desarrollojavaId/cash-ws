package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.ReceivedChecksListMapper;

public class ReceivedChecksListBO extends AbstractBO<IDTO, ReceivedChecksListMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = ReceivedChecksListMapper.class;
	}
}