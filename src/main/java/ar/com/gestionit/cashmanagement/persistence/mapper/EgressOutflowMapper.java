package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.adapter.EgressOutflowItemAdapter;
import ar.com.gestionit.cashmanagement.persistence.dto.EgressOutflowDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface EgressOutflowMapper extends IMapper<IDTO> {
	public void executeLoader(EgressOutflowDTO dto);
	public List<EgressOutflowItemAdapter> findChecks(EgressOutflowDTO dto);
	public List<EgressOutflowItemAdapter> findEgressList(EgressOutflowDTO dto);
}