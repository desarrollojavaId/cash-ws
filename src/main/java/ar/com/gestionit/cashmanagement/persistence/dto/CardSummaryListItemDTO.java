package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"description",
		"amount",
		"percentage"
})
@XmlRootElement(name = ServiceConstant.DTO_CARD_SUMMARY)
public class CardSummaryListItemDTO extends AbstractDTO {
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -1600726894333083824L;

	@XmlElement(name=ServiceConstant.DTO_CARD_SUMMARY_DESC, defaultValue="")
	private String description;
	@XmlElement(name=ServiceConstant.DTO_CARD_SUMMARY_AMOUNT, defaultValue="")
	private int amount;
	@XmlElement(name=ServiceConstant.DTO_CARD_SUMMARY_PERCENTAGE, defaultValue="")
	private String percentage;
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = sanitateString(description);
	}
	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}
	/**
	 * @return the percentage
	 */
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = sanitateString(percentage);
	}
	
}