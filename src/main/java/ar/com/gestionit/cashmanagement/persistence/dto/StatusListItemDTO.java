package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"estn",
		"estdesc",
		"events"})
@XmlRootElement(name = ServiceConstant.TAG_STATUS)
public class StatusListItemDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	//Input
	@XmlTransient
	private String paymentNumber;
	@XmlTransient
	private String paymentSubnumber;
	
	//Input or output
	@XmlTransient
	private String mtqhmv;
	@XmlTransient
	private String kev;
	
	//Ouput
	@XmlElement(name=ServiceConstant.TAG_STATUS_ID, defaultValue="")
	private String estn; //Status number
	//@XmlElement(name=ServiceConstant.TAG_STATUS_SID, defaultValue="")
	@XmlTransient
	private String estsn; //Status sub-number
	@XmlElement(name=ServiceConstant.TAG_STATUS_DESC, defaultValue="")
	private String estdesc; //Status description
	@XmlElementWrapper(name=ServiceConstant.TAG_EVENT_ROOT)
	@XmlElement(name=ServiceConstant.TAG_EVENT, defaultValue="")
	private List<StatusEventDTO> events; //Status events
	
	/**
	 * @return the paymentNumber
	 */
	public String getPaymentNumber() {
		return paymentNumber;
	}
	/**
	 * @param paymentNumber the paymentNumber to set
	 */
	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = sanitateString(paymentNumber);
	}
	/**
	 * @return the paymentSubnumber
	 */
	public String getPaymentSubnumber() {
		return paymentSubnumber;
	}
	/**
	 * @param paymentSubnumber the paymentSubnumber to set
	 */
	public void setPaymentSubnumber(String paymentSubnumber) {
		this.paymentSubnumber = sanitateString(paymentSubnumber);
	}
	/**
	 * @return the estn
	 */
	public String getEstn() {
		return estn;
	}
	/**
	 * @param estn the estn to set
	 */
	public void setEstn(String estn) {
		this.estn = sanitateString(estn);
	}
	/**
	 * @return the estsn
	 */
	public String getEstsn() {
		return estsn;
	}
	/**
	 * @param estsn the estsn to set
	 */
	public void setEstsn(String estsn) {
		this.estsn = sanitateString(estsn);
	}
	/**
	 * @return the estdesc
	 */
	public String getEstdesc() {
		return estdesc;
	}
	/**
	 * @param estdesc the estdesc to set
	 */
	public void setEstdesc(String estdesc) {
		this.estdesc = sanitateString(estdesc);
	}
	/**
	 * @return the events
	 */
	public List<StatusEventDTO> getEvents() {
		return events;
	}
	/**
	 * @param events the events to set
	 */
	public void setEvents(List<StatusEventDTO> events) {
		this.events = events;
	}
	/**
	 * This method adds a new event in the event list
	 * @param event
	 */
	public void setEvent(StatusEventDTO event) {
		if(event == null)
			return;
		if(events == null)
			events = new ArrayList<StatusEventDTO>();
		events.add(event);
	}
	/**
	 * @return the mtqhmv
	 */
	public String getMtqhmv() {
		return mtqhmv;
	}
	/**
	 * @param mtqhmv the mtqhmv to set
	 */
	public void setMtqhmv(String mtqhmv) {
		this.mtqhmv = sanitateString(mtqhmv);
	}
	/**
	 * @return the kev
	 */
	public String getKev() {
		return kev;
	}
	/**
	 * @param kev the kev to set
	 */
	public void setKev(String kev) {
		this.kev = sanitateString(kev);
	}
}