package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"cantCheSinImp",
					 "cantTotChks"		
})
@XmlRootElement(name=ServiceConstant.DTO_POFILESUMMARY)
public class CheckStatusUnPrintedDTO extends AbstractDTO{

	private static final long serialVersionUID = -4991324892118648096L;
	
	//Output
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_CANT_CHEQ, defaultValue="")
	private String cantCheSinImp;
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_CANT_CHEQTOT, defaultValue="")
	private String cantTotChks;

	public String getCantCheSinImp() {
		return cantCheSinImp;
	}

	public void setCantCheSinImp(String cantCheSinImp) {
		this.cantCheSinImp = sanitateString(cantCheSinImp);
	}
	public String getCantTotChks() {
		return cantTotChks;
	}

	public void setCantTotChks(String cantTotChks) {
		this.cantTotChks = sanitateString(cantTotChks);
	}
	
}