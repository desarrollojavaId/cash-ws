package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredBatchDTO;

public interface GenerateRecoveredBatchMapper extends IMapper<RecoveredBatchDTO> {
	
	/**
	 * This method specify the branch in the batch detail
	 * @param dto
	 * @return
	 */
	public int updateBatchDetail(RecoveredBatchDTO dto);
}