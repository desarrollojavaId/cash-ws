package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"tokens",
		"canal",
		"operacion",
		"variante"})
@XmlRootElement(name = ServiceConstant.DTO_FILERECEPTION_ROOT)
public class ArchiveInputListDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7292805869423905403L;
	@XmlElement(name=ServiceConstant.TAG_ID)
	private List<String> tokens;
	@XmlElement(name=ServiceConstant.DTO_EXPORT_CHANNEL, defaultValue="")
	private String canal;
	@XmlElement(name=ServiceConstant.DTO_EXPORT_OPERATION, defaultValue="")
	private String operacion;
	@XmlElement(name=ServiceConstant.DTO_EXPORT_VARIANT, defaultValue="")
	private String variante;
	

	/**
	 * @return the files
	 */
	public List<String> getTokens() {
			return tokens;
	}

	/**
	 * @param statuses the files to set
	 */
	public void setTokens(List<String> tokens) {
		this.tokens = tokens;
	}

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = sanitateString(canal);
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = sanitateString(operacion);
	}

	public String getVariante() {
		return variante;
	}

	public void setVariante(String variante) {
		this.variante = sanitateString(variante);
	}
	
	
}