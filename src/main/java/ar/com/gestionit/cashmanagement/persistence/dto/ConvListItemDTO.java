package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"nroConv",
		"nroSubConv",
		"descripcion"
})
@XmlRootElement(name = ServiceConstant.DTO_CONVLIST)
public class ConvListItemDTO extends AbstractDTO{

	private static final long serialVersionUID = -55321425040004808L;
	
	//Input
	@XmlTransient
	private String adh; 
	@XmlTransient
	private String mrccnv;
	
	//Input-Output
	@XmlTransient
	private String mrtcnv;
		
	//Output
	@XmlElement(name=ServiceConstant.DTO_CONVLIST_NROCONV, defaultValue="")
	private String nroConv; 
	@XmlElement(name=ServiceConstant.DTO_CONVLIST_NROSUBCONV, defaultValue="")
	private String nroSubConv;
	@XmlElement(name=ServiceConstant.DTO_CONVLIST_DESCRIPCION, defaultValue="")
	private String descripcion; 
	@XmlTransient
	private String mrttbo;
	@XmlTransient
	private String mrtdco;
	
	public String getAdh() {
		return adh;
	}
	public void setAdh(String adh) {
		this.adh = sanitateString(adh);
	}
	public String getMrtcnv() {
		return mrtcnv;
	}
	public void setMrtcnv(String mrtcnv) {
		this.mrtcnv = sanitateString(mrtcnv);
	}
	public String getNroConv() {
		return nroConv;
	}
	public void setNroConv(String nroConv) {
		this.nroConv = sanitateString(nroConv);
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = sanitateString(descripcion);
	}
	public String getNroSubConv() {
		return nroSubConv;
	}
	public void setNroSubConv(String nroSubConv) {
		this.nroSubConv = sanitateString(nroSubConv);
	}
	public String getMrttbo() {
		return mrttbo;
	}
	public void setMrttbo(String mrttbo) {
		this.mrttbo = sanitateString(mrttbo);
	}
	public String getMrtdco() {
		return mrtdco;
	}
	public void setMrtdco(String mrtdco) {
		this.mrtdco = sanitateString(mrtdco);
	}
	public String getMrccnv() {
		return mrccnv;
	}
	public void setMrccnv(String mrccnv) {
		this.mrccnv = sanitateString(mrccnv);
	}
}