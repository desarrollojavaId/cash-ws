package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"empresaDesc",
				"convenio",
				"subconvenio",
				"moneda"
})

@XmlRootElement(name = ServiceConstant.DTO_COMPANYLIST)
public class CompanyListItemDTO extends AbstractDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7417395461354872292L;
	
	@XmlElement(name=ServiceConstant.DTO_COMPANYLIST_EMPRESADESC, defaultValue="")
	private String empresaDesc;
	@XmlElement(name=ServiceConstant.DTO_COMPANYLIST_CONVENIO, defaultValue="")
	private String convenio;
	@XmlElement(name=ServiceConstant.DTO_COMPANYLIST_SUBCONVENIO, defaultValue="")
	private String subconvenio;
	@XmlElement(name=ServiceConstant.DTO_COMPANYLIST_MONEDA, defaultValue="")
	private String moneda;

	public String getEmpresaDesc() {
		return empresaDesc;
	}

	public void setEmpresaDesc(String empresaDesc) {
		this.empresaDesc = sanitateString(empresaDesc);
	}

	public String getConvenio() {
		return convenio;
	}

	public void setConvenio(String convenio) {
		this.convenio = sanitateString(convenio);
	}

	public String getSubconvenio() {
		return subconvenio;
	}

	public void setSubconvenio(String subconvenio) {
		this.subconvenio = sanitateString(subconvenio);
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = sanitateString(moneda);
	}
	
	

}
