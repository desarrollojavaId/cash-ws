package ar.com.gestionit.cashmanagement.persistence.bo;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.PortalPaymentListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.PortalPaymentListMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;


public class PortalPaymentListBO extends AbstractBO<IDTO, PortalPaymentListMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = PortalPaymentListMapper.class;
	}
	
	public String findMail(PortalPaymentListItemDTO item) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PortalPaymentListMapper mapper = session.getMapper(mapperInterface);
			String result = mapper.findMail(item);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public String findNoMail(PortalPaymentListItemDTO item) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PortalPaymentListMapper mapper = session.getMapper(mapperInterface);
			String result = mapper.findNoMail(item);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public String findMarkMail(PortalPaymentListItemDTO item) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PortalPaymentListMapper mapper = session.getMapper(mapperInterface);
			String result = mapper.findMarkMail(item);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}