package ar.com.gestionit.cashmanagement.persistence.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

@XmlAccessorType(XmlAccessType.FIELD )
public abstract class AbstractDTO implements Serializable, IDTO {

	/**
	 * Serial version number
	 */
	private static final long serialVersionUID = -6334913738199268689L;
	
	@XmlTransient
	protected int pageFrom;
	@XmlTransient
	protected int pageTo;
	@XmlTransient
	private int rownumber;

	/**
	 * This internal method formats the value given by argument
	 * @param value
	 * @return
	 */
	protected String sanitateString(String value) {
		if(value != null){
			value = value.trim();
			if(value.equalsIgnoreCase(""))
				return "";
			else
				return value;			
		}
		else
			return "";
	}
	
	/**
	 * This internal method formats the date given by argument
	 * @param value
	 * @return
	 */
	protected String sanitateDate(String value) {
		if(value != null && value.trim().equals("00000000"))
			return "";
		return value;
	}
	
	/**
	 * This internal method converts a date from YYYYMMDD to DD-MM-YYYY
	 * @param value
	 * @return
	 */
	protected String formatDateYYYYMMDD(String value) {
		if(value != null && value.length() == 8)
			return value.substring(6, 8) + "/" + value.substring(4, 6) + "/" + value.substring(0, 4);
		return value;
	}

	/**
	 * @return the pageFrom
	 */
	public int getPageFrom() {
		return pageFrom;
	}

	/**
	 * @param pageFrom the pageFrom to set
	 */
	public void setPageFrom(int pageFrom) {
		this.pageFrom = pageFrom;
	}

	/**
	 * @return the pageTo
	 */
	public int getPageTo() {
		return pageTo;
	}

	/**
	 * @param pageTo the pageTo to set
	 */
	public void setPageTo(int pageTo) {
		this.pageTo = pageTo;
	}

	/**
	 * @return the rownumber
	 */
	public int getRownumber() {
		return rownumber;
	}

	/**
	 * @param rownumber the rownumber to set
	 */
	public void setRownumber(int rownumber) {
		this.rownumber = rownumber;
	}
}