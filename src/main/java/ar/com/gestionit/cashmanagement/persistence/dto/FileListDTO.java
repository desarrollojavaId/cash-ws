package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name=ServiceConstant.DTO_FILE_ROOT)
public class FileListDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	
	@XmlElement(name=ServiceConstant.DTO_FILE, defaultValue="")
	private List<FileListItemDTO> list;
	
	/**
	 * Invoked transaction
	 * NOTE: At moment, this attribute always has this value
	 */
	@XmlTransient
	private String invokedTransaction = "Envio_archivo%";
	@XmlTransient
	private String date;
	@XmlTransient
	private String hour;
	@XmlTransient
	private String user;
	@XmlTransient
	private String adherent; 
	/**
	 * @return the list
	 */
	public List<FileListItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<FileListItemDTO> list) {
		this.list = list;
	}
	
	public void addFile(FileListItemDTO file) {
		if(list == null)
			list = new ArrayList<FileListItemDTO>();
		list.add(file);
	}

	/**
	 * @return the invokedTransaction
	 */
	public String getInvokedTransaction() {
		return invokedTransaction;
	}

	/**
	 * @param invokedTransaction the invokedTransaction to set
	 */
	public void setInvokedTransaction(String invokedTransaction) {
		this.invokedTransaction = invokedTransaction;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the hour
	 */
	public String getHour() {
		return hour;
	}

	/**
	 * @param hour the hour to set
	 */
	public void setHour(String hour) {
		this.hour = hour;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	public String getAdherent() {
		return adherent;
	}

	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}
	
}