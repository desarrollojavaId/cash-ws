package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface FileTypeReceptionMapper extends IMapper<IDTO> {}