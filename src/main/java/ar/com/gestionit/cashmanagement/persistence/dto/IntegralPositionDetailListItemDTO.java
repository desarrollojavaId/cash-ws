package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"scrftr",
		"scrrd1",
		"scrrd2",
		"scrre1",
		"scrre2",
		"scrre3",
		"scrre4",
		"scrre5",
		"scritr",
		"scrdfo",
		"scrder",
		"scrqtr",
		"scrivc",
		"scrice",
		"scricc",
		"scripd",
		"scrinc",
		"scricr",
		"scricv",
		"scrtat",
		"scrsuc",
		"scr1im",
		"scrfvt",
		"scrdmo",
		"scrivr",
		"scriad",
		"scriac",
		"scritf",
		"scrnbo",
		"scrboc"
})
@XmlRootElement(name = ServiceConstant.DTO_INTEGRALPOSITION)
public class IntegralPositionDetailListItemDTO extends AbstractDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2316475492343920574L;

		
	//Output
	@XmlTransient
	private String scrqic;
	@XmlTransient
	private String scrqpd;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_FECCOB)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_FECCOB, defaultValue="")
	private String scrftr;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPREC)
	@XmlTransient
	private String scritrExporter;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_FCOBRODESC)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_FCOBRODESC, defaultValue="")
	private String scrdfo;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_NROREC)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_NROREC, defaultValue="")
	private String scrqtr;
	/*
	 * Reference contents
	 */
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_REF1DEUDOR, hasDynamicTitle=true, required=false)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF1DEUDOR, defaultValue="")
	private String scrrd1;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_REF2DEUDOR, hasDynamicTitle=true, required=false)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF2DEUDOR, defaultValue="")
	private String scrrd2;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_REF1COMPROBANTE, hasDynamicTitle=true, required=false)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF1COMPROBANTE, defaultValue="")
	private String scrre1;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_REF2COMPROBANTE, hasDynamicTitle=true, required=false)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF2COMPROBANTE, defaultValue="")
	private String scrre2;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_REF3COMPROBANTE, hasDynamicTitle=true, required=false)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF3COMPROBANTE, defaultValue="")
	private String scrre3;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_REF4COMPROBANTE, hasDynamicTitle=true, required=false)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF4COMPROBANTE, defaultValue="")
	private String scrre4;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_REF5COMPROBANTE, hasDynamicTitle=true, required=false)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_REF5COMPROBANTE, defaultValue="")
	private String scrre5;
	
	/*
	 * Reference titles
	 */
	@XmlTransient
	private String scrrd1Title;
	@XmlTransient
	private String scrrd2Title;
	@XmlTransient
	private String scrre1Title;
	@XmlTransient
	private String scrre2Title;
	@XmlTransient
	private String scrre3Title;
	@XmlTransient
	private String scrre4Title;
	@XmlTransient
	private String scrre5Title;
	
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPREC, defaultValue="")
	private String scritr;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_ESTCOBDESC)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_ESTCOBDESC, defaultValue="")
	private String scrder;
	//@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_CHEVALCOB)
	@XmlTransient
	private String scrivcExporter;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_CHEVALCOB, defaultValue="")
	private String scrivc;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_EFECTIVO)
	@XmlTransient
	private String scriceExporter;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_EFECTIVO, defaultValue="")
	private String scrice;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_CHEDIA)
	@XmlTransient
	private String scriccExporter;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_CHEDIA, defaultValue="")
	private String scricc;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_CHEPAGDIFE)
	@XmlTransient
	private String scripdExporter;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_CHEPAGDIFE, defaultValue="")
	private String scripd;
	//@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_NOTACRED)
	@XmlTransient
	private String scrincExporter;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_NOTACRED, defaultValue="")
	private String scrinc;
	//@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_CERTRETEN)
	@XmlTransient
	private String scricrExporter;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_CERTRETEN, defaultValue="")
	private String scricr;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_VALCONFIRM)
	@XmlTransient
	private String scricvExporter;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_VALCONFIRM, defaultValue="")
	private String scricv;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_TIPOATM)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_TIPOATM, defaultValue="")
	private String scrtat;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_SUCCOBDESC)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_SUCCOBDESC, defaultValue="")
	private String scrsuc;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPPUBLI, defaultValue="")
	private String scr1im;
	//@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPPUBLI)
	@XmlTransient
	private String scr1imExporter;
	//@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_VENCPUBLI)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_VENCPUBLI, defaultValue="")
	private String scrfvt;
	//@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_MONEDA)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_MONEDA, defaultValue="")
	private String scrdmo;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPRECH)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPRECH, defaultValue="")
	private String scrivr = "0.00"; 
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPACRE)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPACRE, defaultValue="")
	private String scriac = "0.00";
	//@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPADEU)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_IMPADEU, defaultValue="")
	private String scriad  = "0.00";
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_TRANSFERENCIA)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_TRANSFERENCIA, defaultValue="")
	private String scritf;
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_BCOORIGENTRANSF)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_BCOORIGENTRANSF, defaultValue="")
	private String scrnbo;
	
	@FieldToExport(name=ServiceConstant.DTO_INTEGRALPOSITION_BOLETA_COBINPRO)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_BOLETA_COBINPRO, defaultValue="")
	private String scrboc;
	
	
	
	
	public String getScrftr() {
		return scrftr;
	}
	public void setScrftr(String scrftr) {
		this.scrftr = sanitateString(scrftr);
	}
	public String getScrrd1() {
		return scrrd1;
	}
	public void setScrrd1(String scrrd1) {
		this.scrrd1 = sanitateString(scrrd1);
	}
	public String getScrrd2() {
		return scrrd2;
	}
	public void setScrrd2(String scrrd2) {
		this.scrrd2 = sanitateString(scrrd2);
	}
	public String getScrre1() {
		return scrre1;
	}
	public void setScrre1(String scrre1) {
		this.scrre1 = sanitateString(scrre1);
	}
	public String getScrre2() {
		return scrre2;
	}
	public void setScrre2(String scrre2) {
		this.scrre2 = sanitateString(scrre2);
	}
	public String getScrre3() {
		return scrre3;
	}
	public void setScrre3(String scrre3) {
		this.scrre3 = sanitateString(scrre3);
	}
	public String getScrre4() {
		return scrre4;
	}
	public void setScrre4(String scrre4) {
		this.scrre4 = sanitateString(scrre4);
	}
	public String getScrre5() {
		return scrre5;
	}
	public void setScrre5(String scrre5) {
		this.scrre5 = sanitateString(scrre5);
	}
	public String getScritr() {
		return scritr;
	}
	public void setScritr(String scritr) {
		this.scritr = sanitateString(scritr);
	}
	public String getScrdfo() {
		return scrdfo;
	}
	public void setScrdfo(String scrdfo) {
		this.scrdfo = sanitateString(scrdfo);
	}
	public String getScrder() {
		return scrder;
	}
	public void setScrder(String scrder) {
		this.scrder = sanitateString(scrder);
	}
	
	public String getScrivc() {
		return scrivc;
	}
	public void setScrivc(String scrivc) {
		this.scrivc = sanitateString(scrivc);
	}
	public String getScrice() {
		return scrice;
	}
	public void setScrice(String scrice) {
		this.scrice = sanitateString(scrice);
	}
	public String getScricc() {
		return scricc;
	}
	public void setScricc(String scricc) {
		this.scricc = sanitateString(scricc);
	}
	public String getScripd() {
		return scripd;
	}
	public void setScripd(String scripd) {
		this.scripd = sanitateString(scripd);
	}
	public String getScrinc() {
		return scrinc;
	}
	public void setScrinc(String scrinc) {
		this.scrinc = sanitateString(scrinc);
	}
	public String getScricr() {
		return scricr;
	}
	public void setScricr(String scricr) {
		this.scricr = sanitateString(scricr);
	}
	public String getScrtat() {
		return scrtat;
	}
	public void setScrtat(String scrtat) {
		this.scrtat = sanitateString(scrtat);
	}
	public String getScrsuc() {
		return scrsuc;
	}
	public void setScrsuc(String scrsuc) {
		this.scrsuc = sanitateString(scrsuc);
	}
	public String getScr1im() {
		return scr1im;
	}
	public void setScr1im(String scr1im) {
		this.scr1im = sanitateString(scr1im);
	}
	public String getScrfvt() {
		return scrfvt;
	}
	public void setScrfvt(String scrfvt) {
		this.scrfvt = sanitateString(scrfvt);
	}
	public String getScrqic() {
		return scrqic;
	}
	public void setScrqic(String scrqic) {
		this.scrqic = sanitateString(scrqic);
	}
	public String getScrqpd() {
		return scrqpd;
	}
	public void setScrqpd(String scrqpd) {
		this.scrqpd = sanitateString(scrqpd);
	}

//	public String getImporteAdeudado() {
//		return importeAdeudado;
//	}
//	public void setImporteAdeudado(String importeAdeudado) {
//		this.importeAdeudado = sanitateString(importeAdeudado);
//	}
	public String getScricv() {
		return scricv;
	}
	public void setScricv(String scricv) {
		this.scricv = sanitateString(scricv);
	}
	public String getScrdmo() {
		return scrdmo;
	}
	public void setScrdmo(String scrdmo) {
		this.scrdmo = sanitateString(scrdmo);
	}
	public String getScritrExporter() {
		return scritrExporter;
	}
	public void setScritrExporter(String scritrExporter) {
		this.scritrExporter = scritrExporter;
	}
	public String getScr1imExporter() {
		return scr1imExporter;
	}
	public void setScr1imExporter(String scr1imExporter) {
		this.scr1imExporter = scr1imExporter;
	}
	public String getScrrd1Title() {
		return scrrd1Title;
	}
	public void setScrrd1Title(String scrrd1Title) {
		this.scrrd1Title = sanitateString(scrrd1Title);
	}
	public String getScrrd2Title() {
		return scrrd2Title;
	}
	public void setScrrd2Title(String scrrd2Title) {
		this.scrrd2Title = sanitateString(scrrd2Title);
	}
	public String getScrre1Title() {
		return scrre1Title;
	}
	public void setScrre1Title(String scrre1Title) {
		this.scrre1Title = sanitateString(scrre1Title);
	}
	public String getScrre2Title() {
		return scrre2Title;
	}
	public void setScrre2Title(String scrre2Title) {
		this.scrre2Title = sanitateString(scrre2Title);
	}
	public String getScrre3Title() {
		return scrre3Title;
	}
	public void setScrre3Title(String scrre3Title) {
		this.scrre3Title = sanitateString(scrre3Title);
	}
	public String getScrre4Title() {
		return scrre4Title;
	}
	public void setScrre4Title(String scrre4Title) {
		this.scrre4Title = sanitateString(scrre4Title);
	}
	public String getScrre5Title() {
		return scrre5Title;
	}
	public void setScrre5Title(String scrre5Title) {
		this.scrre5Title = sanitateString(scrre5Title);
	}
	public String getScrivcExporter() {
		return scrivcExporter;
	}
	public void setScrivcExporter(String scrivcExporter) {
		this.scrivcExporter = scrivcExporter;
	}
	public String getScriceExporter() {
		return scriceExporter;
	}
	public void setScriceExporter(String scriceExporter) {
		this.scriceExporter = scriceExporter;
	}
	public String getScriccExporter() {
		return scriccExporter;
	}
	public void setScriccExporter(String scriccExporter) {
		this.scriccExporter = scriccExporter;
	}
	public String getScripdExporter() {
		return scripdExporter;
	}
	public void setScripdExporter(String scripdExporter) {
		this.scripdExporter = scripdExporter;
	}
	public String getScrincExporter() {
		return scrincExporter;
	}
	public void setScrincExporter(String scrincExporter) {
		this.scrincExporter = scrincExporter;
	}
	public String getScricrExporter() {
		return scricrExporter;
	}
	public void setScricrExporter(String scricrExporter) {
		this.scricrExporter = scricrExporter;
	}
	public String getScricvExporter() {
		return scricvExporter;
	}
	public void setScricvExporter(String scricvExporter) {
		this.scricvExporter = scricvExporter;
	}
	public String getScrqtr() {
		return scrqtr;
	}
	public void setScrqtr(String scrqtr) {
		this.scrqtr = scrqtr;
	}
	public String getScritf() {
		return scritf;
	}
	public void setScritf(String scritf) {
		this.scritf = scritf;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public String getScrivr() {
		return scrivr;
	}
	public void setScrivr(String scrivr) {
		this.scrivr = scrivr;
	}
	public String getScriad() {
		return scriad;
	}
	public void setScriad(String scriad) {
		this.scriad = scriad;
	}
	public String getScriac() {
		return scriac;
	}
	public void setScriac(String scriac) {
		this.scriac = scriac;
	}
	/**
	 * @return the scrnbo
	 */
	public String getScrnbo() {
		return scrnbo;
	}
	/**
	 * @param scrnbo the scrnbo to set
	 */
	public void setScrnbo(String scrnbo) {
		this.scrnbo = scrnbo;
	}
	/**
	 * @return the scrboc
	 */
	public String getScrboc() {
		return scrboc;
	}
	/**
	 * @param scrboc the scrboc to set
	 */
	public void setScrboc(String scrboc) {
		this.scrboc = sanitateString(scrboc);
	}	
	
}