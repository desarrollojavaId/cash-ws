package ar.com.gestionit.cashmanagement.persistence.dto;

public class ReferencesRecoveryDTO extends AbstractDTO {
	
	private static final long serialVersionUID = -7165660923868780349L;
	
	/*
	 * Constant for references IDs
	 */
	public static final String REF_D1 = "1";
	public static final String REF_D2 = "2";
	public static final String REF_C1 = "11";
	public static final String REF_C2 = "12";
	public static final String REF_C3 = "13";
	public static final String REF_C4 = "14";
	public static final String REF_C5 = "15";
	
	private String mreccg;
	private String mrecnv; //convenio
	private String mretbo; //tipo de boleta
	private String mredcg;

	public String getMreccg() {
		return mreccg;
	}

	public void setMreccg(String mreccg) {
		this.mreccg = mreccg;
	}
	
	public String getMredcg() {
		return mredcg;
	}

	public void setMredcg(String mredcg) {
		this.mredcg = mredcg;
	}
	public String getMrecnv() {
		return mrecnv;
	}

	public void setMrecnv(String mrecnv) {
		this.mrecnv = mrecnv;
	}

	public String getMretbo() {
		return mretbo;
	}

	public void setMretbo(String mretbo) {
		this.mretbo = mretbo;
	}

	
	
}
