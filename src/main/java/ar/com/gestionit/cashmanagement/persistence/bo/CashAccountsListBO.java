package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.CashAccountsListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.CashAccountsListMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;


public class CashAccountsListBO extends AbstractBO<IDTO, CashAccountsListMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = CashAccountsListMapper.class;
	}
	
	public CashAccountsListItemDTO findAccount(CashAccountsListItemDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession(true);
			CashAccountsListMapper mapper = session.getMapper(CashAccountsListMapper.class);
			List<CashAccountsListItemDTO> result = mapper.findAccount(dto);
			queryCounter++;
			session.commit();
			
			return result != null && result.size() > 0 ? result.get(0) : null;
		} catch(Exception e) {
			if(session != null)
				session.rollback();
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}