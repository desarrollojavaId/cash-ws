package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.PaymentListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.GeneralRecoveredMapper;
import ar.com.gestionit.cashmanagement.persistence.mapper.PaymentListMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class PaymentListBO extends AbstractBO<PaymentListItemDTO, PaymentListMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = PaymentListMapper.class;
	}

	/**
	 * This method returns a sucursal according to its ID
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public PaymentListItemDTO findSuc(PaymentListItemDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PaymentListMapper mapper = session.getMapper(PaymentListMapper.class);
			PaymentListItemDTO result = mapper.findSuc(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	/**
	 * 
	 */
	public String executeSSCONORP(PaymentListItemDTO input) throws ServiceException {
		SqlSession session = null;
		try {
			StoredProcedureParametersDTO storedDto = new StoredProcedureParametersDTO();
			storedDto.setParam1(String.format("%015d", 0));
			storedDto.setParam2("ES");
			storedDto.setParam3("99999");//reg count
			storedDto.setParam4(String.format("%07d", Long.parseLong(input.getAdherent())));//nro Adherente
			//storedDto.setParam4(input.getAccountNumber()); //nro Cuenta
			storedDto.setParam5(input.getLstCbus().size()>0?input.getLstCbus().get(0):String.format("%022d", 0));//cbu
			storedDto.setParam6(String.format("%08d", Long.parseLong(input.getDateFrom())));//validar
			storedDto.setParam7(String.format("%08d", Long.parseLong(input.getDateTo())));//validar
			storedDto.setParam8("0".equals(input.getPayOrderFrom())?String.format("%15s", ""):String.format("%-15s", input.getPayOrderFrom()));//refClieDesde
			storedDto.setParam9("0".equals(input.getPayOrderTo())?String.format("%15s", ""):String.format("%-15s", input.getPayOrderTo()));//refClieHasta
			storedDto.setParam10("0".equals(input.getBeneficiaryName())?String.format("%40s", ""):String.format("%-40s", input.getBeneficiaryName()));//nombreBenef
			storedDto.setParam11(String.format("%012d", Long.parseLong(input.getBeneficiaryDocument())));//BenefDoc
			storedDto.setParam12(String.format("%08d", Long.parseLong(input.getPayOrder())));//OpsegunCliente
			storedDto.setParam13("-1".equals(input.getAmountFrom())?String.format("%015d", 0):String.format("%015d", Long.parseLong(input.getAmountFrom())));//importeDesde
			storedDto.setParam14("-1".equals(input.getAmountTo())?String.format("%015d", 0):String.format("%015d", Long.parseLong(input.getAmountTo())));//ImporteHasta
			//Means
			storedDto.setParam15(input.getPaymentTypes().size()>0?String.format("%03d", Long.parseLong(input.getPaymentTypes().get(0).getNumber())):String.format("%03d", 0));
			storedDto.setParam16(input.getPaymentTypes().size()>1?String.format("%03d", Long.parseLong(input.getPaymentTypes().get(1).getNumber())):String.format("%03d", 0));
			storedDto.setParam17(input.getPaymentTypes().size()>2?String.format("%03d", Long.parseLong(input.getPaymentTypes().get(2).getNumber())):String.format("%03d", 0));
			storedDto.setParam18(input.getPaymentTypes().size()>3?String.format("%03d", Long.parseLong(input.getPaymentTypes().get(3).getNumber())):String.format("%03d", 0));
			storedDto.setParam19(input.getPaymentTypes().size()>4?String.format("%03d", Long.parseLong(input.getPaymentTypes().get(4).getNumber())):String.format("%03d", 0));
			storedDto.setParam20(input.getPaymentTypes().size()>5?String.format("%03d", Long.parseLong(input.getPaymentTypes().get(5).getNumber())):String.format("%03d", 0));
			storedDto.setParam21(input.getPaymentTypes().size()>6?String.format("%03d", Long.parseLong(input.getPaymentTypes().get(6).getNumber())):String.format("%03d", 0));
					
			//Status
			storedDto.setParam22(input.getStatuses().size()>0?String.format("%03d", Long.parseLong(input.getStatuses().get(0).getNumber())):String.format("%03d", 0));
			storedDto.setParam23(input.getStatuses().size()>1?String.format("%03d", Long.parseLong(input.getStatuses().get(1).getNumber())):String.format("%03d", 0));
			storedDto.setParam24(input.getStatuses().size()>2?String.format("%03d", Long.parseLong(input.getStatuses().get(2).getNumber())):String.format("%03d", 0));
			storedDto.setParam25(input.getStatuses().size()>3?String.format("%03d", Long.parseLong(input.getStatuses().get(3).getNumber())):String.format("%03d", 0));
			storedDto.setParam26(input.getStatuses().size()>4?String.format("%03d", Long.parseLong(input.getStatuses().get(4).getNumber())):String.format("%03d", 0));
			storedDto.setParam27(input.getStatuses().size()>5?String.format("%03d", Long.parseLong(input.getStatuses().get(5).getNumber())):String.format("%03d", 0));
			storedDto.setParam28(input.getStatuses().size()>6?String.format("%03d", Long.parseLong(input.getStatuses().get(6).getNumber())):String.format("%03d", 0));
			
			/*
			storedDto.setParam1("0");
			storedDto.setParam2("ES");
			storedDto.setParam3("99999");//reg count
			storedDto.setParam4(input.getAdherent());//nro Adherente
			//storedDto.setParam4(input.getAccountNumber()); //nro Cuenta
			storedDto.setParam5(input.getLstCbus().size()>0?input.getLstCbus().get(0):"0");//cbu
			storedDto.setParam6(input.getDateFrom());//validar
			storedDto.setParam7(input.getDateTo());//validar
			storedDto.setParam8("0".equals(input.getPayOrderFrom())?"":input.getPayOrderFrom());//refClieDesde
			storedDto.setParam9("0".equals(input.getPayOrderTo())?"":input.getPayOrderTo());//refClieHasta
			storedDto.setParam10("0".equals(input.getBeneficiaryName())?"":input.getBeneficiaryName());//nombreBenef
			storedDto.setParam11(input.getBeneficiaryDocument());//BenefDoc
			storedDto.setParam12(input.getPayOrder());//OpsegunCliente
			storedDto.setParam13("-1".equals(input.getAmountFrom())?"0":input.getAmountFrom());//importeDesde
			storedDto.setParam14("-1".equals(input.getAmountTo())?"0":input.getAmountTo());//ImporteHasta
			//Means
			storedDto.setParam15(input.getPaymentTypes().size()>0?input.getPaymentTypes().get(0).getNumber():"0");
			storedDto.setParam16(input.getPaymentTypes().size()>1?input.getPaymentTypes().get(1).getNumber():"0");
			storedDto.setParam17(input.getPaymentTypes().size()>2?input.getPaymentTypes().get(2).getNumber():"0");
			storedDto.setParam18(input.getPaymentTypes().size()>3?input.getPaymentTypes().get(3).getNumber():"0");
			storedDto.setParam19(input.getPaymentTypes().size()>4?input.getPaymentTypes().get(4).getNumber():"0");
			storedDto.setParam20(input.getPaymentTypes().size()>5?input.getPaymentTypes().get(5).getNumber():"0");
			storedDto.setParam21(input.getPaymentTypes().size()>6?input.getPaymentTypes().get(6).getNumber():"0");		
			//Status
			storedDto.setParam22(input.getStatuses().size()>0?input.getStatuses().get(0).getNumber():"0");
			storedDto.setParam23(input.getStatuses().size()>1?input.getStatuses().get(1).getNumber():"0");
			storedDto.setParam24(input.getStatuses().size()>2?input.getStatuses().get(2).getNumber():"0");
			storedDto.setParam25(input.getStatuses().size()>3?input.getStatuses().get(3).getNumber():"0");
			storedDto.setParam26(input.getStatuses().size()>4?input.getStatuses().get(4).getNumber():"0");
			storedDto.setParam27(input.getStatuses().size()>5?input.getStatuses().get(5).getNumber():"0");
			storedDto.setParam28(input.getStatuses().size()>6?input.getStatuses().get(6).getNumber():"0");	
			*/
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PaymentListMapper mapper = session.getMapper(PaymentListMapper.class);
			mapper.executeSSCONORP(storedDto);
			session.close();
			queryCounter++;

			return storedDto.getParam1();
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	/**
	 * This method returns a sucursal according to its ID
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public List<PaymentListItemDTO> findSSCONORP(PaymentListItemDTO dto) throws ServiceException {
		SqlSession session = null;		
		try {
			loadPagingData(dto);
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PaymentListMapper mapper = session.getMapper(PaymentListMapper.class);
			List<PaymentListItemDTO> result = mapper.findSSCONORP(dto);
			queryCounter++;
			//Process paging
			processResultPaging(result);
			
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	
	/**
	 * This method returns a sucursal according to its ID
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public PaymentListItemDTO findCountSumTotal(PaymentListItemDTO dto) throws ServiceException {
		SqlSession session = null;		
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			PaymentListMapper mapper = session.getMapper(PaymentListMapper.class);
			PaymentListItemDTO result = mapper.findCountSumTotal(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}