package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.FileReceptionMapper;

public class FileReceptionBO extends AbstractBO<IDTO, FileReceptionMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = FileReceptionMapper.class;
	}
}