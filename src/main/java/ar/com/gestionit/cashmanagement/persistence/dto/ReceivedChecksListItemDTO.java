package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"tipo",
		"numero",
		"estado",
		"banco",
		"sucursal",
		"codpos",
		"importe",
		"moneda",
		"tipodoclibrador",
		"doclibrador",
		"fechaacredit",
		"fechacheque",
		"numrecaudacion",
		"ctagirada",
		"fecharecaud",
		"ref1deudor",
		"ref2deudor",
		"ref1comp",
		"ref2comp",
		"ref3comp",
		"ref4comp",
		"ref5comp",
		"codbanco"
})
@XmlRootElement(name = ServiceConstant.DTO_RECEIVEDCHECK)
public class ReceivedChecksListItemDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -227729378343769384L;
	
	//Outputs
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_TIPO, defaultValue="")
	private String tipo;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_TIPO)
	@XmlTransient
	private String tipoExporter;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_NUMERO)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_NUMERO, defaultValue="")
	private String numero;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_ESTADO)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_ESTADO, defaultValue="")
	private String estado;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_BANCHEID)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_BANCHEID, defaultValue="")
	private String codbanco;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_BANCO)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_BANCO, defaultValue="")
	private String banco;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_SUCURSAL)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_SUCURSAL, defaultValue="")
	private String sucursal;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_CODPOS)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_CODPOS, defaultValue="")
	private String codpos;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_IMPORTE, defaultValue="")
	private String importe;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_IMPORTE)
	@XmlTransient
	private String importeExporter;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_TIPODOCLIBRADOR)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_TIPODOCLIBRADOR, defaultValue="")
	private String tipodoclibrador;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_DOCLIBRADOR)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_DOCLIBRADOR, defaultValue="")
	private String doclibrador;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_FECHAACREDIT)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_FECHAACREDIT, defaultValue="")
	private String fechaacredit;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_FECHACHEQUE)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_FECHACHEQUE, defaultValue="")
	private String fechacheque;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_NUMRECAUDACION)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_NUMRECAUDACION, defaultValue="")
	private String numrecaudacion;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_CTAGIRADA)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_CTAGIRADA, defaultValue="")
	private String ctagirada;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_FECHARECAUD)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_FECHARECAUD, defaultValue="")
	private String fecharecaud;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_REF1DEUDOR, hasDynamicTitle=true, required=false)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF1DEUDOR, defaultValue="")
	private String ref1deudor;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_REF2DEUDOR, hasDynamicTitle=true, required=false)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF2DEUDOR, defaultValue="")
	private String ref2deudor;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_REF1COMP, hasDynamicTitle=true, required=false)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF1COMP, defaultValue="")
	private String ref1comp;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_REF2COMP, hasDynamicTitle=true, required=false)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF2COMP, defaultValue="")
	private String ref2comp;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_REF3COMP, hasDynamicTitle=true, required=false)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF3COMP, defaultValue="")
	private String ref3comp;
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF4COMP, defaultValue="")
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_REF4COMP, hasDynamicTitle=true, required=false)
	private String ref4comp;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_REF5COMP, hasDynamicTitle=true, required=false)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_REF5COMP, defaultValue="")
	private String ref5comp;
	@FieldToExport(name=ServiceConstant.DTO_RECEIVEDCHECK_MONEDA)
	@XmlElement(name=ServiceConstant.DTO_RECEIVEDCHECK_MONEDA, defaultValue="")
	private String moneda;
	@XmlTransient
	private String formacobro;
	
	@XmlTransient
	private String ref1deudorTitle;
	@XmlTransient
	private String ref2deudorTitle;
	@XmlTransient
	private String ref1compTitle;
	@XmlTransient
	private String ref2compTitle;
	@XmlTransient
	private String ref3compTitle;
	@XmlTransient
	private String ref4compTitle;
	@XmlTransient
	private String ref5compTitle;
		
	public String getFormacobro() {
		return formacobro;
	}
	public void setFormacobro(String formacobro) {
		this.formacobro = formacobro;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = sanitateString(tipo);
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = sanitateString(numero);
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = sanitateString(estado);
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = sanitateString(banco);
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sanitateString(sucursal);
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = sanitateString(importe);
	}
	public String getDoclibrador() {
		return doclibrador;
	}
	public void setDoclibrador(String doclibrador) {
		this.doclibrador = sanitateString(doclibrador);
	}
	public String getFechaacredit() {
		return fechaacredit;
	}
	public void setFechaacredit(String fechaacredit) {
		this.fechaacredit = sanitateString(fechaacredit);
	}
	public String getFechacheque() {
		return fechacheque;
	}
	public void setFechacheque(String fechacheque) {
		this.fechacheque = sanitateString(fechacheque);
	}
	public String getNumrecaudacion() {
		return numrecaudacion;
	}
	public void setNumrecaudacion(String numrecaudacion) {
		this.numrecaudacion = sanitateString(numrecaudacion);
	}
	public String getCtagirada() {
		return ctagirada;
	}
	public void setCtagirada(String ctagirada) {
		this.ctagirada = sanitateString(ctagirada);
	}
	public String getFecharecaud() {
		return fecharecaud;
	}
	public void setFecharecaud(String fecharecaud) {
		this.fecharecaud = sanitateString(fecharecaud);
	}
	public String getRef1deudor() {
		return ref1deudor;
	}
	public void setRef1deudor(String ref1deudor) {
		this.ref1deudor = sanitateString(ref1deudor);
	}
	public String getRef2deudor() {
		return ref2deudor;
	}
	public void setRef2deudor(String ref2deudor) {
		this.ref2deudor = sanitateString(ref2deudor);
	}
	public String getRef1comp() {
		return ref1comp;
	}
	public void setRef1comp(String ref1comp) {
		this.ref1comp = sanitateString(ref1comp);
	}
	public String getRef2comp() {
		return ref2comp;
	}
	public void setRef2comp(String ref2comp) {
		this.ref2comp = sanitateString(ref2comp);
	}
	public String getRef3comp() {
		return ref3comp;
	}
	public void setRef3comp(String ref3comp) {
		this.ref3comp = sanitateString(ref3comp);
	}
	public String getRef4comp() {
		return ref4comp;
	}
	public void setRef4comp(String ref4comp) {
		this.ref4comp = sanitateString(ref4comp);
	}
	public String getRef5comp() {
		return ref5comp;
	}
	public void setRef5comp(String ref5comp) {
		this.ref5comp = sanitateString(ref5comp);
	}
	public String getCodpos() {
		return codpos;
	}
	public void setCodpos(String codpos) {
		this.codpos = sanitateString(codpos);
	}
	public String getTipodoclibrador() {
		return tipodoclibrador;
	}
	public void setTipodoclibrador(String tipodoclibrador) {
		this.tipodoclibrador = sanitateString(tipodoclibrador);
	}
	public String getCodbanco() {
		return codbanco;
	}
	public void setCodbanco(String codbanco) {
		this.codbanco = codbanco;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = sanitateString(moneda);
	}
	public String getImporteExporter() {
		return importeExporter;
	}
	public void setImporteExporter(String importeExporter) {
		this.importeExporter = sanitateString(importeExporter);
	}
	public String getTipoExporter() {
		return tipoExporter;
	}
	public void setTipoExporter(String tipoExporter) {
		this.tipoExporter = sanitateString(tipoExporter);
	}
	public String getRef1deudorTitle() {
		return ref1deudorTitle;
	}
	public void setRef1deudorTitle(String ref1deudorTitle) {
		this.ref1deudorTitle = ref1deudorTitle;
	}
	public String getRef2deudorTitle() {
		return ref2deudorTitle;
	}
	public void setRef2deudorTitle(String ref2deudorTitle) {
		this.ref2deudorTitle = ref2deudorTitle;
	}
	public String getRef1compTitle() {
		return ref1compTitle;
	}
	public void setRef1compTitle(String ref1compTitle) {
		this.ref1compTitle = ref1compTitle;
	}
	public String getRef2compTitle() {
		return ref2compTitle;
	}
	public void setRef2compTitle(String ref2compTitle) {
		this.ref2compTitle = ref2compTitle;
	}
	public String getRef3compTitle() {
		return ref3compTitle;
	}
	public void setRef3compTitle(String ref3compTitle) {
		this.ref3compTitle = ref3compTitle;
	}
	public String getRef4compTitle() {
		return ref4compTitle;
	}
	public void setRef4compTitle(String ref4compTitle) {
		this.ref4compTitle = ref4compTitle;
	}
	public String getRef5compTitle() {
		return ref5compTitle;
	}
	public void setRef5compTitle(String ref5compTitle) {
		this.ref5compTitle = ref5compTitle;
	} 	
}