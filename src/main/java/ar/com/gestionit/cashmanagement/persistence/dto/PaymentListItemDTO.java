package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.annotation.CodeMapped;
import ar.com.gestionit.cashmanagement.annotation.FieldToExport;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"mtlqop",
		"mtlqso",
		"mtlfpa",
		"mtlqrc",
		"oppronom",
		"sckder",
		"mtlcui",
		"mtljpa",
		"mtlkme",
		"mtlqip",
		"mtovto",
		"statusNumber",
		"kes",
		"fecha",
		"suc",
		"mtxden",
		"dbe",
		"kfp",
		"cuenta",
		"modulo",
		"sucursal",
		"moneda",
		"suboperacion",
		"cbu",
		"motivorechazo",
		"monedalote"})
@XmlRootElement(name = ServiceConstant.DTO_PAYMENT)
public class PaymentListItemDTO extends AbstractDTO {

	/**
	 * Serial version
	 */
	private static final long serialVersionUID = -7798867926488859811L;

	//Input
	@XmlTransient
	private String beneficiaryDocument;
	@XmlTransient
	private String beneficiaryDocumentForLike;
	@XmlTransient
	private String dateFrom;
	@XmlTransient
	private String dateTo;
	@XmlTransient
	private String adherent;
	@XmlTransient
	private String accountNumber;
	@XmlTransient
	private String payOrderFrom;
	@XmlTransient
	private String payOrderTo;
	@XmlTransient
	private String payOrder;
	@XmlTransient
	private String amountFrom;
	@XmlTransient
	private String amountTo;
	@XmlTransient
	private String beneficiaryName;
	@XmlTransient
	private List<PaymentTypeListItemDTO> paymentTypes;
	@XmlTransient
	private List<PaymentStatusInputListItemDTO> statuses;
	@XmlTransient
	private List<String> lstCbus;
	
	
	
	

	//Input-output
	@XmlTransient
	private String mtosuc;
	@XmlTransient
	private String mtgsuc;
	@XmlTransient
	private String proveenom;
	@XmlTransient
	private String mtofem;
	
	@XmlTransient
	private String codSuc;
	
	@XmlTransient
	private String descripcionSuc;
	
	@XmlTransient
	private String executionId;

	//Ouput
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_REFCLI)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_REFCLI, defaultValue="")
	private String mtlqrc;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_STATUSNRO)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_STATUSNRO, defaultValue="")
	private String statusNumber;
	
	@XmlTransient
	private String statusSubnumber;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_STATUSDESC)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_STATUSDESC, defaultValue="")
	private String kes;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_OPPRONOM)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_OPPRONOM, defaultValue="")
	private String oppronom;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_DBE)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_DBE, defaultValue="")
	private String dbe;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_KFP)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_KFP, defaultValue="")
	private String kfp;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_MTOVTO)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_MTOVTO, defaultValue="")
	private String mtovto;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_FECHA)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_FECHA, defaultValue="")
	private String fecha;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_MTLFPA)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_MTLFPA, defaultValue="")
	private String mtlfpa;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_SCKDER)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_SCKDER, defaultValue="")
	private String sckder;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_MTLCUI)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_MTLCUI, defaultValue="")
	private String mtlcui;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_MTLQIP)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_MTLQIP, defaultValue="")
	private String mtlqip;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_MTLQOP)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_MTLQOP, defaultValue="")
	private String mtlqop;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_MTLQSO)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_MTLQSO, defaultValue="")
	private String mtlqso;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_MTLKME)
	@CodeMapped
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_MTLKME, defaultValue="")
	private String mtlkme;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_MTXDEN)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_MTXDEN, defaultValue="")
	private String mtxden;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_MODULO)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_MODULO, defaultValue="")
	private String modulo;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_SUCURSAL)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_SUCURSAL, defaultValue="")
	private String sucursal; 
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_MONEDA)
	@CodeMapped
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_MONEDA, defaultValue="")
	private String moneda;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_MTLJPA)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_MTLJPA, defaultValue="")
	private String mtljpa;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_SUBOPERA)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_SUBOPERA, defaultValue="")
	private String suboperacion;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_CUENTA)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_CUENTA, defaultValue="")
	private String cuenta;
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_CBU)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_CBU, defaultValue="")
	private String cbu;
	
	@FieldToExport(name=ServiceConstant.DTO_PAYMENT_MOTIVORECH)
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_MOTIVORECH, defaultValue="")
	private String motivorechazo;
	
	
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_MONEDALOTE, defaultValue="")
	private String monedalote;
	
	
	
	@XmlTransient
	private String submedio;
	

	@XmlTransient
	private String mtaasde;
	
	@XmlTransient
	private String countNum;
	
	@XmlTransient
	private String sumAmount;

	@XmlElement(name=ServiceConstant.DTO_PAYMENT_SUC, defaultValue="")
	private String suc;

	public void setMtlqrc(String mtlqrc) {
		this.mtlqrc = sanitateString(mtlqrc);
	}
	public void setKes(String kes) {
		this.kes = sanitateString(kes);
	}
	public void setOppronom(String oppronom) {
		this.oppronom = sanitateString(oppronom);
	}
	public void setDbe(String dbe) {
		this.dbe = sanitateString(dbe);
	}
	public void setMtovto(String mtovto) {
		this.mtovto = sanitateString(mtovto);
	}
	public void setFecha(String fecha) {
		this.fecha = sanitateString(fecha);
	}
	public void setSckder(String sckder) {
		this.sckder = sanitateString(sckder);
	}
	public void setMtlfpa(String mtlfpa) {
		this.mtlfpa = sanitateString(mtlfpa);
	}
	public void setMtljpa(String mtljpa) {
		this.mtljpa = sanitateString(mtljpa);
	}
	public void setMtlcui(String mtlcui) {
		this.mtlcui = sanitateString(mtlcui);
	}
	public void setMtlqip(String mtlqip) {
		this.mtlqip = sanitateString(mtlqip);
	}
	public void setMtlqop(String mtlqop) {
		this.mtlqop = sanitateString(mtlqop);
	}
	public void setMtlqso(String mtlqso) {
		this.mtlqso = sanitateString(mtlqso);
	}
	public void setMtlkme(String mtlkme) {
		this.mtlkme = sanitateString(mtlkme);
	}
	public void setMtaasde(String mtaasde) {
		this.mtaasde = sanitateString(mtaasde);
	}
	public void setSuc(String suc) {
		this.suc = sanitateString(suc);
	}
	public void setMtosuc(String mtosuc) {
		this.mtosuc = sanitateString(mtosuc);
	}
	public void setMtgsuc(String mtgsuc) {
		this.mtgsuc = sanitateString(mtgsuc);
	}
	public void setMtofem(String mtofem) {
		this.mtofem = sanitateString(mtofem);
	}
	public String getSuc() {
		return suc;
	}
	public String getMtaasde() {
		return mtaasde;
	}
	public String getBeneficiaryDocument() {
		return beneficiaryDocument;
	}
	public void setBeneficiaryDocument(String beneficiaryDocument) {
		this.beneficiaryDocument = beneficiaryDocument;
	}
	public String getDateFrom() {
		return dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	public String getAdherent() {
		return adherent;
	}
	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}
	public String getPayOrderFrom() {
		return payOrderFrom;
	}
	public void setPayOrderFrom(String payOrderFrom) {
		this.payOrderFrom = payOrderFrom;
	}
	public String getPayOrderTo() {
		return payOrderTo;
	}
	public void setPayOrderTo(String payOrderTo) {
		this.payOrderTo = payOrderTo;
	}
	public String getBeneficiaryName() {
		return beneficiaryName;
	}
	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}
	/**
	 * @return the paymentTypes
	 */
	public List<PaymentTypeListItemDTO> getPaymentTypes() {
		return paymentTypes;
	}
	/**
	 * @param paymentTypes the paymentTypes to set
	 */
	public void setPaymentTypes(List<PaymentTypeListItemDTO> paymentTypes) {
		this.paymentTypes = paymentTypes;
	}
	public String getMtlqrc() {
		return mtlqrc;
	}
	public String getKes() {
		return kes;
	}
	public String getMtosuc() {
		return this.getCodSuc();
	}
	public String getMtgsuc() {
		return this.getCodSuc();
	}
	public String getOppronom() {
		return oppronom;
	}
	public String getDbe() {
		return dbe;
	}
	public String getMtovto() {
		return mtovto;
	}
	public String getMtofem() {
		return mtofem;
	}
	public String getFecha() {
		return fecha;
	}
	public String getSckder() {
		return sckder;
	}
	public String getMtlfpa() {
		return mtlfpa;
	}
	public String getMtljpa() {
		return mtljpa;
	}
	public String getMtlcui() {
		return mtlcui;
	}
	public String getMtlqip() {
		return mtlqip;
	}
	public String getMtlqop() {
		return mtlqop;
	}
	public String getMtlqso() {
		return mtlqso;
	}
	public String getMtlkme() {
		return mtlkme;
	}
	/**
	 * @return the statuses
	 */
	public List<PaymentStatusInputListItemDTO> getStatuses() {
		return statuses;
	}
	/**
	 * @param statuses the statuses to set
	 */
	public void setStatuses(List<PaymentStatusInputListItemDTO> statuses) {
		this.statuses = statuses;
	}
	public String getProveenom() {
		return proveenom;
	}
	public void setProveenom(String proveenom) {
		this.proveenom = sanitateString(proveenom);
	}
	public String getMtxden() {
		return this.getDescripcionSuc();
	}
	public void setMtxden(String mtxden) {
		this.mtxden = sanitateString(mtxden);
	}
	/**
	 * @return the beneficiaryDocumentForLike
	 */
	public String getBeneficiaryDocumentForLike() {
		return beneficiaryDocumentForLike;
	}
	/**
	 * @param beneficiaryDocumentForLike the beneficiaryDocumentForLike to set
	 */
	public void setBeneficiaryDocumentForLike(String beneficiaryDocumentForLike) {
		this.beneficiaryDocumentForLike = beneficiaryDocumentForLike;
	}
	/**
	 * @return the amountFrom
	 */
	public String getAmountFrom() {
		return amountFrom;
	}
	/**
	 * @param amountFrom the amountFrom to set
	 */
	public void setAmountFrom(String amountFrom) {
		this.amountFrom = amountFrom;
	}
	/**
	 * @return the amountTo
	 */
	public String getAmountTo() {
		return amountTo;
	}
	/**
	 * @param amountTo the amountTo to set
	 */
	public void setAmountTo(String amountTo) {
		this.amountTo = amountTo;
	}
	
	/**
	 * @return the payOrder
	 */
	public String getPayOrder() {
		return payOrder;
	}
	/**
	 * @param payOrder the payOrder to set
	 */
	public void setPayOrder(String payOrder) {
		this.payOrder = payOrder;
	}
	/**
	 * @return the accountNumber
	 */
	public String getAccountNumber() {
		return accountNumber;
	}
	/**
	 * @param accountNumber the accountNumber to set
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	/**
	 * @return the kfp
	 */
	public String getKfp() {
		return kfp;
	}
	/**
	 * @param kfp the kfp to set
	 */
	public void setKfp(String kfp) {
		this.kfp = sanitateString(kfp);
	}
	/**
	 * @return the statusNumber
	 */
	public String getStatusNumber() {
		return statusNumber;
	}
	/**
	 * @param statusNumber the statusNumber to set
	 */
	public void setStatusNumber(String statusNumber) {
		this.statusNumber = sanitateString(statusNumber);
	}
	/**
	 * @return the statusSubnumber
	 */
	public String getStatusSubnumber() {
		return statusSubnumber;
	}
	/**
	 * @param statusSubnumber the statusSubnumber to set
	 */
	public void setStatusSubnumber(String statusSubnumber) {
		this.statusSubnumber = sanitateString(statusSubnumber);
	}
	
	
	public String getModulo() {
		return modulo;
	}
	public void setModulo(String modulo) {
		this.modulo = sanitateString(modulo);
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sanitateString(sucursal);
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = sanitateString(moneda);
	}
	public String getSuboperacion() {
		return suboperacion;
	}
	public void setSuboperacion(String suboperacion) {
		this.suboperacion = sanitateString(suboperacion);
	}
	

	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = sanitateString(cuenta);
	}
	public String getSubmedio() {
		return submedio;
	}
	public void setSubmedio(String submedio) {
		this.submedio = submedio;
	}
	public List<String> getLstCbus() {
		return lstCbus;
	}
	public void setLstCbus(List<String> lstCbus) {
		this.lstCbus = lstCbus;
	}
	public String getCbu() {
		return cbu;
	}
	public void setCbu(String cbu) {
		this.cbu = sanitateString(cbu);
	}
	public String getCodSuc() {
		return codSuc;
	}
	public void setCodSuc(String codSuc) {
		this.codSuc = codSuc;
	}
	public String getDescripcionSuc() {
		return descripcionSuc;
	}
	public void setDescripcionSuc(String descripcionSuc) {
		this.descripcionSuc = descripcionSuc;
	}
	public String getMotivorechazo() {
		return motivorechazo;
	}
	public void setMotivorechazo(String motivorechazo) {
		this.motivorechazo =  sanitateString(motivorechazo);
	}
	/**
	 * @return the executionId
	 */
	public String getExecutionId() {
		return executionId;
	}
	/**
	 * @param executionId the executionId to set
	 */
	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}
	public String getCountNum() {
		return countNum;
	}
	public void setCountNum(String countNum) {
		this.countNum = countNum;
	}
	public String getSumAmount() {
		return sumAmount;
	}
	public void setSumAmount(String sumAmount) {
		this.sumAmount = sumAmount;
	}
	/**
	 * @return the monedalote
	 */
	public String getMonedalote() {
		return monedalote;
	}
	/**
	 * @param monedalote the monedalote to set
	 */
	public void setMonedalote(String monedalote) {
		this.monedalote = monedalote;
	}
	
	
	
}