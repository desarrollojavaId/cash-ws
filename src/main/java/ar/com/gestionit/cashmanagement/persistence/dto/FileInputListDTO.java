package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"files"})
@XmlRootElement(name = ServiceConstant.DTO_FILERECEPTION_ROOT)
public class FileInputListDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7292805869423905403L;
	@XmlElement(name=ServiceConstant.TAG_ID)
	private List<String> files;
	
	
	/**
	 * @return the files
	 */
	public List<String> getFiles() {
		return files;
	}

	/**
	 * @param statuses the files to set
	 */
	public void setFiles(List<String> files) {
		this.files = files;
	}

}