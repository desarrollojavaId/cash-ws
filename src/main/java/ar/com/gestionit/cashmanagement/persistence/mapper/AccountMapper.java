package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;

public interface AccountMapper extends IMapper<IDTO>{
	public void findAccount(StoredProcedureParametersDTO dto);
	public String findDefaultCBU(String adherent);
}