package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.CheckStatusListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.CheckStatusListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.CheckStatusListMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class CheckStatusListBO extends AbstractBO<CheckStatusListItemDTO, CheckStatusListMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = CheckStatusListMapper.class;
	}
	
	
	/**
	 * This method returns a sucursal according to its ID
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public List<CheckStatusListItemDTO> findChk(CheckStatusListDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			//Load paging data
			super.loadPagingData(dto);
			
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			CheckStatusListMapper mapper = session.getMapper(CheckStatusListMapper.class);
			List<CheckStatusListItemDTO> result = mapper.findChk(dto);
			queryCounter++;
			
			//Process paging
			processResultPaging(result);
			
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	/**
	 * This method returns a sucursal according to its ID
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public String findChkUnPrinted(CheckStatusListDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			CheckStatusListMapper mapper = session.getMapper(CheckStatusListMapper.class);
			String result = mapper.findChkUnPrinted(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
	
	public String findTotChk(CheckStatusListDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			CheckStatusListMapper mapper = session.getMapper(CheckStatusListMapper.class);
			String result = mapper.findTotChk(dto);
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}