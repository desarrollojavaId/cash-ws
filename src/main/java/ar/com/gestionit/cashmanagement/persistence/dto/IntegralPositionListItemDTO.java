package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"groupingDescription",
		"incomeType",
		"montoRec",
		"money",
		"agreements"
})
@XmlRootElement(name = ServiceConstant.DTO_INTEGRALPOSITION_CANAL)
public class IntegralPositionListItemDTO extends AbstractDTO {

	private static final long serialVersionUID = -5680743257985240257L;
	
	@XmlTransient
	private String grouping;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_TIPOREC_DESC, defaultValue="")
	private String groupingDescription;
	@XmlTransient
	private String agreement;
	@XmlTransient
	private String subAgreement;
	@XmlTransient
	private String agreementDescription;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_TIPORECA, defaultValue="")
	private String incomeType;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_MONTOREC, defaultValue="")
	private String montoRec;
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_MONEDA, defaultValue="")
	private String money;
	@XmlElementWrapper(name=ServiceConstant.DTO_INTEGRALPOSITION_COBRO_ROOT)
	@XmlElement(name=ServiceConstant.DTO_INTEGRALPOSITION_COBRO, defaultValue="")
	private List<IntegralPositionListAgreementDTO> agreements;
	
	/**
	 * @return the grouping
	 */
	public String getGrouping() {
		return grouping;
	}
	/**
	 * @param grouping the grouping to set
	 */
	public void setGrouping(String grouping) {
		this.grouping = grouping;
	}
	/**
	 * @return the groupingDescription
	 */
	public String getGroupingDescription() {
		return groupingDescription;
	}
	/**
	 * @param groupingDescription the groupingDescription to set
	 */
	public void setGroupingDescription(String groupingDescription) {
		this.groupingDescription = groupingDescription;
	}
	/**
	 * @return the agreement
	 */
	public String getAgreement() {
		return agreement;
	}
	/**
	 * @param agreement the agreement to set
	 */
	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}
	/**
	 * @return the subAgreement
	 */
	public String getSubAgreement() {
		return subAgreement;
	}
	/**
	 * @param subAgreement the subAgreement to set
	 */
	public void setSubAgreement(String subAgreement) {
		this.subAgreement = subAgreement;
	}
	/**
	 * @return the agreementDescription
	 */
	public String getAgreementDescription() {
		return agreementDescription;
	}
	/**
	 * @param agreementDescription the agreementDescription to set
	 */
	public void setAgreementDescription(String agreementDescription) {
		this.agreementDescription = agreementDescription;
	}
	/**
	 * @return the montoRec
	 */
	public String getMontoRec() {
		return montoRec;
	}
	/**
	 * @param montoRec the montoRec to set
	 */
	public void setMontoRec(String montoRec) {
		this.montoRec = montoRec;
	}
	/**
	 * @return the agreements
	 */
	public List<IntegralPositionListAgreementDTO> getAgreements() {
		return agreements;
	}
	/**
	 * @param agreements the agreements to set
	 */
	public void setAgreements(List<IntegralPositionListAgreementDTO> agreements) {
		this.agreements = agreements;
	}
	/**
	 * @return the incomeType
	 */
	public String getIncomeType() {
		return incomeType;
	}
	/**
	 * @param incomeType the incomeType to set
	 */
	public void setIncomeType(String incomeType) {
		this.incomeType = incomeType;
	}
	public String getMoney() {
		return money;
	}
	public void setMoney(String money) {
		this.money = sanitateString(money);
	}
}		