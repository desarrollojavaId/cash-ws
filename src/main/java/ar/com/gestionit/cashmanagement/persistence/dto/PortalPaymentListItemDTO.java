package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"mtlqop",
					"mtlqso",
					"mtlfer",
					"mtlnep",
					"tDocEmisor",
					"mtlqcp",
					"mtlkfp",
					"mtljpa",
					"mtlfal",
					"estCod",
					"estDesc",
					"adherent",
					"mail",
					"markMail",
					"mtlkme",
					"tbdabr"
})
@XmlRootElement(name = ServiceConstant.DTO_CARD)
public class PortalPaymentListItemDTO extends AbstractDTO{

	
	private static final long serialVersionUID = -2234199377986286067L;
	
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_NOP, defaultValue="")
	private String mtlqop;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_SNOP, defaultValue="")
	private String mtlqso;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_FECPAGO, defaultValue="")
	private String mtlfer;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_EMISOR, defaultValue="")
	private String mtlnep;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_TDOCEMISOR, defaultValue="")
	private String tDocEmisor;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_DOCEMISOR, defaultValue="")
	private String mtlqcp;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_IDFP, defaultValue="")
	private String mtlkfp;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_IMP, defaultValue="")
	private String mtljpa;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_FECEMISION, defaultValue="")
	private String mtlfal;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_IDEST, defaultValue="")
	private String estCod;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_ESTDESC, defaultValue="")
	private String estDesc;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_BENMAIL, defaultValue="")
	private String mail;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_ADHERENTE, defaultValue="")
	private String adherent;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_ENVIAMAIL, defaultValue="")
	private String markMail;
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_MONEDA, defaultValue="")
	private String tbdabr;
	
	
	@XmlElement(name=ServiceConstant.DTO_PORTALPAYMENT_MEDPAGO, defaultValue="")
	//MTLKME
	private String mtlkme;
	
	@XmlTransient
	private String estSCod;
	
	@XmlTransient
	private String fecCheq;	
	
	public String getMtlqop() {
		return mtlqop;
	}
	public void setMtlqop(String mtlqop) {
		this.mtlqop = sanitateString(mtlqop);
	}
	public String getMtlqso() {
		return mtlqso;
	}
	public void setMtlqso(String mtlqso) {
		this.mtlqso = sanitateString(mtlqso);
	}
	public String getMtlfer() {
		return mtlfer;
	}
	public void setMtlfer(String mtlfer) {
		this.mtlfer = (mtlfer);
	}
	public String getMtlnep() {
		return mtlnep;
	}
	public void setMtlnep(String mtlnep) {
		this.mtlnep = sanitateString(mtlnep);
	}
	public String gettDocEmisor() {
		return tDocEmisor;
	}
	public void settDocEmisor(String tDocEmisor) {
		this.tDocEmisor = sanitateString(tDocEmisor);
	}
	public String getMtlqcp() {
		return mtlqcp;
	}
	public void setMtlqcp(String mtlqcp) {
		this.mtlqcp = sanitateString(mtlqcp);
	}
	public String getMtlkfp() {
		return mtlkfp;
	}
	public void setMtlkfp(String mtlkfp) {
		this.mtlkfp = sanitateString(mtlkfp);
	}
	public String getMtljpa() {
		return mtljpa;
	}
	public void setMtljpa(String mtljpa) {
		this.mtljpa = sanitateString(mtljpa);
	}
	public String getMtlfal() {
		return mtlfal;
	}
	public void setMtlfal(String mtlfal) {
		this.mtlfal = sanitateString(mtlfal);
	}
	public String getEstDesc() {
		return estDesc;
	}
	public void setEstDesc(String estDesc) {
		this.estDesc = sanitateString(estDesc);
	}
	/**
	 * @return the estCod
	 */
	public String getEstCod() {
		return estCod;
	}
	/**
	 * @param estCod the estCod to set
	 */
	public void setEstCod(String estCod) {
		this.estCod = estCod;
	}
	/**
	 * @return the estSCod
	 */
	public String getEstSCod() {
		return estSCod;
	}
	/**
	 * @param estSCod the estSCod to set
	 */
	public void setEstSCod(String estSCod) {
		this.estSCod = estSCod;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = sanitateString(mail);
	}
	public String getAdherent() {
		return adherent;
	}
	public void setAdherent(String adherent) {
		this.adherent = sanitateString(adherent);
	}
	public String getMarkMail() {
		return markMail;
	}
	public void setMarkMail(String markMail) {
		this.markMail = markMail;
	}
	public String getMtlkme() {
		return mtlkme;
	}
	public void setMtlkme(String mtlkme) {
		this.mtlkme = mtlkme;
	}
	public String getTbdabr() {
		return tbdabr;
	}
	public void setTbdabr(String tbdabr) {
		this.tbdabr = sanitateString(tbdabr);
	}
	public String getFecCheq() {
		return fecCheq;
	}
	public void setFecCheq(String fecCheq) {
		this.fecCheq = fecCheq;
	}
	
	
	
	
}