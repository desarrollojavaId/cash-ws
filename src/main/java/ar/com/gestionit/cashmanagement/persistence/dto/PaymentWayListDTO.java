package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list",
})
@XmlRootElement(name = ServiceConstant.DTO_PAYMENT_WAY_FORMASDEPAGO)
public class PaymentWayListDTO extends AbstractDTO{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7216995175981339991L;
	@XmlElement(name=ServiceConstant.DTO_PAYMENT_WAY_FORMADEPAGO, defaultValue="")
	private List<PaymentWayListItemDTO> list;

	public List<PaymentWayListItemDTO> getList() {
		return list;
	}

	public void setList(List<PaymentWayListItemDTO> list) {
		this.list = list;
	}
}
