package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_CONVLIST_ROOT)
public class ConvListDTO extends AbstractDTO{
	
	private static final long serialVersionUID = -3829644289280172064L;
	
	@XmlElement(name=ServiceConstant.DTO_CONVLIST)
	private List<ConvListItemDTO> list;

	/**
	 * @return the list
	 */
	public List<ConvListItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<ConvListItemDTO> list) {
		this.list = list;
	}
	
	public void setError(ConvListItemDTO dto) {
		if(list == null)
			list = new ArrayList<ConvListItemDTO>();
		list.add(dto);
	}
}