package ar.com.gestionit.cashmanagement.persistence.bo;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.SecurityTicketDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.SecurityTicketMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class SecurityTicketBO extends AbstractBO<SecurityTicketDTO, SecurityTicketMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = SecurityTicketMapper.class;
	}

	/**
	 * This method returns a sucursal according to its ID
	 * @param dto
	 * @return
	 * @throws ServiceException
	 */
	public SecurityTicketDTO findSuc(SecurityTicketDTO dto) throws ServiceException {
		SqlSession session = null;
		try {
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			SecurityTicketMapper mapper = session.getMapper(SecurityTicketMapper.class);
			SecurityTicketDTO result = mapper.findSuc(dto);
			queryCounter++;
			session.close();
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}