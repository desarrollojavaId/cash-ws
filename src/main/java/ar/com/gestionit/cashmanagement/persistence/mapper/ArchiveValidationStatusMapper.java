package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.ArchiveValidationStatusAccountListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ArchiveValidationStatusListDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.ArchiveValidationStatusListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.FileAccountListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface ArchiveValidationStatusMapper extends IMapper<IDTO> {
	public List<ArchiveValidationStatusAccountListItemDTO> find (ArchiveValidationStatusListDTO dto);

	public List<FileAccountListItemDTO> findAccounts(ArchiveValidationStatusListItemDTO input);
}
