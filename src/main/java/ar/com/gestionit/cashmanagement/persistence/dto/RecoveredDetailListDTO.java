package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name=ServiceConstant.DTO_RECOVCHECK_ROOT)
public class RecoveredDetailListDTO extends AbstractDTO {
	
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 4969943919872788248L;
	
	//Inputs
	@XmlTransient
	private String adherent;
	@XmlTransient
	private String agreementNumber;
	@XmlTransient
	private String agreementSubnumber;
	@XmlTransient
	private String dateRecoveredFrom;
	@XmlTransient
	private String dateRecoveredTo;
	@XmlTransient
	private String amountFrom;
	@XmlTransient
	private String amountTo;
	@XmlTransient
	private List<String> statuses;
	@XmlTransient
	private String checkNumber;
	@XmlTransient
	private String batchId;
	
	//This attributes are for Check List To Recover Service
	@XmlTransient
	private String dateAcredFrom;
	@XmlTransient
	private String dateAcredTo;
	@XmlTransient
	private String dateCheckFrom;
	@XmlTransient
	private String dateCheckTo;
	@XmlTransient
	private String recoveredStatus;
	
	//Input/Outputs
	@XmlTransient
	private String cpdDateTo;
	
	//Outputs
	@XmlElement(name=ServiceConstant.DTO_RECOVCHECK, defaultValue="")
	private List<RecoveredDetailListItemDTO> list;

	/**
	 * @return the adherent
	 */
	public String getAdherent() {
		return adherent;
	}
	
	public String getAgreementNumber() {
		return agreementNumber;
	}

	public void setAgreementNumber(String agreementNumber) {
		this.agreementNumber = agreementNumber;
	}

	public String getAgreementSubnumber() {
		return agreementSubnumber;
	}

	public void setAgreementSubnumber(String agreementSubnumber) {
		this.agreementSubnumber = agreementSubnumber;
	}

	/**
	 * @param adherent the adherent to set
	 */
	public void setAdherent(String adherent) {
		this.adherent = adherent;
	}

	/**
	 * @return the dateRecoveredFrom
	 */
	public String getDateRecoveredFrom() {
		return dateRecoveredFrom;
	}

	/**
	 * @param dateRecoveredFrom the dateRecoveredFrom to set
	 */
	public void setDateRecoveredFrom(String dateRecoveredFrom) {
		this.dateRecoveredFrom = dateRecoveredFrom;
	}

	/**
	 * @return the dateRecoveredTo
	 */
	public String getDateRecoveredTo() {
		return dateRecoveredTo;
	}

	/**
	 * @param dateRecoveredTo the dateRecoveredTo to set
	 */
	public void setDateRecoveredTo(String dateRecoveredTo) {
		this.dateRecoveredTo = dateRecoveredTo;
	}

	/**
	 * @return the amountFrom
	 */
	public String getAmountFrom() {
		return amountFrom;
	}

	/**
	 * @param amountFrom the amountFrom to set
	 */
	public void setAmountFrom(String amountFrom) {
		this.amountFrom = amountFrom;
	}

	/**
	 * @return the amountTo
	 */
	public String getAmountTo() {
		return amountTo;
	}

	/**
	 * @param amountTo the amountTo to set
	 */
	public void setAmountTo(String amountTo) {
		this.amountTo = amountTo;
	}

	/**
	 * @return the statuses
	 */
	public List<String> getStatuses() {
		return statuses;
	}

	/**
	 * @param statuses the statuses to set
	 */
	public void setStatuses(List<String> statuses) {
		this.statuses = statuses;
	}

	/**
	 * @return the list
	 */
	public List<RecoveredDetailListItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<RecoveredDetailListItemDTO> list) {
		this.list = list;
	}

	/**
	 * @return the cpdDateTo
	 */
	public String getCpdDateTo() {
		return cpdDateTo;
	}

	/**
	 * @param cpdDateTo the cpdDateTo to set
	 */
	public void setCpdDateTo(String cpdDateTo) {
		this.cpdDateTo = sanitateString(cpdDateTo);
	}

	/**
	 * @return the checkNumber
	 */
	public String getCheckNumber() {
		return checkNumber;
	}

	/**
	 * @param checkNumber the checkNumber to set
	 */
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	/**
	 * @return the batchId
	 */
	public String getBatchId() {
		return batchId;
	}

	/**
	 * @param batchId the batchId to set
	 */
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	/**
	 * @return the dateAcredFrom
	 */
	public String getDateAcredFrom() {
		return dateAcredFrom;
	}

	/**
	 * @param dateAcredFrom the dateAcredFrom to set
	 */
	public void setDateAcredFrom(String dateAcredFrom) {
		this.dateAcredFrom = dateAcredFrom;
	}

	/**
	 * @return the dateAcredTo
	 */
	public String getDateAcredTo() {
		return dateAcredTo;
	}

	/**
	 * @param dateAcredTo the dateAcredTo to set
	 */
	public void setDateAcredTo(String dateAcredTo) {
		this.dateAcredTo = dateAcredTo;
	}

	/**
	 * @return the dateCheckFrom
	 */
	public String getDateCheckFrom() {
		return dateCheckFrom;
	}

	/**
	 * @param dateCheckFrom the dateCheckFrom to set
	 */
	public void setDateCheckFrom(String dateCheckFrom) {
		this.dateCheckFrom = dateCheckFrom;
	}

	/**
	 * @return the dateCheckTo
	 */
	public String getDateCheckTo() {
		return dateCheckTo;
	}

	/**
	 * @param dateCheckTo the dateCheckTo to set
	 */
	public void setDateCheckTo(String dateCheckTo) {
		this.dateCheckTo = dateCheckTo;
	}

	/**
	 * @return the recoveredStatus
	 */
	public String getRecoveredStatus() {
		return recoveredStatus;
	}

	/**
	 * @param recoveredStatus the recoveredStatus to set
	 */
	public void setRecoveredStatus(String recoveredStatus) {
		this.recoveredStatus = sanitateString(recoveredStatus);
	}
}