package ar.com.gestionit.cashmanagement.persistence.dto;

public class TransactionDTO extends AbstractDTO {
	
	/**
	 * Serial version
	 */
	private static final long serialVersionUID = 1228095331381889818L;
	
	//Constants to values by default to persist in SSTRANS table
	public static final String FILE_STATUS_ANPT = "ANPT";
	public static final String FILE_STATUS_ATRS = "ATRS";
	public static final String FILE_STATUS_ANPA = "ANPA";
	public static final String FILE_STATUS_AOKB = "AOKB";
	public static final String DATE_EMPTY = "'0001-01-01'";
	public static final String HOUR_EMPTY = "00:00:00";
	
	//MTCONVAL table
	private int mtvutr; //Key
	private String mtvfdp; //Depuration date for queries
	
	//SSTRANS table
	private int stqtrs; //Key
	private String stktrs; //Service name
	private short stkcnl; //Channel
	private long stqadh; //Adherent
	private String stidpr; //Process ID
	private String stfgen; //current date
	private String sthgen; //current hour
	private String stnars; //File ID
	private String stjtot;
	private String stqtot;
	private String user;
	private String stkcvn; //Agreement number
	private String stktbo; //Sub-agreement number
	private String stdobs; //Desc. Observaciones 
	private String serviceName; //STWSGS
	private String stkstr;
	private String stnarc; //File name according to the client
	private String stunec; //Cantidad de usuarios necesarios
	private String stfau1 = DATE_EMPTY;
	private String sthau1 = HOUR_EMPTY;
	private String stfau2 = DATE_EMPTY;
	private String sthau2 = HOUR_EMPTY;
	private String stfau3;
	private String sthau3;
	private String stkapr;
	private String stkpep;
	private String stkest = ""; //Status
	
	/**
	 * @return the mtvutr
	 */
	public int getMtvutr() {
		return mtvutr;
	}
	/**
	 * @param mtvutr the mtvutr to set
	 */
	public void setMtvutr(int mtvutr) {
		this.mtvutr = mtvutr;
	}
	/**
	 * @return the stqtrs
	 */
	
	/**
	 * @return the stktrs
	 */
	public String getStktrs() {
		return stktrs;
	}
	/**
	 * @param stktrs the stktrs to set
	 */
	public void setStktrs(String stktrs) {
		this.stktrs = sanitateString(stktrs);
	}
	/**
	 * @return the stkcnl
	 */
	public short getStkcnl() {
		return stkcnl;
	}
	/**
	 * @param stkcnl the stkcnl to set
	 */
	public void setStkcnl(short stkcnl) {
		this.stkcnl = stkcnl;
	}
	/**
	 * @return the stqadh
	 */
	public long getStqadh() {
		return stqadh;
	}
	/**
	 * @param stqadh the stqadh to set
	 */
	public void setStqadh(long stqadh) {
		this.stqadh = stqadh;
	}
	/**
	 * @return the stidpr
	 */
	public String getStidpr() {
		return stidpr;
	}
	/**
	 * @param stidpr the stidpr to set
	 */
	public void setStidpr(String stidpr) {
		this.stidpr = sanitateString(stidpr);
	}
	/**
	 * @return the stfgen
	 */
	public String getStfgen() {
		return stfgen;
	}
	/**
	 * @param stfgen the stfgen to set
	 */
	public void setStfgen(String stfgen) {
		this.stfgen = sanitateString(stfgen);
	}
	/**
	 * @return the stnars
	 */
	public String getStnars() {
		return stnars;
	}
	/**
	 * @param stnars the stnars to set
	 */
	public void setStnars(String stnars) {
		this.stnars = stnars;
	}
	/**
	 * @return the sthgen
	 */
	public String getSthgen() {
		return sthgen;
	}
	/**
	 * @param sthgen the sthgen to set
	 */
	public void setSthgen(String sthgen) {
		this.sthgen = sanitateString(sthgen);
	}
	/**
	 * @return the stjtot
	 */
	public String getStjtot() {
		return stjtot;
	}
	/**
	 * @param stjtot the stjtot to set
	 */
	public void setStjtot(String stjtot) {
		this.stjtot = stjtot;
	}
	/**
	 * @return the stqtot
	 */
	public String getStqtot() {
		return stqtot;
	}
	/**
	 * @param stqtot the stqtot to set
	 */
	public void setStqtot(String stqtot) {
		this.stqtot = stqtot;
	}
	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
	
	public String getStkcvn() {
		return stkcvn;
	}
	public void setStkcvn(String stkcvn) {
		this.stkcvn = stkcvn;
	}
	public String getStktbo() {
		return stktbo;
	}
	public void setStktbo(String stktbo) {
		this.stktbo = stktbo;
	}
	/**
	 * 
	 * @return the stdobs
	 */
	public String getStdobs() {
		return stdobs;
	}
	
	/**
	 * 
	 * @param stdobs the stdobs to set
	 */
	public void setStdobs(String stdobs) {
		this.stdobs = stdobs;
	}
	/**
	 * @return the serviceName
	 */
	public String getServiceName() {
		return serviceName;
	}
	/**
	 * @param serviceName the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
	/**
	 * @return the mtvfdp
	 */
	public String getMtvfdp() {
		return mtvfdp;
	}
	/**
	 * @param mtvfdp the mtvfdp to set
	 */
	public void setMtvfdp(String mtvfdp) {
		this.mtvfdp = sanitateString(mtvfdp);
	}
	/**
	 * @return the stkstr
	 */
	public String getStkstr() {
		return stkstr;
	}
	/**
	 * @param stkstr the stkstr to set
	 */
	public void setStkstr(String stkstr) {
		this.stkstr = stkstr;
	}
	/**
	 * @return the stnarc
	 */
	public String getStnarc() {
		return stnarc;
	}
	/**
	 * @param stnarc the stnarc to set
	 */
	public void setStnarc(String stnarc) {
		this.stnarc = stnarc;
	}
	/**
	 * @return the stunec
	 */
	public String getStunec() {
		return stunec;
	}
	/**
	 * @param stunec the stunec to set
	 */
	public void setStunec(String stunec) {
		this.stunec = stunec;
	}
	/**
	 * @return the stfau3
	 */
	public String getStfau3() {
		return stfau3;
	}
	/**
	 * @param stfau3 the stfau3 to set
	 */
	public void setStfau3(String stfau3) {
		this.stfau3 = stfau3;
	}
	/**
	 * @return the sthau3
	 */
	public String getSthau3() {
		return sthau3;
	}
	/**
	 * @param sthau3 the sthau3 to set
	 */
	public void setSthau3(String sthau3) {
		this.sthau3 = sthau3;
	}
	/**
	 * @return the stkapr
	 */
	public String getStkapr() {
		return stkapr;
	}
	/**
	 * @param stkapr the stkapr to set
	 */
	public void setStkapr(String stkapr) {
		this.stkapr = stkapr;
	}
	/**
	 * @return the stkpep
	 */
	public String getStkpep() {
		return stkpep;
	}
	/**
	 * @param stkpep the stkpep to set
	 */
	public void setStkpep(String stkpep) {
		this.stkpep = stkpep;
	}
	/**
	 * @return the stkest
	 */
	public String getStkest() {
		return stkest;
	}
	/**
	 * @param stkest the stkest to set
	 */
	public void setStkest(String stkest) {
		this.stkest = stkest;
	}
	/**
	 * @return the stfau1
	 */
	public String getStfau1() {
		return stfau1;
	}
	/**
	 * @param stfau1 the stfau1 to set
	 */
	public void setStfau1(String stfau1) {
		this.stfau1 = stfau1;
	}
	/**
	 * @return the sthau1
	 */
	public String getSthau1() {
		return sthau1;
	}
	/**
	 * @param sthau1 the sthau1 to set
	 */
	public void setSthau1(String sthau1) {
		this.sthau1 = sthau1;
	}
	/**
	 * @return the stfau2
	 */
	public String getStfau2() {
		return stfau2;
	}
	/**
	 * @param stfau2 the stfau2 to set
	 */
	public void setStfau2(String stfau2) {
		this.stfau2 = stfau2;
	}
	/**
	 * @return the sthau2
	 */
	public String getSthau2() {
		return sthau2;
	}
	/**
	 * @param sthau2 the sthau2 to set
	 */
	public void setSthau2(String sthau2) {
		this.sthau2 = sthau2;
	}
	public int getStqtrs() {
		return stqtrs;
	}
	public void setStqtrs(int stqtrs) {
		this.stqtrs = stqtrs;
	}
}