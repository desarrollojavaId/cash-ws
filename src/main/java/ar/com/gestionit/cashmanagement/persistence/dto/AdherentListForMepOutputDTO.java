package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"adherents"
})
@XmlRootElement(name = ServiceConstant.DTO_ADHERENTLISTFORMEP_ROOT)
public class AdherentListForMepOutputDTO extends AbstractDTO{

	private static final long serialVersionUID = -1414758864739578889L;

	@XmlElement(name=ServiceConstant.DTO_ADHERENTLISTFORMEP, defaultValue="")
	private List<AdherentListForMepOutputItemDTO> adherents;

	/**
	 * @return the adherents
	 */
	public List<AdherentListForMepOutputItemDTO> getAdherents() {
		return adherents;
	}

	/**
	 * @param adherents the adherents to set
	 */
	public void setAdherents(List<AdherentListForMepOutputItemDTO> adherents) {
		this.adherents = adherents;
	}
}