package ar.com.gestionit.cashmanagement.persistence.mapper;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.RecoveredCheckListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;

public interface RecoveredCheckListMapper extends IMapper<IDTO> {
	
	/**
	 * @return the days to pass by argument to the stored procedure which we must execute then
	 */
	public String findDays();
	
	/*
	 * Stored Procedures executers
	 */
	public void executeS0004(StoredProcedureParametersDTO dto);
	public void executeS0003(StoredProcedureParametersDTO dto);
	
	/**
	 * This method validates if the check belongs to Fiserv company or not
	 * @param check DTO
	 * @return "2" if the check belongs to Fiserv company or other value if not
	 */
	public String belongToFiservCompany(RecoveredCheckListItemDTO dto);
}