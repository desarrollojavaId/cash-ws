package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.EscrowForSendMapper;

public class EscrowForSendBO extends AbstractBO<IDTO, EscrowForSendMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = EscrowForSendMapper.class;
	}
}