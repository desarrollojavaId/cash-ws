package ar.com.gestionit.cashmanagement.persistence.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"number"
		})
@XmlRootElement(name = ServiceConstant.TAG_STATUS)
public class StatusInputListItemDTO  extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 899926104726560330L;
	
	@XmlElement(name=ServiceConstant.TAG_STATUS_ID, defaultValue="")
	public String number;
	//@XmlElement(name=ServiceConstant.TAG_STATUS_SID, defaultValue="")
	@XmlTransient
	public String subnumber;
	
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getSubnumber() {
		return subnumber;
	}
	public void setSubnumber(String subnumber) {
		this.subnumber = subnumber;
	}
	
	


}
