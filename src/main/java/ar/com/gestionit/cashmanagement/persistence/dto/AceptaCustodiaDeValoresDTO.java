package ar.com.gestionit.cashmanagement.persistence.dto;

public class AceptaCustodiaDeValoresDTO extends AbstractDTO {
	
	private static final long serialVersionUID = -7165660923868780349L;
	
	private String mrtcuv; //Acepta custodia de valores?
	private String mrecnv; //convenio
	private String mretbo; //tipo de boleta
	
	public String getMrecnv() {
		return mrecnv;
	}

	public void setMrecnv(String mrecnv) {
		this.mrecnv = mrecnv;
	}

	public String getMretbo() {
		return mretbo;
	}

	public void setMretbo(String mretbo) {
		this.mretbo = mretbo;
	}

	public String getMrtcuv() {
		return mrtcuv;
	}

	public void setMrtcuv(String mrtcuv) {
		this.mrtcuv = mrtcuv;
	}
	
}