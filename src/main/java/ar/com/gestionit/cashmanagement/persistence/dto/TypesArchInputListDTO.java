package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"types"})
@XmlRootElement(name = ServiceConstant.TAG_STATUS2)
public class TypesArchInputListDTO extends AbstractDTO {


	/**
	 * 
	 */
	private static final long serialVersionUID = 5952848408960302148L;
	@XmlElement(name=ServiceConstant.TAG_ID)
	private List<String> types;

	/**
	 * @return the types
	 */
	public List<String> getTypes() {
		return types;
	}

	/**
	 * @param statuses the statuses to set
	 */
	public void setTypes(List<String> types) {
		this.types = types;
	}
}