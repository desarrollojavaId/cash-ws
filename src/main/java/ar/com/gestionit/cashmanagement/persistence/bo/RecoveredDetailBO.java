package ar.com.gestionit.cashmanagement.persistence.bo;

import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.RecoveredDetailMapper;

public class RecoveredDetailBO extends AbstractBO<IDTO, RecoveredDetailMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = RecoveredDetailMapper.class;
	}
}