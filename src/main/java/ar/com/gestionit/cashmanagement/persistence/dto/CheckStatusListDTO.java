package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_CHECK_STATUS_LIST_ROOT)
public class CheckStatusListDTO extends AbstractDTO{

	private static final long serialVersionUID = -708341426856672627L;

	//input
	@XmlTransient
	private String nroAdh;	
	@XmlTransient
	private String nombreBenef;
	@XmlTransient
	private String docBenef;
	@XmlTransient
	private String impDesde;
	@XmlTransient
	private String impHasta;
	@XmlTransient
	private String nroCtaCash;
	@XmlTransient
	private List<StatusInputListItemDTO> lstEstados;
	@XmlTransient
	private String nroCheDsd;
	@XmlTransient
	private String nroCheHas;
	
	//Outputs
	@XmlElement(name=ServiceConstant.DTO_CHECK_STATUS_LIST_ITEM, defaultValue="")
	private List<CheckStatusListItemDTO> list;
	
	

	public String getNroAdh() {
		return nroAdh;
	}

	public void setNroAdh(String nroAdh) {
		this.nroAdh = nroAdh;
	}

	public String getNombreBenef() {
		return nombreBenef;
	}

	public void setNombreBenef(String nombreBenef) {
		this.nombreBenef = nombreBenef;
	}

	public String getDocBenef() {
		return docBenef;
	}

	public void setDocBenef(String docBenef) {
		this.docBenef = docBenef;
	}

	public String getImpDesde() {
		return impDesde;
	}

	public void setImpDesde(String impDesde) {
		this.impDesde = impDesde;
	}

	public String getImpHasta() {
		return impHasta;
	}

	public void setImpHasta(String impHasta) {
		this.impHasta = impHasta;
	}

	public String getNroCtaCash() {
		return nroCtaCash;
	}

	public void setNroCtaCash(String nroCtaCash) {
		this.nroCtaCash = nroCtaCash;
	}

	public List<StatusInputListItemDTO> getLstEstados() {
		return lstEstados;
	}

	public void setLstEstados(List<StatusInputListItemDTO> lstEstados) {
		this.lstEstados = lstEstados;
	}

	public List<CheckStatusListItemDTO> getList() {
		return list;
	}

	public void setList(List<CheckStatusListItemDTO> list) {
		this.list = list;
	}

	/**
	 * @return the nroCheDsd
	 */
	public String getNroCheDsd() {
		return nroCheDsd;
	}

	/**
	 * @param nroCheDsd the nroCheDsd to set
	 */
	public void setNroCheDsd(String nroCheDsd) {
		this.nroCheDsd = nroCheDsd;
	}

	/**
	 * @return the nroCheHas
	 */
	public String getNroCheHas() {
		return nroCheHas;
	}

	/**
	 * @param nroCheHas the nroCheHas to set
	 */
	public void setNroCheHas(String nroCheHas) {
		this.nroCheHas = sanitateString(nroCheHas);
	}

	
	
	
}