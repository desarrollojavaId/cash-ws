package ar.com.gestionit.cashmanagement.persistence.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType (propOrder={"list"})
@XmlRootElement(name = ServiceConstant.DTO_ESCROWS_ROOT)
public class EscrowListDTO extends AbstractDTO{
		
	private static final long serialVersionUID = -2188135454889891925L;
	
	@XmlElement(name=ServiceConstant.DTO_ESCROWS)
	private List<EscrowListItemDTO> list;

	/**
	 * @return the list
	 */
	public List<EscrowListItemDTO> getList() {
		return list;
	}

	/**
	 * @param list the list to set
	 */
	public void setList(List<EscrowListItemDTO> list) {
		this.list = list;
	}
		
}