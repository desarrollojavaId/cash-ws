package ar.com.gestionit.cashmanagement.persistence.mapper;

import java.util.List;

import ar.com.gestionit.cashmanagement.persistence.dto.CashAccountsListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;

public interface CashAccountsListMapper extends IMapper<IDTO> {
	public CashAccountsListItemDTO findList(CashAccountsListItemDTO dto);
	public List<CashAccountsListItemDTO> findAccount(CashAccountsListItemDTO dto);
}