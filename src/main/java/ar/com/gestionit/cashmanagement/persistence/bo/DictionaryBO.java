package ar.com.gestionit.cashmanagement.persistence.bo;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.DictionaryListItemDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.DictionaryMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;

public class DictionaryBO extends AbstractBO<IDTO, DictionaryMapper>{
	@Override
	protected void specifyMapper() {
		mapperInterface = DictionaryMapper.class;
	}
	
	public List<DictionaryListItemDTO> findDic() throws ServiceException {
		SqlSession session = null;
		try {
			//Get result list
			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			DictionaryMapper mapper = session.getMapper(mapperInterface);
			List<DictionaryListItemDTO> result = mapper.findDic();
			queryCounter++;
			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}
}