package ar.com.gestionit.cashmanagement.persistence.bo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.persistence.dto.IDTO;
import ar.com.gestionit.cashmanagement.persistence.dto.StoredProcedureParametersDTO;
import ar.com.gestionit.cashmanagement.persistence.mapper.GeneralRecoveredMapper;
import ar.com.gestionit.cashmanagement.service.util.ServiceConstant;
import ar.com.gestionit.cashmanagement.util.DateUtil;

public class GeneralRecoveredBO extends AbstractBO<IDTO, GeneralRecoveredMapper>{

	@Override
	protected void specifyMapper() {
		mapperInterface = GeneralRecoveredMapper.class;
	}

	/**
	 * This method executes a stored procedure to get the request number
	 * according to the DTO given by argument
	 * @param adherent
	 * @param agreementNumber
	 * @param agreementSubnumber
	 * @return
	 * @throws ServiceException
	 */
	public String executeSS0003(String adherent, String agreementNumber, String agreementSubnumber) throws ServiceException {
		SqlSession session = null;
		try {
			StoredProcedureParametersDTO dto = new StoredProcedureParametersDTO();
			dto.setParam1("1");
			dto.setParam2(adherent);
			dto.setParam3(String.valueOf(agreementNumber));
			dto.setParam4(String.valueOf(agreementSubnumber));
			dto.setParam5("0");

			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			GeneralRecoveredMapper mapper = session.getMapper(GeneralRecoveredMapper.class);
			mapper.executeSS0003(dto);
			session.close();
			queryCounter++;

			return dto.getParam5();
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	/**
	 * This method executes a stored procedure to work with dates
	 * @param dto
	 * @throws ServiceException
	 */
	public String executeSS0004() throws ServiceException {
		SqlSession session = null;
		try {
			StoredProcedureParametersDTO sp = new StoredProcedureParametersDTO();
			sp.setParam2("0");
			sp.setParam3("1");
			sp.setParam5("");
			sp.setParam6("");

			String date = ServiceConstant.SDF_AMERICAN_DATE.format(new Date());
			if(date != null)
				date = date.replace("/", "");
			sp.setParam1(date);

			session = CashManagementWsApplication.sqlSessionFactory.openSession();
			GeneralRecoveredMapper mapper = session.getMapper(GeneralRecoveredMapper.class);

			//Get days to run the stored procedure
			sp.setParam4(mapper.findDays());
			queryCounter++;

			//Run stored procedure S0004
			mapper.executeSS0004(sp);
			queryCounter++;

			//Format the date
			date = sp.getParam2();
			if(date != null && !date.trim().equals("") && date.length() >= 6 ) {
				date = new StringBuffer(date.substring(0, 4))
						.append("-")
						.append(date.substring(4, 6))
						.append("-")
						.append(date.substring(6))
						.toString();
			}
			date = DateUtil.changeFormat(date, "yyyy-MM-dd", "dd/MM/yyyy");

			return date;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			if(session != null)
				session.close();
		}
	}

	/**
	 * This method deletes the non recovered checks and returns
	 * the number of the checks enabled for the recovered batch
	 * @param requestNumber
	 * @param string
	 * @param string2
	 * @return the number of the checks enabled for the recovered batch
	 * @throws ServiceException
	 */
	public int deleteNonRecuperableChecks(String requestNumber,
			String string,
			String string2,
			SqlSession session,
			boolean manageSession) throws ServiceException {

		try {
			StoredProcedureParametersDTO dto = new StoredProcedureParametersDTO();
			dto.setParam1(requestNumber);
			dto.setParam2(String.valueOf(string));
			dto.setParam3(String.valueOf(string2));
			dto.setParam4(executeSS0004());

			//Validate if the method must manage the session or not
			if(session == null)
				session = CashManagementWsApplication.sqlSessionFactory.openSession();

			GeneralRecoveredMapper mapper = session.getMapper(GeneralRecoveredMapper.class);

			mapper.deleteNonRecoveredChecks(dto);
			queryCounter++;

			return  mapper.findChecksForRecovered(dto);
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_ERRORBYDEFAULT, e);
		} finally {
			//Validate if the session is managed by this method or other BO is working with it
			if(session != null && manageSession)
				session.close();
		}
	}

	/**
	 * This method adds an event
	 * @param requestNumber
	 * @param status
	 * @param eventType
	 * @return
	 * @throws ServiceException 
	 */
	public int addEvent(String requestNumber, String status, String eventType, SqlSession session, boolean manageSession) throws ServiceException {
		try {
			//Define and initialize variables
			Date d = new Date();
			SimpleDateFormat template = new SimpleDateFormat("dd/MM/yy");
			SimpleDateFormat template2 = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss.S000");
			SimpleDateFormat template3 = new SimpleDateFormat("HH:mm:ss");

			String dateDDMMYY = template.format(d);
			String timeStamp = template2.format(d);
			String hour = template3.format(d);

			StoredProcedureParametersDTO dto = new StoredProcedureParametersDTO();
			dto.setParam1(requestNumber);
			dto.setParam2(status);
			dto.setParam3(eventType);
			dto.setParam4(timeStamp);
			dto.setParam5(dateDDMMYY);
			dto.setParam6(hour);

			//Validate if the session is managed by other BO
			if(session == null)
				session = CashManagementWsApplication.sqlSessionFactory.openSession();
			GeneralRecoveredMapper mapper = session.getMapper(GeneralRecoveredMapper.class);

			//Get fields to persist event
			List<StoredProcedureParametersDTO> list;
			if (status != null && status.equals("5") && eventType != null && eventType.equals("4"))
				list = mapper.findAutomaticEventFields(dto);
			else {
				list = mapper.findEventFields(dto);
			}

			//Validate if there are not registers in the data source
			if(list == null || list.size() == 0) {
				list = new ArrayList<StoredProcedureParametersDTO>();
				list.add(new StoredProcedureParametersDTO());
			}

			//Insert each register
			Iterator<StoredProcedureParametersDTO> i = list.iterator();
			StoredProcedureParametersDTO a;
			int result = 0;
			while(i.hasNext()) {
				a = i.next();

				//Validating...
				if(a == null)
					a = new StoredProcedureParametersDTO();
				if(a.getParam7() == null)
					a.setParam7("0");
				if(a.getParam8() == null)
					a.setParam8("0");
				if(a.getParam9() == null)
					a.setParam9("");

				//Load these fields in the main DTO
				dto.setParam7(a.getParam7());
				dto.setParam8(a.getParam8());
				dto.setParam9(a.getParam9());

				//Persist
				result++;
				if (status != null && status.equals("5") && eventType != null && eventType.equals("4"))
					mapper.addEventAutomaticExpiration(dto);
				else
					mapper.addEvent(dto);
				queryCounter++;
			}

			return result;
		} catch(Exception e) {
			throw new ServiceException(ServiceConstant.SERVICERETURN_KEY_QUERYNOTEXEC, e);
		} finally {
			//Validate if the session is managed by other BO
			if(session != null && manageSession)
				session.close();
		}
	}
}