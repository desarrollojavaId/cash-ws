package ar.com.gestionit.cashmanagement.persistence;

public class TestDTO {
	
	private String mtlaco;

	/**
	 * @return the mtlaco
	 */
	public String getMtlaco() {
		return mtlaco;
	}

	/**
	 * @param mtlaco the mtlaco to set
	 */
	public void setMtlaco(String mtlaco) {
		this.mtlaco = mtlaco;
	}
}