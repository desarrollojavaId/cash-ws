package ar.com.gestionit.cashmanagement.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import junit.framework.TestCase;

public class SQLInjectionTest extends TestCase {
	
	private TestBO bo;
	private List<TestDTO> list;
	
	/**
	 * This method initializes the context for the test cases
	 */
	protected void setUp() {
		//Initialize Mybatis context
		try {
			String resource = "mybatis-config.xml";
			InputStream inputStream = Resources.getResourceAsStream(resource);
			CashManagementWsApplication.sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		//Initialize variables
		bo = new TestBO();
	}
	
	
	/**
	 * This method is an example about how to prevent
	 * an SQL injection attack.
	 * For this, we must use #{variable} as structure in the query mapping
	 */
	public void testAvoidAttack() {
		//Define and initialize variables 
		TestDTO dto = new TestDTO();
		dto.setMtlaco("%R%");
		
		try {
			list = bo.find(dto);
			printResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This method is an example about how to attack via
	 * SQL injection vulnerability.
	 * For this, we must use ${variable} as structure in the query mapping
	 */
	public void testAttack() {
		//Define and initialize variables 
		TestDTO dto = new TestDTO();
		dto.setMtlaco("%R%') OR mtlaco LIKE ('%NEC%') --");
		
		try {
			list = bo.attack(dto);
			printResult();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This internal method processes the result
	 */
	private void printResult() {
		if(list == null) {
			System.out.println("List is NULL");
			return;
		}
		if(list.size() == 0) {
			System.out.println("The list is empty");
			return;
		}
		Iterator<TestDTO> i = list.iterator();
		while(i.hasNext()) {
			System.out.println("Node: " + i.next().getMtlaco());
		}
	}
}