package ar.com.gestionit.cashmanagement.persistence;

import java.util.List;

public interface TestMapper {
	public List<TestDTO> find(TestDTO dto);
	public List<TestDTO> attack(TestDTO dto);
}