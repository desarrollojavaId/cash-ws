package ar.com.gestionit.cashmanagement.service;

import java.util.ArrayList;

import ar.com.gestionit.cashmanagement.exception.ServiceException;
import ar.com.gestionit.cashmanagement.message.ExecuteRequest;
import ar.com.gestionit.cashmanagement.message.entity.ArrayOfstring20;
import junit.framework.TestCase;

public class SendFileServiceTest extends TestCase {
	
	public void testGenerateChecksum() {
		ExecuteRequest request = new ExecuteRequest();
		request.setAcnco((short) 21);
		request.setBopco((short) 3);
		request.setCvart((short) 16);
		request.setDrqcl("123456");
		
		ArrayOfstring20 etdats = new ArrayOfstring20();
		etdats.setItem(new ArrayList<String>());
		etdats.getItem().add(0, "C");
		etdats.getItem().add(1, "C");
		etdats.getItem().add(2, "C");
		etdats.getItem().add(3, "C");
		etdats.getItem().add(4, "L");
		etdats.getItem().add(5, "C");
		etdats.getItem().add(6, "C");
		etdats.getItem().add(7, "L");
		etdats.getItem().add(8, "L");
		etdats.getItem().add(9, "C");
		etdats.getItem().add(10, "C");
		request.setEtdats(etdats);
		
		ArrayOfstring20 evalcs = new ArrayOfstring20();
		evalcs.setItem(new ArrayList<String>());
		evalcs.getItem().add(1, "699");
		evalcs.getItem().add(9, "po");
		evalcs.getItem().add(10, "1");
		request.setEvalcs(evalcs);
		
		ArrayOfstring20 evalls = new ArrayOfstring20();
		evalls.setItem(new ArrayList<String>());
		evalls.getItem().add(4, "yZqk3+u18exHF2cmzOK1eoMYbQ5s6f4b5iU5PwzIURERIUYdMRxzOt+MDGYMXALjK3dkoS0MiClP\n6v1pFii9q6mjh+JpoR4C");
		request.setEvalls(evalls);
		
		SendFileService service = new SendFileService();
		try {
			service.execute(request);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
