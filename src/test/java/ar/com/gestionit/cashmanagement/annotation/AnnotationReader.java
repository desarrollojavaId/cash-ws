package ar.com.gestionit.cashmanagement.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import javax.xml.bind.annotation.XmlElement;

import ar.com.gestionit.cashmanagement.persistence.dto.CardListItemDTO;
import junit.framework.TestCase;

public class AnnotationReader extends TestCase {
	
	public void testReader() {
		XmlElement ann;
		try {
			Annotation[] anns = CardListItemDTO.class.getAnnotations();
			Field[] fields = CardListItemDTO.class.getDeclaredFields();
			Field field;
			if(fields != null) {
				for(int i = 0; i < fields.length; i++) {
					field = fields[i];
					System.out.println("Field name: " + field.getName());
					ann = field.getAnnotation(XmlElement.class);
					if(ann != null) {
						System.out.println("Annotantion Name: " + ann.name());
					}
				}
			}
			
			for(int i = 0; i < anns.length; i++) {
				Annotation a  = anns[i];
				if(a instanceof XmlElement) {
					ann = (XmlElement)a;
					System.out.println("Namespacee: " + ann.namespace());
				}
			}
		} catch(Exception e) {
			
		}
	}
}
