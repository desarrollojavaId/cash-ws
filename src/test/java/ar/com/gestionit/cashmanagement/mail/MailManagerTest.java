package ar.com.gestionit.cashmanagement.mail;

import ar.com.gestionit.cashmanagement.CashManagementWsApplication;
import ar.com.gestionit.cashmanagement.exception.ServiceException;
import junit.framework.TestCase;

public class MailManagerTest extends TestCase {
	
	public void testPostEmail() {
		//Load the global properties in cache
		try {
			CashManagementWsApplication.loadPorperties();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//Generate mail content
		String subject = "Mail Manager Test by JDigruttola";
		String message = "This is a test content to test the capability of the application to send a mail. Good luck for me!";
		
		//Sending mail...
		try {
			MailManager manager = new MailManager();
			manager.postMail(subject, message);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
	}

}
